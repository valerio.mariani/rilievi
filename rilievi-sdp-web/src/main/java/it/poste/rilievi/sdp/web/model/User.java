package it.poste.rilievi.sdp.web.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class User {

	private String userId;

	private List<String> role;

	private Office office;

	private LocalDate contabilizationDate;

	private String branchNumber;

	private String operationNumber;

	private String phase;

	private String state;

	private String originalOperationNumber;

	private String restartLevel;

	private String channel;

	private String compliance;

	private String client;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getRole() {
		if (this.role == null) {
			this.role = new ArrayList<String>();
		}
		return role;
	}

	public void setRole(List<String> role) {
		this.role = role;
	}


	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public LocalDate getContabilizationDate() {
		return contabilizationDate;
	}

	public void setContabilizationDate(LocalDate contabilizationDate) {
		this.contabilizationDate = contabilizationDate;
	}

	public String getBranchNumber() {
		return branchNumber;
	}

	public void setBranchNumber(String branchNumber) {
		this.branchNumber = branchNumber;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getOriginalOperationNumber() {
		return originalOperationNumber;
	}

	public void setOriginalOperationNumber(String originalOperationNumber) {
		this.originalOperationNumber = originalOperationNumber;
	}

	public String getRestartLevel() {
		return restartLevel;
	}

	public void setRestartLevel(String restartLevel) {
		this.restartLevel = restartLevel;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCompliance() {
		return compliance;
	}

	public void setCompliance(String compliance) {
		this.compliance = compliance;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}

}
