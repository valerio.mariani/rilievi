package it.poste.rilievi.sdp.web.endpoint;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.ExchangeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import it.poste.rilievi.sdp.web.model.Office;
import it.poste.rilievi.sdp.web.model.User;
import it.poste.rilievi.sdp.web.route.processor.ErrorResponse;
import it.poste.rilievi.sdp.web.util.CommonUtil;

@RestController()
@Component
public class AppController {
	
	private static final Logger logger = LoggerFactory.getLogger(AppController.class);
	
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
	
	@Autowired
	private ProducerTemplate producerTemplate;
	
	@Autowired
	private CamelContext context;
	
	 
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.ALL_VALUE,  
    		method = RequestMethod.GET, 
    		value = "initSDP")
	@ResponseStatus(code = HttpStatus.MOVED_PERMANENTLY)
	public ModelAndView initSDP(ModelMap model,
			@RequestParam("Fraz") String fractional,
			@RequestParam("DataCont") String contabilizationDate,
			@RequestParam("NumSpo") String branchNumber,
			@RequestParam("NumOpe") String operationNumber,
			@RequestParam("CodFase") String phase,
			@RequestParam("UserId") String usr,
			@RequestParam("Stato") String state,
			@RequestParam("NumOpeOr") String originalOperationNumber,
			@RequestParam("LivRest") String restartLevel,
			@RequestParam("Chann") String channel,
			@RequestParam(value= "Cmp", required=false) String compliance,
			@RequestParam("Client") String client,
			HttpSession httpSession) {
		logger.info("session initialization by SDP");
		
		//build user object
		User user = new User();
		user.setUserId(usr);
		user.getRole().add(CommonUtil.ROLE_SDP);
		user.setBranchNumber(branchNumber);
		user.setChannel(channel);
		user.setClient(client);
		user.setCompliance(compliance);
		user.setContabilizationDate(LocalDate.parse(contabilizationDate, formatter));
		user.setOperationNumber(operationNumber);
		user.setOriginalOperationNumber(originalOperationNumber);
		user.setPhase(phase);
		user.setRestartLevel(restartLevel);
		user.setState(state);
		
		Exchange exchange = ExchangeBuilder.anExchange(context)
				.withHeader(Exchange.HTTP_METHOD, HttpMethod.GET)
				.withHeader(Exchange.HTTP_PATH, "/office?fractional=" + fractional)
				.withHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.withHeader((CommonUtil.USER_ID), usr)
				.withHeader((CommonUtil.USER_PROFILE), CommonUtil.ROLE_SDP)
				.build();
		
		//call the BE endpoint
		producerTemplate.send("direct://forward", exchange);
		
		//check if there is an error
		if(exchange.getIn().getBody(ErrorResponse.class) == null) {
			Map<?, ?> map = exchange.getOut().getBody(Map.class);
			if(map != null && map.get("id") != null) {
				Long id = new Long((Integer)(map.get("id")));
				user.setOffice(new Office(id, fractional));
			}
		} else {
			logger.info("error during office decode: " + fractional);
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR, "it is not possible to decode Fraz parameter");
		}
		
		//save the user object in session
		CommonUtil.setUser(httpSession, user);
		
		String redirectUrl = new String();
		
		if(CommonUtil.REGULARIZATION.equalsIgnoreCase(state))  {
			redirectUrl="/searchAndManageSdp";
		} else if(CommonUtil.UNDO_REGULARIZATION.equalsIgnoreCase(state))  {
			
			StringBuilder sb = new StringBuilder();
			sb.append("/processSDP?")
			.append("opeNumber=")
			.append(originalOperationNumber)
			.append("&fractional=")
			.append(fractional)
			.append("&branchNumber=")
			.append(branchNumber)
			.append("&date=")
			.append(user.getContabilizationDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
			
			
			Exchange exchange3 = ExchangeBuilder.anExchange(context)
					.withHeader(Exchange.HTTP_METHOD, HttpMethod.GET)
					.withHeader(Exchange.HTTP_PATH, sb.toString())
					.withHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON)
					.withHeader((CommonUtil.USER_ID), usr)
					.withHeader((CommonUtil.USER_PROFILE),  CommonUtil.ROLE_SDP)
					.build();
			Long evalutationId = null;
			//call the BE endpoint
			producerTemplate.send("direct://forward", exchange3);
			//check if there is an error
			if(exchange3.getIn().getBody(ErrorResponse.class) == null) {
				Map<?, ?> map = exchange3.getOut().getBody(Map.class);
				if(map != null && map.get("evaluationId") != null) {
					evalutationId = new Long((Integer)(map.get("evaluationId")));					
				}else{
					logger.info("Error during get SDP Process");
					throw new ResponseStatusException(
					           HttpStatus.INTERNAL_SERVER_ERROR, "Error during get SDP Process");
				}
			} else {
				logger.info("Error during get SDP Process");
				throw new ResponseStatusException(
				           HttpStatus.INTERNAL_SERVER_ERROR, "Error during get SDP Process");
			}
			redirectUrl="/undoRegularizationSdp?eval=" + evalutationId.longValue();
		} else {
			redirectUrl="/";
		}

        return new ModelAndView("redirect:" + redirectUrl, model);
	}
	
	

}
