package it.poste.rilievi.sdp.web;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Bean
	public FilterRegistrationBean<SessionFilter> sessionFilter(){
	    FilterRegistrationBean<SessionFilter> registrationBean = new FilterRegistrationBean<>();
	    registrationBean.setFilter(new SessionFilter());
	    registrationBean.addUrlPatterns("/*");
	    return registrationBean;    
	}
	

}