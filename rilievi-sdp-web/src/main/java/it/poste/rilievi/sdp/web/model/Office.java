package it.poste.rilievi.sdp.web.model;

public class Office {
	
	private Long id;

	private String fractional;

	public Office() {
	}
	
	public Office(Long id, String fractional) {
		this.id=id;
		this.fractional=fractional;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFractional() {
		return fractional;
	}

	public void setFractional(String fractional) {
		this.fractional = fractional;
	}
	
	

}
