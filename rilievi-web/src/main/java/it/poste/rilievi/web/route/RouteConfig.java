package it.poste.rilievi.web.route;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpClientConfigurer;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.poste.rilievi.web.route.processor.ExceptionProcessor;

@Configuration
public class RouteConfig {

	private static final Logger logger = LoggerFactory.getLogger(RouteConfig.class);
	
	@Bean
	public RoutesBuilder myRouter(@Value("${rilievi.api.url}") String apiUrl) {
		logger.info("rilievi api URL: " + apiUrl);
		
		return new RouteBuilder() {

			@Override
			public void configure() throws Exception {

				// define a global exception handler
				this.onException(Exception.class)
					.handled(true)
					.process(new ExceptionProcessor(apiUrl))
					.marshal()
					.json(JsonLibrary.Jackson);

				from("servlet://?matchOnUriPrefix=true")
					.to("direct://process");
				
				from("direct://forward")
					.to("direct://process")
					.unmarshal()
					.json(JsonLibrary.Jackson);
				
				// define the main route
				from("direct://process")
					.to(buildHttp4ApiUrl(apiUrl) + "?bridgeEndpoint=true&"
							+ "throwExceptionOnFailure=false&"
							+ "httpClientConfigurer=httpConfigurer&"
							+ "copyHeaders=false");
			}
			
		};
	}

	@Bean(name = "httpConfigurer")
	public HttpClientConfigurer createCustomConfigurer(@Value("${rilievi.api.url.socketTimeout}") int socketTimeout,
			@Value("${rilievi.api.url.connectionTimeout}") int connectionTimeout) {
		return new HttpClientConfigurer() {

			@Override
			public void configureHttpClient(HttpClientBuilder clientBuilder) {
				RequestConfig.Builder requestBuilder = RequestConfig.custom();
				logger.info("setting connectionTimeout to {} ms ", socketTimeout);
				requestBuilder.setConnectTimeout(connectionTimeout);
				logger.info("setting socketTimeout to {} ms ", socketTimeout);
				requestBuilder.setSocketTimeout(socketTimeout);
				clientBuilder.setRetryHandler(new DefaultHttpRequestRetryHandler(0, false));		
				clientBuilder.setDefaultRequestConfig(requestBuilder.build());
				
				
				// use the TrustSelfSignedStrategy to allow Self Signed Certificates
		        SSLContext sslContext = null;
				try {
					sslContext = SSLContextBuilder.create()
					        .loadTrustMaterial(new TrustSelfSignedStrategy())
					        .build();
				} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
					logger.error("error during sslContext initialization", e);
				}

		        // we can optionally disable hostname verification. 
		        // if you don't want to further weaken the security, you don't have to include this.
		        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
		        
		        // create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
		        // and allow all hosts verifier.
		        SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
		        clientBuilder.setSSLSocketFactory(connectionFactory);
		        //force to build the connectionManager during HttpClient building
		        clientBuilder.setConnectionManager(null);
		        
			}

		};
	}
	
	/**
	 * It makes the apiUrl compliant to camel http4 component
	 * @param apiUrl the original api url
	 * @return the compliant url
	 */
	protected String buildHttp4ApiUrl(String apiUrl) {
		String value = null;
		if(apiUrl != null) {
			if(apiUrl.startsWith("http://")) {
				value = apiUrl.replaceFirst("http://", "http4://");
			} else if (apiUrl.startsWith("https://")) {
				value = apiUrl.replaceFirst("https://", "https4://");
			}
		}
		return value;
	}
	

	
}
