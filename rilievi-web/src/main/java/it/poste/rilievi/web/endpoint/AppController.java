package it.poste.rilievi.web.endpoint;


import javax.servlet.http.HttpSession;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import it.poste.rilievi.web.model.User;
import it.poste.rilievi.web.util.CommonUtil;

@RestController()
@Component
public class AppController {
	
	private static final Logger logger = LoggerFactory.getLogger(AppController.class);
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.ALL_VALUE,  
    		method = RequestMethod.GET, 
    		value = "init")
	@ResponseStatus(code = HttpStatus.MOVED_PERMANENTLY)
	public ModelAndView init(ModelMap model,
			@RequestHeader(CommonUtil.USERID) String userId,
			@RequestHeader(CommonUtil.ROLE) String role,
			HttpSession httpSession ) {
		logger.info("session initialization by intranet");
		
		User user = new User();
		user.setUserId(userId);
		user.getRole().add(role);
		//save the user object in session
		CommonUtil.setUser(httpSession, user);

        return new ModelAndView("redirect:/", model);
	}
	
	 
}