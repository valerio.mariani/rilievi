package it.poste.rilievi.web.util;

import javax.servlet.http.HttpSession;

import it.poste.rilievi.web.model.User;

public class CommonUtil {
	
	public static final String SMSESSION = "SMSESSION";
	
	public static final String USERID = "USERID";
	
	public static final String ROLE = "ROLE";
	
	public static final String ROLE_SDP = "SDP";
	
	public static final String USER_ID = "USER_ID";
	
	public static final String USER_PROFILE = "USER_PROFILE";
	
	public static final String REGULARIZATION = "N";
	
	public static final String UNDO_REGULARIZATION = "A";
	
	public static User getUser(HttpSession session) {
		return (User) session.getAttribute(USERID);
	}
	
	public static void setUser(HttpSession session, User user) {
		session.setAttribute(USERID, user);
	}
	

}
