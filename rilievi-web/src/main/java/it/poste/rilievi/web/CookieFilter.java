package it.poste.rilievi.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.poste.rilievi.web.util.CommonUtil;

public class CookieFilter implements Filter {
	private static final Logger logger = LoggerFactory.getLogger(CookieFilter.class);


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		chain.doFilter(request, response);

		//set the siteminder cookie if it's present
		try {
			// retrieve the siteminder cookie
			Cookie[] cookies = ((HttpServletRequest)request).getCookies();
			if (cookies != null) {
				Optional<Cookie> smcookie = Arrays.asList(cookies).stream()
						.filter(cookie -> CommonUtil.SMSESSION.equals(cookie.getName())).findFirst();

				if (smcookie.isPresent()) {
					logger.debug("one SiteMinder cookie found");
					((HttpServletResponse)response).addCookie(smcookie.get());
				}
			}
		} catch (Exception e) {
			logger.error("unable to set the SiteMinder cookie",e);
		}

	}

}
