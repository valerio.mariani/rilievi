package it.poste.rilievi.web.endpoint;

import java.util.Arrays;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.web.model.User;
import it.poste.rilievi.web.util.CommonUtil;

@RestController()
@Component
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, value = "logout")
	@ResponseStatus(code = HttpStatus.OK)
	public void logout(HttpSession session, HttpServletRequest request) {
		logger.info("invalidating the session {}", session.getId());
		if (session != null) {
			session.invalidate();
		}

		if (request != null) {
			// retrieve the siteminder cookie
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
			
				Arrays.asList(cookies).stream().forEach(cookie -> {
					if (CommonUtil.SMSESSION.equals(cookie.getName())) {
						logger.info("invalidating the SiteMinder cookie");
						//invalid the cookie
						cookie.setMaxAge(0);
					}
				});

			}
		}
	}

	@RequestMapping(consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, value = "user")
	@ResponseStatus(code = HttpStatus.OK)
	public User getUser(HttpSession httpSession) {
		return CommonUtil.getUser(httpSession);
	}

}
