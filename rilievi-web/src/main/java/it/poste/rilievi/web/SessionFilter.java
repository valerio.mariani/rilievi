package it.poste.rilievi.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class SessionFilter implements Filter {
	private static final Logger logger = LoggerFactory.getLogger(SessionFilter.class);


	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
	
		try {
			
			HttpServletRequest request = (HttpServletRequest)servletRequest;
			boolean existingSession = request.getSession(false) != null;
		
			if( StringUtils.endsWithAny(String.valueOf(request.getRequestURI()), new String[] {"/init","/logout"}) 
				||  existingSession ){						
				chain.doFilter(request, servletResponse);				
			}else{
				if(!existingSession) {
					logger.error("there isn't a valid server session!!!");
				}
				HttpServletResponse response = (HttpServletResponse)servletResponse;			
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
			}
			
		}catch (Exception e) {
			logger.error("Error to check Session",e);
		}
	}

}
