package it.poste.rilievi.web.route.processor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

	private HttpStatus status;
	private String error_code;
	private String message;
	private String path;
	protected String timestamp;
	
	public ErrorResponse() {
		
	}
	
	public ErrorResponse(HttpStatus status, String error_code, String message, String path) {
		this.status=status;
		this.error_code=error_code;
		this.message=message;
		this.path=path;
		this.timestamp= LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
