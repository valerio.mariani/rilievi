package it.poste.rilievi.web.route.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class ExceptionProcessor implements Processor{
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionProcessor.class);
	
	private String apiUrl;
	
	public ExceptionProcessor (String apiUrl) {
		this.apiUrl = apiUrl;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Exception ex = getException(exchange);
		String originalUri = exchange.getIn().getHeader(Exchange.HTTP_URI, String.class);
		String httpPath = exchange.getIn().getHeader(Exchange.HTTP_PATH, String.class);
		
		logger.error(getInfo(originalUri, httpPath), ex);
		
		//build the response
		ErrorResponse response = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"generic remote api error", 
					originalUri);
		
		exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.value());
		exchange.getOut().setBody(response);
	}
	
	
	private Exception getException(Exchange exchange) {
		return exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
	}
	
	
	private String getInfo(String original, String target) {
		StringBuilder sb = new StringBuilder();
		sb.append("error during remote api invocation for original uri: [")
		.append(original)
		.append("] and target uri: [")
		.append(apiUrl)
		.append("/")
		.append(target)
		.append("]");
		return sb.toString();
	}

}
