-- MySQL dump 10.13  Distrib 5.6.42, for Linux (x86_64)
--
-- Host: localhost    Database: rilievi_
-- ------------------------------------------------------
-- Server version	5.6.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abbinamento_voci_daco_servizi`
--

DROP TABLE IF EXISTS `abbinamento_voci_daco_servizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abbinamento_voci_daco_servizi` (
  `ID_ABBINAMENTO` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_VOCE_DACO` int(11) DEFAULT NULL,
  `RIF_SERVIZIO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_ABBINAMENTO`),
  KEY `abbinamento_voci_daco_servizi_voce_fk` (`RIF_VOCE_DACO`),
  KEY `abbinamento_voci_daco_servizi_servizio_fk` (`RIF_SERVIZIO`),
  CONSTRAINT `abbinamento_voci_daco_servizi_servizio_fk` FOREIGN KEY (`RIF_SERVIZIO`) REFERENCES `tipolog_servizi` (`ID_SERVIZIO`),
  CONSTRAINT `abbinamento_voci_daco_servizi_voce_fk` FOREIGN KEY (`RIF_VOCE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `anag_profili`
--

DROP TABLE IF EXISTS `anag_profili`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anag_profili` (
  `ID_PROFILO` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_IAM` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `NOME` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DESCRIZIONE` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_PROFILO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `anag_profili_abilitazione`
--

DROP TABLE IF EXISTS `anag_profili_abilitazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anag_profili_abilitazione` (
  `ID_ANAG_PROFILI_ABILITAZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_PROFILO` int(11) DEFAULT NULL,
  `RIF_FUNZIONE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_ANAG_PROFILI_ABILITAZIONE`),
  KEY `anag_profili_abilitazione_anag_profili_fk` (`RIF_PROFILO`),
  KEY `anag_profili_abilitazione_tipolog_funzioni_fk` (`RIF_FUNZIONE`),
  CONSTRAINT `anag_profili_abilitazione_anag_profili_fk` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `anag_profili_abilitazione_tipolog_funzioni_fk` FOREIGN KEY (`RIF_FUNZIONE`) REFERENCES `tipolog_funzioni` (`ID_FUNZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staging_agg_rilievi`
--

DROP TABLE IF EXISTS `staging_agg_rilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staging_agg_rilievi` (
  `ID_AGG` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `NUM_RILIEVO` int(11) DEFAULT NULL,
  `RIF_FILIALE` int(11) DEFAULT NULL,
  `DATA_COMPETENZA` datetime DEFAULT NULL,
  `DATA_COGE` datetime DEFAULT NULL,
  `DATA_INVIO_SRC` datetime DEFAULT NULL,
  `RIF_FRAZIONARIO` int(11) DEFAULT NULL,
  `RIF_CAUSALE` int(11) DEFAULT NULL,
  `RIF_VOCE_DACO` int(11) DEFAULT NULL,
  `MOTIVO` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `DEBITI_EURO` decimal(13,2) DEFAULT NULL,
  `CREDITI_EURO` decimal(13,2) DEFAULT NULL,
  `RIF_PROFILO` int(11) DEFAULT NULL,
  `RIF_STATO_RILIEVO` int(11) DEFAULT NULL,
  `DATA_REGOL_CUMUL` datetime DEFAULT NULL,
  `DATA_ANNULLAMENTO` datetime DEFAULT NULL,
  `FLAG_ANNULLATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_TIPO_CHIUSURA` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `STATUS_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_RILIEVO_PADRE` int(11) DEFAULT NULL,
  `RIF_RILIEVO_FIGLIO` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_VALIDATA` bit(1) DEFAULT NULL,
  `DATA_REGOLARIZZAZIONE` datetime DEFAULT NULL,
  `REGOLARIZZATO_DA_UTENTE` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_REGOLARIZZATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_CAUSALE_PRECEDENTE` int(11) DEFAULT NULL,
  `UTENTE_INSERIMENTO` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `RIF_SERVIZIO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_AGG`),
  KEY `staging_agg_rilievi_tipolog_filiali_fk` (`RIF_FILIALE`),
  KEY `staging_agg_rilievi_tipolog_uffici_fk` (`RIF_FRAZIONARIO`),
  KEY `staging_agg_rilievi_tipolog_causale_fk` (`RIF_CAUSALE`),
  KEY `staging_agg_rilievi_tipolog_daco_fk` (`RIF_VOCE_DACO`),
  KEY `staging_agg_rilievi_anag_profili_fk` (`RIF_PROFILO`),
  KEY `staging_agg_rilievi_tipolog_stato_rilievo_fk` (`RIF_STATO_RILIEVO`),
  KEY `staging_agg_rilievi_storage_rilievi_fk_1` (`RIF_RILIEVO_PADRE`),
  KEY `staging_agg_rilievi_storage_rilievi_fk_2` (`RIF_RILIEVO_FIGLIO`),
  KEY `staging_agg_rilievi_tipolog_causale_fk_1` (`RIF_CAUSALE_PRECEDENTE`),
  CONSTRAINT `staging_agg_rilievi_anag_profili_fk` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `staging_agg_rilievi_storage_rilievi_fk` FOREIGN KEY (`RIF_FILIALE`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `staging_agg_rilievi_storage_rilievi_fk_1` FOREIGN KEY (`RIF_RILIEVO_PADRE`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `staging_agg_rilievi_storage_rilievi_fk_2` FOREIGN KEY (`RIF_RILIEVO_FIGLIO`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `staging_agg_rilievi_tipolog_causale_fk` FOREIGN KEY (`RIF_CAUSALE`) REFERENCES `tipolog_causale` (`ID_CAUSALE`),
  CONSTRAINT `staging_agg_rilievi_tipolog_causale_fk_1` FOREIGN KEY (`RIF_CAUSALE_PRECEDENTE`) REFERENCES `tipolog_causale` (`ID_CAUSALE`),
  CONSTRAINT `staging_agg_rilievi_tipolog_daco_fk` FOREIGN KEY (`RIF_VOCE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`),
  CONSTRAINT `staging_agg_rilievi_tipolog_filiali_fk` FOREIGN KEY (`RIF_FILIALE`) REFERENCES `tipolog_filiali` (`ID_FILIALE`),
  CONSTRAINT `staging_agg_rilievi_tipolog_stato_rilievo_fk` FOREIGN KEY (`RIF_STATO_RILIEVO`) REFERENCES `tipolog_stato_rilievo` (`ID_STATO_RILIEVO`),
  CONSTRAINT `staging_agg_rilievi_tipolog_uffici_fk` FOREIGN KEY (`RIF_FRAZIONARIO`) REFERENCES `tipolog_uffici` (`ID_UFFICIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staging_daco_escludere_maxi_rilievi`
--

DROP TABLE IF EXISTS `staging_daco_escludere_maxi_rilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staging_daco_escludere_maxi_rilievi` (
  `ID_STAG_VOCE_DACO_ESCL_MAXI_RILIEVI` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_CODICE_DACO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_STAG_VOCE_DACO_ESCL_MAXI_RILIEVI`),
  KEY `staging_daco_escludere_maxi_rilievi_FK` (`RIF_CODICE_DACO`),
  CONSTRAINT `staging_daco_escludere_maxi_rilievi_FK` FOREIGN KEY (`RIF_CODICE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staging_maxirilievi`
--

DROP TABLE IF EXISTS `staging_maxirilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staging_maxirilievi` (
  `ID_REGOLA_MAXIRIL` int(11) NOT NULL AUTO_INCREMENT,
  `ID_RILIEVI` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_REGOLA_MAXIRIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staging_processi_sdp`
--

DROP TABLE IF EXISTS `staging_processi_sdp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staging_processi_sdp` (
  `ID_PROC_SDP` int(11) NOT NULL AUTO_INCREMENT,
  `NUM_SPORTELLO` int(11) DEFAULT NULL,
  `DATA_CONTABILE` datetime DEFAULT NULL,
  `FRAZIONARIO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `NUM_OPERAZIONE` int(11) DEFAULT NULL,
  `LIV_RESTART` int(11) DEFAULT NULL,
  `DATA_INTERVENTO` datetime DEFAULT NULL,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `UTENTE_SDP` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_INTERVENTO_COMPLETO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_PROC_SDP`),
  KEY `staging_processi_sdp_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `staging_processi_sdp_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_abilitazioni_daco`
--

DROP TABLE IF EXISTS `storage_abilitazioni_daco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_abilitazioni_daco` (
  `ID_ABILITAZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_VOCE_DACO` int(11) DEFAULT NULL,
  `CASO_ABILITAZ` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_PROFILO` int(11) NOT NULL,
  PRIMARY KEY (`ID_ABILITAZIONE`),
  KEY `storage_abilitazioni_daco_tipolog_daco_fk` (`RIF_VOCE_DACO`),
  KEY `storage_abilitazioni_daco_FK` (`RIF_PROFILO`),
  CONSTRAINT `storage_abilitazioni_daco_FK` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `storage_abilitazioni_daco_tipolog_daco_fk` FOREIGN KEY (`RIF_VOCE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_allegati`
--

DROP TABLE IF EXISTS `storage_allegati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_allegati` (
  `ID_ALLEGATI` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `NOME_ALLEGATO` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `UTENTE_INSERIMENTO` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `CONTENUTO` blob,
  `UTENTE_RIMOZIONE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_RIMOZIONE` datetime DEFAULT NULL,
  `ATTIVO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_ALLEGATI`),
  KEY `storage_allegati_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `storage_allegati_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_arch_rilievi`
--

DROP TABLE IF EXISTS `storage_arch_rilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_arch_rilievi` (
  `ID_RILIEVI` int(11) NOT NULL AUTO_INCREMENT,
  `NUM_RILIEVO` int(11) DEFAULT NULL,
  `CODICE_FILIALE` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_FILIALE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `DATA_COMPETENZA` datetime DEFAULT NULL,
  `DATA_COGE` datetime DEFAULT NULL,
  `DATA_INVIO_SRC` datetime DEFAULT NULL,
  `RIF_FRAZIONARIO` int(11) DEFAULT NULL,
  `CODICE_CAUSALE` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_CAUSALE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `VOCE_DACO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `MOTIVO` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `DEBITI_LIRE` decimal(13,2) DEFAULT NULL,
  `DEBITI_EURO` decimal(13,2) DEFAULT NULL,
  `DEBITI_CTV` decimal(13,2) DEFAULT NULL,
  `DEBITI_EURO_CTV` decimal(13,2) DEFAULT NULL,
  `CREDITI_LIRE` decimal(13,2) DEFAULT NULL,
  `CREDITI_EURO` decimal(13,2) DEFAULT NULL,
  `CREDITI_DTV` decimal(13,2) DEFAULT NULL,
  `CREDITI_EURO_CTV` decimal(13,2) DEFAULT NULL,
  `TIPO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `BLOCCO` bit(1) DEFAULT NULL,
  `TRASM` bit(1) DEFAULT NULL,
  `DECADE` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_PROFILO` int(11) DEFAULT NULL,
  `CODICE_STATO_RILIEVO` int(11) DEFAULT NULL,
  `DESCR_STATO_RILIEVO` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_REGOL_CUMUL` datetime DEFAULT NULL,
  `TIPO_CHIUSURA` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `STATUS_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_RILIEVO_PADRE` int(11) DEFAULT NULL,
  `RIF_RILIEVO_FIGLIO` int(11) DEFAULT NULL,
  `NOTE` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_REGOLARIZZAZIONE` datetime DEFAULT NULL,
  `REGOLARIZZATO_DA_UTENTE` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FLAG_REGOLARIZZATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FILIALE_EM` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `NUM_RIL_EM` decimal(18,0) DEFAULT NULL,
  PRIMARY KEY (`ID_RILIEVI`),
  KEY `storage_arch_rilievi_tipolog_uffici_fk` (`RIF_FRAZIONARIO`),
  KEY `storage_arch_rilievi_anag_profili_fk_1` (`RIF_PROFILO`),
  KEY `storage_arch_rilievi_storage_rilievi_fk` (`RIF_RILIEVO_PADRE`),
  KEY `storage_arch_rilievi_storage_rilievi_fk_1` (`RIF_RILIEVO_FIGLIO`),
  CONSTRAINT `storage_arch_rilievi_anag_profili_fk` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `storage_arch_rilievi_anag_profili_fk_1` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `storage_arch_rilievi_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVO_PADRE`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `storage_arch_rilievi_storage_rilievi_fk_1` FOREIGN KEY (`RIF_RILIEVO_FIGLIO`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `storage_arch_rilievi_tipolog_uffici_fk` FOREIGN KEY (`RIF_FRAZIONARIO`) REFERENCES `tipolog_uffici` (`ID_UFFICIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_date_estrazione`
--

DROP TABLE IF EXISTS `storage_date_estrazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_date_estrazione` (
  `DATA_FISSA` datetime DEFAULT NULL,
  `DATA_CHIUSURA` datetime DEFAULT NULL,
  `ID_DATE_ESTRAZIONE` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_DATE_ESTRAZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_log_azioni`
--

DROP TABLE IF EXISTS `storage_log_azioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_log_azioni` (
  `ID_AZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `UTENTE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_TIPO_AZIONE` int(11) DEFAULT NULL,
  `ESITO_AZIONE` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_AZIONE` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_AZIONE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_AZIONE`),
  KEY `storage_log_azioni_tipolog_tipo_azioni_fk` (`RIF_TIPO_AZIONE`),
  CONSTRAINT `storage_log_azioni_tipolog_tipo_azioni_fk` FOREIGN KEY (`RIF_TIPO_AZIONE`) REFERENCES `tipolog_tipo_azioni` (`ID_TIPO_AZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_note_aggiuntive`
--

DROP TABLE IF EXISTS `storage_note_aggiuntive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_note_aggiuntive` (
  `ID_NOTA` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `NOTA` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `UTENTE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_NOTA`),
  KEY `storage_note_aggiuntive_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `storage_note_aggiuntive_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_processi_sdp`
--

DROP TABLE IF EXISTS `storage_processi_sdp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_processi_sdp` (
  `ID_PROC_SDP` int(11) NOT NULL AUTO_INCREMENT,
  `NUM_SPORTELLO` int(11) DEFAULT NULL,
  `DATA_CONTABILE` datetime DEFAULT NULL,
  `FRAZIONARIO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `NUM_OPERAZIONE` int(11) DEFAULT NULL,
  `NUM_OPERAZIONE_ORIG` int(11) DEFAULT NULL,
  `STATO_OPERAZIONE` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_VALIDITA` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_FINE_VALIDITA` datetime DEFAULT NULL,
  `LIV_RESTART` int(11) DEFAULT NULL,
  `DATA_INTERVENTO` datetime DEFAULT NULL,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `UTENTE_SDP` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_INTERVENTO_COMPLETO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_PROC_SDP`),
  KEY `storage_processi_sdp_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `storage_processi_sdp_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_rilievi`
--

DROP TABLE IF EXISTS `storage_rilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_rilievi` (
  `ID_RILIEVI` int(11) NOT NULL AUTO_INCREMENT,
  `NUM_RILIEVO` int(11) DEFAULT NULL,
  `RIF_FILIALE` int(11) DEFAULT NULL,
  `DATA_COMPETENZA` datetime DEFAULT NULL,
  `DATA_COGE` datetime DEFAULT NULL,
  `DATA_INVIO_SRC` datetime DEFAULT NULL,
  `RIF_FRAZIONARIO` int(11) DEFAULT NULL,
  `RIF_CAUSALE` int(11) DEFAULT NULL,
  `RIF_VOCE_DACO` int(11) DEFAULT NULL,
  `MOTIVO` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `DEBITI_EURO` decimal(13,2) DEFAULT NULL,
  `CREDITI_EURO` decimal(13,2) DEFAULT NULL,
  `RIF_PROFILO` int(11) DEFAULT NULL,
  `RIF_STATO_RILIEVO` int(11) DEFAULT NULL,
  `DATA_REGOL_CUMUL` datetime DEFAULT NULL,
  `DATA_ANNULLAMENTO` datetime DEFAULT NULL,
  `FLAG_ANNULLATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `STATUS_RIALLINEATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_RILIEVO_PADRE` int(11) DEFAULT NULL,
  `RIF_RILIEVO_FIGLIO` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(600) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_REGOLARIZZAZIONE` datetime DEFAULT NULL,
  `REGOLARIZZATO_DA_UTENTE` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `FLAG_REGOLARIZZATO` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_CAUSALE_PRECEDENTE` int(11) DEFAULT NULL,
  `UTENTE_INSERIMENTO` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `RIF_SERVIZIO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_RILIEVI`),
  KEY `storage_rilievi_tipolog_uffici_fk` (`RIF_FRAZIONARIO`),
  KEY `storage_rilievi_tipolog_filiali_fk` (`RIF_FILIALE`),
  KEY `storage_rilievi_tipolog_causale_fk` (`RIF_CAUSALE`),
  KEY `storage_rilievi_tipolog_daco_fk` (`RIF_VOCE_DACO`),
  KEY `storage_rilievi_anag_profili_fk` (`RIF_PROFILO`),
  KEY `storage_rilievi_tipolog_stato_rilievo_fk` (`RIF_STATO_RILIEVO`),
  KEY `storage_rilievi_tipolog_causale_fk_1` (`RIF_CAUSALE_PRECEDENTE`),
  KEY `storage_rilievi_tipolog_servizi_fk` (`RIF_SERVIZIO`),
  CONSTRAINT `storage_rilievi_anag_profili_fk` FOREIGN KEY (`RIF_PROFILO`) REFERENCES `anag_profili` (`ID_PROFILO`),
  CONSTRAINT `storage_rilievi_tipolog_causale_fk_1` FOREIGN KEY (`RIF_CAUSALE_PRECEDENTE`) REFERENCES `tipolog_causale` (`ID_CAUSALE`),
  CONSTRAINT `storage_rilievi_tipolog_daco_fk` FOREIGN KEY (`RIF_VOCE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`),
  CONSTRAINT `storage_rilievi_tipolog_filiali_fk` FOREIGN KEY (`RIF_FILIALE`) REFERENCES `tipolog_filiali` (`ID_FILIALE`),
  CONSTRAINT `storage_rilievi_tipolog_servizi_fk` FOREIGN KEY (`RIF_SERVIZIO`) REFERENCES `tipolog_servizi` (`ID_SERVIZIO`),
  CONSTRAINT `storage_rilievi_tipolog_stato_rilievo_fk` FOREIGN KEY (`RIF_STATO_RILIEVO`) REFERENCES `tipolog_stato_rilievo` (`ID_STATO_RILIEVO`),
  CONSTRAINT `storage_rilievi_tipolog_uffici_fk` FOREIGN KEY (`RIF_FRAZIONARIO`) REFERENCES `tipolog_uffici` (`ID_UFFICIO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_rilievi_maxi_dettaglio`
--

DROP TABLE IF EXISTS `storage_rilievi_maxi_dettaglio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_rilievi_maxi_dettaglio` (
  `ID_RILIEVI_MAXI_DETTAGLIO` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_FILIALE` int(11) DEFAULT NULL,
  `RIF_UFFICIO` int(11) DEFAULT NULL,
  `RIF_CAUSALE` int(11) DEFAULT NULL,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `IMPORTO` decimal(18,2) DEFAULT NULL,
  `DATA_MXR` datetime DEFAULT NULL,
  `DATA_LIM` datetime DEFAULT NULL,
  `DATA_REG` datetime DEFAULT NULL,
  `ID_RILIEVI_MAXI_TOTALE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_RILIEVI_MAXI_DETTAGLIO`),
  KEY `storage_rilievi_maxi_dettaglio_tipolog_filiali_fk` (`RIF_FILIALE`),
  KEY `storage_rilievi_maxi_dettaglio_tipolog_uffici_fk` (`RIF_UFFICIO`),
  KEY `storage_rilievi_maxi_dettaglio_tipolog_causale_fk` (`RIF_CAUSALE`),
  KEY `storage_rilievi_maxi_dettaglio_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `storage_rilievi_maxi_dettaglio_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `storage_rilievi_maxi_dettaglio_tipolog_causale_fk` FOREIGN KEY (`RIF_CAUSALE`) REFERENCES `tipolog_causale` (`ID_CAUSALE`),
  CONSTRAINT `storage_rilievi_maxi_dettaglio_tipolog_filiali_fk` FOREIGN KEY (`RIF_FILIALE`) REFERENCES `tipolog_filiali` (`ID_FILIALE`),
  CONSTRAINT `storage_rilievi_maxi_dettaglio_tipolog_uffici_fk` FOREIGN KEY (`RIF_UFFICIO`) REFERENCES `tipolog_uffici` (`ID_UFFICIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storage_rilievi_maxi_totale`
--

DROP TABLE IF EXISTS `storage_rilievi_maxi_totale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_rilievi_maxi_totale` (
  `ID_RILIEVI_MAXI_TOTALE` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_CAUSALE` int(11) DEFAULT NULL,
  `TOT_IMPORTO` decimal(18,2) DEFAULT NULL,
  `TOT_RILIEVI` int(11) DEFAULT NULL,
  `RIF_RILIEVI` int(11) DEFAULT NULL,
  `IMPORTO` decimal(18,2) DEFAULT NULL,
  `DATA_MXR` datetime DEFAULT NULL,
  `DATA_LIM` datetime DEFAULT NULL,
  `DATA_REG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_RILIEVI_MAXI_TOTALE`),
  KEY `storage_rilievi_maxi_totale_tipolog_causale_fk` (`RIF_CAUSALE`),
  KEY `storage_rilievi_maxi_totale_storage_rilievi_fk` (`RIF_RILIEVI`),
  CONSTRAINT `storage_rilievi_maxi_totale_storage_rilievi_fk` FOREIGN KEY (`RIF_RILIEVI`) REFERENCES `storage_rilievi` (`ID_RILIEVI`),
  CONSTRAINT `storage_rilievi_maxi_totale_tipolog_causale_fk` FOREIGN KEY (`RIF_CAUSALE`) REFERENCES `tipolog_causale` (`ID_CAUSALE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_causale`
--

DROP TABLE IF EXISTS `tipolog_causale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_causale` (
  `ID_CAUSALE` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_CAUSALE` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_CAUSALE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_CAUSALE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_daco`
--

DROP TABLE IF EXISTS `tipolog_daco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_daco` (
  `ID_VOCE_DACO` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_DACO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `DESCRIZIONE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `CONTO_CONTABILE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `CONTO_CONTABILE_DESC` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_INIZIO_VALIDITA` datetime DEFAULT NULL,
  `DATA_FINE_VALIDITA` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_VOCE_DACO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_daco_escludere_maxi_rilievi`
--

DROP TABLE IF EXISTS `tipolog_daco_escludere_maxi_rilievi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_daco_escludere_maxi_rilievi` (
  `ID_VOCE_DACO_ESCL_MAXI_RILIEVI` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_CODICE_DACO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_VOCE_DACO_ESCL_MAXI_RILIEVI`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_filiali`
--

DROP TABLE IF EXISTS `tipolog_filiali`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_filiali` (
  `ID_FILIALE` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_FILIALE` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_FILIALE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `PROGR` varchar(1) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_INIZIO_VALIDITA` datetime DEFAULT NULL,
  `DATA_FINE_VALIDITA` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_FILIALE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_funzioni`
--

DROP TABLE IF EXISTS `tipolog_funzioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_funzioni` (
  `ID_FUNZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `NOME_FUNZIONE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_FUNZIONE` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_FUNZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_param_funzioni`
--

DROP TABLE IF EXISTS `tipolog_param_funzioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_param_funzioni` (
  `ID_PARAMETRO` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_FUNZIONE` int(11) DEFAULT NULL,
  `PARAMETRO` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_PARAMETRO` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `VALORE` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_PARAMETRO`),
  KEY `tipolog_param_funzioni_tipolog_funzioni_fk` (`RIF_FUNZIONE`),
  CONSTRAINT `tipolog_param_funzioni_tipolog_funzioni_fk` FOREIGN KEY (`RIF_FUNZIONE`) REFERENCES `tipolog_funzioni` (`ID_FUNZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_servizi`
--

DROP TABLE IF EXISTS `tipolog_servizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_servizi` (
  `ID_SERVIZIO` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_SERVIZIO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `DESCRIZIONE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_SERVIZIO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_stato_rilievo`
--

DROP TABLE IF EXISTS `tipolog_stato_rilievo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_stato_rilievo` (
  `ID_STATO_RILIEVO` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE_STATO_RILIEVO` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_STATO_RILIEVO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_tipo_azioni`
--

DROP TABLE IF EXISTS `tipolog_tipo_azioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_tipo_azioni` (
  `ID_TIPO_AZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `AZIONE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO_AZIONE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_uffici`
--

DROP TABLE IF EXISTS `tipolog_uffici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_uffici` (
  `ID_UFFICIO` int(11) NOT NULL AUTO_INCREMENT,
  `FRAZIONARIO` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `DESCR_FRAZIONARIO` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `TIPO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `RIF_FILIALE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID_UFFICIO`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipolog_utenti_servizi`
--

DROP TABLE IF EXISTS `tipolog_utenti_servizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipolog_utenti_servizi` (
  `ID_UTENTE_SERVIZIO` int(11) NOT NULL AUTO_INCREMENT,
  `RIF_VOCE_DACO` int(11) DEFAULT NULL,
  `RIF_ABILITAZIONE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_UTENTE_SERVIZIO`),
  KEY `tipolog_utenti_servizi_tipolog_daco_fk` (`RIF_VOCE_DACO`),
  KEY `tipolog_utenti_servizi_anag_profili_abilitazione_fk` (`RIF_ABILITAZIONE`),
  CONSTRAINT `tipolog_utenti_servizi_anag_profili_abilitazione_fk` FOREIGN KEY (`RIF_ABILITAZIONE`) REFERENCES `anag_profili_abilitazione` (`ID_ANAG_PROFILI_ABILITAZIONE`),
  CONSTRAINT `tipolog_utenti_servizi_tipolog_daco_fk` FOREIGN KEY (`RIF_VOCE_DACO`) REFERENCES `tipolog_daco` (`ID_VOCE_DACO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-28 18:27:41
