package it.poste.rilievi.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.api.model.dto.EvaluationDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class EvaluationApiTest extends ApiTestBase {

	EvaluationDTO updateEvalut;

	@Test
	public void changeFractional() throws Exception {

		JSONObject evalut = new JSONObject();

		evalut.put("id", "2");

		String json = evalut.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/2/office",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertNotNull(response.getBody().getId());

	}

	@Test
	public void setLossState() throws Exception {

		JSONObject status = new JSONObject();
		status.put("id", 1);

		String json = status.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/3/lossstate",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);

	}

	@Test
	public void undoLossState() throws Exception {

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(null, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/4/undolossstate",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertNotNull(response.getBody().getId());

	}

	@Test
	public void testDeleteEvaluation() throws Exception {

		ResponseEntity<PaginableResultSearchImpl<EvaluationDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "evaluation/5", HttpMethod.DELETE, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<EvaluationDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void undoRcReason() throws Exception {

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(null, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/6/undorcreason",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertNotNull(response.getBody().getId());
	}

	@Test
	public void setRCReason() throws Exception {

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(null, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/7/rcreason",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void setDescription() throws Exception {

		JSONObject status = new JSONObject();
		status.put("description", "test");
		String json = status.toString();
		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
		ResponseEntity<EvaluationDTO> response = new TestRestTemplate().exchange(baseUrl + "evaluation/8/description",
				HttpMethod.PUT, httpEntity, EvaluationDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertNotNull(response.getBody().getId());
	}

}
