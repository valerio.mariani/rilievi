package it.poste.rilievi.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import net.minidev.json.JSONObject;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class FunctionApiTest extends ApiTestBase {

	@Autowired
	private TestRestTemplate restTemplate;

	FunctionDTO insertItem;

	FunctionDTO updatedItem;

	@Test
	public void insertNewFunctionOK() throws Exception {

		JSONObject function = new JSONObject();

		function.put("name", "nomerilievo");
		function.put("description", "Descrizione");

		String json = function.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);

		ResponseEntity<FunctionDTO> response = restTemplate.postForEntity(new URL(baseUrl + "function").toString(),
				httpEntity, FunctionDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);
		assertNotNull(response.getBody().getId());

		insertItem = response.getBody();

		updateFunctionOK();
		testGetAllFunction();

	}

	public void testGetAllFunction() throws Exception {

		ResponseEntity<PaginableResultSearchImpl<FunctionDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "functions", HttpMethod.GET, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<FunctionDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		boolean founded = false;
		for (FunctionDTO function : entity.getBody().getItems()) {
			if (function.getId().intValue() == insertItem.getId()) {
				founded = true;
				break;
			}
		}
		assertEquals(founded, true);
	}

	public void updateFunctionOK() throws Exception {

		String descMod = "descrizione function";

		JSONObject function = new JSONObject();
		function.put("id", insertItem.getId());
		function.put("description", descMod);

		String json = function.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
		ResponseEntity<FunctionDTO> response = new TestRestTemplate().exchange(baseUrl + "function/1", HttpMethod.PUT,
				httpEntity, FunctionDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertEquals(descMod, response.getBody().getDescription());
		assertNotNull(response.getBody().getId());
		updatedItem = response.getBody();
	}

}
