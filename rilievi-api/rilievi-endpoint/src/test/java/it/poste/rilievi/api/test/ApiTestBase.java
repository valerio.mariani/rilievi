package it.poste.rilievi.api.test;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;

import it.poste.rilievi.api.endpoint.filter.IdentityFilter;

public class ApiTestBase {
	static {
		System.setProperty("rilievi-api.spring.config.location", "classpath:");
	}
	@LocalServerPort
	protected int port;

	protected String baseUrl;

	@BeforeEach
	public void setup() {
		this.baseUrl = "http://localhost:" + port + "/rilievi-api/";
	}

	protected HttpHeaders getheaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add(IdentityFilter.HEADER_USER, "12345");
		requestHeaders.add(IdentityFilter.HEADER_USER_PROFILE, "P01");
		return requestHeaders;
	}

}
