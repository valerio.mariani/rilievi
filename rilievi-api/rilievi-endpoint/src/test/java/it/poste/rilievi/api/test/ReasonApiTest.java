package it.poste.rilievi.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.api.model.dto.ReasonDTO;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ReasonApiTest extends ApiTestBase {

	@Test
	public void testGetAllReason() throws Exception {

		ResponseEntity<List<ReasonDTO>> entity = new TestRestTemplate().exchange(baseUrl + "reasons", HttpMethod.GET,
				new HttpEntity<Object>(getheaders()), new ParameterizedTypeReference<List<ReasonDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		boolean founded = false;
		for (ReasonDTO daco : entity.getBody()) {
			if (daco.getId().intValue() == 1) {
				founded = true;
				break;
			}
		}
		assertEquals(founded, true);
	}

}
