package it.poste.rilievi.api.endpoint;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import it.poste.rilievi.api.model.dto.DacoCriteriaDTO;
import it.poste.rilievi.api.model.dto.DacoDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.service.api.IDacoService;
import it.poste.rilievi.api.service.impl.base.ContextStoreHolder;


@RestController
@Component
public class DacoRsImpl {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DacoRsImpl.class);
	
	private IDacoService dacoService;
	
	@Autowired
	public DacoRsImpl(IDacoService service) {
		this.dacoService = service;
		
	}

	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            		produces = MediaType.APPLICATION_JSON_VALUE,  
            		method = RequestMethod.POST, 
            		value = "/daco")
	@ResponseStatus(code = HttpStatus.CREATED)
	public DacoDTO insertDaco(@RequestBody DacoDTO request) {
		DacoDTO ret = dacoService.insertDaco(request); 
		return ret;
	}
	 
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/daco")
	@ResponseStatus(code = HttpStatus.OK)
	public DacoDTO updatetDaco(@RequestBody DacoDTO request) {	
		DacoDTO ret = dacoService.updateDaco(request); 
		return ret;
	}
	 
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/dacos")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<DacoDTO> getAllDaco(DacoCriteriaDTO criteria) {	
		return dacoService.getDacos(criteria); 
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/profile/dacos")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DacoDTO> getDacoUserProfile() {	
		Long profileId = ContextStoreHolder.getContextStore().getAppUser().getProfile().getId();
		return dacoService.getDacoUserProfile(profileId); 
	}
	
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/dacos/maxi-evaluation")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DacoDTO> getDacoMaxiEvaluation() {	
		return dacoService.getDacoMaxiEvaluation(); 
	}
	
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/dacos/maxi-evaluation-excluded")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DacoDTO> getDacoUserProfileMaxiEvaluation() {	
		return dacoService.getExcludedDaco();
	}
}
