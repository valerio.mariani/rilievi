package it.poste.rilievi.api.endpoint;

import java.io.ByteArrayInputStream;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.api.model.dto.EvaluationDTO;
import it.poste.rilievi.api.model.dto.ExportTableDTO;
import it.poste.rilievi.api.model.dto.IPaginableResultSearch;
import it.poste.rilievi.api.model.dto.MaxiEvaluationSettingsDTO;
import it.poste.rilievi.api.service.api.IEvaluationService;
import it.poste.rilievi.api.service.api.IMaxiEvaluationService;
import it.poste.rilievi.api.service.api.ISettingsService;


@RestController()
@Component
public class MaxiEvaluationRsImpl {
	
	
	private IEvaluationService evaluationService;
	
	@Autowired
	IMaxiEvaluationService maxiEvaluationService;
	
	@Autowired
	ISettingsService setting;
	
	@Autowired
	public MaxiEvaluationRsImpl(IEvaluationService service) {
		this.evaluationService = service;
	}
	
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "maxi-evaluations")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<IPaginableResultSearch<EvaluationDTO>> getMaxiEvaluations( EvaluationCriteriaDTO request){	
		IPaginableResultSearch<EvaluationDTO> ret = evaluationService.getNotRegularizedEvaluations(request);
		return new ResponseEntity<IPaginableResultSearch<EvaluationDTO>>(ret, HttpStatus.OK);
	}
	
	@RequestMapping(
			consumes = MediaType.ALL_VALUE,
    		method = RequestMethod.GET, 
    		value = "maxi-evaluations/export")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Resource> exportMaxiEvaluations(EvaluationCriteriaDTO criteria) {	
		ExportTableDTO table=  evaluationService.exportMaxiEvaluations(criteria); 
		byte[] fileByte = Base64.getDecoder().decode(table.getContent().getBytes());
        Resource file = new InputStreamResource(new ByteArrayInputStream(fileByte));
        return ResponseEntity.ok()
        		.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + table.getFileName() + "\"")
        		.header(HttpHeaders.CONTENT_TYPE, table.getContentType() + "; charset=utf-8")
        		.body(file);
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "maxi-evaluations-settings")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<MaxiEvaluationSettingsDTO> setMaxiEvaluationsSettings(@RequestBody MaxiEvaluationSettingsDTO request){	
		MaxiEvaluationSettingsDTO ret = evaluationService.setMaxiRegularizationSettings(request);
		return new ResponseEntity<MaxiEvaluationSettingsDTO>(ret, HttpStatus.OK);
	}
	
}
