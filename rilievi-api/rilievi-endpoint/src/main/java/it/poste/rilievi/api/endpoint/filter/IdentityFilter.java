package it.poste.rilievi.api.endpoint.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.google.gson.Gson;

import it.poste.rilievi.api.endpoint.exception.ErrorResponse;
import it.poste.rilievi.api.model.dto.AppProfileDTO;
import it.poste.rilievi.api.model.dto.AppUserDTO;
import it.poste.rilievi.api.service.api.IProfileService;
import it.poste.rilievi.api.service.impl.base.ContextStoreHolder;

public class IdentityFilter implements  Filter{

	private static Logger logger = LoggerFactory.getLogger(IdentityFilter.class);
	public static final String HEADER_USER = "USER_ID";
	public static final String HEADER_USER_PROFILE = "USER_PROFILE";
	public static final String POST_METHOD = "POST";

	
	@Autowired
	IProfileService profileService;
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		if(identify(request)) {
			chain.doFilter(servletRequest, servletResponse);
		}else {
			HttpServletResponse response = (HttpServletResponse)servletResponse;			
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			ErrorResponse responseBody = new ErrorResponse.ErrorResponseBuilder()
					.withStatus(HttpStatus.UNAUTHORIZED)
					.withError_code(""+HttpStatus.UNAUTHORIZED.value())
					.withMessage(HttpStatus.UNAUTHORIZED.getReasonPhrase())
					.build();
			response.getOutputStream().print(new Gson().toJson(responseBody));
		}
		
	}

	
	private boolean identify(HttpServletRequest request) {
		boolean ret = false;
		try {

				String userCode = request.getHeader(HEADER_USER);
				String profileCode = request.getHeader(HEADER_USER_PROFILE);
				if (StringUtils.isNotEmpty(userCode) && StringUtils.isNotEmpty(profileCode)) {
					AppUserDTO appUser = new AppUserDTO();
					AppProfileDTO profile = profileService.getProfileByCode(profileCode);
					if (profile != null) {
						appUser.setProfile(profile);
						appUser.setCode(userCode);
						ContextStoreHolder.getContextStore().setAppUser(appUser);
						ret = true;
					} else {
						logger.error("User not Identified code:" + userCode + " profile: " + profileCode);
					}
				} else {
					logger.error("User not Identified code:" + userCode + " profile: " + profileCode);
				}
			
		} catch (Exception e) {
			logger.error("identify filter error:" + e.getMessage(), e);
		}
		return ret;
	}
}
