package it.poste.rilievi.api.endpoint.exception;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import it.poste.rilievi.api.service.impl.exception.ApplicationError;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	private final static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	private static final String INTERNAL_SERVER = "Something went wrong while processing request";
	private static final String BAD_REQUEST = "Error in request path";
	private static final String METHOD_NOT_ALLOWED = "Method Not Allowed";
	private static final String UNSUPPORTED_MEDIA_TYPE = "Unsupported Media Type";
	
	//Error 400 MethodArgumentNotValidException
	@ExceptionHandler({ MethodArgumentNotValidException.class })
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		final HashMap<String, List<String>> fieldErrors = toHashMap(ex.getBindingResult().getFieldErrors());		
		final List<String> errors = new ArrayList<String>();
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}	
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetails(errors)
				.withErrorFields(fieldErrors)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}
	

	//Error 400 BindException
	@ExceptionHandler({ BindException.class })
	protected ResponseEntity<Object> handleBindException(final BindException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final HashMap<String, List<String>> fieldErrors = toHashMap(ex.getBindingResult().getFieldErrors());
		final List<String> errors = new ArrayList<String>();
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		ErrorResponse response = new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetails(errors)
				.withErrorFields(fieldErrors)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}


	//Error 400 MissingServletRequestPartException
	@ExceptionHandler({ MissingServletRequestPartException.class })
	protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex, final WebRequest request) {
		logger.info(ex.getClass().getName(),ex);
		//
		final String error = ex.getRequestPartName() + " part is missing";
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetail(error)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}

	//Error 400 MissingServletRequestParameterException
	@ExceptionHandler({ MissingServletRequestParameterException.class })
	protected ResponseEntity<Object> handleMissingServletRequestParameter(final MissingServletRequestParameterException ex, final WebRequest request) {

		logger.info(ex.getClass().getName(),ex);
		final String error = "Required " + ex.getParameterName() + " parameter is missing";
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetail(error)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}
 
	//Error 400 MethodArgumentTypeMismatchException
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex, final WebRequest request) {
		logger.info(ex.getClass().getName(),ex);
		final String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetail(error)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}

	//Error 400 ConstraintViolationException
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex, final WebRequest request) {
		logger.info(ex.getClass().getName(),ex);
		final List<String> details = new ArrayList<String>();
		for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			details.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage());
		}
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(BAD_REQUEST)
				.withDetails(details)
				.build();
		return new ResponseEntity<>(response, response.getStatus());

	}

	//Error 404 NoHandlerFoundException
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({ NoHandlerFoundException.class })
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex, final WebRequest request) {
		logger.info(ex.getClass().getName(),ex);
		final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.NOT_FOUND)
				.withError_code(""+HttpStatus.NOT_FOUND.value())
				.withMessage(BAD_REQUEST)
				.withDetail(error)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}

	//Error 404 NoSuchElementException
	@ExceptionHandler({NoSuchElementException.class })
	public ResponseEntity<Object> handleNoObjectFoundException(final Exception ex, final WebRequest request) {
		logger.info(ex.getClass().getName(),ex);
		
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.NOT_FOUND)
				.withError_code(""+HttpStatus.NOT_FOUND.value())
				.withMessage(BAD_REQUEST)
				.withDetail(ex.getMessage())
				.build();
		logger.error("Object Not Found",  response.getPath());
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	
	

	//Error 405
	@ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException ex, final WebRequest request) {
        logger.info(ex.getClass().getName(),ex);      
        //
        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

    	ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.METHOD_NOT_ALLOWED)
				.withError_code(""+HttpStatus.METHOD_NOT_ALLOWED.value())
				.withMessage(METHOD_NOT_ALLOWED)
				.withDetail(builder.toString())
				.build();
		return new ResponseEntity<>(response, response.getStatus());
    }

    // 415
	@ExceptionHandler({ HttpMediaTypeNotSupportedException.class })
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex, final WebRequest request) {
		  logger.info(ex.getClass().getName(),ex);    
        //
        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t + " "));
    	ErrorResponse response = new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
				.withError_code(""+HttpStatus.UNSUPPORTED_MEDIA_TYPE.value())
				.withMessage(UNSUPPORTED_MEDIA_TYPE)
				.withDetail(builder.toString())
				.build();
    	return new ResponseEntity<>(response, response.getStatus());
    }


	
	//Error 500
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleOthers(final Exception ex, final WebRequest request) {
		  logger.info(ex.getClass().getName(),ex);    
		ErrorResponse response = new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
				.withError_code(""+HttpStatus.INTERNAL_SERVER_ERROR.value())
				.withMessage(INTERNAL_SERVER)
				.build();
		return new ResponseEntity<>(response, response.getStatus());
	}

	
	//@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<ErrorResponse> handleValidationExceptions(ServiceException ex, final WebRequest request) {	 
		
		HttpStatus status = isNotAuthorized(ex) ? HttpStatus.UNAUTHORIZED : HttpStatus.BAD_REQUEST;
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(status)
				.withError_code(""+status.value())
				.withMessage(INTERNAL_SERVER)
				.withDetails(errorListToString(ex.getErrors()))
				.withErrorFields(ex.getFieldsErrors())
				.build();
		return new ResponseEntity<>(response,response.getStatus());
	}
	
	
	@ExceptionHandler(Throwable.class)
	public ResponseEntity<ErrorResponse> handleThrowable(Throwable ex, final WebRequest request) {	 
		
		
		ErrorResponse response =new ErrorResponse.ErrorResponseBuilder(request)
				.withStatus(HttpStatus.BAD_REQUEST)
				.withError_code(""+HttpStatus.BAD_REQUEST.value())
				.withMessage(INTERNAL_SERVER)
				//.withDetails(errorListToString(ex.getErrors()))
				//.withErrorFields(ex.getFieldsErrors())
				.build();
		return new ResponseEntity<>(response,response.getStatus());
	}
	
	private HashMap<String, List<String>> toHashMap(List<FieldError> errors){		
		final HashMap<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
		for (final FieldError error :errors) {
			if(!fieldErrors.containsKey(error.getField())) {
				fieldErrors.put(error.getField(), new ArrayList<String>());
			}
			fieldErrors.get(error.getField()).add(error.getDefaultMessage());
		}
		return fieldErrors;
	}
	
	
	private List<String> errorListToString(List<ApplicationError > errors) {
		if(errors == null ) {
			return null;
		}			
		List<String> ret = new ArrayList<String>();
		for(ApplicationError err : errors ) {
			ret.add(StringUtils.defaultIfEmpty(err.getCode(), "") + StringUtils.defaultIfEmpty(err.getMessage(), ""));
		}
		return ret;
	}
	
	private boolean isNotAuthorized(ServiceException ex) {
		if(ex.getErrors() == null ) {
			return false;
		}
		for(ApplicationError err : ex.getErrors() ) {
			if(ServiceException.NotAuthorized.getCode().equalsIgnoreCase(err.getCode())) {
				return true;
			}
		}
		return false;
	}
}
