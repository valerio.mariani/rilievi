package it.poste.rilievi.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.OfficeDTO;
import it.poste.rilievi.api.service.api.IOfficeService;


@RestController
@Component
public class OfficeRsImpl {
	
	private IOfficeService officeService;
	
	@Autowired
	public OfficeRsImpl(IOfficeService service) {
		this.officeService = service;
	}
	 
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "branch/{id_branch}/offices")
	@ResponseStatus(code = HttpStatus.OK)
	public List<OfficeDTO> getOffices(@PathVariable("id_branch") Long idBranch) {	
		return officeService.getOffices(idBranch); 
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "office")
	@ResponseStatus(code = HttpStatus.OK)
	public OfficeDTO getOfficeByFractional(@RequestParam String fractional) {	
		return officeService.getOfficeByFractional(fractional); 
	}
}
