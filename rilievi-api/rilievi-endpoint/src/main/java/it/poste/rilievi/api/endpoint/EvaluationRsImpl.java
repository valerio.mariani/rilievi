package it.poste.rilievi.api.endpoint;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.poste.rilievi.api.model.dto.AppSettings;
import it.poste.rilievi.api.model.dto.AttachmentDTO;
import it.poste.rilievi.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.api.model.dto.EvaluationDTO;
import it.poste.rilievi.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.api.model.dto.ExportTableDTO;
import it.poste.rilievi.api.model.dto.IPaginableResultSearch;
import it.poste.rilievi.api.model.dto.OfficeDTO;
import it.poste.rilievi.api.service.api.IEvaluationService;
import it.poste.rilievi.api.service.api.ISettingsService;
import it.poste.rilievi.api.service.impl.base.ContextStoreHolder;





@RestController()
@Component
public class EvaluationRsImpl {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(EvaluationRsImpl.class);
	
	private IEvaluationService evaluationService;
	
	@Autowired
	ISettingsService settingsService;
	
	@Autowired
	ObjectMapper jsonMapper;
	
	
	@Autowired
	public EvaluationRsImpl(IEvaluationService service) {
		this.evaluationService = service;
	}

	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "sysDate")
	@ResponseStatus(code = HttpStatus.OK)
	public HashMap<String, LocalDateTime> sysDate() {
		LocalDateTime sysDate = evaluationService.getSysDate(); 
		HashMap<String, LocalDateTime> jsonObj = new HashMap<String, LocalDateTime>();
		jsonObj.put("sysDate", sysDate);
		return jsonObj;
	}

	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            		produces = MediaType.APPLICATION_JSON_VALUE,  
            		method = RequestMethod.POST, 
            		value = "evaluation")
	@ResponseStatus(code = HttpStatus.CREATED)
	public EvaluationDTO insertEvaluation(@RequestBody EvaluationDTO request) {
		request.setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
		return evaluationService.insertEvaluation(request); 
	}
	
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "evaluation/{id_evaluation}")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO getEvaluation(@PathVariable("id_evaluation") Long evaluationId) {
		return evaluationService.getEvaluation(evaluationId);
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
		    		produces = MediaType.APPLICATION_JSON_VALUE,  
		    		method = RequestMethod.GET, 
		    		value = "evaluations")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<IPaginableResultSearch<EvaluationDTO>> getEvaluations(EvaluationCriteriaDTO criteria ){
		IPaginableResultSearch<EvaluationDTO> ret = evaluationService.getEvaluations(criteria);
		return new ResponseEntity<IPaginableResultSearch<EvaluationDTO>>(ret, HttpStatus.OK);
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "daily-evaluations")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<IPaginableResultSearch<EvaluationDTO>> getDailyEvaluations(EvaluationCriteriaDTO criteria ){
		IPaginableResultSearch<EvaluationDTO> ret = evaluationService.getDailyEvaluations(criteria);
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}
	
	
	@RequestMapping(
			consumes = MediaType.ALL_VALUE,
    		method = RequestMethod.GET, 
    		value = "daily-evaluations/export")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Resource> exportDaily(EvaluationCriteriaDTO criteria) {	
		ExportTableDTO table=  evaluationService.exportUserDailyEvaluations(criteria.getAppUserCode()); 
		byte[] fileByte = Base64.getDecoder().decode(table.getContent().getBytes());
        Resource file = new InputStreamResource(new ByteArrayInputStream(fileByte));
        return ResponseEntity.ok()
        		.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + table.getFileName() + "\"")
        		.header(HttpHeaders.CONTENT_TYPE, table.getContentType() + "; charset=utf-8")
        		.body(file);
	}
	

	@RequestMapping(
			consumes = MediaType.ALL_VALUE,
    		method = RequestMethod.GET, 
    		value = "evaluations/export")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Resource> export(EvaluationCriteriaDTO criteria) {	
		ExportTableDTO table=  evaluationService.exportEvaluations(criteria); 
		byte[] fileByte = Base64.getDecoder().decode(table.getContent().getBytes());
        Resource file = new InputStreamResource(new ByteArrayInputStream(fileByte));
        return ResponseEntity.ok()
        		.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + table.getFileName() + "\"")
        		.header(HttpHeaders.CONTENT_TYPE, table.getContentType() + "; charset=utf-8")
        		.body(file);
	}
	
	
	
    @PostMapping(name="evaluation",consumes =  MediaType.MULTIPART_FORM_DATA_VALUE)
    public EvaluationDTO inserEvaluationMultiform(@RequestParam("file") MultipartFile[] files, @RequestParam("jsonData") String jsonData, RedirectAttributes redirectAttributes) {     
    	
    	try {
    		EvaluationDTO evaluation  = jsonMapper.readValue(jsonData.getBytes(), EvaluationDTO.class);    		
    		for(MultipartFile file : files) {
    			AttachmentDTO attachment = new AttachmentDTO();
    			attachment.setContent(Base64.getEncoder().encodeToString(file.getBytes()));
    			attachment.setActive(true);
    			attachment.setMimeType(file.getContentType());
    			attachment.setName(file.getOriginalFilename());
    			attachment.setSize(file.getSize());
    			evaluation.getAttachments().add(attachment);    			
    		}
    		evaluation.setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
    		EvaluationDTO response = evaluationService.insertEvaluation(evaluation); 
    		return response;	
		} catch (IOException e) {
			throw new RuntimeException("");
		}
    }
    
    
    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "evaluation/reclassification")
	@ResponseStatus(code = HttpStatus.CREATED)
	public EvaluationDTO reclassification(@RequestBody EvaluationDTO request) {
		request.setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
		request.getGenerated().setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
		return evaluationService.reclassificationDacoEvaluation(request); 
	}
    
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "config")
	@ResponseStatus(code = HttpStatus.OK)
	public AppSettings getConfig() {
		return settingsService.getAppSettings();
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT, 
    		value = "evaluation/{id_evaluation}/office")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO changeFractional(@RequestBody OfficeDTO office, @PathVariable ("id_evaluation") Long evaluationId) {
		return evaluationService.changeFractional(office, evaluationId);
	}
	
	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, 
			method = RequestMethod.DELETE, 
			value = "evaluation/{id_evaluation}")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO deleteEvaluation(@PathVariable("id_evaluation") Long evaluationId) {
		return evaluationService.deleteEvaluation(evaluationId);
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/lossstate")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO setLossState(@RequestBody EvaluationStatusDTO request, @PathVariable("id_evaluation") Long idEvaluation ) {			
		return evaluationService.setLossState(idEvaluation, request); 
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/undolossstate")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO undoLossState(@PathVariable("id_evaluation") Long idEvaluation ) {	
		return evaluationService.undoLossState(idEvaluation); 
	}

	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/undorcreason")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO undoRcReason(@PathVariable("id_evaluation") Long idEvaluation ) {		
		return evaluationService.undoRcReason(idEvaluation); 
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/description")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO setDescription(@RequestBody EvaluationDTO request, @PathVariable("id_evaluation") Long idEvaluation ) {			
		return evaluationService.setDescription(request,idEvaluation); 
	}
	
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/rcreason")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO setRCReason(@PathVariable("id_evaluation") Long idEvaluation ) {		
		return evaluationService.setRCReason(idEvaluation); 
	}

	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/evaluation/{id_evaluation}/modEvaluation")
	@ResponseStatus(code = HttpStatus.OK)
	public EvaluationDTO updateEvaluation(@PathVariable("id_evaluation") Long idEvaluation ) {		
		return evaluationService.modEvaluation(idEvaluation); 
	}
}
