package it.poste.rilievi.api.endpoint;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.ExtractionDatesDTO;
import it.poste.rilievi.api.service.api.IExtractionDatesService;

@RestController
@Component
public class ExtractionDatesRsImpl {
	
	
	private IExtractionDatesService extractionDatesService;
	
	@Autowired
	public ExtractionDatesRsImpl(IExtractionDatesService service) {
		this.extractionDatesService = service;
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/extractionDates")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ExtractionDatesDTO insertExtractionDates(@RequestBody ExtractionDatesDTO request) {
		return extractionDatesService.insertExtractionDates(request); 
	}

	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/extractionDates")
	@ResponseStatus(code = HttpStatus.OK)
	public List<ExtractionDatesDTO> getAllExtrctionDates() {	
		return extractionDatesService.getExtractionDatesItems();
	}

}
