package it.poste.rilievi.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.service.api.IFunctionService;

@RestController
@Component
public class FunctionRsImpl {
	
	private IFunctionService functionService;
	
	@Autowired
	public FunctionRsImpl(IFunctionService service ) {
		this.functionService= service;
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/function")
	@ResponseStatus(code = HttpStatus.CREATED)
	public FunctionDTO insertFunction(@RequestBody FunctionDTO request) {
		return functionService.insertFunction(request); 
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/function/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public FunctionDTO updateFunction(@RequestBody FunctionDTO request, @PathVariable("id") Long idFunction ) {	
		request.setId(idFunction);
		return functionService.updateFunction(request); 
	}
	
	
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/functions")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<FunctionDTO> getAllFunction(FunctionCriteriaDTO func) {	
		return functionService.getAllFunction(func); 
	}
	
	
	//Funzioni abilitate di un profilo
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/profile/{id}/functions")
	@ResponseStatus(code = HttpStatus.OK)
	public List<FunctionDTO> getFunctionByProfile(@PathVariable("id") Long idProfile) {	
		return functionService.getFunctionByProfile(idProfile); 
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/profile/functions")
	@ResponseStatus(code = HttpStatus.OK)
	public List<FunctionDTO> getFunctionByProfile(@RequestParam("code") String code) {	
		return functionService.getFunctionByProfileCode(code); 
	}
	
	
	//Funzioni non ancora abilitate di un profilo
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/profile/{id}/otherfunctions")
	@ResponseStatus(code = HttpStatus.OK)
	public List<FunctionDTO> getOtherFunctionByProfile(@PathVariable("id") Long idProfile) {	
		return functionService.getOtherFunctionByProfile(idProfile); 
	}
	
	//Aggiungere una funzione ad un profilo
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/profile/{id}/function/{id_function}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public FunctionDTO addFunctionProfile(@PathVariable("id") Long idProfile, @PathVariable("id_function") Long idFunction) {	
		return functionService.addFunctionProfile(idProfile, idFunction); 
	}
}
