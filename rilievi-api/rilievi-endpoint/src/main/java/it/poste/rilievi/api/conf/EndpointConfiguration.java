package it.poste.rilievi.api.conf;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import it.poste.rilievi.api.endpoint.filter.IdentityFilter;

@Configuration
@EnableEncryptableProperties()
public class EndpointConfiguration {
		
	@Value("${poste.rilievi.format.date}")
	private String dateFormat;
	
	@Value("${poste.rilievi.format.datetime}")
	private String dateTimeFormat;
	
	
	/*
	 * Conf: Formato Date JSON
	 */
    @Bean
    public Formatter<LocalDateTime> localDateTimeFormatter() {
      return new Formatter<LocalDateTime>() {
        @Override
        public LocalDateTime parse(String text, Locale locale) throws ParseException {
        		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
        		 
        		 return LocalDateTime.parse(text, formatter);           		
        }
        @Override
        public String print(LocalDateTime object, Locale locale) {
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
        	ZonedDateTime zdt = ZonedDateTime.of(object, ZoneId.systemDefault());
			LocalDateTime localDateTime = LocalDateTime.ofInstant(zdt.toInstant(), ZoneOffset.UTC);
        	return  localDateTime.format(formatter);
        }
      };
    }
    
    @Bean
    public Formatter<LocalDate> localDateFormatter() {
      return new Formatter<LocalDate>() {
        @Override
        public LocalDate parse(String text, Locale locale) throws ParseException {        	
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
			return LocalDate.parse(text, formatter);   		
        	}
        @Override
        public String print(LocalDate object, Locale locale) {
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        	return object.format(formatter);
        }
      };
    }
    
    @Bean
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializerByType(LocalDate.class, new JsonSerializer<LocalDate>() {
				@Override
				public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers)
						throws IOException {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		        	gen.writeString(value.format(formatter));
				}	
        	});
        
        builder.deserializerByType(LocalDate.class, new JsonDeserializer<LocalDate>() {	
        	@Override
			public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				 DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
				 String date = p.getValueAsString().substring(0, 10);				 
        		 return LocalDate.parse(date, formatter);           	
			
			}	
    	});
        builder.serializerByType(LocalDateTime.class, new JsonSerializer<LocalDateTime>() {
			@Override
			public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers)
					throws IOException {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
				ZonedDateTime zdt = ZonedDateTime.of(value, ZoneId.systemDefault());
				LocalDateTime localDateTime = LocalDateTime.ofInstant(zdt.toInstant(), ZoneOffset.UTC);
	        	gen.writeString(localDateTime.format(formatter));
			}	
    	});
        builder.deserializerByType(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {	
        	@Override
			public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);				
				LocalDateTime date =  LocalDateTime.parse( p.getValueAsString(), formatter);
				return date;
			}	
    	});
        return builder;
    }

    @Bean
    public Filter identityFilter() {
      return new IdentityFilter();
    }
    
    @Bean
    public FilterRegistrationBean<Filter> identityFilterRegistration() {
      FilterRegistrationBean<Filter> result = new FilterRegistrationBean<Filter>();
      result.setFilter(this.identityFilter());
      result.setUrlPatterns(Arrays.asList("/*"));
      result.setName("Identity Filter");
      result.setOrder(1);
      return result;
    }


}
