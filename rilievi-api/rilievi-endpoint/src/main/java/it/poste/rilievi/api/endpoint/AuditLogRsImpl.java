package it.poste.rilievi.api.endpoint;


import java.io.ByteArrayInputStream;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.api.model.dto.AuditLogDTO;
import it.poste.rilievi.api.model.dto.ExportTableDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.service.api.IAuditService;

@RestController
@Component
public class AuditLogRsImpl {
	
	
	private IAuditService auditService;
	
	@Autowired
	public AuditLogRsImpl(IAuditService service) {
		this.auditService = service;
	}
	
	
	@RequestMapping(
			consumes = MediaType.ALL_VALUE, 
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "auditlogs")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<AuditLogDTO> getLogs(AuditLogCriteriaDTO auditLogCriteria) {
		return auditService.getAuditLogs(auditLogCriteria);
	}
	
	@RequestMapping(
			consumes = MediaType.ALL_VALUE, 
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "auditlogs/export")
	@ResponseStatus(code = HttpStatus.OK)
		
	public ResponseEntity<Resource> exportLogs(AuditLogCriteriaDTO auditLogCriteria) {	
		ExportTableDTO table=  auditService.exportLogs(auditLogCriteria); 
		byte[] fileByte = Base64.getDecoder().decode(table.getContent().getBytes());
        Resource file = new InputStreamResource(new ByteArrayInputStream(fileByte));
        return ResponseEntity.ok()
        		.header(HttpHeaders.CONTENT_DISPOSITION, "log; filename=\"" + table.getFileName() + "\"")
        		.header(HttpHeaders.CONTENT_TYPE, table.getContentType() + "; charset=utf-8")
        		.body(file);
	}
	
	
}

