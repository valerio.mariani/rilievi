package it.poste.rilievi.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.AttachmentCriteriaDTO;
import it.poste.rilievi.api.model.dto.AttachmentDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.service.api.IAttachmentService;
import it.poste.rilievi.api.service.impl.base.ContextStoreHolder;

@RestController
@Component
public class AttachmentRsImpl {

	private IAttachmentService attachmentService;

	@Autowired
	public AttachmentRsImpl(IAttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, 
			method = RequestMethod.GET, 
			value = "evaluation/{id_evaluation}/attachments")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<AttachmentDTO> getAttachmentByIdEvaluation(AttachmentCriteriaDTO request,
			@PathVariable("id_evaluation") Long evaluationId) {
		return attachmentService.getAttachmentByIdEvaluation(request, evaluationId);

	}

	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, 
			method = RequestMethod.DELETE, 
			value = "evaluation/{id_evaluation}/attachment/{id_attachment}")
	@ResponseStatus(code = HttpStatus.OK)
	public AttachmentDTO deleteAttachmentById(@PathVariable("id_attachment") Long attachmentId) {
		return attachmentService.deleteAttachmentById(attachmentId, ContextStoreHolder.getContextStore().getAppUser().getCode());
	}

	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST, 
			value = "evaluation/{id_evaluation}/attachment")
	@ResponseStatus(code = HttpStatus.CREATED)
	public AttachmentDTO insertAttachment(@RequestBody AttachmentDTO insertAttachment,
			@PathVariable("id_evaluation") Long evaluationId) {
		insertAttachment.setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
		return attachmentService.insertAttachmentById(insertAttachment, evaluationId);
	}
	
	
	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, 
			method = RequestMethod.POST, 
			value = "evaluation/{id_evaluation}/attachments")
	@ResponseStatus(code = HttpStatus.CREATED)
	public List<AttachmentDTO> insertAttachments(@RequestBody List<AttachmentDTO> insertAttachments,
			@PathVariable("id_evaluation") Long evaluationId) {
		for(AttachmentDTO att : insertAttachments ) {
			att.setInsertUser(ContextStoreHolder.getContextStore().getAppUser());
		}
		return attachmentService.insertAttachments(insertAttachments, evaluationId);
	}


	@RequestMapping(consumes = MediaType.ALL_VALUE,
			method = RequestMethod.GET, 
        	value = "evaluation/{id_evaluation}/attachment/{id_attachment}")
	@ResponseStatus(code = HttpStatus.OK)
	public AttachmentDTO getAttachmentById(@PathVariable("id_attachment") Long attachmentId) {
		return attachmentService.getAttachmentById(attachmentId);
	}

}