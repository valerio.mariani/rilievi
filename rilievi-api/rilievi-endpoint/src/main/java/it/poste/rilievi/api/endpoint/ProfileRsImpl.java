package it.poste.rilievi.api.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.api.model.dto.AppProfileCriteriaDTO;
import it.poste.rilievi.api.model.dto.AppProfileDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.service.api.IProfileService;

@RestController
@Component
public class ProfileRsImpl {
	
	private IProfileService profileService;
	
	@Autowired
	public ProfileRsImpl(IProfileService service) {
		this.profileService = service;
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/profiles")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<AppProfileDTO> getAllProfile(AppProfileCriteriaDTO profileCriteria) {	
		return profileService.getProfileItems(profileCriteria);
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/profile")
	@ResponseStatus(code = HttpStatus.CREATED)
	public AppProfileDTO insertProfile(@RequestBody AppProfileDTO profileCriteria) {	
		return profileService.insertProfileItem(profileCriteria);
	}
	
	
	
}
