package it.poste.rilievi.api;

import java.io.File;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;


@Configuration
@ServletComponentScan
@SpringBootApplication(scanBasePackages = { "it.poste.rilievi" })
@EnableEncryptableProperties
@PropertySources(value={
		@PropertySource(value = "${rilievi-api.spring.config.location}/application.properties") ,
})
public class RilieviServletInitializer extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(RilieviServletInitializer.class);
	private static Class<RilieviServletInitializer> servletInitializerClass = RilieviServletInitializer.class;

	@Value("${logging.config.location}")
	private String loggingConfigLocation;
	
		
	//Running with java standalone
	public static void main(String[] args) {
		SpringApplication.run(servletInitializerClass, args);
	}

	//Running in traditional war deployment
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(servletInitializerClass);
	}
	
	@PostConstruct
	private void initLog() {
		File file = new File(loggingConfigLocation);
		if (file.exists()) {
			Configurator.initialize(null, loggingConfigLocation);
		} else {
			logger.warn("the logging configuration file doesn't exist: {}", loggingConfigLocation);
		}
	}


}
