package it.poste.rilievi.api.service.impl;

import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.poste.rilievi.api.model.dto.EvaluationDTO;
import it.poste.rilievi.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.api.model.dto.OfficeDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.AnagProfili;
import it.poste.rilievi.api.model.entity.StorageRilievi;
import it.poste.rilievi.api.model.entity.TipologCausale;
import it.poste.rilievi.api.model.entity.TipologDaco;
import it.poste.rilievi.api.model.entity.TipologFiliali;
import it.poste.rilievi.api.model.entity.TipologServizi;
import it.poste.rilievi.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.api.model.entity.TipologUffici;
import it.poste.rilievi.api.model.utils.Utils;
import it.poste.rilievi.api.repository.IAbbinamentoVociDacoServiziDao;
import it.poste.rilievi.api.repository.IEvaluationDao;
import it.poste.rilievi.api.repository.IEvaluationStatusDao;
import it.poste.rilievi.api.repository.IOfficeDao;
import it.poste.rilievi.api.repository.IProfileDao;
import it.poste.rilievi.api.service.api.IEvaluationService;
import it.poste.rilievi.api.service.api.ISettingsService;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

@Component
public class EvaluationValidator extends ValidatorBase implements IValidator {

	private static Logger logger = LoggerFactory.getLogger(EvaluationValidator.class);

	@Autowired
	IEvaluationService service;

	@Autowired
	IEvaluationDao evaluationDao;

	@Autowired
	IProfileDao profileDao;

	@Autowired
	IOfficeDao officeDao;

	@Autowired
	IEvaluationStatusDao evaluationStatusDao;
	
	@Autowired
	ISettingsService settings;
	
	@Autowired
	IAbbinamentoVociDacoServiziDao dacoServicesDao;

	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException {
		switch (operation) {

			case NEW_EVALUATION:
				EvaluationDTO evalut = (EvaluationDTO) obj[0];
				
				validateInsert(errors, evalut, null);			break;
			
			case RECLASSIFICATION_DACO:
				EvaluationDTO reclassDaco = (EvaluationDTO) obj[0];
				validateReclassificationDaco(errors, reclassDaco);
				break;
	
			case DELETE_EVALUATION:
				if (!ValidationUtils.rejectIfFalse(obj != null && obj.length > 0 && obj[0] != null, "idEvaluation", resolveMessage("evaluation.id.empty"), errors)) {
					Long id = (Long) obj[0];
					validateDelete(errors, id);
				}
	
				break;
	
			case CHANGE_EVALUATION_FRACTIONAL:
				OfficeDTO office = (OfficeDTO) obj[0];
				Long evaluationId = (Long) obj[1];
				validateChangeFractional(errors, office, evaluationId);
				break;
	
			case SET_LOSS_STATE:
	
				Long id = (Long) obj[0];
	
				EvaluationStatusDTO evstatus = (EvaluationStatusDTO) obj[1];
	
				validateSetLossState(errors, id, evstatus);
				break;
	
			case UNDO_LOSS_STATE:
				Long idev = (Long) obj[0];
				validateUndoLossState(errors, idev);
				break;
				
			case SET_RC_REASON:
				Long IdEvaluation = (Long) obj[0];
				validateSetRCReason(errors, IdEvaluation);
				break;
	
			case UNDO_RC_REASON:
				Long ideva = (Long) obj[0];
				validateUndoRcReason(errors, ideva);
				break;
				
			case SET_EVALUATION_DESCRIPTION:
				if( !ValidationUtils.rejectIfTrue(obj==null || obj.length < 1 || obj[1] == null, resolveMessage("evaluation.id.notExist"), errors)) {
					validateSetDescription(errors, (Long)obj[1]);
				}			
				break;
				
			case REGULARIZATION:			
				validateRegularization(errors, (Long) obj[0], (LocalDate)obj[1]);
				break;
				
			case UNDO_REGULARIZATION:			
				validateUndoRegularization(errors, (Long) obj[0]);
				break;
			default:
				break;
		}
	}

	/*
	 * 	causale = ‘RC’
  		data regolarizzazione non valorizzata
  		data contabilizzazione valorizzata
		data invio verso SRC valorizzata
  		stato rilievo non in contenzioso
  		data inserimento minore della data odierna
	 */
	private void validateRegularization(ValidationError errors, Long id, LocalDate regularizationDate ) {
		EvaluationDTO evalut = service.getEvaluation(id);
		boolean checkRcReason = settings.getRcReason().getId().longValue() == evalut.getReason().getId().longValue();
		ValidationUtils.rejectIfFalse(checkRcReason, resolveMessage("regularizzation.reason.notValid"), errors);
		ValidationUtils.rejectIfFalse(evalut.getRegularizationDate() == null, resolveMessage("regularizzation.date.null"), errors);
		ValidationUtils.rejectIfFalse(evalut.getAccountDate() != null, resolveMessage("regularizzation.accountDate.notNull"), errors);
		ValidationUtils.rejectIfFalse(evalut.getSendedSrcDate() != null, resolveMessage("regularizzation.srcDate.notNull"), errors);
		
		boolean checkStatus = settings.getUnLossState().getId().longValue() == evalut.getStatus().getId().longValue();

		ValidationUtils.rejectIfFalse(checkStatus, resolveMessage("regularizzation.status.null"), errors);	
		ValidationUtils.rejectIfDateIsAfterOrEqual(evalut.getInsertDate().toLocalDate(), service.getSysDate().toLocalDate(), resolveMessage("regularizzation.insertDate.afterOrEqual"), errors);
		//regularizationDate
		ValidationUtils.rejectIfFalse(regularizationDate != null, resolveMessage("regularizzation.date.notNull"), errors);
	}
	
	

	private void validateUndoRegularization(ValidationError errors, Long id) {
		EvaluationDTO evalut = service.getEvaluation(id);
		boolean checkRcReason = settings.getRcReason().getId().longValue() == evalut.getReason().getId().longValue();
		ValidationUtils.rejectIfFalse(checkRcReason, resolveMessage("regularizzation.reason.notValid"), errors);
		ValidationUtils.rejectIfFalse(evalut.getRegularizationDate() != null, resolveMessage("regularizzation.date.notNull"), errors);
		ValidationUtils.rejectIfFalse(evalut.getAccountDate() != null, resolveMessage("regularizzation.accountDate.notNull"), errors);
		ValidationUtils.rejectIfFalse(evalut.getSendedSrcDate() != null, resolveMessage("regularizzation.srcDate.notNull"), errors);
		boolean checkStatus = settings.getUnLossState().getId().longValue() == evalut.getStatus().getId().longValue();
		ValidationUtils.rejectIfFalse(checkStatus, resolveMessage("regularizzation.status.null"), errors);	
		ValidationUtils.rejectIfDateIsAfterOrEqual(evalut.getInsertDate().toLocalDate(), service.getSysDate().toLocalDate(), resolveMessage("regularizzation.insertDate.afterOrEqual"), errors);
	}
	
	private void validateUndoRcReason(ValidationError errors, Long id) {
		
		EvaluationDTO evalut = service.getEvaluation(id);
		
		if (!ValidationUtils.rejectIfNull(evalut, resolveMessage("evaluation.id.notExist"), errors))  {

			ValidationUtils.rejectIfTrue(evalut.isDeleted(), resolveMessage("evaluation.deleteFlag.deleted"), errors);

			boolean checkDeleteDate = true;
			if (evalut.getDeleteDate() != null) {
				checkDeleteDate = false;
			}
			ValidationUtils.rejectIfFalse(checkDeleteDate, resolveMessage("evaluation.deleteDate.deleted"), errors);

			boolean checkReasonRc = true;
			if (evalut.getReason().getId().longValue() != settings.getRcReason().getId().longValue()) {
				checkReasonRc = false;
			}
			ValidationUtils.rejectIfFalse(checkReasonRc,resolveMessage("evaluation.Reason.NotEqual"),
					errors);

			boolean checkAccountDate = false;
			if (evalut.getAccountDate() != null) {
				checkAccountDate = true;
			}

			ValidationUtils.rejectIfTrue(checkAccountDate,resolveMessage("evaluation.AccountDate.NotEqual"), errors);

		}
	 

	}

	private void validateUndoLossState(ValidationError errors, Long id) {

		EvaluationDTO evalut = service.getEvaluation(id);
		
		if (!ValidationUtils.rejectIfNull(evalut, resolveMessage("evaluation.id.notExist"), errors))  {
			
			ValidationUtils.rejectIfTrue(evalut.isDeleted(), resolveMessage("evaluation.deleteFlag.deleted"), errors);
			
			boolean checkDeleteDate = true;
			if (evalut.getDeleteDate() != null) {
				checkDeleteDate = false;
			}
			ValidationUtils.rejectIfFalse(checkDeleteDate,resolveMessage("evaluation.deleteDate.deleted"), errors);

			boolean checkReason = true;
			if (evalut.getReason().getId().longValue() != settings.getRcReason().getId().longValue()) {
				checkReason = false;
			}

			ValidationUtils.rejectIfFalse(checkReason,resolveMessage("evaluation.Reason.NotEqual"), errors);

			boolean checkCredit = true;
			if (evalut.getCreditAmount() == null || evalut.getCreditAmount().floatValue() <= 0) {
				checkCredit = false;
			}

			ValidationUtils.rejectIfFalse(checkCredit,resolveMessage("evaluation.CreditAmount.NotEqual"), errors);

			boolean checkAccountDate = true;
			if (evalut.getAccountDate() == null) {
				checkAccountDate = false;
			}

			ValidationUtils.rejectIfFalse(checkAccountDate,resolveMessage("evaluation.AccountDate.NotEqual"), errors);

			boolean checkRegDate = true;
			if (evalut.getRegularizationDate() != null) {
				checkRegDate = false;
			}

			ValidationUtils.rejectIfFalse(checkRegDate,resolveMessage("evaluation.RegularizationDate.NotEqual"), errors);

			boolean checkStatus = true;
			if (evalut.getStatus().getId().longValue() == settings.getUnLossState().getId().longValue()) {
				checkStatus = false;
			}
			ValidationUtils.rejectIfFalse(checkStatus,resolveMessage("evaluation.Status.Null"), errors);
		}
	}

	private void validateSetLossState(ValidationError errors, Long id, EvaluationStatusDTO evstatus) {

		EvaluationDTO evalut = service.getEvaluation(id);
		
		if (!ValidationUtils.rejectIfNull(evalut, resolveMessage("evaluation.id.notExist"), errors))  {
			

			ValidationUtils.rejectIfTrue(evalut.isDeleted(), resolveMessage("evaluation.deleteFlag.deleted"), errors);

			boolean checkDeleteDate = true;
			if (evalut.getDeleteDate() != null) {
				checkDeleteDate = false;
			}
			ValidationUtils.rejectIfFalse(checkDeleteDate,resolveMessage("evaluation.deleteDate.deleted"), errors);

	
			
			TipologStatoRilievo status = evaluationStatusDao.findById(evstatus.getId(), TipologStatoRilievo.class);

			ValidationUtils.rejectIfNull(status, "idStatus", resolveMessage("evaluation.idStatus.notExits"), errors);

			boolean checkReason = true;
			if (evalut.getReason().getId().longValue() != settings.getRcReason().getId().longValue()) {
				checkReason = false;
			}

			ValidationUtils.rejectIfFalse(checkReason,resolveMessage("evaluation.Reason.NotEqual"), errors);

			boolean checkCredit = true;
			if (evalut.getCreditAmount() == null || evalut.getCreditAmount().floatValue() <= 0) {
				checkCredit = false;
			}

			ValidationUtils.rejectIfFalse(checkCredit,resolveMessage("evaluation.CreditAmount.NotEqual"), errors);

			boolean checkAccountDate = true;
			if (evalut.getAccountDate() == null) {
				checkAccountDate = false;
			}

			ValidationUtils.rejectIfFalse(checkAccountDate,resolveMessage("evaluation.AccountDate.NotEqual"), errors);

			boolean checkRegDate = true;
			if (evalut.getRegularizationDate() != null) {
				checkRegDate = false;
			}

			ValidationUtils.rejectIfFalse(checkRegDate,resolveMessage("evaluation.RegularizationDate.NotEqual"), errors);

			boolean checkStatus = true;
			if ( evalut.getStatus().getId().longValue() != settings.getUnLossState().getId().longValue() ) {
				checkStatus = false;
			}
			ValidationUtils.rejectIfFalse(checkStatus,resolveMessage("evaluation.Status.NotEqual"), errors);
		}
	}

	private void validateDelete(ValidationError errors, Long id) {
		
		EvaluationDTO evalut = service.getEvaluation(id);		
		
		if (!ValidationUtils.rejectIfNull(evalut, resolveMessage("evaluation.id.notExist"), errors))  {			
			if (!ValidationUtils.rejectIfFalse(evalut.getIdChild() == null, resolveMessage("evaluation.parent.notdelete"), errors)) {
				boolean checkDate = true;
				if (evalut.getAccountDate() != null) {
					checkDate = false;
				}
				ValidationUtils.rejectIfFalse(checkDate,resolveMessage("evaluation.accountDate.NotNull"), errors);				
			}					
		}
	}

	
	private void validateReclassificationDaco(ValidationError errors, EvaluationDTO evalut) {
		validateInsert(errors, evalut, null);		
		if(!ValidationUtils.rejectIfNull(evalut.getGenerated(), resolveMessage("reclassification.generated.NotNull"), errors)) {
			validateInsert(errors, evalut.getGenerated(), "generated");
		}
		ValidationUtils.rejectIfFalse(Utils.isDebitDaco(evalut.getDaco()) == Utils.isDebitDaco(evalut.getGenerated().getDaco()), resolveMessage("reclassification.daco.different"),errors);	
	}
	
	
	private String addPrefix(String field, String prefix ) {
		return prefix != null ? prefix + "." + field : field;
	}
	
	private void validateInsert(ValidationError errors, EvaluationDTO evalut, String fieldPrefix) {

		LocalDate now = service.getSysDate().toLocalDate();
		
		// Data Inserimento
		if (!ValidationUtils.rejectIfNull(evalut.getInsertDate(), addPrefix("insertDate",fieldPrefix),
				resolveMessage("evaluation.insertDate.empty"), errors)) {
			boolean chekInsertDate = now.isEqual(evalut.getInsertDate().toLocalDate());
			ValidationUtils.rejectIfFalse(chekInsertDate, "insertDate", resolveMessage("evaluation.insertDate.error"),
					errors);
		}

		ValidationUtils.rejectIfDateIsAfterOrEqual(evalut.getCompetenceDate(), now, addPrefix("competenceDate",fieldPrefix),
				resolveMessage("evaluation.competenceDate.afterOrEqual"), errors);
		// Data Compentenza
		ValidationUtils.rejectIfNull(evalut.getCompetenceDate(), addPrefix("competenceDate",fieldPrefix),
				resolveMessage("evaluation.competenceDate.empty"), errors);


		// Importi
		if ((evalut.getCreditAmount() != null && evalut.getDebitAmount() != null)
				|| (evalut.getCreditAmount() == null && evalut.getDebitAmount() == null)) {
			errors.addFieldError(addPrefix("creditAmount",fieldPrefix), resolveMessage("evaluation.amount.alternative"));
		}
		// Importo Credito
		if (evalut.getCreditAmount() != null) {
			ValidationUtils.rejectIfDoubleIsLess(evalut.getCreditAmount(), new Double(0), addPrefix("creditAmount",fieldPrefix), resolveMessage("evaluation.amount.credit.negative"), errors);
		}

		// Importo a debito
		if (evalut.getDebitAmount() != null) {
			ValidationUtils.rejectIfDoubleIsLess(evalut.getDebitAmount(), new Double(0), addPrefix("debitAmount",fieldPrefix), resolveMessage("evaluation.amount.debit.negative"), errors);
		}
		//
		if (!ValidationUtils.rejectIfNull(evalut.getBranch(), addPrefix("branch",fieldPrefix), resolveMessage("evaluation.branch.empty"),
				errors)) {
			if (!ValidationUtils.rejectIfNull(evalut.getBranch().getId(), addPrefix("branch",fieldPrefix), resolveMessage("evaluation.branch.empty"), errors)) {
				TipologFiliali filiale = evaluationDao.findById(evalut.getBranch().getId(), TipologFiliali.class);
				ValidationUtils.rejectIfNull(filiale, addPrefix("branch",fieldPrefix), resolveMessage("evaluation.branch.notExits"), errors);
			}
		}

		if (!ValidationUtils.rejectIfNull(evalut.getOffice(), addPrefix("office",fieldPrefix), resolveMessage("evaluation.office.empty"), errors)) {
			if (!ValidationUtils.rejectIfNull(evalut.getOffice().getId(), addPrefix("office",fieldPrefix), resolveMessage("evaluation.office.empty"), errors)) {
				TipologUffici ufficio = evaluationDao.findById(evalut.getOffice().getId(), TipologUffici.class);
				ValidationUtils.rejectIfNull(ufficio, addPrefix("office",fieldPrefix), resolveMessage("evaluation.office.notExits"), errors);
			}
		}

		TipologDaco daco = null;
		if (!ValidationUtils.rejectIfNull(evalut.getDaco(), addPrefix("daco",fieldPrefix), resolveMessage("evaluation.daco.empty"), errors)) {
			if (!ValidationUtils.rejectIfNull(evalut.getDaco().getId(), addPrefix("daco",fieldPrefix), resolveMessage("evaluation.daco.empty"), errors)) {
				daco = evaluationDao.findById(evalut.getDaco().getId(), TipologDaco.class);
				ValidationUtils.rejectIfNull(daco, addPrefix("daco",fieldPrefix), resolveMessage("evaluation.daco.notExits"), errors);
			}
		}

		if (!ValidationUtils.rejectIfNull(evalut.getReason(), addPrefix("reason",fieldPrefix), resolveMessage("evaluation.reason.empty"),
				errors)) {
			if (!ValidationUtils.rejectIfNull(evalut.getReason().getId(), addPrefix("reason",fieldPrefix), resolveMessage("evaluation.reason.empty"), errors)) {
				TipologCausale causale = evaluationDao.findById(evalut.getReason().getId(), TipologCausale.class);
				ValidationUtils.rejectIfNull(causale, addPrefix("reason",fieldPrefix), resolveMessage("evaluation.reason.notExits"), errors);
			}
		}

		if (evalut.getPreviousReason() != null) {
			TipologCausale causalePrev = evaluationDao.findById(evalut.getPreviousReason().getId(), TipologCausale.class);
			ValidationUtils.rejectIfNull(causalePrev, addPrefix("reason",fieldPrefix), resolveMessage("evaluation.reason.notExits"), errors);
		}

		AnagProfili profile = null;
		if (!ValidationUtils.rejectIfNull(evalut.getInsertUser(), resolveMessage("evaluation.inserUser.empty"), errors)) {
			if (!ValidationUtils.rejectIfNull(evalut.getInsertUser().getProfile(), resolveMessage("evaluation.profile.empty"), errors)) {
				if (!ValidationUtils.rejectIfNull(evalut.getInsertUser().getProfile().getId(), resolveMessage("evaluation.profile.empty"), errors)) {
					profile = evaluationDao.findById(evalut.getInsertUser().getProfile().getId(), AnagProfili.class);
					ValidationUtils.rejectIfNull(profile, addPrefix("profile",fieldPrefix), resolveMessage("evaluation.profile.notExits"),errors);
				}
			}
		}

		TipologServizi servizio = null;
		if (evalut.getService() != null) {
			servizio = evaluationDao.findById(evalut.getService().getId(), TipologServizi.class);
			ValidationUtils.rejectIfNull(servizio, addPrefix("service",fieldPrefix), resolveMessage("evaluation.service.notExits"), errors);
		}

		if (daco != null) {
			if (servizio != null) {
				// Check servizio/daco
				boolean checkServiceDaco = dacoServicesDao.exitsDacoServiceAssociation(daco.getIdVoceDaco(), servizio.getIdServizio());
				ValidationUtils.rejectIfFalse(checkServiceDaco, addPrefix("service",fieldPrefix), resolveMessage("evaluation.service.daco.notAssociated"), errors);
			}

			if (profile != null) {
				// Check Daco Associato al profile e funzione
				boolean checkProfileDaco = profileDao.checkProfileByCode(profile.getIdProfilo(), evalut.getDaco().getId());
				ValidationUtils.rejectIfFalse(checkProfileDaco, resolveMessage("evaluation.profile.daco.notAssociated"), errors);
			}
		}

	}

	private void validateChangeFractional(ValidationError errors, OfficeDTO office, Long idEvaluation) {

		StorageRilievi rilievo = evaluationDao.findById(idEvaluation, StorageRilievi.class);
		ValidationUtils.rejectIfNull(rilievo, "evaluation", resolveMessage("evaluation.id.notExist"), errors);
		if (rilievo != null) {

			boolean checkFlagAnnullato = true;
			if (rilievo.getFlagAnnullato().equals(StorageRilievi.FLAG_ANNULLATO)) {
				checkFlagAnnullato = false;
				ValidationUtils.rejectIfFalse(checkFlagAnnullato, "Flag",
						resolveMessage("evaluation.deleteFlag.deleted"), errors);
			}

			if (rilievo.getFlagAnnullato().equals(StorageRilievi.FLAG_NON_ANNULLATO)) {

				Long rcReason = settings.getRcReason().getId();

				boolean checkReason = true;
				if (rilievo.getTipologCausale1().getIdCausale() != rcReason) {
					checkReason = false;
					ValidationUtils.rejectIfFalse(checkReason, "Reason", resolveMessage("evaluation.Reason.NotRC"),
							errors);
				}

				if (rilievo.getDataCoge() == null && rilievo.getTipologCausale1().getIdCausale() == rcReason) {

					// Verifico che l'ufficio inserito esiste
					TipologUffici checkOffice = officeDao.findById(office.getId(), TipologUffici.class);
					ValidationUtils.rejectIfNull(checkOffice, "office", resolveMessage("office.notExist"), errors);

					if (checkOffice != null) {

						// Verifico che l'ufficio esiste nella filiale
						StorageRilievi evaluation = evaluationDao.findById(idEvaluation, StorageRilievi.class);
						boolean checkOfficeInBranch = true;
						if (checkOffice.getTipologFiliali().getIdFiliale() != evaluation.getTipologFiliali()
								.getIdFiliale())
							checkOfficeInBranch = false;
						ValidationUtils.rejectIfFalse(checkOfficeInBranch, "office",
								resolveMessage("office.branch.notExist"), errors);
					}

				}

			}
		}
	}

	private void validateSetRCReason(ValidationError errors, Long idEvaluation) {

		StorageRilievi rilievo = evaluationDao.findById(idEvaluation, StorageRilievi.class);
		ValidationUtils.rejectIfNull(rilievo, "evaluation", resolveMessage("evaluation.id.notExist"), errors);

		if (rilievo != null) {
			boolean checkDeleteDate = true;
			if (rilievo.getDataAnnullamento() != null) {
				checkDeleteDate = false;
				logger.info("Validazione data annullamento fallita");
				
			}
			ValidationUtils.rejectIfFalse(checkDeleteDate, "Delete date",resolveMessage("evaluation.deleteDate.deleted"), errors);
            
			boolean checkFlagAnnullato = true;

			if (rilievo.getFlagAnnullato().equals(StorageRilievi.FLAG_ANNULLATO)) {
				checkFlagAnnullato = false;
				logger.info("Validazione flag annullamento fallita");
		
			}
			ValidationUtils.rejectIfFalse(checkFlagAnnullato, "Flag", resolveMessage("evaluation.deleteFlag.deleted"),
					errors);
			

			boolean checkDataCoge = true;
			if (rilievo.getDataCoge() != null) {
				checkDataCoge = false;
				logger.info("Validazione data contabile fallita");
			
			}
			ValidationUtils.rejectIfFalse(checkDataCoge, "Data coge", resolveMessage("evaluation.dataCoge.isNotNull"),
					errors);
	
		
			EvaluationDTO evalut = service.getEvaluation(idEvaluation);
			boolean checkReason = false;
			if (evalut.getReason().getId().longValue() == settings.getRsReason().getId().longValue() || evalut.getReason().getId().longValue() == settings.getRdReason().getId().longValue()) {
				checkReason = true;
				//logger.info("Validazione reason completata con successo");
			}
			ValidationUtils.rejectIfFalse(checkReason, "Reason", resolveMessage("evaluation.setReason.notEqual"),errors);
			

			
		}
	}

	private void validateSetDescription(ValidationError errors, Long id) {
			
		EvaluationDTO evalut = service.getEvaluation(id);
		if (!ValidationUtils.rejectIfNull(evalut, resolveMessage("evaluation.id.notExist"), errors))  {
				
			ValidationUtils.rejectIfTrue(evalut.isDeleted(), resolveMessage("evaluation.deleteFlag.deleted"), errors);
	
			boolean checkReason = true;
			if (evalut.getReason().getId().longValue() != settings.getRcReason().getId().longValue()) {
				checkReason = false;
			}	
			ValidationUtils.rejectIfFalse(checkReason, resolveMessage("evaluation.Reason.NotEqual"), errors);
	
			boolean checkCredit = true;
			if (evalut.getCreditAmount() == null || evalut.getCreditAmount().floatValue() <= 0) {
				checkCredit = false;
			}	
			ValidationUtils.rejectIfFalse(checkCredit, resolveMessage("evaluation.CreditAmount.NotEqual"), errors);
	
			boolean checkAccountDate = true;
			if (evalut.getAccountDate() == null) {
				checkAccountDate = false;
			}	
			ValidationUtils.rejectIfFalse(checkAccountDate, resolveMessage("evaluation.AccountDate.NotEqual"), errors);
	
			boolean checkRegDate = true;
			if (evalut.getRegularizationDate() != null) {
				checkRegDate = false;
			}	
			ValidationUtils.rejectIfFalse(checkRegDate, resolveMessage("evaluation.RegularizationDate.NotEqual"), errors);
	
		}
	}
}
