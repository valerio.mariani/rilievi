package it.poste.rilievi.api.service.impl.base;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.repository.impl.FunctionDaoImpl;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.AOPUtils;

@Component
@Aspect
public class CheckAuthorizationsAspect implements Ordered {
	
	@Autowired
	FunctionDaoImpl functionDaoImpl;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckAuthorizationsAspect.class);
	
	@Around("@annotation(it.poste.rilievi.api.service.impl.base.CheckAuthorizations)")
	public Object checkAuthorizationsAspect(ProceedingJoinPoint joinPoint) throws Throwable {
		
		CheckAuthorizations checkAuthorizationsAnnotation = AOPUtils.getAnnotation(joinPoint, CheckAuthorizations.class);
		ServicesCatalog[] arrayAutorizzazioni = checkAuthorizationsAnnotation.services();
		List<ServicesCatalog> listaAutorizzazioni = Arrays.asList(arrayAutorizzazioni);
		List<TipologFunzioni> listaFunzioni = functionDaoImpl .getFunctionByProfile(ContextStoreHolder.getContextStore().getAppUser().getProfile().getId());

		if (listaFunzioni == null || (listaFunzioni != null && CollectionUtils.intersection(listaAutorizzazioni, listaFunzioni).isEmpty())) {
			logger.info("USER not Authorized.");
			throw new ServiceException(ServiceException.NotAuthorized);
		}

		Object ret = joinPoint.proceed();

		return ret;
	}

	@Override
	public int getOrder() {
		return 7;
	}

}
