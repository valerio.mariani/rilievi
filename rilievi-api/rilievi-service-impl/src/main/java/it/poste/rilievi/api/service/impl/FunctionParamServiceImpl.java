package it.poste.rilievi.api.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.model.entity.TipologParamFunzioni;
import it.poste.rilievi.api.repository.IFunctionParamDao;
import it.poste.rilievi.api.service.api.IFunctionParamService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.base.Valid;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class FunctionParamServiceImpl extends ServiceBase implements IFunctionParamService {

	private static Logger logger = LoggerFactory.getLogger(FunctionParamServiceImpl.class);

	@Autowired
	IFunctionParamDao functionParamDao;

	@Transactional
	@Auditable(operation = ServicesCatalog.NEW_FUNCTION_PARAM,
			trace = "Inserimento Parametro Funzione Id=${(response.id)!'?'}, Descrizione Parametro=${(functionParam.description)!'?'}, per la Funzione Id=${(functionParam.function.id)!'?'}")
	@Valid(value = FunctionParamValidator.class, operation = ServicesCatalog.NEW_FUNCTION_PARAM)
	public FunctionParamDTO insertFunctionParam(FunctionParamDTO functionParam) {
		try {
			logger.info("insert new param: " + functionParam.toString());
			functionParam.setDateCreation(this.getSysDate());

			TipologParamFunzioni functionParamEnt = DTOConverter.toEntity(functionParam);

			TipologFunzioni function = functionParamDao.findById(functionParam.getFunction().getId(),
					TipologFunzioni.class);
			functionParamEnt.setTipologFunzioni(function);

			functionParamDao.insertFunctionParam(functionParamEnt);

			return DTOConverter.toDto(functionParamEnt);
		} catch (ServiceException e) {
			logger.error("Error insertFunctionParam ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error insertFunctionParam Generic Exception...", e);
			throw new ServiceException(e);
		}

	}


	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_FUNCTION_PARAM,
			trace = "Recupero Parametri per la Funzione Id=${(idFunction)!'?'}, Numero di Righe=${(response.totalRows)!'?'}")
	public PaginableResultSearchImpl<FunctionParamDTO> getAllFunctionById(FunctionParamCriteriaDTO functionParam,
			Long idFunction) {
		try {
			int totalRows = functionParamDao.countFunctionParam(functionParam, idFunction);
			List<TipologParamFunzioni> functionItems = functionParamDao.getAllFunctionParamById(functionParam,idFunction);
			PaginableResultSearchImpl<FunctionParamDTO> ret = new PaginableResultSearchImpl<FunctionParamDTO>(functionParam);
			functionItems.stream().forEach(functionEnt -> {
				ret.addItem(DTOConverter.toDto(functionEnt));
			});

			ret.setTotalRows(totalRows);
			ret.setPageNumber(functionParam.getPageNumber());
			ret.setPageSize(functionParam.getPageSize());
			logger.info("found {} params, returned page {} with page size {} ", totalRows, functionParam.getPageNumber(), functionParam.getPageSize() );
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFunctionParamItems ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFunctionParamItems Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Transactional
	@Auditable(operation = ServicesCatalog.UPD_FUNCTION_PARAM,
			trace = "Aggiornamento Parametri Funzione Id=${(function.id)!'?'}")
	@Valid(value = FunctionParamValidator.class, operation = ServicesCatalog.UPD_FUNCTION_PARAM)
	public FunctionParamDTO updateFunctionParam(FunctionParamDTO function) {
		try {
			logger.info("update param:" + function.toString());
			TipologParamFunzioni functionEnt = functionParamDao.getFunctionParamById(function.getId());
			functionEnt.setValore(function.getValue());
			functionParamDao.updateFunctionParam(functionEnt);
			return DTOConverter.toDto(functionEnt);
		} catch (ServiceException e) {
			logger.error("Error updateFunctionParam ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error updateFunctionParam Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_FUNCTION_PARAM,
			trace = "Recupero Parametro con Id=${(idParam)!'?'}")
	public FunctionParamDTO getFunctionParam(Long idParam) {

		try {
			TipologParamFunzioni functionEnt = functionParamDao.getFunctionParamById(idParam);
			FunctionParamDTO ret = DTOConverter.toDto(functionEnt);
			logger.info("found and return param:" + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFunctionParam ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFunctionParam Generic Exception...", e);
			throw new ServiceException(e);
		}
	}
}
