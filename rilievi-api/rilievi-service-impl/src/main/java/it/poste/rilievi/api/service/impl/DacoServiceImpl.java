package it.poste.rilievi.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.DacoCriteriaDTO;
import it.poste.rilievi.api.model.dto.DacoDTO;
import it.poste.rilievi.api.model.dto.FunctionEnum;
import it.poste.rilievi.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.model.dto.ParamEnum;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.StagingDacoEscludereMaxiRilievi;
import it.poste.rilievi.api.model.entity.TipologDaco;
import it.poste.rilievi.api.model.utils.Utils;
import it.poste.rilievi.api.repository.IDacoDao;
import it.poste.rilievi.api.service.api.IDacoService;
import it.poste.rilievi.api.service.api.IDacoServiceService;
import it.poste.rilievi.api.service.api.IFunctionParamService;
import it.poste.rilievi.api.service.api.ISettingsService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.base.Valid;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class DacoServiceImpl extends ServiceBase implements IDacoService{
	
	private static Logger logger = LoggerFactory.getLogger(DacoServiceImpl.class);
	
	@Autowired
	IDacoDao dacoDao;
	
	@Autowired
	IDacoServiceService dacoServiceService;

	@Autowired
	IFunctionParamService paramService;
	
	@Autowired
	ISettingsService settings;
	
	@Transactional
	@Auditable(operation = ServicesCatalog.NEW_DACO,
			trace = "Inserita nuova voce daco con Id=${(response.id)!'?'}")
	@Valid(value = DacoValidator.class, operation = ServicesCatalog.NEW_DACO)
	public DacoDTO insertDaco( DacoDTO daco) {
		try {
			daco.setStartDate(getSysDate().toLocalDate());
			TipologDaco dacoEnt = DTOConverter.toEntity(daco);
			dacoDao.insertDaco(dacoEnt);
			DacoDTO ret =  DTOConverter.toDto(dacoEnt);
			logger.info("inserted daco item: " + ret.toString());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error insertDaco ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error insertDaco Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Transactional
	@Auditable(operation = ServicesCatalog.UPD_DACO,
			trace = "Aggiornata voce daco con Id=${(daco.id)!'?'}")
	@Valid(value = DacoValidator.class, operation = ServicesCatalog.UPD_DACO)
	public DacoDTO updateDaco(DacoDTO daco) {
		try {
			TipologDaco dacoEnt = dacoDao.getDacoById(daco.getId());
			dacoEnt.setDescrizione(daco.getDescription());
			dacoEnt.setDataFineValidita(Utils.setEndOfDay(Utils.localDateToDate(daco.getEndDate())));
			dacoDao.updateDaco(dacoEnt);
			DacoDTO ret =  DTOConverter.toDto(dacoEnt);
			logger.info("uodated daco item: "+ ret.toString());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error updateDaco ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error updateDaco Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista delle voci daco")
	public List<DacoDTO> getDacoItems() {
		try {
			List<TipologDaco> dacoItems = dacoDao.getAllValidDaco(Utils.localDateTimeToDate(getSysDate()));
			List<DacoDTO> ret  = new ArrayList<DacoDTO>();
			dacoItems.stream().forEach(dacoEnt -> {
				ret.add(DTOConverter.toDto(dacoEnt));
			});
			logger.info("found and returned:" + ret.size() + " daco items");
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getDacoItems ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getDacoItems Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista delle voci daco per profilo=${(profileId)!'?'} ")
	public List<DacoDTO> getDacoUserProfile(Long profileId) {
		try {
			List<TipologDaco> dacoItems = dacoDao.getDacoUserProfile(Utils.localDateTimeToDate(getSysDate()), profileId);
			List<DacoDTO> ret  = new ArrayList<DacoDTO>();
			dacoItems.stream().forEach(dacoEnt -> {
				DacoDTO daco = DTOConverter.toDto(dacoEnt);
				daco.getServices().addAll(dacoServiceService.getServices(daco.getId()));
				ret.add(daco);
			});
			logger.info("found and returned " + ret.size() + "daco items for profile " + profileId);
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getDacoProfile ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getDacoProfile Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
	
	
	@Transactional(readOnly = true)
	public DacoDTO getDaco(Long dacoId) {
		try {
			TipologDaco dacoEnt = dacoDao.getDacoById(dacoId);
			return DTOConverter.toDto(dacoEnt);
		}catch (ServiceException e) {
			logger.error("Error getDaco ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getDaco Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista delle voci daco per Regolarizzazione Maxi Rilievi")
	public List<DacoDTO> getDacoMaxiEvaluation() {
		try {
			FunctionParamDTO paramFlag =  settings.getAppSettings().getParam(FunctionEnum.REGO_MAXI_EVAL, ParamEnum.FLAG_SET_EXCLUDED_DACO);
			
			if(paramFlag == null) {
				throw new ServiceException("Funzione Maxi Regolarizazzione non configurata.");
			}
			
			paramFlag = this.paramService.getFunctionParam(paramFlag.getId());
			List<TipologDaco> dacoItems = null;
			if(StringUtils.defaultString(paramFlag.getValue()).equalsIgnoreCase("1")) {
				logger.debug("Daco da escludere valorizzati dall'ultima Regolarizzazione. Lettura da staging");
				dacoItems = dacoDao.getDacoMaxiEvaluationStaging(Utils.localDateTimeToDate(getSysDate()));
			}else {
				logger.debug("Daco da escludere non valorizzati dall'ultima Regolarizzazione. Lettura da storage");
				dacoItems = dacoDao.getDacoMaxiEvaluationStorage(Utils.localDateTimeToDate(getSysDate()));
			}
			
			List<DacoDTO> ret  = new ArrayList<DacoDTO>();
			dacoItems.stream().forEach(dacoEnt -> {
				DacoDTO daco = DTOConverter.toDto(dacoEnt);
				ret.add(daco);
			});
			logger.info("found and returned " + ret.size() + " daco items");
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getDacoUserProfileMaxiEvaluation ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getDacoUserProfileMaxiEvaluation Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
	
	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista delle voci daco da escludere per Regolarizzazione Maxi Rilievi")
	public List<DacoDTO> getExcludedDaco() {
		try {
			FunctionParamDTO paramFlag =  settings.getAppSettings().getParam(FunctionEnum.REGO_MAXI_EVAL, ParamEnum.FLAG_SET_EXCLUDED_DACO);
			
			if(paramFlag == null) {
				throw new ServiceException("Funzione Maxi Regolarizazzione non configurata.");
			}
			List<TipologDaco> dacoItems = null;
			paramFlag = this.paramService.getFunctionParam(paramFlag.getId());
			if(StringUtils.defaultString(paramFlag.getValue()).equalsIgnoreCase("1")) {
				 dacoItems = dacoDao.getExcludedDacoStaging(Utils.localDateTimeToDate(getSysDate()));		
			}else {
				 dacoItems = dacoDao.getExcludedDacoStorage(Utils.localDateTimeToDate(getSysDate()));		
			}
		
			List<DacoDTO> ret  = new ArrayList<DacoDTO>();
			dacoItems.stream().forEach(dacoEnt -> {
				DacoDTO daco = DTOConverter.toDto(dacoEnt);
				ret.add(daco);
			});
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getExcludedDaco ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getExcludedDaco Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

		
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista delle voci daco")
	public PaginableResultSearchImpl<DacoDTO> getDacos(DacoCriteriaDTO criteria) {
		try {
			PaginableResultSearchImpl<DacoDTO> result = new PaginableResultSearchImpl<DacoDTO>(criteria);

			Long totalRows = dacoDao.countAllDaco();
			List<TipologDaco> evalList = dacoDao.getAllDaco(criteria);
			evalList.stream().forEach(evaluationEnt -> {
				result.addItem(DTOConverter.toDto(evaluationEnt));
			});
			result.setTotalRows(totalRows.intValue());
			result.setPageNumber(criteria.getPageNumber());
			result.setPageSize(criteria.getPageSize());

			logger.info("found {} daco items, returned page {} with page size", totalRows, criteria.getPageNumber(), criteria.getPageSize());
			return result;
		} catch (ServiceException e) {
			logger.error("Error getDacos ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getDacos Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void removeExcludedDacoStaging() {
		try {
			dacoDao.removeExcludedDacoStaging();	
		}catch (ServiceException e) {
			logger.error("Error removeExcludedDacoStaging ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error removeExcludedDacoStaging Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void insertExcludedDacosStaging(List<DacoDTO> dacos) {
		try {
			
			dacos.forEach(daco ->{
				TipologDaco dacoEnt = dacoDao.findById(daco.getId(), TipologDaco.class);
				StagingDacoEscludereMaxiRilievi esclusione = new StagingDacoEscludereMaxiRilievi();
				esclusione.setTipologDaco(dacoEnt);
				dacoDao.insert(esclusione);				
			});
			
		}catch (ServiceException e) {
			logger.error("Error insertExcludedDacosStaging ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error insertExcludedDacosStaging Generic Exception...",e);
			throw new ServiceException(e);
		}
		
	}
}
