package it.poste.rilievi.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.AttachmentDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.repository.IAttachmentDao;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;

@Component
public class AttachmentValidator extends ValidatorBase implements IValidator {
	
	@Autowired
	IAttachmentDao attachmentDao;
	
	
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {
		 
		 switch (operation) {
			case INSERT_EVALUATION_ATTACHMENT:
				AttachmentDTO attachment = (AttachmentDTO)target[0];
				Long evaluationID = (Long) target[1];
				validateInsert(errors, attachment,evaluationID);
				break;
			default:
				break;
		}		 
	}


	private void validateInsert(ValidationError errors, AttachmentDTO attachment, Long evaluationID) {
		if(!ValidationUtils.rejectIfNull(attachment.getName(), "name", resolveMessage("attachment.name.null"), errors)) {
			boolean checkAttachment = ! attachmentDao.checkExist(attachment.getName(), evaluationID);
			ValidationUtils.rejectIfFalse(checkAttachment, "name", resolveMessage("attachment.evaluation.exist"), errors);
		}
	}
}
