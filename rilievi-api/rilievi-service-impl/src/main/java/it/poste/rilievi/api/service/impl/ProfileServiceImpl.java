package it.poste.rilievi.api.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.AppProfileCriteriaDTO;
import it.poste.rilievi.api.model.dto.AppProfileDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.AnagProfili;
import it.poste.rilievi.api.repository.IProfileDao;
import it.poste.rilievi.api.service.api.IProfileService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.base.Valid;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class ProfileServiceImpl extends ServiceBase implements IProfileService {

	private static Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

	@Autowired
	IProfileDao profileDao;

	@Override
	@Auditable(operation = ServicesCatalog.GET_ALL_PROFILE,
			trace = "Recupero Profili, numero di righe=${(response.totalRows)!'?'}, numero di pagina=${(response.pageNumber)!'?'}")
	public PaginableResultSearchImpl<AppProfileDTO> getProfileItems(AppProfileCriteriaDTO profileCriteria) {
		try {
			logger.info("Start getAllProfileItems. Criteria:"+profileCriteria.toString());
			int totalRows = profileDao.searchProfileCount();
			List<AnagProfili> profileItems = profileDao.getProfileItems(profileCriteria);
			PaginableResultSearchImpl<AppProfileDTO> ret  = new PaginableResultSearchImpl<AppProfileDTO>(profileCriteria);
			profileItems.stream().forEach(profileEnt -> {
				ret.addItem(DTOConverter.toDto(profileEnt));
			});
			ret.setTotalRows(totalRows);
			ret.setPageNumber(profileCriteria.getPageNumber());
			ret.setPageSize(profileCriteria.getPageSize());
			logger.info("found {} profiles, returned page {} with page size {}", totalRows, profileCriteria.getPageNumber(), profileCriteria.getPageSize());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getAllProfileItems ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getAllProfileItems Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public AppProfileDTO getProfileByCode(String code) {
		try {
			logger.info("get profile by code:" + code);
			AnagProfili profilo = profileDao.getProfileByCode(code);
			AppProfileDTO ret = profilo != null ? DTOConverter.toDto(profilo) : null;
			logger.info("found profile: " + ret.toString());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getProfileByCode ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getProfileByCode Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.NEW_PROFILE,
			trace = "Inserimento nuovo Profilo Id=${(response.id)!'?'}")
	@Valid(value = ProfileValidator.class, operation = ServicesCatalog.NEW_PROFILE)
	public AppProfileDTO insertProfileItem(AppProfileDTO profile) {
		try {
			profile.setCreationDate(this.getSysDate());
			AnagProfili profileEnt = DTOConverter.toEntity(profile);
			profileDao.insertProfile(profileEnt);
			AppProfileDTO ret = DTOConverter.toDto(profileEnt);
			logger.info("inserted profile item: "+ profile.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error insertProfileItem ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error insertProfileItem Generic Exception...",e);
			throw new ServiceException(e);
		}
		
	}
	
}
