package it.poste.rilievi.api.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.StorageNoteAggiuntive;
import it.poste.rilievi.api.model.entity.StorageRilievi;
import it.poste.rilievi.api.repository.IEvaluationNoteDao;
import it.poste.rilievi.api.service.api.IEvaluationNoteService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.base.Valid;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class EvaluationNoteServiceImpl extends ServiceBase implements IEvaluationNoteService {

	private static Logger logger = LoggerFactory.getLogger(FunctionParamServiceImpl.class);

	@Autowired
	IEvaluationNoteDao evaluationNoteDao;

	@Transactional
	@Auditable(operation = ServicesCatalog.NEW_EVALUATION_NOTE,
			trace = "Inserita nota Id=${(response.idnote)!'?'} per il rilievo Id=${(evaluationNote.evaluation.id)!'?'}")
	@Valid(value = EvaluationNoteValidator.class, operation = ServicesCatalog.NEW_EVALUATION_NOTE)
	public EvaluationNoteDTO insertEvaluationNote(EvaluationNoteDTO evaluationNote) {
		try {
			evaluationNote.setDateCreation(this.getSysDate());

			StorageNoteAggiuntive evaluationNoteEnt = DTOConverter.toEntity(evaluationNote);

			StorageRilievi storage = evaluationNoteDao.findById(evaluationNote.getEvaluation().getId(),
					StorageRilievi.class);
			evaluationNoteEnt.setStorageRilievi(storage);

			evaluationNoteDao.insertEvaluationNote(evaluationNoteEnt);

			EvaluationNoteDTO ret = DTOConverter.toDto(evaluationNoteEnt);
			logger.info("inserted a new note: " + ret.toString());
			return ret;

		} catch (ServiceException e) {
			logger.error("Error insertEvaluationNote ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error insertEvaluationNote Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Transactional
	@Auditable(operation = ServicesCatalog.DELETE_EVALUATION_NOTE,
			trace = "Cancellata nota con Id=${(noteId)!'?'}")
	@Valid(value = EvaluationNoteValidator.class, operation = ServicesCatalog.DELETE_EVALUATION_NOTE)
	public void deleteEvaluationNote(Long noteId) {

		try {
			StorageNoteAggiuntive evaluationNoteEnt = evaluationNoteDao.findById(noteId, StorageNoteAggiuntive.class);
			evaluationNoteDao.deleteEvaluationNote(evaluationNoteEnt.getIdNota());
			logger.info("deleted note with id:" + noteId);
		} catch (ServiceException e) {
			logger.error("Error deleteEvaluationNote ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error deleteEvaluationNote Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_EVALUATION_NOTE,
			trace = "Recuperate note per il rilievo con Id=${(evaluationId)!'?'}")
	public PaginableResultSearchImpl<EvaluationNoteDTO> getEvaluationNote(EvaluationNoteCriteriaDTO criteria,
			Long evaluationId) {

		try {
			logger.info("find note with criteria:"+ criteria.toString());
			PaginableResultSearchImpl<EvaluationNoteDTO> result = new PaginableResultSearchImpl<EvaluationNoteDTO>(
					criteria);
			int totalRows = evaluationNoteDao.countEvaluationNote(criteria);
			List<StorageNoteAggiuntive> evaluationNoteItems = evaluationNoteDao.getEvaluationNote(criteria);
			evaluationNoteItems.stream().forEach(evaluationNoteEnt -> {
				result.addItem(DTOConverter.toDto(evaluationNoteEnt));
			});
			result.setTotalRows(totalRows);
			result.setPageNumber(criteria.getPageNumber());
			result.setPageSize(criteria.getPageSize());
			logger.info("found and returned " + totalRows +" notes");
			return result;
		} catch (ServiceException e) {
			logger.error("Error getEvaluationNote ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getEvaluationNote Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

}
