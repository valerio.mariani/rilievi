package it.poste.rilievi.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.AppProfileDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.repository.IFunctionDao;
import it.poste.rilievi.api.repository.IProfileDao;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

@Component
public class ProfileValidator extends ValidatorBase implements IValidator{

	@Autowired
		IProfileDao profileDao;
	
	@Autowired
		IFunctionDao functionDao;
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException {
		
		switch (operation) {
			case NEW_PROFILE: 
				AppProfileDTO profile = (AppProfileDTO)obj[0];
				validateInsert(errors, profile);
				break;
			case GET_FUNCTION_BY_PROFILE: 
				Long idProfile = (Long) obj[0];
				validateFunctionProfile(errors,idProfile);
				break;
			case GET_OTHER_FUNCTION_BY_PROFILE: 
				Long idP = (Long) obj[0];
		  		validateFunctionProfile(errors,idP);
		  		break;
			case ADD_FUNCTION_PROFILE: 
				Long profileID = (Long) obj[0];
				Long functionID = (Long) obj[1];
				validateAddFunctionProfile(errors,profileID,functionID);
				break;				  	
			default: 
				break;
				
		}
		
	}

	private void validateInsert(ValidationError errors, AppProfileDTO profile) {
		
		//Nome nullo
		if(!ValidationUtils.rejectIfNull(profile.getName(), "name", resolveMessage("profile.name.null"), errors)) {
			//IAM nullo
			if(!ValidationUtils.rejectIfNull(profile.getCode(), "code", resolveMessage("profile.iam.null"), errors)) {
				//Descrizione nulla
				ValidationUtils.rejectIfNull(profile.getDescription(), "description", resolveMessage("profile.description.null"), errors);
			}
		}
		

		//Nome già esistente
		if (profile.getName() != null) {
		boolean checkInsertName = ! profileDao.checkExistName(profile.getName());
			if(!ValidationUtils.rejectIfFalse(checkInsertName, "name", resolveMessage("profile.name.exist"), errors)) {
				//IAM già esistente
				if(profile.getCode() != null) {
					boolean checkInsertIAM = ! profileDao.checkExistIAM(profile.getCode());
					ValidationUtils.rejectIfFalse(checkInsertIAM, "code", resolveMessage("profile.iam.exist"), errors);
				}
		}
		}
		
	}
	
	
	private void validateFunctionProfile(ValidationError errors, Long idProfile) {
		if(!ValidationUtils.rejectIfNull(idProfile, "id", resolveMessage("profile.id.null"), errors)) {
			boolean checkInsertId = profileDao.checkExistId(idProfile);
			ValidationUtils.rejectIfFalse(checkInsertId, "id", resolveMessage("profile.id.notFound"), errors);
		}	
	}
	
	private void validateAddFunctionProfile(ValidationError errors, Long profileID, Long functionID) {
		if(!ValidationUtils.rejectIfNull(profileID, "profileID", resolveMessage("profile.id.null"), errors)) {
			boolean checkInsertId = profileDao.checkExistId(profileID);
			if(!ValidationUtils.rejectIfFalse(checkInsertId, "profileID", resolveMessage("profile.id.notFound"), errors)) {
				
				if(!ValidationUtils.rejectIfNull(functionID, "functionID", resolveMessage("function.id.null"), errors)) {
					TipologFunzioni funct = functionDao.getFunctionById(functionID);
					if(!ValidationUtils.rejectIfNull(funct, "functionID", resolveMessage("function.id.notFound"), errors)) {
						//Controllo che la funzione non sia già abilitata al profilo
						boolean abFuncProfile = ! functionDao.checkFunctionProfileExist(profileID,functionID);
						ValidationUtils.rejectIfFalse(abFuncProfile, "function profile", resolveMessage("function.profile.alreadyExist"), errors);
					}
				}
			}
		}	
		
	}

}
