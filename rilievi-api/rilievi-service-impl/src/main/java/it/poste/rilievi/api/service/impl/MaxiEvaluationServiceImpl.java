package it.poste.rilievi.api.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.MaxiEvaluationDTO;
import it.poste.rilievi.api.model.entity.StorageRilieviMaxiTotale;
import it.poste.rilievi.api.repository.IMaxiEvaluationDao;
import it.poste.rilievi.api.service.api.IMaxiEvaluationService;
import it.poste.rilievi.api.service.api.ISettingsService;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class MaxiEvaluationServiceImpl extends ServiceBase implements IMaxiEvaluationService {

	private static Logger logger = LoggerFactory.getLogger(MaxiEvaluationServiceImpl.class);

	@Autowired
	IMaxiEvaluationDao evaluationDao;
	
	@Autowired
	ISettingsService setting;

	@Override
	public MaxiEvaluationDTO checkMaxiEvaluationRaised() {
		List<StorageRilieviMaxiTotale> maxTotEnt = evaluationDao.getMaxiEvaluationRaised();
		if(maxTotEnt.size() > 0) {
			logger.warn("Esiste una maxi regolarizzazione non terminata!");
			return DTOConverter.toDto(maxTotEnt.get(0));
		}
		logger.info("Nessuna maxi regolarizzazione non terminata!");
		return null;
	}
}
