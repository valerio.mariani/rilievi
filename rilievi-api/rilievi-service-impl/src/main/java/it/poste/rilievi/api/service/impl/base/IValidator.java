package it.poste.rilievi.api.service.impl.base;

import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

public interface IValidator {

	void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException;

}
