package it.poste.rilievi.api.service.impl.base;

import it.poste.rilievi.api.model.dto.AppUserDTO;

public class ContextStore {

	private AppUserDTO appUser;

	public AppUserDTO getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUserDTO appUser) {
		this.appUser = appUser;
	}

	public void clear() {
		this.appUser = null;
	}

}
