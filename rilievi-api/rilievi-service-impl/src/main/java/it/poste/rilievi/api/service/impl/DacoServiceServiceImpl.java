package it.poste.rilievi.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.DacoServiceDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologServizi;
import it.poste.rilievi.api.repository.IDacoServiceDao;
import it.poste.rilievi.api.service.api.IDacoServiceService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class DacoServiceServiceImpl extends ServiceBase implements IDacoServiceService{
	
	private static Logger logger = LoggerFactory.getLogger(DacoServiceServiceImpl.class);
	
	@Autowired
	IDacoServiceDao dacoServiceDao;
	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_DACO,
			trace = "Recuperata lista servizi associati alla voce daco con Id=${(dacoId)!'?'}")
	public List<DacoServiceDTO> getServices(Long dacoId) {
		try {
			List<DacoServiceDTO> ret  = new ArrayList<DacoServiceDTO>();
			List<TipologServizi> serviceList = dacoServiceDao.getServices(dacoId);
			serviceList.stream().forEach(serviceEnt -> {
				ret.add(DTOConverter.toDto(serviceEnt));
			});
			logger.info("found and returned " + ret.size()+" services for Daco id:" + dacoId);
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getServices ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getServices Generic Exception...",e);
			throw new ServiceException(e);
		}
	}


}
