package it.poste.rilievi.api.service.impl.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.poste.rilievi.api.service.impl.exception.ApplicationError;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

public class ValidationError {

	private List<ApplicationError> errors = new ArrayList<ApplicationError>();

	private HashMap<String, List<String>> fieldsError = new HashMap<String, List<String>>();

	public void addFieldError(String field, String message) {
		if (!fieldsError.containsKey(field)) {
			fieldsError.put(field, new ArrayList<String>());
		}
		fieldsError.get(field).add(message);
	}

	public List<String> getFieldErrors(String field) {
		if (!fieldsError.containsKey(field)) {
			fieldsError.put(field, new ArrayList<String>());
		}
		return fieldsError.get(field);
	}

	public void addApplicationError(String code, String message) {
		errors.add(new ApplicationError(code, message));
	}

	public List<ApplicationError> getErrors() {
		return errors;
	}

	public HashMap<String, List<String>> getFieldsError() {
		return fieldsError;
	}

	public boolean hasErrors() {
		return hasFieldsErrors() || hasApplicationErrors();
	}

	public boolean hasFieldsErrors() {
		return fieldsError.size() > 0;
	}

	public boolean hasApplicationErrors() {
		return errors.size() > 0;
	}

	public ServiceException toServiceException() {
		ServiceException servEx = new ServiceException();
		servEx.setFieldsErrors(getFieldsError());
		servEx.setErrors(getErrors());
		return servEx;
	}
}
