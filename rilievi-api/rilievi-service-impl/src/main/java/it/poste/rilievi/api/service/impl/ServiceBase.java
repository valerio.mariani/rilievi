package it.poste.rilievi.api.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import it.poste.rilievi.api.repository.impl.DaoBaseImpl;
import it.poste.rilievi.api.service.api.IService;

@Service
public abstract class ServiceBase implements IService {

	@Autowired
	@Qualifier("daoBase")
	DaoBaseImpl daoBase;

	@Autowired
	MessageSource messageSource;

	@Override
	public LocalDateTime getSysDate() {
		return daoBase.getSysDate();
	}

}
