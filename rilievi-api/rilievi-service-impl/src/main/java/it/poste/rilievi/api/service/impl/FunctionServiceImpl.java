package it.poste.rilievi.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.AnagProfili;
import it.poste.rilievi.api.model.entity.AnagProfiliAbilitazione;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.repository.IFunctionDao;
import it.poste.rilievi.api.service.api.IFunctionService;
import it.poste.rilievi.api.service.impl.audit.Auditable;
import it.poste.rilievi.api.service.impl.base.Valid;
import it.poste.rilievi.api.service.impl.exception.ServiceException;
import it.poste.rilievi.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class FunctionServiceImpl extends ServiceBase implements IFunctionService {

	private static Logger logger = LoggerFactory.getLogger(FunctionServiceImpl.class);

	@Autowired
	IFunctionDao functionDao;

	@Transactional
	@Auditable(operation = ServicesCatalog.NEW_FUNCTION,
			trace = "Inserimento Nuova Funzione Id=${(response.id)!'?'}")
	@Valid(value = FunctionValidator.class, operation = ServicesCatalog.NEW_FUNCTION)
	public FunctionDTO insertFunction(FunctionDTO function) {
		try {
			function.setDateCreation(this.getSysDate());
			TipologFunzioni functionEnt = DTOConverter.toEntity(function);
			functionDao.insertFunction(functionEnt);
			FunctionDTO ret = DTOConverter.toDto(functionEnt);
			logger.info("inserted new function:" + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error insertFunction ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error insertFunction Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Transactional
	@Auditable(operation = ServicesCatalog.UPD_FUNCTION,
			trace = "Aggiornamento Funzione Id=${(function.id)!'?'}")
	@Valid(value = FunctionValidator.class, operation = ServicesCatalog.UPD_FUNCTION)
	public FunctionDTO updateFunction(FunctionDTO function) {
		try {
			TipologFunzioni functionEnt = functionDao.getFunctionById(function.getId());
			functionEnt.setDescrFunzione(function.getDescription());
			functionDao.updateFunction(functionEnt);
			FunctionDTO ret = DTOConverter.toDto(functionEnt);
			logger.info("updated function" + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error updateFunction ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error updateFunction Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Transactional(readOnly = true)
	public FunctionDTO getFunction(Long functionId) {
		try {
			logger.info("get function with Id:" + functionId);
			TipologFunzioni functionEnt = functionDao.getFunctionById(functionId);
			FunctionDTO ret = DTOConverter.toDto(functionEnt);
			logger.info("founf function:" + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFunction ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFunction Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_FUNCTION,
			trace = "Recupero di Tutte le Funzioni, numero righe=${(response.pageNumber)!'?'}, numero di pagina=${(response.totalRows)!'?'}")
	public PaginableResultSearchImpl<FunctionDTO> getAllFunction(FunctionCriteriaDTO function) {

		try {
			int totalRows = functionDao.countFunction(function);
			List<TipologFunzioni> functionItems = functionDao.getAllFunction(function);
			PaginableResultSearchImpl<FunctionDTO> ret = new PaginableResultSearchImpl<FunctionDTO>(function);
			functionItems.stream().forEach(functionEnt -> {
				ret.addItem(DTOConverter.toDto(functionEnt));
			});			
			ret.setTotalRows(totalRows);
			ret.setPageNumber(function.getPageNumber());
			ret.setPageSize(function.getPageSize());
			logger.info("found {} functions, returned page {} with page size {}", totalRows, function.getPageNumber(), function.getPageSize());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFunctionItems ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFunctionItems Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	

	@Override
	@Auditable(operation = ServicesCatalog.GET_FUNCTION_BY_PROFILE,
			trace = "Recupero Funzioni per Profilo Id=${(idProfile)!'?'}")
	@Valid(value = ProfileValidator.class, operation = ServicesCatalog.GET_FUNCTION_BY_PROFILE)
	public List<FunctionDTO> getFunctionByProfile(Long idProfile) {
		try {
			logger.info("get functions for profile id:" + idProfile);
			List<TipologFunzioni> functionItems = functionDao.getFunctionByProfile(idProfile);
			List<FunctionDTO> ret = new ArrayList<FunctionDTO>();
			functionItems.stream().forEach(functionEnt -> {
				ret.add(DTOConverter.toDto(functionEnt));
			});
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFucntionByProfile ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFucntionByProfile Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.GET_OTHER_FUNCTION_BY_PROFILE,
			trace = "Recupero Altre Funzioni per Profilo Id=${(idProfile)!'?'}")
	@Valid(value = ProfileValidator.class, operation = ServicesCatalog.GET_OTHER_FUNCTION_BY_PROFILE)
	public List<FunctionDTO> getOtherFunctionByProfile(Long idProfile) {
		try {
			logger.info("get other functions for profile id:" + idProfile);
			List<TipologFunzioni> functionItems = functionDao.getOtherFunctionByProfile(idProfile);
			List<FunctionDTO> ret = new ArrayList<FunctionDTO>();
			functionItems.stream().forEach(functionEnt -> {
				ret.add(DTOConverter.toDto(functionEnt));
			});
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getOtherFucntionByProfile ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getOtherFucntionByProfile Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.ADD_FUNCTION_PROFILE,
			trace = "Inserimento Funzione Id=${(idFunction)!'?'} per il Profilo Id=${(idProfile)!'?'}")
	@Valid(value = ProfileValidator.class, operation = ServicesCatalog.ADD_FUNCTION_PROFILE)
	public FunctionDTO addFunctionProfile(Long idProfile, Long idFunction) {
		try {
			logger.info("add function  with profile id:"+idProfile + " - function Id:" + idFunction);
			AnagProfiliAbilitazione abProfile = new AnagProfiliAbilitazione();

			AnagProfili profile = functionDao.findById(idProfile, AnagProfili.class);
			abProfile.setAnagProfili(profile);

			TipologFunzioni function = functionDao.findById(idFunction, TipologFunzioni.class);
			abProfile.setTipologFunzioni(function);

			functionDao.insertFunctionProfile(abProfile);
			return DTOConverter.toDto(function);		
		} catch (ServiceException e) {
			logger.error("Error addFunctionProfile ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error addFunctionProfile Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	
	
	@Override
	@Auditable(operation = ServicesCatalog.GET_FUNCTION_BY_PROFILE_CODE, trace = "Recupero Funzioni per Profilo Codice=${(code)!'?'}")
	@Valid(value = ProfileValidator.class, operation = ServicesCatalog.GET_FUNCTION_BY_PROFILE_CODE)
	public List<FunctionDTO> getFunctionByProfileCode(String code) {
		try {
			logger.info("get function by profile code:" + code);
			List<TipologFunzioni> functionItems = functionDao.getFunctionByProfileCode(code);
			List<FunctionDTO> ret = new ArrayList<FunctionDTO>();
			functionItems.stream().forEach(functionEnt -> {
				ret.add(DTOConverter.toDto(functionEnt));
			});
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getFunctionByProfileCode ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getFunctionByProfileCode Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

}
