package it.poste.rilievi.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.DacoDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.service.api.IDacoService;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;

@Component
public class DacoValidator extends ValidatorBase  implements IValidator {

	@Autowired
	IDacoService service;
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {
		DacoDTO daco = (DacoDTO) target[0];

		switch (operation) {
			case NEW_DACO:
				validateInsert(errors, daco);
				break;
			case UPD_DACO:
				validateUpdate(errors, daco);
				break;
			default:
				break;
		}
	}
	
	private void validateInsert(ValidationError errors,  DacoDTO daco) {
		 ValidationUtils.rejectIfEmpty( daco.getCode(), "code", resolveMessage("daco.code.empty"), errors);
		 ValidationUtils.rejectIfEmpty(daco.getDescription(), "description", resolveMessage("daco.description.empty"), errors);
		 ValidationUtils.rejectIfEmpty(daco.getAccountNumber(), "accountNumber", resolveMessage("daco.accountNumber.empty"), errors);
	}

	private void validateUpdate(ValidationError errors,  DacoDTO daco) {
		 ValidationUtils.rejectIfNull(daco.getId(), "id", resolveMessage("daco.id.empty"), errors);
		 ValidationUtils.rejectIfEmpty(daco.getDescription(), "description", resolveMessage("daco.description.empty"), errors);
		 DacoDTO oldDaco = service.getDaco(daco.getId());
		 ValidationUtils.rejectIfDateIsBefore(daco.getEndDate(), oldDaco.getStartDate(), "endDate", resolveMessage("daco.endDate.min"), errors);
	}
}
