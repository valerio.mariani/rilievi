package it.poste.rilievi.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.repository.IFunctionDao;
import it.poste.rilievi.api.service.api.IFunctionService;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;

@Component
public class FunctionValidator extends ValidatorBase  implements IValidator {
	
	@Autowired
	IFunctionService service; 
	
	@Autowired
	IFunctionDao functionDao;
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {
		FunctionDTO function = (FunctionDTO) target[0];

		switch (operation) {
			case NEW_FUNCTION:
				validateInsert(errors, function);
				break;
			case UPD_FUNCTION:
				validateUpdate(errors, function);
				break;
			case GET_FUNCTION_BY_PROFILE_CODE:
				if (!ValidationUtils.rejectIfFalse(target != null && target.length > 0 && target[0] != null, "profileCode",
						resolveMessage("profile.code.empty"), errors)) {
	
				}
			default:
				break;
		}
	}
	

	private void validateInsert(ValidationError errors,  FunctionDTO function) {
		ValidationUtils.rejectIfEmpty( function.getDescription(), "description", resolveMessage("function.description.empty"), errors);
		ValidationUtils.rejectIfEmpty(function.getName(), "name", resolveMessage("function.name.empty"), errors);		
	} 

	private void validateUpdate(ValidationError errors,  FunctionDTO function) {
		ValidationUtils.rejectIfEmpty( function.getDescription(), "description", resolveMessage("function.description.empty"), errors);	 
		TipologFunzioni funzioni = functionDao.getFunctionById(function.getId());
		ValidationUtils.rejectIfNull(funzioni, "idFunction", resolveMessage("function.idFunction.notExits"), errors);
	} 

}
