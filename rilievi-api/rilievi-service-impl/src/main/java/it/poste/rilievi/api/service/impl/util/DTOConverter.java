package it.poste.rilievi.api.service.impl.util;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import it.poste.rilievi.api.model.dto.AppProfileDTO;
import it.poste.rilievi.api.model.dto.AppUserDTO;
import it.poste.rilievi.api.model.dto.AttachmentDTO;
import it.poste.rilievi.api.model.dto.AuditLogDTO;
import it.poste.rilievi.api.model.dto.BranchDTO;
import it.poste.rilievi.api.model.dto.DacoDTO;
import it.poste.rilievi.api.model.dto.DacoServiceDTO;
import it.poste.rilievi.api.model.dto.EvaluationDTO;
import it.poste.rilievi.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.api.model.dto.ExtractionDatesDTO;
import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.FunctionEnum;
import it.poste.rilievi.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.api.model.dto.MaxiEvaluationDTO;
import it.poste.rilievi.api.model.dto.MaxiEvaluationDetailDTO;
import it.poste.rilievi.api.model.dto.OfficeDTO;
//import it.poste.rilievi.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.api.model.dto.ReasonDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.AnagProfili;
import it.poste.rilievi.api.model.entity.StorageAllegati;
import it.poste.rilievi.api.model.entity.StorageDateEstrazione;
import it.poste.rilievi.api.model.entity.StorageLogAzioni;
import it.poste.rilievi.api.model.entity.StorageNoteAggiuntive;
//import it.poste.rilievi.api.model.entity.StorageProcessiSdp;
import it.poste.rilievi.api.model.entity.StorageRilievi;
import it.poste.rilievi.api.model.entity.StorageRilieviMaxiDettaglio;
import it.poste.rilievi.api.model.entity.StorageRilieviMaxiTotale;
import it.poste.rilievi.api.model.entity.TipologCausale;
import it.poste.rilievi.api.model.entity.TipologDaco;
import it.poste.rilievi.api.model.entity.TipologFiliali;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.model.entity.TipologParamFunzioni;
import it.poste.rilievi.api.model.entity.TipologServizi;
import it.poste.rilievi.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.api.model.entity.TipologUffici;
import it.poste.rilievi.api.model.utils.Utils;
import it.poste.rilievi.api.service.impl.exception.ServiceException;

public final class DTOConverter {



	public static TipologDaco toEntity(DacoDTO daco) {
		TipologDaco ret = new TipologDaco();
		ret.setCodiceDaco(daco.getCode());
		ret.setDescrizione(daco.getDescription());
		ret.setContoContabile(daco.getAccountNumber());
		ret.setContoContabileDesc(daco.getAccountDescription());
		ret.setDataFineValidita(Utils.localDateToDate(daco.getEndDate()));
		ret.setDataInizioValidita(Utils.localDateToDate(daco.getStartDate()));
		return ret;
	}

	public static DacoDTO toDto(TipologDaco daco) {
		DacoDTO ret = new DacoDTO();
		ret.setCode(daco.getCodiceDaco());
		ret.setDescription(daco.getDescrizione());
		ret.setAccountNumber(daco.getContoContabile());
		ret.setAccountDescription(daco.getContoContabileDesc());
		ret.setEndDate(Utils.dateToLocalDate(daco.getDataFineValidita()));
		ret.setStartDate(Utils.dateToLocalDate(daco.getDataInizioValidita()));
		ret.setId(daco.getIdVoceDaco());
		return ret;
	}

	public static StorageLogAzioni toEntity(AuditLogDTO auditDto) {
		StorageLogAzioni ret = new StorageLogAzioni();
		ret.setDataAzione(Utils.localDateTimeToDate(auditDto.getDateTime()));
		ret.setDescrAzione(auditDto.getDescrAction());
		ret.setEsitoAzione(auditDto.getResult());
		if (auditDto.getUser() != null) {
			ret.setUtente(auditDto.getUser().getCode());
		}
		return ret;
	}

	public static AuditLogDTO toDto(StorageLogAzioni azione) {
		AuditLogDTO ret = new AuditLogDTO();
		ret.setDateTime(Utils.dateToLocalDateTime(azione.getDataAzione()));
		if (azione.getIdAzione() != null) {
			ret.setId(azione.getIdAzione());
		}
		ret.setServiceCode(ServicesCatalog.findByCode(String.valueOf(azione.getTipologTipoAzioni().getIdTipoAzione())));
		ret.setResult(azione.getEsitoAzione());
		if (StringUtils.isNotEmpty(azione.getUtente())) {
			ret.setUser(new AppUserDTO());
			ret.getUser().setCode(azione.getUtente());
		}
		ret.setDescrAction(azione.getDescrAzione());
		ret.setServiceDescr(azione.getTipologTipoAzioni().getAzione());

		return ret;
	}

	public static BranchDTO toDto(TipologFiliali filiale) {
		BranchDTO ret = new BranchDTO();
		ret.setCode(filiale.getCodiceFiliale());
		ret.setDescription(filiale.getDescrFiliale());
		ret.setStartDate(Utils.dateToLocalDate(filiale.getDataInizioValidita()));
		ret.setEndDate(Utils.dateToLocalDate(filiale.getDataFineValidita()));
		ret.setId(filiale.getIdFiliale());
		return ret;
	}

	public static StorageDateEstrazione toEntity(ExtractionDatesDTO extractionDates) {

		StorageDateEstrazione ret = new StorageDateEstrazione();
		ret.setDataFissa(extractionDates.getFixedDate());
		ret.setDataChiusura(extractionDates.getClosingDate());
		return ret;

	}

	public static ExtractionDatesDTO toDto(StorageDateEstrazione extractionDates) {
		ExtractionDatesDTO ret = new ExtractionDatesDTO();
		ret.setClosingDate(extractionDates.getDataChiusura());
		ret.setFixedDate(extractionDates.getDataFissa());
		return ret;
	}

	public static TipologFunzioni toEntity(FunctionDTO function) {
		TipologFunzioni ret = new TipologFunzioni();
		ret.setDataCreazione(Utils.localDateTimeToDate(function.getDateCreation()));
		ret.setDescrFunzione(function.getDescription());
		ret.setNomeFunzione(function.getName());
		return ret;

	}

	public static FunctionDTO toDto(TipologFunzioni function) {
		FunctionDTO ret = new FunctionDTO();
		ret.setDateCreation(Utils.dateToLocalDateTime(function.getDataCreazione()));
		ret.setDescription(function.getDescrFunzione());
		ret.setId(function.getIdFunzione());
		ret.setName(function.getNomeFunzione());
		try {
			FunctionEnum func = FunctionEnum.valueOf(function.getNomeFunzione());
			ret.setExtendedName(func.getDescription());
		} catch (Exception e) {
			ret.setExtendedName(function.getNomeFunzione());
		}
		return ret;
	}

	public static TipologParamFunzioni toEntity(FunctionParamDTO functionParam) {
		TipologParamFunzioni ret = new TipologParamFunzioni();
		ret.setDataCreazione(Utils.localDateTimeToDate(functionParam.getDateCreation()));
		ret.setDescrParametro(functionParam.getDescription());
		ret.setParametro(functionParam.getNameparam());
		ret.setValore(functionParam.getValue());
		return ret;

	}

	public static FunctionParamDTO toDto(TipologParamFunzioni functionParam) {
		FunctionParamDTO ret = new FunctionParamDTO();
		ret.setDateCreation(Utils.dateToLocalDateTime(functionParam.getDataCreazione()));
		ret.setDescription(functionParam.getDescrParametro());
		ret.setNameparam(functionParam.getParametro());
		ret.setValue(functionParam.getValore());
		ret.setId(functionParam.getIdParametro());

		return ret;
	}

	public static OfficeDTO toDto(TipologUffici ufficio) {
		OfficeDTO ret = new OfficeDTO();
		ret.setEmail(ufficio.getEmail());
		ret.setFractional(ufficio.getFrazionario());
		ret.setFractionalDescription(ufficio.getDescrFrazionario());
		// ret.setBranch(toDto(ufficio.getTipologFiliali()));
		ret.setType(ufficio.getTipo());
		ret.setId(ufficio.getIdUfficio());
		return ret;
	}

	public static ReasonDTO toDto(TipologCausale causale) {
		ReasonDTO ret = new ReasonDTO();
		ret.setId(causale.getIdCausale());
		ret.setCode(causale.getCodiceCausale());
		ret.setDescription(causale.getDescrCausale());
		return ret;
	}

	public static DacoServiceDTO toDto(TipologServizi servizio) {
		DacoServiceDTO ret = new DacoServiceDTO();
		ret.setId(servizio.getIdServizio());
		ret.setCode(servizio.getCodiceServizio());
		ret.setDescription(servizio.getDescrizione());
		return ret;
	}

	public static AttachmentDTO toDto(StorageAllegati allegato) {
		return toDto(allegato, false);
	}

	@SuppressWarnings("deprecation")
	public static AttachmentDTO toDto(StorageAllegati allegato, boolean loadContent) {
		AttachmentDTO ret = new AttachmentDTO();
		ret.setActive(StorageAllegati.ACTIVE.equalsIgnoreCase(allegato.getAttivo()) ? true : false);
		ret.setName(allegato.getNomeAllegato());
		ret.setInsertDate(Utils.dateToLocalDateTime(allegato.getDataInserimento()));
		ret.setDeleteDate(Utils.dateToLocalDateTime(allegato.getDataRimozione()));
		if (StringUtils.isNotEmpty(allegato.getUtenteInserimento())) {
			ret.setInsertUser(new AppUserDTO());
			ret.getInsertUser().setCode(allegato.getUtenteInserimento());
		}
		ret.setId(allegato.getIdAllegati());
		if (loadContent) {
			try {
				ret.setContent(IOUtils.toString(allegato.getContenuto()));
			} catch (IOException e) {
				throw new ServiceException(e);
			}
		}
		return ret;

	}

	public static EvaluationDTO toDto(StorageRilievi rilievo) {
		EvaluationDTO ret = new EvaluationDTO();
		ret.setBranch(toDto(rilievo.getTipologFiliali()));
		ret.setCompetenceDate(Utils.dateToLocalDate(rilievo.getDataCompetenza()));
		ret.setRegularizationDate(Utils.dateToLocalDateTime(rilievo.getDataRegolarizzazione()));
		ret.setAccountDate(Utils.dateToLocalDate(rilievo.getDataCoge()));
		ret.setDeletedFlag(rilievo.getFlagAnnullato());
		ret.setDeleteDate(Utils.dateToLocalDateTime(rilievo.getDataAnnullamento()));
		ret.setSendedSrcDate(Utils.dateToLocalDate(rilievo.getDataInvioSrc()));
		ret.setRealignedFlag(rilievo.getFlagRiallineato());
		ret.setRealignedStatus(rilievo.getStatusRiallineato());
		ret.setRegularizationFlag(rilievo.getFlagRegolarizzato());


		ret.setRegularizationFromUser(rilievo.getRegolarizzatoDaUtente());

		ret.setIdChild(rilievo.getRifRilievoFiglio());
		ret.setIdParent(rilievo.getRifRilievoPadre());

		if (rilievo.getCreditiEuro() != null) {
			ret.setCreditAmount(rilievo.getCreditiEuro().doubleValue());
		}

		if (rilievo.getTipologDaco() != null) {
			ret.setDaco(toDto(rilievo.getTipologDaco()));
		}
		if (rilievo.getDebitiEuro() != null) {
			ret.setDebitAmount(rilievo.getDebitiEuro().doubleValue());
		}

		if (rilievo.getTipologDaco() != null) {
			ret.setDaco(toDto(rilievo.getTipologDaco()));
		}

		if (rilievo.getTipologFiliali() != null) {
			ret.setBranch(toDto(rilievo.getTipologFiliali()));
		}

		if (rilievo.getTipologUffici() != null) {
			ret.setOffice(toDto(rilievo.getTipologUffici()));
		}
		if (rilievo.getTipologCausale1() != null) {
			ret.setReason(toDto(rilievo.getTipologCausale1()));
		}
		if (rilievo.getTipologCausale2() != null) {
			ret.setPreviousReason(toDto(rilievo.getTipologCausale2()));
		}

		ret.setDescription(rilievo.getDescrizione());
		ret.setId(rilievo.getIdRilievi());
		ret.setInsertDate(Utils.dateToLocalDateTime(rilievo.getDataInserimento()));
		if (StringUtils.isNotEmpty(rilievo.getUtenteInserimento())) {
			ret.setInsertUser(new AppUserDTO());
			ret.getInsertUser().setCode(rilievo.getUtenteInserimento());
			ret.getInsertUser().setProfile(new AppProfileDTO());
		}

		if (rilievo.getStorageNoteAggiuntives() != null) {
			for (StorageNoteAggiuntive nota : rilievo.getStorageNoteAggiuntives()) {
				ret.getNotes().add(toDto(nota));
			}
		}

		ret.setNumber(rilievo.getNumRilievo());
		if (rilievo.getTipologServizi() != null) {
			ret.setService(toDto(rilievo.getTipologServizi()));
		}

		for (StorageAllegati allegato : rilievo.getStorageAllegatis()) {
			if(StorageAllegati.ACTIVE.equals(allegato.getAttivo()))
			ret.getAttachments().add(toDto(allegato));
		}

		if (rilievo.getTipologStatoRilievo() != null) {
			ret.setStatus(toDto(rilievo.getTipologStatoRilievo()));
		}

		if (rilievo.getUtenteInserimento() != null){
			ret.setUtenteInserimento(rilievo.getUtenteInserimento());
		}

		return ret;
	}

	public static MaxiEvaluationDetailDTO toDto(StorageRilieviMaxiDettaglio maxiRilievoDett) {
		MaxiEvaluationDetailDTO ret = new MaxiEvaluationDetailDTO();
		if (maxiRilievoDett.getImporto() != null) {
			ret.setAmount(maxiRilievoDett.getImporto().doubleValue());
		}

		ret.setCreationDate(Utils.dateToLocalDateTime(maxiRilievoDett.getDataLim()));
		ret.setMxrDate(Utils.dateToLocalDateTime(maxiRilievoDett.getDataMxr()));
		ret.setRegularizationDate(Utils.dateToLocalDateTime(maxiRilievoDett.getDataReg()));

		ret.setReason(toDto(maxiRilievoDett.getTipologCausale()));

		ret.setBranch(toDto(maxiRilievoDett.getTipologFiliali()));
		ret.setOffice(toDto(maxiRilievoDett.getTipologUffici()));

		ret.setEvaluation(toDto(maxiRilievoDett.getStorageRilievi()));

		return ret;
	}

	public static MaxiEvaluationDTO toDto(StorageRilieviMaxiTotale maxiTot) {
		MaxiEvaluationDTO ret = new MaxiEvaluationDTO();
		if (maxiTot.getImporto() != null) {
			ret.setAmount(maxiTot.getImporto().doubleValue());
		}
		ret.setReason(toDto(maxiTot.getTipologCausale()));
		ret.setCreationDate(Utils.dateToLocalDateTime(maxiTot.getDataLim()));

		if (maxiTot.getTotImporto() != null) {
			ret.setTotalAmount(maxiTot.getTotImporto().doubleValue());
		}
		ret.setMxrDate(Utils.dateToLocalDateTime(maxiTot.getDataMxr()));
		ret.setRegularizationDate(Utils.dateToLocalDateTime(maxiTot.getDataReg()));
		ret.setTotalEvaluation(maxiTot.getTotRilievi());

		return ret;
	}

	public static StorageRilievi toEntity(EvaluationDTO rilievo) {
		StorageRilievi ret = new StorageRilievi();

		if (rilievo.getCreditAmount() != null) {
			ret.setCreditiEuro(new BigDecimal(rilievo.getCreditAmount()));
		}

		if (rilievo.getDebitAmount() != null) {
			ret.setDebitiEuro(new BigDecimal(rilievo.getDebitAmount()));
		}
		ret.setDescrizione(rilievo.getDescription());
		ret.setDataInserimento(Utils.localDateTimeToDate(rilievo.getInsertDate()));

		if (rilievo.getCompetenceDate() != null) {
			ret.setDataCompetenza(Utils.localDateToDate(rilievo.getCompetenceDate()));
		}

		if (rilievo.getRegularizationDate() != null) {
			ret.setDataRegolarizzazione(Utils.localDateTimeToDate(rilievo.getRegularizationDate()));
		}

		if (rilievo.getInsertUser() != null) {
			ret.setUtenteInserimento(rilievo.getInsertUser().getCode());
		}

		ret.setNumRilievo(rilievo.getNumber());
		ret.setDataAnnullamento(Utils.localDateTimeToDate(rilievo.getDeleteDate()));
		ret.setFlagAnnullato(rilievo.getDeletedFlag());

		ret.setFlagRiallineato(rilievo.getRealignedFlag());
		ret.setStatusRiallineato(rilievo.getRealignedStatus());
		ret.setFlagRegolarizzato(rilievo.getRegularizationFlag());
		ret.setDataRegolarizzazione(Utils.localDateTimeToDate(rilievo.getRegularizationDate()));
		ret.setRegolarizzatoDaUtente(rilievo.getRegularizationFromUser());

		return ret;
	}

	public static AppProfileDTO toDto(AnagProfili anagProfili) {

		AppProfileDTO ret = new AppProfileDTO();
		ret.setId(anagProfili.getIdProfilo());
		ret.setCode(anagProfili.getCodiceIam());
		ret.setCreationDate(Utils.dateToLocalDateTime(anagProfili.getDataCreazione()));
		ret.setName(anagProfili.getNome());
		ret.setDescription(anagProfili.getDescrizione());
		return ret;
	}

	public static AnagProfili toEntity(AppProfileDTO profile) {
		AnagProfili ret = new AnagProfili();
		ret.setCodiceIam(profile.getCode());
		ret.setIdProfilo(profile.getId());
		ret.setDescrizione(profile.getDescription());
		ret.setNome(profile.getName());
		ret.setDataCreazione(Utils.localDateTimeToDate(profile.getCreationDate()));
		return ret;
	}

	public static EvaluationNoteDTO toDto(StorageNoteAggiuntive evaluationNote) {

		EvaluationNoteDTO ret = new EvaluationNoteDTO();
		ret.setIdnote(evaluationNote.getIdNota());
		ret.setDateCreation(Utils.dateToLocalDateTime(evaluationNote.getDataInserimento()));
		ret.setNote(evaluationNote.getNota());
		ret.setUser(evaluationNote.getUtente());

		return ret;
	}

	public static StorageNoteAggiuntive toEntity(EvaluationNoteDTO evaluationNote) {
		StorageNoteAggiuntive ret = new StorageNoteAggiuntive();
		ret.setDataInserimento(Utils.localDateTimeToDate(evaluationNote.getDateCreation()));
		ret.setNota(evaluationNote.getNote());
		ret.setUtente(evaluationNote.getUser());
		return ret;
	}

	public static StorageAllegati toEntity(AttachmentDTO allegato) {
		StorageAllegati ret = new StorageAllegati();
		if (!allegato.isActive()) {
			ret.setAttivo(StorageAllegati.NOT_ACTIVE);
		} else
			ret.setAttivo(StorageAllegati.ACTIVE);
		ret.setDataInserimento(Utils.localDateTimeToDate(allegato.getInsertDate()));
		ret.setNomeAllegato(allegato.getName());
		ret.setUtenteInserimento(allegato.getInsertUser().getCode());
		ret.setContenuto(allegato.getContent().getBytes());

		return ret;

	}

	public static EvaluationStatusDTO toDto(TipologStatoRilievo evaluationStatus) {
		EvaluationStatusDTO ret = new EvaluationStatusDTO();
		ret.setCode(evaluationStatus.getCodiceStatoRilievo());
		ret.setDescription(evaluationStatus.getDescrizione());
		ret.setId(evaluationStatus.getIdStatoRilievo());
		return ret;
	}

	public static TipologCausale toEntity(ReasonDTO causale) {
		TipologCausale ret = new TipologCausale();
		ret.setCodiceCausale(causale.getCode());
		ret.setDescrCausale(causale.getDescription());
		return ret;
	}

}
