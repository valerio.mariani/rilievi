package it.poste.rilievi.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.api.model.dto.ServicesCatalog;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.model.entity.TipologParamFunzioni;
import it.poste.rilievi.api.repository.IFunctionParamDao;
import it.poste.rilievi.api.service.api.IFunctionParamService;
import it.poste.rilievi.api.service.impl.base.IValidator;
import it.poste.rilievi.api.service.impl.base.ValidationError;
import it.poste.rilievi.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.api.service.impl.base.ValidatorBase;

@Component
public class FunctionParamValidator extends ValidatorBase implements IValidator {

	@Autowired
	IFunctionParamService service;

	@Autowired
	IFunctionParamDao functionParamDao;

	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {

		switch (operation) {
			case NEW_FUNCTION_PARAM:
				FunctionParamDTO function1 = (FunctionParamDTO) target[0];
				validateInsert(errors, function1);
				break;

			case UPD_FUNCTION_PARAM:
				FunctionParamDTO function2 = (FunctionParamDTO) target[0];
				validateUpdate(errors, function2);
				break;
			default:
				break;
		}
	}
	

	private void validateUpdate(ValidationError errors, FunctionParamDTO function) {
		
		ValidationUtils.rejectIfEmpty(function.getValue(), "value", resolveMessage("functionParam.value.empty"),
				errors);
		TipologParamFunzioni funzioniParam = functionParamDao.getFunctionParamById(function.getId());
		ValidationUtils.rejectIfNull(funzioniParam, "idParam", resolveMessage("functionParam.idParam.notExits"),
				errors);

	}

	private void validateInsert(ValidationError errors, FunctionParamDTO function) {

		if (!ValidationUtils.rejectIfNull(function.getFunction(), "idFunction",
				resolveMessage("functionParam.Function.empty"), errors)) {
			if (!ValidationUtils.rejectIfNull(function.getFunction().getId(), "idFunction",
					resolveMessage("functionParam.idFunction.empty"), errors)) {
				TipologFunzioni funzioni = functionParamDao.findById(function.getFunction().getId(),
						TipologFunzioni.class);
				ValidationUtils.rejectIfNull(funzioni, "idFunction",
						resolveMessage("functionParam.idFunction.notExits"), errors);
			}
		}

		ValidationUtils.rejectIfEmpty(function.getNameparam(), "nameParam",
				resolveMessage("functionParam.nameParam.empty"), errors);
		ValidationUtils.rejectIfEmpty(function.getDescription(), "description",
				resolveMessage("functionParam.description.empty"), errors);
		ValidationUtils.rejectIfEmpty(function.getValue(), "value", resolveMessage("functionParam.value.empty"),
				errors);

	}

}
