package it.poste.rilievi.api.service.impl.conf;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.springframework.stereotype.Component;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import it.poste.rilievi.api.service.impl.audit.Auditable;

@Component
public class TraceBuilder {

	private Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);

	@PostConstruct
	private void init() {
		Set<Method> methodsAnnotatedWith = new Reflections("it.poste.rilievi.api.service.impl", new MethodAnnotationsScanner()).getMethodsAnnotatedWith(Auditable.class);
		
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		cfg.setTemplateLoader(stringLoader);

		if (methodsAnnotatedWith != null) {
			for (Method method : methodsAnnotatedWith) {
				String template = method.getDeclaredAnnotation(Auditable.class).trace();
				stringLoader.putTemplate(getTemplateKey(method), template);
			}
		}
	}

	public String build(String templateKey, Map<String, Object> model) throws TemplateException, IOException {
		String trace = new String();

		Template template = cfg.getTemplate(templateKey);
		if (template != null) {
			StringWriter outputWriter = new StringWriter();
			template.process(model, outputWriter);
			trace = outputWriter.toString();
		}

		return trace;
	}
	
	private String getTemplateKey(Method method) {
		String className = method.getDeclaringClass().getName();
		String methodName = method.getName();
		return getTemplateKey(className, methodName);
	}
	
	public String getTemplateKey(String className, String methodName) {
		return className + "." + methodName;
	} 

}
