package it.poste.rilievi.api.service.impl.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.poste.rilievi.api.model.dto.ServicesCatalog;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Auditable {

	ServicesCatalog operation();

	String trace() default "";
}
