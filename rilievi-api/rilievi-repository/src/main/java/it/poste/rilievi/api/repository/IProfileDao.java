package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.dto.AppProfileCriteriaDTO;
import it.poste.rilievi.api.model.entity.AnagProfili;

public interface IProfileDao extends IDaoBase {

	List<AnagProfili> getProfileItems(AppProfileCriteriaDTO profileCriteria);

	AnagProfili getProfileByCode(String code);

	AnagProfili insertProfile(AnagProfili profile);

	boolean checkExistName(String name);

	boolean checkExistIAM(String code);

	boolean checkExistId(Long idProfile);

	Integer searchProfileCount();

	boolean checkProfileByCode(Long profileId, Long dacoId);

	Integer countProfile(AppProfileCriteriaDTO criteria);

}
