package it.poste.rilievi.api.repository.impl;


import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.api.model.entity.TipologParamFunzioni;
import it.poste.rilievi.api.repository.IFunctionParamDao;

@Repository
public class FunctionParamDaoImpl extends DaoBaseImpl implements IFunctionParamDao {

	@Override
	public TipologParamFunzioni insertFunctionParam(TipologParamFunzioni functionParam) {
		getEm().persist(functionParam);
		return functionParam;
	}

	@Override
	public void deleteFunctionParam(Long functionParamId) {
		getEm().remove(getEm().find(TipologParamFunzioni.class, functionParamId));

	}


	@Override
	public List<TipologParamFunzioni> getAllFunctionParamById(FunctionParamCriteriaDTO function, Long idfunction) {
		TypedQuery<TipologParamFunzioni> query = getEm().createQuery(buildSearchFunctionParams(function, false),
				TipologParamFunzioni.class);
		setParameterSearchFunctionParams(query, function, idfunction);
		if (function.getPageNumber() != null && function.getPageNumber() > 0) {
			query.setMaxResults(function.getPageSize());
			query.setFirstResult(function.getFirstRow());
		}

		return query.getResultList();
	}

	@Override
	public TipologParamFunzioni updateFunctionParam(TipologParamFunzioni function) {
		return getEm().merge(function);

	}

	@Override
	public TipologParamFunzioni getFunctionParamById(Long idParam) {
		return getEm().find(TipologParamFunzioni.class, idParam);
	}

	@Override
	public Integer countFunctionParam(FunctionParamCriteriaDTO functionParam, Long idFunction) {
		Query query = getEm().createQuery(buildSearchFunctionParams(functionParam, true), Long.class);
		setParameterSearchFunctionParams(query, functionParam, idFunction);
		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	private void setParameterSearchFunctionParams(Query query, FunctionParamCriteriaDTO functionParam,
			Long idFunction) {
		query.setParameter("id", idFunction);

	}

	private String buildSearchFunctionParams(FunctionParamCriteriaDTO functionParam, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? "SELECT COUNT(*) " : " SELECT s ");
		sb.append(" FROM TipologParamFunzioni s where s.tipologFunzioni.idFunzione= :id ");

		if( StringUtils.isNotEmpty(functionParam.getDBOrderField()) ) {
			sb.append(" ORDER BY "+functionParam.getDBOrderField() + " " + functionParam.getOrderType());
		}
		return sb.toString();
	}

	

}
