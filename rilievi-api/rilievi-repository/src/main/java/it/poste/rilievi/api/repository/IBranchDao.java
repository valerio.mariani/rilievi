package it.poste.rilievi.api.repository;

import java.util.Date;
import java.util.List;

import it.poste.rilievi.api.model.entity.TipologFiliali;

public interface IBranchDao {

	List<TipologFiliali> getAllBranches(Date validationDate);

}