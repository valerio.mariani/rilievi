package it.poste.rilievi.api.repository.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.DacoCriteriaDTO;
import it.poste.rilievi.api.model.entity.StagingDacoEscludereMaxiRilievi;
import it.poste.rilievi.api.model.entity.TipologDaco;
import it.poste.rilievi.api.repository.IDacoDao;

@Repository
public class DacoDaoImpl extends DaoBaseImpl implements IDacoDao{


	@Override
	public TipologDaco insertDaco(TipologDaco daco ) {
		getEm().persist(daco);
		return daco;
	}

	@Override
	public TipologDaco getDacoById(Long idDaco) {
		return getEm().find(TipologDaco.class, idDaco); 
	}

	@Override
	public TipologDaco updateDaco(TipologDaco daco) {
		return getEm().merge(daco);
	}

	@Override
	public List<TipologDaco> getAllValidDaco(Date validationDate) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetAllDaco,TipologDaco.class)
				
				.getResultList();
	}
	
	
	@Override
	public List<TipologDaco> getDacoUserProfile(Date validationDate, Long idProfile) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoProfile,TipologDaco.class)
				
				.setParameter("profilo", idProfile)
				.getResultList();
	}
	
	
	@Override
	public List<TipologDaco> getDacoProfileFunction(Date validationDate, Long idProfile, Long idFunction) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoProfileFunction,TipologDaco.class)
				
				.setParameter("profilo", idProfile)
				.setParameter("function", idFunction)
				.getResultList();
	}
	
	@Override
	public List<TipologDaco> getAllDaco(DacoCriteriaDTO criteria) {

		TypedQuery<TipologDaco> query = getEm().createQuery(buildSearchFunctions(criteria, false), TipologDaco.class);

		if (criteria.getPageNumber() != null && criteria.getPageNumber() > 0) {
			query.setMaxResults(criteria.getPageSize());
			query.setFirstResult(criteria.getFirstRow());
		}

		return query.getResultList();

	}
	
	
	
	@Override
	public List<TipologDaco> getDacoMaxiEvaluationStorage(Date validationDate) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoProfileMaxiProfile,TipologDaco.class)
				
				.getResultList();
	}
	
	@Override
	public List<TipologDaco> getDacoMaxiEvaluationStaging(Date validationDate) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoProfileMaxiProfileStaging,TipologDaco.class)
				
				.getResultList();
	}

	@Override
	public List<TipologDaco> getExcludedDacoStaging(Date validationDate) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoExcludedStaging,TipologDaco.class)
				
				.getResultList();
	}
	
	@Override
	public List<TipologDaco> getExcludedDacoStorage(Date validationDate) {
		return getEm()
				.createNamedQuery(TipologDaco.QueryGetDacoExcludedStorage,TipologDaco.class)
				
				.getResultList();
	}
	
	
	@Override
	public Long countAllDaco() {
		Query query = getEm().createNamedQuery(TipologDaco.QueryGetAllDacoPaginableCount, Long.class);
		Long rows = (Long) query.getSingleResult();
		return rows;
	}
	
	@Override
	public Integer countDaco(DacoCriteriaDTO criteria) {
		Query query = getEm().createQuery(buildSearchFunctions(criteria, true), Long.class);

		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	private String buildSearchFunctions(DacoCriteriaDTO criteria, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT(*)" : " SELECT s ");
		sb.append(" FROM TipologDaco s ");
		
		if( StringUtils.isNotEmpty(criteria.getDBOrderField()) ) {
			sb.append(" ORDER BY "+criteria.getDBOrderField() + " " + criteria.getOrderType());
		}

		return sb.toString();
	}

	@Override
	public void removeExcludedDacoStaging() {
		getEm().createNamedQuery(StagingDacoEscludereMaxiRilievi.RemoveAll).executeUpdate();		
	}


}
