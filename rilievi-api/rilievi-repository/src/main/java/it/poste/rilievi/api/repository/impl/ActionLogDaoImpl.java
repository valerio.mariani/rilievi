package it.poste.rilievi.api.repository.impl;


import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageLogAzioni;
import it.poste.rilievi.api.model.utils.Utils;
import it.poste.rilievi.api.repository.IActionLogDao;

@Repository
public class ActionLogDaoImpl extends DaoBaseImpl implements IActionLogDao{


	@Override
	public StorageLogAzioni insertActionLog(StorageLogAzioni azione) {		
		getEm().persist(azione);
		return azione;
	}


	@Override
	public List<StorageLogAzioni> getAuditLog(AuditLogCriteriaDTO auditLog) {
		
		TypedQuery<StorageLogAzioni> query = getEm().createQuery(buildSearchAuditLog(auditLog, false), StorageLogAzioni.class);
		setParameterSearchAuditLog(query, auditLog);
		if(auditLog.getPageNumber() != null && auditLog.getPageNumber() > 0 ) {
			query.setMaxResults(auditLog.getPageSize());
			query.setFirstResult(auditLog.getFirstRow());
		}
		return query.getResultList();
	}
		

	@Override
	public Integer searchAuditLogCount(AuditLogCriteriaDTO request) {
		Query query = getEm().createQuery(buildSearchAuditLog(request, true), Long.class);
		setParameterSearchAuditLog(query, request);
		Long rows= (Long) query.getSingleResult();
		return rows.intValue();
	}

	private String buildSearchAuditLog(AuditLogCriteriaDTO auditLog, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT( * ) " :" SELECT s  ");
		sb.append(" FROM StorageLogAzioni s WHERE 1=1 ");
		
		if(auditLog.getEndDate()!=null && auditLog.getStartDate()!=null) {
			sb.append(" AND s.dataAzione BETWEEN :startDate and :endDate ");
			
		}
		if(auditLog.getUser()!= null) {
			sb.append(" AND s.utente = :user ");
		}
		if( StringUtils.isNotEmpty(auditLog.getDBOrderField()) ) {
			sb.append(" ORDER BY "+auditLog.getDBOrderField() + " " + auditLog.getOrderType());
		}	
		return sb.toString();
	}
	
	private void setParameterSearchAuditLog(Query query, AuditLogCriteriaDTO auditLog )	{
		if(auditLog.getEndDate()!=null && auditLog.getStartDate()!=null) {
			query.setParameter("startDate", Utils.setStartOfDay(Utils.localDateToDate(auditLog.getStartDate())));
			 query.setParameter("endDate", Utils.setEndOfDay(Utils.localDateToDate(auditLog.getEndDate())));
			 if(auditLog.getUser()!=null) {
				 query.setParameter("user", auditLog.getUser().getCode());
			 }
		}
		
		if (auditLog.getEndDate()==null && auditLog.getStartDate()==null) {
			if(auditLog.getUser()!=null) {
				query.setParameter("user", auditLog.getUser().getCode());
			}
		}
			
	}

}

	
	

