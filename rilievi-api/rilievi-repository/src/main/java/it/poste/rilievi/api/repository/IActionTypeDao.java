package it.poste.rilievi.api.repository;

import it.poste.rilievi.api.model.entity.TipologTipoAzioni;

public interface IActionTypeDao extends IDaoBase {

	TipologTipoAzioni getTipoAzioneByName(String name);

}