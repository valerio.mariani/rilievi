package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageNoteAggiuntive;

public interface IEvaluationNoteDao extends IDaoBase {
	
	StorageNoteAggiuntive insertEvaluationNote(StorageNoteAggiuntive evaluationNote);
	
	void deleteEvaluationNote(Long noteId);
	
	Integer countEvaluationNote(EvaluationNoteCriteriaDTO criteria);

	List<StorageNoteAggiuntive> getEvaluationNote(EvaluationNoteCriteriaDTO evaluationNote);

}
