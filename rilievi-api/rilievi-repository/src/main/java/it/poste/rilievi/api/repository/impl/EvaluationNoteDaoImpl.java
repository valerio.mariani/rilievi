package it.poste.rilievi.api.repository.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageNoteAggiuntive;
import it.poste.rilievi.api.repository.IEvaluationNoteDao;

@Repository
public class EvaluationNoteDaoImpl extends DaoBaseImpl implements IEvaluationNoteDao {

	@Override
	public StorageNoteAggiuntive insertEvaluationNote(StorageNoteAggiuntive evaluationNote) {

		getEm().persist(evaluationNote);
		return evaluationNote;
	}

	@Override
	public void deleteEvaluationNote(Long noteId) {
		getEm().remove(getEm().find(StorageNoteAggiuntive.class, noteId));

	}

	@Override
	public List<StorageNoteAggiuntive> getEvaluationNote(EvaluationNoteCriteriaDTO evaluationNote) {
		
		TypedQuery<StorageNoteAggiuntive> query = getEm().createQuery(buildSearchEvaluationNotes(evaluationNote, false), StorageNoteAggiuntive.class);
		setParameterSearchEvaluations(query, evaluationNote);
		if(evaluationNote.getPageNumber() != null && evaluationNote.getPageNumber() > 0) {
			query.setFirstResult(evaluationNote.getFirstRow()) ;//tupla di partenza
			query.setMaxResults(evaluationNote.getPageSize());
		}

		return query.getResultList();
	}

	@Override
	public Integer countEvaluationNote(EvaluationNoteCriteriaDTO criteria) {
		Query query = getEm().createQuery(buildSearchEvaluationNotes(criteria, true), Long.class);
		setParameterSearchEvaluations(query, criteria);
		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	private void setParameterSearchEvaluations(Query query, EvaluationNoteCriteriaDTO criteria) {
			query.setParameter("id", criteria.getEvaluation().getId());
	}

	private String buildSearchEvaluationNotes(EvaluationNoteCriteriaDTO criteria, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT(*)" : " SELECT s ");
		sb.append(" FROM StorageNoteAggiuntive s  where s.storageRilievi.idRilievi= :id ORDER BY s.dataInserimento desc");

		return sb.toString();
	}

}
