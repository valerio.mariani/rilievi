package it.poste.rilievi.api.repository;

public interface IAbbinamentoVociDacoServiziDao {

	boolean exitsDacoServiceAssociation(Long idDaco, Long idService );
	
}