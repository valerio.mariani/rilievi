package it.poste.rilievi.api.repository.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.AttachmentCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageAllegati;
import it.poste.rilievi.api.repository.IAttachmentDao;

@Repository
public class AttachmentDaoImpl extends DaoBaseImpl implements IAttachmentDao {

	@Override
	public List<StorageAllegati> getAttachmentById(AttachmentCriteriaDTO criteria, Long evaluationId) {

		TypedQuery<StorageAllegati> query = getEm()
				.createQuery(buildSearchAttachment(criteria, false), StorageAllegati.class)
				.setParameter("evaluationId", evaluationId);

		if (criteria.getPageNumber() != null && criteria.getPageNumber() > 0) {
			query.setMaxResults(criteria.getPageSize());
			query.setFirstResult(criteria.getFirstRow());
		}

		return query.getResultList();
	}
	
	
	private String buildSearchAttachment(AttachmentCriteriaDTO criteria, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT(*)" : " SELECT s ");
		sb.append(" FROM StorageAllegati s WHERE s.storageRilievi.idRilievi = :evaluationId  AND s.attivo = '" + StorageAllegati.ACTIVE +"'");
		
		if( StringUtils.isNotEmpty(criteria.getDBOrderField()) ) {
			sb.append(" ORDER BY "+criteria.getDBOrderField() + " " + criteria.getOrderType());
		}

		return sb.toString();
	}
	
	
		
	@Override
	public Integer countAttachment(AttachmentCriteriaDTO criteria) {
		Query query = getEm().createQuery(buildSearchAttachment(criteria, true), Long.class);

		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	


	public Integer countAttachmentById(AttachmentCriteriaDTO criteria, Long evaluationId) {
		
		Query query = getEm().createNamedQuery(StorageAllegati.countAttachmentById,Long.class)
								.setParameter("evaluationId", evaluationId);
		Long rows= (Long) query.getSingleResult();
		return rows.intValue();
	}
	
	@Override
	public void deleteAttachment(StorageAllegati allegato) {
		
		getEm().merge(allegato);

	}

	@Override
	public StorageAllegati insertAttachment(StorageAllegati attachmentEnt) {
		getEm().persist(attachmentEnt);
		return attachmentEnt;
	}

	@Override
	public StorageAllegati getAttachmentById(Long idAttachment) {
		return getEm().find(StorageAllegati.class, idAttachment);
	}

	@SuppressWarnings("unused")
	@Override
	public boolean checkExist(String name, Long evaluationID) {
		try {
			StorageAllegati checkname = getEm()
					.createNamedQuery(StorageAllegati.findAttachmentByNameAndID, StorageAllegati.class)
					.setParameter("name",name)
					.setParameter("evaluationID",evaluationID)
					.getSingleResult();
			return true;
			}
		catch (NoResultException e) {
			return false;
		}

		}

}
