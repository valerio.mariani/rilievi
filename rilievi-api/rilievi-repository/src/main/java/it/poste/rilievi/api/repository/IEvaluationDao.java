package it.poste.rilievi.api.repository;

import java.time.LocalDate;
import java.util.List;

import it.poste.rilievi.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageRilievi;

public interface IEvaluationDao extends IDaoBase{

	List<StorageRilievi> searchEvaluations(EvaluationCriteriaDTO criteria);
	
	Integer searchEvaluationsCount(EvaluationCriteriaDTO criteria);
	
	StorageRilievi insertEvaluation(StorageRilievi rilievo);

	StorageRilievi updateFractional(StorageRilievi rilievo);
	
	void deleteEvaluation(StorageRilievi rilievo);
	
	void updateEvaluationById(LocalDate dataRegolarizzazione, String regolarizzatoDaUtente, String flagRegolarizzato, Long idRilievi);

	StorageRilievi updateEvaluation(StorageRilievi rilievo);
	
	
	
}