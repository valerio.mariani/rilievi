package it.poste.rilievi.api.repository;

import it.poste.rilievi.api.model.entity.AnagProfili;

public interface IUserProfileDao {

	AnagProfili getProfileByCode(String code);

}