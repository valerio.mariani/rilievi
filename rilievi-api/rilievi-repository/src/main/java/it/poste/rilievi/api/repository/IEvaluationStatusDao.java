package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.TipologStatoRilievo;

public interface IEvaluationStatusDao extends IDaoBase {
	
	List<TipologStatoRilievo> getAllEvaluationStatus();

	TipologStatoRilievo getStatusByCode(String code);
}
