package it.poste.rilievi.api.repository.impl;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.entity.TipologFiliali;
import it.poste.rilievi.api.repository.IBranchDao;

@Repository
public class BranchDaoImpl extends DaoBaseImpl implements IBranchDao{


	@Override
	public List<TipologFiliali> getAllBranches( Date validationDate) {
		return getEm()
				.createNamedQuery(TipologFiliali.GetAllBranches,TipologFiliali.class)
				
				.getResultList();
		
	}
}
