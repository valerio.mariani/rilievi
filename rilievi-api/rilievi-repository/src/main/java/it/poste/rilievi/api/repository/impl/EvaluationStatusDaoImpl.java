package it.poste.rilievi.api.repository.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.api.repository.IEvaluationStatusDao;

@Repository
public class EvaluationStatusDaoImpl extends DaoBaseImpl implements IEvaluationStatusDao{

	@Override
	public List<TipologStatoRilievo> getAllEvaluationStatus() {
		return getEm()
				.createNamedQuery(TipologStatoRilievo.QueryGetAllEvaluationStatus,TipologStatoRilievo.class)				
				.getResultList();
	}

	@Override
	public TipologStatoRilievo getStatusByCode(String code) {
		try {
			return getEm().createNamedQuery(TipologStatoRilievo.QueryGetEvaluationStatusByCode, TipologStatoRilievo.class)
					.setParameter("code", Integer.parseInt(code))
					.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
		
	}

}
