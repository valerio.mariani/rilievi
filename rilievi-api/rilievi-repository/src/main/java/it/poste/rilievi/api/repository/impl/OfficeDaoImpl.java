package it.poste.rilievi.api.repository.impl;


import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import it.poste.rilievi.api.model.entity.TipologUffici;
import it.poste.rilievi.api.repository.IOfficeDao;

@Repository
public class OfficeDaoImpl extends DaoBaseImpl implements IOfficeDao{
	private static Logger logger = LoggerFactory.getLogger(OfficeDaoImpl.class);
	@Override
	public List<TipologUffici> getOffices(Long rifBranch) {
		return getEm()
				.createNamedQuery(TipologUffici.GetBranchOffices,TipologUffici.class)
				.setParameter("rifBranch", rifBranch)
				.getResultList();
	}
	
	
	@Override
	public TipologUffici getOfficeByFractional(String fractional) {
		try {
			
			return getEm()
					.createNamedQuery(TipologUffici.GetOfficeByFractional,TipologUffici.class)
					.setParameter("frazionario", fractional)
					.getSingleResult();
		}catch (NoResultException  e) {
			return null;
		}catch (NonUniqueResultException e) {
			logger.error("Esiste piu di un Ufficio con lo stesso Frazionario");
			throw e;
		}
		
	}
}
