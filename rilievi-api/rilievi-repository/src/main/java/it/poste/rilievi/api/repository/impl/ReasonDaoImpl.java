package it.poste.rilievi.api.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.entity.TipologCausale;
import it.poste.rilievi.api.repository.IReasonDao;

@Repository
public class ReasonDaoImpl extends DaoBaseImpl implements IReasonDao {

	@Override
	public List<TipologCausale> getAllReason() {

		return getEm().createNamedQuery(TipologCausale.QueryGetAllReason, TipologCausale.class).getResultList();
	}

}
