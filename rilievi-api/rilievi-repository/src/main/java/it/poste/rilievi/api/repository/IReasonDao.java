package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.TipologCausale;

public interface IReasonDao extends IDaoBase {

	List<TipologCausale> getAllReason();

}
