package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.StorageRilieviMaxiTotale;

public interface IMaxiEvaluationDao extends IDaoBase {

	List<StorageRilieviMaxiTotale> getMaxiEvaluationRaised();

}