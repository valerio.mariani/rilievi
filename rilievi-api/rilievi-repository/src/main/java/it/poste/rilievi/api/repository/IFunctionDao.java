package it.poste.rilievi.api.repository;


import java.util.List;

import it.poste.rilievi.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.api.model.entity.AnagProfiliAbilitazione;
import it.poste.rilievi.api.model.entity.TipologFunzioni;

public interface IFunctionDao extends IDaoBase{
	
	TipologFunzioni insertFunction(TipologFunzioni function);
	
	TipologFunzioni updateFunction(TipologFunzioni function);
	
	List<TipologFunzioni> getAllFunction(FunctionCriteriaDTO function);
	
	TipologFunzioni getFunctionById( Long idFunction );
	
	List<TipologFunzioni> getFunctionByProfile(Long idProfile);

	List<TipologFunzioni> getOtherFunctionByProfile(Long idProfile);

	void insertFunctionProfile(AnagProfiliAbilitazione anagProfiliAbilitazione);
	
	boolean checkFunctionProfileExist(Long idProfilo, Long idFunzione);

	Integer countFunction(FunctionCriteriaDTO function);

	List<TipologFunzioni> getFunctionByNames(List<String> names);

	List<TipologFunzioni> getFunctionByProfileCode(String code);

}
