package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.StorageDateEstrazione;

public interface IExtractionDatesDao {

	StorageDateEstrazione insertExtractionDates(StorageDateEstrazione extractionDates);

	List<StorageDateEstrazione> getAllExtractionDates();

}
