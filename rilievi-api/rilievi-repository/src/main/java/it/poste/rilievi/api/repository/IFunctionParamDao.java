package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.api.model.entity.TipologParamFunzioni;

public interface IFunctionParamDao extends IDaoBase {

	TipologParamFunzioni insertFunctionParam(TipologParamFunzioni functionparam);

	void deleteFunctionParam(Long functionParamId);

	List<TipologParamFunzioni> getAllFunctionParamById(FunctionParamCriteriaDTO function, Long idfunction);

	TipologParamFunzioni updateFunctionParam(TipologParamFunzioni function);

	TipologParamFunzioni getFunctionParamById(Long idFunction);

	Integer countFunctionParam(FunctionParamCriteriaDTO functionParam, Long idFunction);

}
