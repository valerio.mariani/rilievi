package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.TipologUffici;

public interface IOfficeDao extends IDaoBase {

	List<TipologUffici> getOffices( Long idBranch );

	TipologUffici getOfficeByFractional(String fractional);
			
}