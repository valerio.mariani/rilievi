package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.entity.TipologServizi;

public interface IDacoServiceDao {

	List<TipologServizi> getServices(Long idDaco);

}