package it.poste.rilievi.api.repository;

import java.util.List;

import it.poste.rilievi.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.api.model.entity.StorageLogAzioni;

public interface IActionLogDao {

	StorageLogAzioni insertActionLog(StorageLogAzioni azione);

	List<StorageLogAzioni> getAuditLog(AuditLogCriteriaDTO auditLog);

	Integer searchAuditLogCount(AuditLogCriteriaDTO request);

}