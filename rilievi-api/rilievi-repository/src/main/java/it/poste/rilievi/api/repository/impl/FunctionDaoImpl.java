package it.poste.rilievi.api.repository.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.api.model.entity.AnagProfiliAbilitazione;
import it.poste.rilievi.api.model.entity.TipologFunzioni;
import it.poste.rilievi.api.repository.IFunctionDao;

@SuppressWarnings({"unchecked","unused"})
@Repository
public class FunctionDaoImpl extends DaoBaseImpl implements IFunctionDao {

	@Override
	public TipologFunzioni insertFunction(TipologFunzioni function) {
		getEm().persist(function);
		return function;
	}

	@Override
	public TipologFunzioni updateFunction(TipologFunzioni function) {
		return getEm().merge(function);
	}

	@Override
	public List<TipologFunzioni> getAllFunction(FunctionCriteriaDTO function) {
		TypedQuery<TipologFunzioni> query = getEm().createQuery(buildSearchFunctions(function, false),
				TipologFunzioni.class);

		if (function.getPageNumber() != null && function.getPageNumber() > 0) {
			query.setMaxResults(function.getPageSize());
			query.setFirstResult(function.getFirstRow());
		}

		return query.getResultList();

	}

	@Override
	public TipologFunzioni getFunctionById(Long idFunction) {
		return getEm().find(TipologFunzioni.class, idFunction);
	}

	@Override
	public List<TipologFunzioni> getFunctionByNames(List<String> names) {
		Query query = getEm().createNamedQuery(TipologFunzioni.GetFunctionByNames, TipologFunzioni.class)
				.setParameter("names", names);
		return query.getResultList();
	}
	
	
	@Override
	public List<TipologFunzioni> getFunctionByProfile(Long idProfile) {
		Query query = getEm().createNamedQuery(TipologFunzioni.findFunctionProfile, TipologFunzioni.class)
				.setParameter("profile", idProfile);
		return query.getResultList();
	}
	

	@Override
	public List<TipologFunzioni> getFunctionByProfileCode(String code ) {
		Query query = getEm().createNamedQuery(TipologFunzioni.findFunctionProfileCode, TipologFunzioni.class)
				.setParameter("profile", code);
		return query.getResultList();
	}

	@Override
	public List<TipologFunzioni> getOtherFunctionByProfile(Long idProfile) {
		Query query = getEm().createNamedQuery(TipologFunzioni.findOtherFunctionProfile, TipologFunzioni.class)
				.setParameter("profile", idProfile);
		return query.getResultList();
	}

	@Override
	public void insertFunctionProfile(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		getEm().persist(anagProfiliAbilitazione);

	}

	@Override
	public boolean checkFunctionProfileExist(Long idProfilo, Long idFunzione) {
		try {
			AnagProfiliAbilitazione checkFunctionProfileExist = getEm()
					.createNamedQuery(AnagProfiliAbilitazione.FindFunctionProfileExist, AnagProfiliAbilitazione.class)
					.setParameter("idProfile", idProfilo).setParameter("idFunction", idFunzione).getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public Integer countFunction(FunctionCriteriaDTO function) {
		Query query = getEm().createQuery(buildSearchFunctions(function, true), Long.class);

		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	private String buildSearchFunctions(FunctionCriteriaDTO function, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT(*)" : " SELECT s ");
		sb.append(" FROM TipologFunzioni s ");
		
		if( StringUtils.isNotEmpty(function.getDBOrderField()) ) {
			sb.append(" ORDER BY "+function.getDBOrderField() + " " + function.getOrderType());
		}

		return sb.toString();
	}

}
