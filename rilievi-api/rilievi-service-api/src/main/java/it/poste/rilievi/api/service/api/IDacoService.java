package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.DacoCriteriaDTO;
import it.poste.rilievi.api.model.dto.DacoDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IDacoService extends IService {

	DacoDTO insertDaco(DacoDTO daco);

	DacoDTO updateDaco(DacoDTO daco);

	DacoDTO getDaco(Long dacoId);

	List<DacoDTO> getDacoItems();

	List<DacoDTO> getDacoUserProfile(Long profileId);

	List<DacoDTO> getDacoMaxiEvaluation();

	PaginableResultSearchImpl<DacoDTO> getDacos(DacoCriteriaDTO criteria);

	List<DacoDTO> getExcludedDaco();

	void removeExcludedDacoStaging();

	void insertExcludedDacosStaging(List<DacoDTO> dacos);

}
