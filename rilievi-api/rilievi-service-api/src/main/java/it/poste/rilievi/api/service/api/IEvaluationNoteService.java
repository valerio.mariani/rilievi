package it.poste.rilievi.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IEvaluationNoteService extends IService {

	EvaluationNoteDTO insertEvaluationNote(EvaluationNoteDTO evaluationNote);

	void deleteEvaluationNote(Long noteId);

	PaginableResultSearchImpl<EvaluationNoteDTO> getEvaluationNote(EvaluationNoteCriteriaDTO evaluationNote,Long EvaluationId);

}
