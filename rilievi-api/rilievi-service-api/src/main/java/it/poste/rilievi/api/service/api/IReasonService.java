package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.ReasonDTO;

@Service
public interface IReasonService extends IService {

	List<ReasonDTO> getAllReason();

}
