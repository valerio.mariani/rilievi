package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.EvaluationStatusDTO;

@Service
public interface IEvaluationStatusService extends IService {

	List<EvaluationStatusDTO> getAllEvaluationStatus();
}
