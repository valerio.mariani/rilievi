package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.api.model.dto.FunctionDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IFunctionService extends IService {

	FunctionDTO insertFunction(FunctionDTO function);

	FunctionDTO updateFunction(FunctionDTO function);

	FunctionDTO getFunction(Long FunctionId);

	PaginableResultSearchImpl<FunctionDTO> getAllFunction(FunctionCriteriaDTO function);

	List<FunctionDTO> getFunctionByProfile(Long idProfile);

	List<FunctionDTO> getOtherFunctionByProfile(Long idProfile);

	FunctionDTO addFunctionProfile(Long idProfile, Long idFunction);

	List<FunctionDTO> getFunctionByProfileCode(String code);

}
