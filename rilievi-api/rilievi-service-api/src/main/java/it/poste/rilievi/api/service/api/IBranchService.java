package it.poste.rilievi.api.service.api;

import java.util.List;

import it.poste.rilievi.api.model.dto.BranchDTO;

public interface IBranchService {

	List<BranchDTO> getAllBraches();

}
