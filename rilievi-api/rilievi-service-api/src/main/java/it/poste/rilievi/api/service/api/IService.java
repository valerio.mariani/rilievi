package it.poste.rilievi.api.service.api;

import java.time.LocalDateTime;

public interface IService {

	LocalDateTime getSysDate();

}
