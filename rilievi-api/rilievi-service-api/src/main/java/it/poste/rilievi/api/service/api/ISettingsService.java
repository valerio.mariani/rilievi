package it.poste.rilievi.api.service.api;

import it.poste.rilievi.api.model.dto.AppSettings;
import it.poste.rilievi.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.api.model.dto.ReasonDTO;

public interface ISettingsService extends IService {

	AppSettings getAppSettings();

	ReasonDTO getRcReason();

	ReasonDTO getRdReason();

	ReasonDTO getRaReason();

	ReasonDTO getRsReason();

	EvaluationStatusDTO getUnLossState();

}