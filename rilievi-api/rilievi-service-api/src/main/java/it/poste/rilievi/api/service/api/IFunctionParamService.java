package it.poste.rilievi.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;


@Service
public interface IFunctionParamService extends IService {
	
	FunctionParamDTO insertFunctionParam (FunctionParamDTO functionParam);
	
	FunctionParamDTO updateFunctionParam (FunctionParamDTO function);
	
	FunctionParamDTO getFunctionParam( Long idParam );
	
	PaginableResultSearchImpl<FunctionParamDTO> getAllFunctionById(FunctionParamCriteriaDTO functionParam, Long idFunction);

}
