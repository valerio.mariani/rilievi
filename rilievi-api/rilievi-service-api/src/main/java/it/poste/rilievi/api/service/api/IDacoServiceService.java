package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.DacoServiceDTO;

@Service
public interface IDacoServiceService extends IService {

	List<DacoServiceDTO> getServices(Long dacoId);

}
