package it.poste.rilievi.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.AttachmentCriteriaDTO;
import it.poste.rilievi.api.model.dto.AttachmentDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IAttachmentService {

	PaginableResultSearchImpl<AttachmentDTO> getAttachmentByIdEvaluation(AttachmentCriteriaDTO request, Long evaluationId);

	AttachmentDTO deleteAttachmentById(Long attachmentId, String user);

	AttachmentDTO insertAttachmentById(AttachmentDTO insertAttachment, Long evaluationId);

	List<AttachmentDTO> insertAttachments(List<AttachmentDTO> insertAttachment, Long evaluationId);

	AttachmentDTO getAttachmentById(Long attachmentId);

}
