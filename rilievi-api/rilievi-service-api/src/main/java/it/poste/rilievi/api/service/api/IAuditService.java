package it.poste.rilievi.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.api.model.dto.AuditLogDTO;
import it.poste.rilievi.api.model.dto.ExportTableDTO;
import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IAuditService {

	AuditLogDTO insertAudit(AuditLogDTO audit);

	PaginableResultSearchImpl<AuditLogDTO> getAuditLogs(AuditLogCriteriaDTO criteria);

	ExportTableDTO exportLogs(AuditLogCriteriaDTO auditLogCriteria);

}
