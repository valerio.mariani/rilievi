package it.poste.rilievi.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.api.model.dto.MaxiEvaluationDTO;

@Service
public interface IMaxiEvaluationService extends IService {

	MaxiEvaluationDTO checkMaxiEvaluationRaised();
}
