package it.poste.rilievi.api.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the staging_maxirilievi database table.
 * 
 */
@Entity
@Table(name="staging_maxirilievi")
@NamedQuery(name="StagingMaxirilievi.findAll", query="SELECT s FROM StagingMaxirilievi s")
public class StagingMaxirilievi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_REGOLA_MAXIRIL")
	private int idRegolaMaxiril;

	@Column(name="ID_RILIEVI")
	private int idRilievi;

	public StagingMaxirilievi() {
	}

	public int getIdRegolaMaxiril() {
		return this.idRegolaMaxiril;
	}

	public void setIdRegolaMaxiril(int idRegolaMaxiril) {
		this.idRegolaMaxiril = idRegolaMaxiril;
	}

	public int getIdRilievi() {
		return this.idRilievi;
	}

	public void setIdRilievi(int idRilievi) {
		this.idRilievi = idRilievi;
	}

}