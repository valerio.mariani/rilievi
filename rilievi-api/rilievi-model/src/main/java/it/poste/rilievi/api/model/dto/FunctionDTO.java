package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;
import java.util.HashMap;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class FunctionDTO {
	private Long id;
	private String name;
	private String extendedName;
	private String description;
	private LocalDateTime dateCreation;
	
	private HashMap<String,FunctionParamDTO> params = new HashMap<String,FunctionParamDTO>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}
	public HashMap<String, FunctionParamDTO> getParams() {
		return params;
	}
	public void setParams(HashMap<String, FunctionParamDTO> params) {
		this.params = params;
	}
	public String getExtendedName() {
		return extendedName;
	}
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
