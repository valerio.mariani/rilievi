package it.poste.rilievi.api.model.dto;

import java.util.List;

public interface IPaginableResultSearch<T> {

	public int getTotalRows();
	
	public void setTotalRows(int totalRows);
	
	public int getTotalPages();
	
	public List<T> getItems();
	
	public void setItems(List<T> items);

	public void addItem(T item);
	
}
