package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AuditLogDTO {
	
	private ServicesCatalog serviceCode;
	
	private String serviceDescr;
	
	private Long id;
	
	private AppUserDTO user;
	
	private String descrAction;
	
	private LocalDateTime dateTime;
	
	private String result;
	
	
	

	public ServicesCatalog getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(ServicesCatalog serviceCode) {
		this.serviceCode = serviceCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AppUserDTO getUser() {
		return user;
	}

	public void setUser(AppUserDTO user) {
		this.user = user;
	}

	public String getDescrAction() {
		return descrAction;
	}

	public void setDescrAction(String descrAction) {
		this.descrAction = descrAction;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getServiceDescr() {
		return serviceDescr;
	}

	public void setServiceDescr(String serviceDescr) {
		this.serviceDescr = serviceDescr;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
