package it.poste.rilievi.api.model.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import it.poste.rilievi.api.model.entity.StorageRilievi;

public class EvaluationDTO {
		
	private Long id;
	
	private LocalDateTime insertDate;
	
	private LocalDateTime regularizationDate;
	
	private String regularizationFromUser;
	
	private String regularizationFlag;
	
	private Integer number;
	
	private LocalDate competenceDate;
	
	private LocalDate accountDate;
	
	private LocalDate sendedSrcDate;
	
	private String fractional;
	
	private BranchDTO branch;
	
	private OfficeDTO office;
	
	private DacoDTO daco;
	
	private ReasonDTO reason;
	
	private ReasonDTO previousReason;

	private DacoServiceDTO service;
	
	private String description;
	
	private Double debitAmount;
	
	private Double creditAmount;
	
	private List<EvaluationNoteDTO> notes = new ArrayList<EvaluationNoteDTO>();
	
	private List<AttachmentDTO> attachments = new ArrayList<AttachmentDTO>();
	
	private AppUserDTO insertUser;
	
	private EvaluationDTO generated;
	
	private EvaluationStatusDTO status;
	
	private String deletedFlag;
	
	private LocalDateTime deleteDate;
	
	private Long idParent;
	
	private Long idChild;
	
	private String realignedStatus;
	
	private String realignedFlag;

	private String utenteInserimento;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}

	

	public LocalDateTime getRegularizationDate() {
		return regularizationDate;
	}

	public void setRegularizationDate(LocalDateTime regularizationDate) {
		this.regularizationDate = regularizationDate;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	
	public LocalDate getCompetenceDate() {
		return competenceDate;
	}

	
	public void setCompetenceDate(LocalDate competenceDate) {
		this.competenceDate = competenceDate;
	}

	
	public LocalDate getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(LocalDate accountDate) {
		this.accountDate = accountDate;
	}

	public LocalDate getSendedSrcDate() {
		return sendedSrcDate;
	}

	public void setSendedSrcDate(LocalDate sendedSrcDate) {
		this.sendedSrcDate = sendedSrcDate;
	}

	public String getFractional() {
		return fractional;
	}

	public void setFractional(String fractional) { this.fractional = fractional; }
	
	public ReasonDTO getReason() {
		return reason;
	}
	
	public void setReason(ReasonDTO reason) { this.reason = reason; }

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;		
	}

	public Double getDebitAmount() {		
		return debitAmount;
	}
	
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;		
	}
	
	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;		
	}

	public BranchDTO getBranch() {
		return branch;
	}

	public void setBranch(BranchDTO branch) {
		this.branch = branch;
	}

	public OfficeDTO getOffice() {
		return office;
	}

	public void setOffice(OfficeDTO office) {
		this.office = office;
	}

	public DacoDTO getDaco() {
		return daco;
	}

	public void setDaco(DacoDTO daco) {
		this.daco = daco;
	}

	public DacoServiceDTO getService() {
		return service;
	}

	public void setService(DacoServiceDTO service) {
		this.service = service;
	}

	public List<EvaluationNoteDTO> getNotes() {
		return notes;
	}

	public void setNotes(List<EvaluationNoteDTO> notes) {
		this.notes = notes;
	}

	public List<AttachmentDTO> getAttachments() {
		if( attachments == null ) {
			attachments = new ArrayList<AttachmentDTO>();
		}
		return attachments;
	}

	public void setAttachments(List<AttachmentDTO> attachments) {
		this.attachments = attachments;
	}

	public AppUserDTO getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(AppUserDTO insertUser) {
		this.insertUser = insertUser;
	}

	public ReasonDTO getPreviousReason() {
		return previousReason;
	}

	public void setPreviousReason(ReasonDTO previousReason) {
		this.previousReason = previousReason;
	}

	public EvaluationDTO getGenerated() {
		return generated;
	}

	public void setGenerated(EvaluationDTO generated) {
		this.generated = generated;
	}

	public EvaluationStatusDTO getStatus() {
		return status;
	}

	public void setStatus(EvaluationStatusDTO status) {
		this.status = status;
	}

	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public LocalDateTime getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(LocalDateTime localDateTime) {
		this.deleteDate = localDateTime;
	}

	public Long getIdParent() {
		return idParent;
	}

	public void setIdParent(Long idParent) {
		this.idParent = idParent;
	}

	public Long getIdChild() {
		return idChild;
	}

	public void setIdChild(Long idChild) {
		this.idChild = idChild;
	}

	
	public boolean isDeleted() {
		return this.getDeleteDate() != null || StorageRilievi.FLAG_ANNULLATO.equalsIgnoreCase(this.getDeletedFlag());
	}

	public String getRegularizationFlag() {
		return regularizationFlag;
	}

	public void setRegularizationFlag(String regularizationFlag) {
		this.regularizationFlag = regularizationFlag;
	}

	public String getRealignedStatus() {
		return realignedStatus;
	}

	public void setRealignedStatus(String realignedStatus) {
		this.realignedStatus = realignedStatus;
	}

	public String getRealignedFlag() {
		return realignedFlag;
	}

	public void setRealignedFlag(String realignedFlag) {
		this.realignedFlag = realignedFlag;
	}

	public String getRegularizationFromUser() {
		return regularizationFromUser;
	}

	public void setRegularizationFromUser(String regularizationFromUser) { this.regularizationFromUser = regularizationFromUser; }

	public String getUtenteInserimento() { return utenteInserimento; }

	public void setUtenteInserimento(String utenteInserimento) { this.utenteInserimento = utenteInserimento; }

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
