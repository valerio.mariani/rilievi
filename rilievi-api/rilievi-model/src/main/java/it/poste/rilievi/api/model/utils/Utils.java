package it.poste.rilievi.api.model.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.poste.rilievi.api.model.dto.AccountingDate;
import it.poste.rilievi.api.model.dto.DacoDTO;

public class Utils {
	
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	public static Date localDateToDate(LocalDate dateToConv) {
		return dateToConv != null ? Date.from(dateToConv.atStartOfDay()
			      					.atZone(ZoneId.systemDefault())
			      					.toInstant()) : null;
	}
	
	
	
	public static Date localDateTimeToDate(LocalDateTime dateToConv) {
		if( dateToConv != null ) {
			ZonedDateTime zdt = dateToConv.atZone(ZoneId.systemDefault());
			return  Date.from(zdt.toInstant());
		}else {
			return null;
		}
		
	}
		
	public static LocalDate dateToLocalDate(Date dateToConvert) {
	    return dateToConvert != null ? dateToConvert.toInstant()
								      .atZone(ZoneId.systemDefault())
								      .toLocalDate() : null;
	}
	
	public static LocalDateTime dateToLocalDateTime(Date dateToConvert) {
	    return dateToConvert != null ?  Instant.ofEpochMilli(dateToConvert.getTime())
	    	      						.atZone(ZoneId.systemDefault())
	    	      						.toLocalDateTime() : null;
	}
	
	public static LocalDateTime localDateToLocalDateTime(Date dateToConvert) {
		return dateToConvert != null ? Instant.ofEpochMilli(dateToConvert.getTime())
										.atZone(ZoneId.systemDefault())
											.toLocalDateTime() : null;
	}
	
	
	public static Date setStartOfDay(Date date) {
		if (date!=null) {
        Calendar cal = Calendar.getInstance();  
        cal.setTime(date);  
        cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);  
        return cal.getTime(); 
		}else {
			return null;
		}
		
    }
	
	public static Date setEndOfDay(Date date) {  
		if(date!=null) {
		Calendar cal = Calendar.getInstance();  
        cal.setTime(date);  
        cal.set(Calendar.HOUR_OF_DAY, 23);  
        cal.set(Calendar.MINUTE, 59);  
        cal.set(Calendar.SECOND, 59);  
        cal.set(Calendar.MILLISECOND, 59);  
        return cal.getTime(); 
		}else {
			return null;
		}
    }
	
	public static boolean isDebitDaco(DacoDTO daco) {
		return daco != null && StringUtils.defaultString(daco.getCode()).length()>1 && "D".equalsIgnoreCase(daco.getCode().substring(1, 2));
	}
	
	
	public static LocalDate convertToLocalDate(AccountingDate accountingDate) {
		LocalDate localDate = null;
		
		if(accountingDate !=null) {
		
			StringBuilder sb = new StringBuilder();
			sb.append(accountingDate.getYear()).append("-").append(accountingDate.getMonth()).append("-").append(accountingDate.getDay());
			try {
				localDate = LocalDate.parse(sb.toString());
			} catch (DateTimeParseException ex) {
				logger.error("error during formatting date: {}", sb.toString() );
			}
		}
		
		return localDate;
	}
}
