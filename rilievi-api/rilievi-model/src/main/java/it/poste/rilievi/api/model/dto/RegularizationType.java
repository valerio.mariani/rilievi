package it.poste.rilievi.api.model.dto;

public enum RegularizationType {
	
	U,//utente manuale
	R,// (Rilievi) 
	B,// (Batch) per regolarizzazione tramite mondo Rilievi.

}
