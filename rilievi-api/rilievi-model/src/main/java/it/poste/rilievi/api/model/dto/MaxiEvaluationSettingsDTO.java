package it.poste.rilievi.api.model.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class MaxiEvaluationSettingsDTO {

	private List<DacoDTO> excludedDacos = new ArrayList<DacoDTO>();

	private LocalDate limitDate;

	public List<DacoDTO> getExcludedDacos() {
		return excludedDacos;
	}

	public void setExcludedDacos(List<DacoDTO> excludedDacos) {
		this.excludedDacos = excludedDacos;
	}

	public LocalDate getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(LocalDate limitDate) {
		this.limitDate = limitDate;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
