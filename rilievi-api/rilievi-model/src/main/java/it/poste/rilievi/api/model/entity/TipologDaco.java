package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tipolog_daco database table.
 * 
 */
@Entity
@Table(name="tipolog_daco")
@NamedQueries(value = {		@NamedQuery(name=TipologDaco.QueryGetDacoProfileFunction, 
										query="SELECT DISTINCT t FROM TipologDaco t JOIN t.tipologUtentiServizis tus JOIN tus.anagProfiliAbilitazione apa WHERE apa.anagProfili.idProfilo = :profilo "
												+ "AND apa.tipologFunzioni.idFunzione= :function"),	
		
							@NamedQuery(name=TipologDaco.QueryGetDacoProfile, 
										query="SELECT t FROM TipologDaco t JOIN t.storageAbilitazioniDacos abi  WHERE abi.anagProfili.idProfilo = :profilo "),
							
							@NamedQuery(name=TipologDaco.QueryGetAllDaco, 
										query="SELECT t FROM TipologDaco t "),
							
							@NamedQuery(name=TipologDaco.QueryGetDacoProfileMaxiProfile, 
									query="SELECT DISTINCT t FROM TipologDaco t  WHERE  "
											+ " t.idVoceDaco NOT IN(Select e.rifCodiceDaco FROM TipologDacoEscludereMaxiRilievi e)"),	
							
							@NamedQuery(name=TipologDaco.QueryGetDacoProfileMaxiProfileStaging, 
							query="SELECT DISTINCT t FROM TipologDaco t  WHERE  "
									+ " t.idVoceDaco NOT IN(Select e.tipologDaco.idVoceDaco FROM StagingDacoEscludereMaxiRilievi e)"),	
							
							@NamedQuery(name=TipologDaco.QueryGetDacoExcludedStaging, 
							query="SELECT DISTINCT t FROM TipologDaco t  WHERE  "
									+ " t.idVoceDaco IN(Select e.tipologDaco.idVoceDaco FROM StagingDacoEscludereMaxiRilievi e)"),	
							
							@NamedQuery(name=TipologDaco.QueryGetDacoExcludedStorage, 
							query="SELECT DISTINCT t FROM TipologDaco t  WHERE  "
									+ "t.idVoceDaco IN(Select e.rifCodiceDaco FROM TipologDacoEscludereMaxiRilievi e)"),	
	
							/*query="SELECT t FROM TipologDaco t JOIN t.tipologUtentiServizis tus JOIN tus.anagProfiliAbilitazione apa WHERE apa.anagProfili.idProfilo = :profilo"
										+ " AND t.dataFineValidita is null or :validationDate between t.dataInizioValidita and t.dataFineValidita AND t.idVoceDaco NOT IN(Select e.rifCodiceDaco FROM TipologDacoEscludereMaxiRilievi e)"),
							*/
							@NamedQuery(name=TipologDaco.QueryGetAllDacoPaginable, 
							query="SELECT t FROM TipologDaco t "),
							
							@NamedQuery(name=TipologDaco.QueryGetAllDacoPaginableCount, 
							query="SELECT COUNT(*) FROM TipologDaco t "),
				
						})
public class TipologDaco implements Serializable {
	public static final String QueryGetAllDaco = "TipologDaco.getAllDaco";
	public static final String QueryGetAllDacoPaginable = "TipologDaco.QueryGetAllDacoPaginable";
	public static final String QueryGetAllDacoPaginableCount = "TipologDaco.QueryGetAllDacoPaginableCount";
	public static final String QueryGetDacoProfile = "QueryGetDacoProfile";
	public static final String QueryGetDacoProfileFunction = "QueryGetDacoProfileFunction";
	public static final String QueryGetDacoProfileMaxiProfile = "QueryGetDacoProfileMaxiProfile";
	public static final String QueryGetDacoProfileMaxiProfileStaging = "QueryGetDacoProfileMaxiProfileStaging";	
	
	public static final String QueryGetDacoExcludedStaging = "QueryGetDacoExcludedStaging";	
	public static final String QueryGetDacoExcludedStorage = "QueryGetDacoExcludedStorage";
	
	private static final long serialVersionUID = 1L; 

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_VOCE_DACO")
	private Long idVoceDaco;

	@Column(name="CODICE_DACO")
	private String codiceDaco;

	@Column(name="CONTO_CONTABILE")
	private String contoContabile;

	@Column(name="CONTO_CONTABILE_DESC")
	private String contoContabileDesc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	private String descrizione;

	//bi-directional many-to-one association to StorageAbilitazioniDaco
	@OneToMany(mappedBy="tipologDaco")
	private List<StorageAbilitazioniDaco> storageAbilitazioniDacos;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologDaco")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to TipologUtentiServizi
	@OneToMany(mappedBy="tipologDaco")
	private List<TipologUtentiServizi> tipologUtentiServizis;

	//bi-directional many-to-one association to AbbinamentoVociDacoServizi
	@OneToMany(mappedBy="tipologDaco")
	private List<AbbinamentoVociDacoServizi> abbinamentoVociDacoServizis;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologDaco")
	private List<StagingAggRilievi> stagingAggRilievis;

	public TipologDaco() {
	}

	public Long getIdVoceDaco() {
		return this.idVoceDaco;
	}

	public void setIdVoceDaco(Long idVoceDaco) {
		this.idVoceDaco = idVoceDaco;
	}

	public String getCodiceDaco() {
		return this.codiceDaco;
	}

	public void setCodiceDaco(String codiceDaco) {
		this.codiceDaco = codiceDaco;
	}

	public String getContoContabile() {
		return this.contoContabile;
	}

	public void setContoContabile(String contoContabile) {
		this.contoContabile = contoContabile;
	}

	public String getContoContabileDesc() {
		return this.contoContabileDesc;
	}

	public void setContoContabileDesc(String contoContabileDesc) {
		this.contoContabileDesc = contoContabileDesc;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataInizioValidita() {
		return this.dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<StorageAbilitazioniDaco> getStorageAbilitazioniDacos() {
		return this.storageAbilitazioniDacos;
	}

	public void setStorageAbilitazioniDacos(List<StorageAbilitazioniDaco> storageAbilitazioniDacos) {
		this.storageAbilitazioniDacos = storageAbilitazioniDacos;
	}

	public StorageAbilitazioniDaco addStorageAbilitazioniDaco(StorageAbilitazioniDaco storageAbilitazioniDaco) {
		getStorageAbilitazioniDacos().add(storageAbilitazioniDaco);
		storageAbilitazioniDaco.setTipologDaco(this);

		return storageAbilitazioniDaco;
	}

	public StorageAbilitazioniDaco removeStorageAbilitazioniDaco(StorageAbilitazioniDaco storageAbilitazioniDaco) {
		getStorageAbilitazioniDacos().remove(storageAbilitazioniDaco);
		storageAbilitazioniDaco.setTipologDaco(null);

		return storageAbilitazioniDaco;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setTipologDaco(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setTipologDaco(null);

		return storageRilievi;
	}

	public List<TipologUtentiServizi> getTipologUtentiServizis() {
		return this.tipologUtentiServizis;
	}

	public void setTipologUtentiServizis(List<TipologUtentiServizi> tipologUtentiServizis) {
		this.tipologUtentiServizis = tipologUtentiServizis;
	}

	public TipologUtentiServizi addTipologUtentiServizi(TipologUtentiServizi tipologUtentiServizi) {
		getTipologUtentiServizis().add(tipologUtentiServizi);
		tipologUtentiServizi.setTipologDaco(this);

		return tipologUtentiServizi;
	}

	public TipologUtentiServizi removeTipologUtentiServizi(TipologUtentiServizi tipologUtentiServizi) {
		getTipologUtentiServizis().remove(tipologUtentiServizi);
		tipologUtentiServizi.setTipologDaco(null);

		return tipologUtentiServizi;
	}

	public List<AbbinamentoVociDacoServizi> getAbbinamentoVociDacoServizis() {
		return this.abbinamentoVociDacoServizis;
	}

	public void setAbbinamentoVociDacoServizis(List<AbbinamentoVociDacoServizi> abbinamentoVociDacoServizis) {
		this.abbinamentoVociDacoServizis = abbinamentoVociDacoServizis;
	}

	public AbbinamentoVociDacoServizi addAbbinamentoVociDacoServizi(AbbinamentoVociDacoServizi abbinamentoVociDacoServizi) {
		getAbbinamentoVociDacoServizis().add(abbinamentoVociDacoServizi);
		abbinamentoVociDacoServizi.setTipologDaco(this);

		return abbinamentoVociDacoServizi;
	}

	public AbbinamentoVociDacoServizi removeAbbinamentoVociDacoServizi(AbbinamentoVociDacoServizi abbinamentoVociDacoServizi) {
		getAbbinamentoVociDacoServizis().remove(abbinamentoVociDacoServizi);
		abbinamentoVociDacoServizi.setTipologDaco(null);

		return abbinamentoVociDacoServizi;
	}

	public List<StagingAggRilievi> getStagingAggRilievis() {
		return this.stagingAggRilievis;
	}

	public void setStagingAggRilievis(List<StagingAggRilievi> stagingAggRilievis) {
		this.stagingAggRilievis = stagingAggRilievis;
	}

	public StagingAggRilievi addStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().add(stagingAggRilievi);
		stagingAggRilievi.setTipologDaco(this);

		return stagingAggRilievi;
	}

	public StagingAggRilievi removeStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().remove(stagingAggRilievi);
		stagingAggRilievi.setTipologDaco(null);

		return stagingAggRilievi;
	}

}