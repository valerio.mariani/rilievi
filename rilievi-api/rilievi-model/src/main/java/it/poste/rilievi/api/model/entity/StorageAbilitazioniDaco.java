package it.poste.rilievi.api.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the storage_abilitazioni_daco database table.
 * 
 */
@Entity
@Table(name="storage_abilitazioni_daco")


@NamedQueries({
	@NamedQuery(name=StorageAbilitazioniDaco.CheckDacoProfile, query="SELECT t FROM StorageAbilitazioniDaco t WHERE t.tipologDaco.idVoceDaco = :idVoceDaco AND t.anagProfili.idProfilo = :profile "
								+ "AND (t.tipologDaco.dataFineValidita IS NULL OR :validantionDate BETWEEN t.tipologDaco.dataInizioValidita and t.tipologDaco.dataFineValidita ) ")
})
public class StorageAbilitazioniDaco implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String CheckDacoProfile = "CheckDacoProfile";
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ABILITAZIONE")
	private Long idAbilitazione;

	//bi-directional many-to-one association to AnagProfili
	@ManyToOne
	@JoinColumn(name="RIF_PROFILO")
	private AnagProfili anagProfili;	
	
	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_VOCE_DACO")
	private TipologDaco tipologDaco;

	public StorageAbilitazioniDaco() {
	}

	public Long getIdAbilitazione() {
		return this.idAbilitazione;
	}

	public void setIdAbilitazione(Long idAbilitazione) {
		this.idAbilitazione = idAbilitazione;
	}


	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public AnagProfili getAnagProfili() {
		return anagProfili;
	}

	public void setAnagProfili(AnagProfili anagProfili) {
		this.anagProfili = anagProfili;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

}