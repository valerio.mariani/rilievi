package it.poste.rilievi.api.model.dto;

import java.time.LocalDate;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ExtractionDatesDTO {
	
	private LocalDate fixedDate;
	
	private LocalDate closingDate;

	public LocalDate getFixedDate() {
		return fixedDate;
	}

	public void setFixedDate(LocalDate fixedDate) {
		this.fixedDate = fixedDate;
	}

	public LocalDate getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(LocalDate closingDate) {
		this.closingDate = closingDate;
	}

	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	
}
