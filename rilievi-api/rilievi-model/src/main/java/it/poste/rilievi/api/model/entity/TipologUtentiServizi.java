package it.poste.rilievi.api.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_utenti_servizi database table.
 * 
 */
@Entity
@Table(name="tipolog_utenti_servizi")
/*@NamedQueries({
	@NamedQuery(name=TipologUtentiServizi.CheckDacoProfile, query="SELECT t FROM TipologUtentiServizi t JOIN t.anagProfiliAbilitazione apa "
															+ "WHERE t.tipologDaco.idVoceDaco = :idVoceDaco AND ap.idProfilo = :profile  AND apa.tipologFunzioni.idFunzione = :functionId "
															+ "AND (t.tipologDaco.dataFineValidita IS NULL OR :validantionDate BETWEEN t.tipologDaco.dataInizioValidita and t.tipologDaco.dataFineValidita ) ")
})*/
public class TipologUtentiServizi implements Serializable {
	private static final long serialVersionUID = 1L;

	//public static final String CheckDacoProfile = "CheckDacoProfile";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_UTENTE_SERVIZIO")
	private Long idUtenteServizio;

	//bi-directional many-to-one association to AnagProfiliAbilitazione
	@ManyToOne
	@JoinColumn(name="RIF_ABILITAZIONE")
	private AnagProfiliAbilitazione anagProfiliAbilitazione;

	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_VOCE_DACO")
	private TipologDaco tipologDaco;

	public TipologUtentiServizi() {
	}

	public Long getIdUtenteServizio() {
		return this.idUtenteServizio;
	}

	public void setIdUtenteServizio(Long idUtenteServizio) {
		this.idUtenteServizio = idUtenteServizio;
	}

	public AnagProfiliAbilitazione getAnagProfiliAbilitazione() {
		return this.anagProfiliAbilitazione;
	}

	public void setAnagProfiliAbilitazione(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		this.anagProfiliAbilitazione = anagProfiliAbilitazione;
	}

	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

}