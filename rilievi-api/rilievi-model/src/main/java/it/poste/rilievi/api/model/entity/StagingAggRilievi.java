package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the staging_agg_rilievi database table.
 * 
 */
@Entity
@Table(name="staging_agg_rilievi")
@NamedQuery(name="StagingAggRilievi.findAll", query="SELECT s FROM StagingAggRilievi s")
public class StagingAggRilievi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_AGG")
	private Long idAgg;

	@Column(name="CREDITI_EURO")
	private BigDecimal creditiEuro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ANNULLAMENTO")
	private Date dataAnnullamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COGE")
	private Date dataCoge;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COMPETENZA")
	private Date dataCompetenza;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INSERIMENTO")
	private Date dataInserimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INVIO_SRC")
	private Date dataInvioSrc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOL_CUMUL")
	private Date dataRegolCumul;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOLARIZZAZIONE")
	private Date dataRegolarizzazione;

	@Column(name="DEBITI_EURO")
	private BigDecimal debitiEuro;

	private String descrizione;

	@Column(name="FLAG_ANNULLATO")
	private String flagAnnullato;

	@Column(name="FLAG_REGOLARIZZATO")
	private String flagRegolarizzato;

	@Column(name="FLAG_RIALLINEATO")
	private String flagRiallineato;

	@Column(name="FLAG_TIPO_CHIUSURA")
	private String flagTipoChiusura;

	@Column(name="FLAG_VALIDATA")
	private byte flagValidata;

	private String motivo;

	@Column(name="NUM_RILIEVO")
	private int numRilievo;

	@Column(name="REGOLARIZZATO_DA_UTENTE")
	private String regolarizzatoDaUtente;

	@Column(name="RIF_RILIEVI")
	private int rifRilievi;

	@Column(name="RIF_SERVIZIO")
	private int rifServizio;

	@Column(name="STATUS_RIALLINEATO")
	private String statusRiallineato;

	@Column(name="UTENTE_INSERIMENTO")
	private String utenteInserimento;

	//bi-directional many-to-one association to AnagProfili
	@ManyToOne
	@JoinColumn(name="RIF_PROFILO")
	private AnagProfili anagProfili;


	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVO_PADRE")
	private StorageRilievi storageRilievi2;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVO_FIGLIO")
	private StorageRilievi storageRilievi3;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE")
	private TipologCausale tipologCausale1;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE_PRECEDENTE")
	private TipologCausale tipologCausale2;

	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_VOCE_DACO")
	private TipologDaco tipologDaco;

	//bi-directional many-to-one association to TipologFiliali
	@ManyToOne
	@JoinColumn(name="RIF_FILIALE")
	private TipologFiliali tipologFiliali;

	//bi-directional many-to-one association to TipologStatoRilievo
	@ManyToOne
	@JoinColumn(name="RIF_STATO_RILIEVO")
	private TipologStatoRilievo tipologStatoRilievo;

	//bi-directional many-to-one association to TipologUffici
	@ManyToOne
	@JoinColumn(name="RIF_FRAZIONARIO")
	private TipologUffici tipologUffici;

	public StagingAggRilievi() {
	}

	public Long getIdAgg() {
		return this.idAgg;
	}

	public void setIdAgg(Long idAgg) {
		this.idAgg = idAgg;
	}

	public BigDecimal getCreditiEuro() {
		return this.creditiEuro;
	}

	public void setCreditiEuro(BigDecimal creditiEuro) {
		this.creditiEuro = creditiEuro;
	}

	public Date getDataAnnullamento() {
		return this.dataAnnullamento;
	}

	public void setDataAnnullamento(Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

	public Date getDataCoge() {
		return this.dataCoge;
	}

	public void setDataCoge(Date dataCoge) {
		this.dataCoge = dataCoge;
	}

	public Date getDataCompetenza() {
		return this.dataCompetenza;
	}

	public void setDataCompetenza(Date dataCompetenza) {
		this.dataCompetenza = dataCompetenza;
	}

	public Date getDataInserimento() {
		return this.dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataInvioSrc() {
		return this.dataInvioSrc;
	}

	public void setDataInvioSrc(Date dataInvioSrc) {
		this.dataInvioSrc = dataInvioSrc;
	}

	public Date getDataRegolCumul() {
		return this.dataRegolCumul;
	}

	public void setDataRegolCumul(Date dataRegolCumul) {
		this.dataRegolCumul = dataRegolCumul;
	}

	public Date getDataRegolarizzazione() {
		return this.dataRegolarizzazione;
	}

	public void setDataRegolarizzazione(Date dataRegolarizzazione) {
		this.dataRegolarizzazione = dataRegolarizzazione;
	}

	public BigDecimal getDebitiEuro() {
		return this.debitiEuro;
	}

	public void setDebitiEuro(BigDecimal debitiEuro) {
		this.debitiEuro = debitiEuro;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getFlagAnnullato() {
		return this.flagAnnullato;
	}

	public void setFlagAnnullato(String flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	public String getFlagRegolarizzato() {
		return this.flagRegolarizzato;
	}

	public void setFlagRegolarizzato(String flagRegolarizzato) {
		this.flagRegolarizzato = flagRegolarizzato;
	}

	public String getFlagRiallineato() {
		return this.flagRiallineato;
	}

	public void setFlagRiallineato(String flagRiallineato) {
		this.flagRiallineato = flagRiallineato;
	}

	public String getFlagTipoChiusura() {
		return this.flagTipoChiusura;
	}

	public void setFlagTipoChiusura(String flagTipoChiusura) {
		this.flagTipoChiusura = flagTipoChiusura;
	}

	public byte getFlagValidata() {
		return this.flagValidata;
	}

	public void setFlagValidata(byte flagValidata) {
		this.flagValidata = flagValidata;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public int getNumRilievo() {
		return this.numRilievo;
	}

	public void setNumRilievo(int numRilievo) {
		this.numRilievo = numRilievo;
	}

	public String getRegolarizzatoDaUtente() {
		return this.regolarizzatoDaUtente;
	}

	public void setRegolarizzatoDaUtente(String regolarizzatoDaUtente) {
		this.regolarizzatoDaUtente = regolarizzatoDaUtente;
	}

	public int getRifRilievi() {
		return this.rifRilievi;
	}

	public void setRifRilievi(int rifRilievi) {
		this.rifRilievi = rifRilievi;
	}

	public int getRifServizio() {
		return this.rifServizio;
	}

	public void setRifServizio(int rifServizio) {
		this.rifServizio = rifServizio;
	}

	public String getStatusRiallineato() {
		return this.statusRiallineato;
	}

	public void setStatusRiallineato(String statusRiallineato) {
		this.statusRiallineato = statusRiallineato;
	}

	public String getUtenteInserimento() {
		return this.utenteInserimento;
	}

	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}

	public AnagProfili getAnagProfili() {
		return this.anagProfili;
	}

	public void setAnagProfili(AnagProfili anagProfili) {
		this.anagProfili = anagProfili;
	}

	public StorageRilievi getStorageRilievi2() {
		return this.storageRilievi2;
	}

	public void setStorageRilievi2(StorageRilievi storageRilievi2) {
		this.storageRilievi2 = storageRilievi2;
	}

	public StorageRilievi getStorageRilievi3() {
		return this.storageRilievi3;
	}

	public void setStorageRilievi3(StorageRilievi storageRilievi3) {
		this.storageRilievi3 = storageRilievi3;
	}

	public TipologCausale getTipologCausale1() {
		return this.tipologCausale1;
	}

	public void setTipologCausale1(TipologCausale tipologCausale1) {
		this.tipologCausale1 = tipologCausale1;
	}

	public TipologCausale getTipologCausale2() {
		return this.tipologCausale2;
	}

	public void setTipologCausale2(TipologCausale tipologCausale2) {
		this.tipologCausale2 = tipologCausale2;
	}

	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

	public TipologFiliali getTipologFiliali() {
		return this.tipologFiliali;
	}

	public void setTipologFiliali(TipologFiliali tipologFiliali) {
		this.tipologFiliali = tipologFiliali;
	}

	public TipologStatoRilievo getTipologStatoRilievo() {
		return this.tipologStatoRilievo;
	}

	public void setTipologStatoRilievo(TipologStatoRilievo tipologStatoRilievo) {
		this.tipologStatoRilievo = tipologStatoRilievo;
	}

	public TipologUffici getTipologUffici() {
		return this.tipologUffici;
	}

	public void setTipologUffici(TipologUffici tipologUffici) {
		this.tipologUffici = tipologUffici;
	}

}