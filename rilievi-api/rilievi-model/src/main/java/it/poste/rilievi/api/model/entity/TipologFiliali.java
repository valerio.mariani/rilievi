package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tipolog_filiali database table.
 * 
 */
@Entity
@Table(name="tipolog_filiali")
@NamedQueries({
	@NamedQuery(name = TipologFiliali.GetAllBranches, query = "SELECT t FROM TipologFiliali t ")
	})
public class TipologFiliali implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GetAllBranches = "GetAllBranches";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_FILIALE")
	private Long idFiliale;

	@Column(name="CODICE_FILIALE")
	private String codiceFiliale;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Column(name="DESCR_FILIALE")
	private String descrFiliale;

	private String progr;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologFiliali")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to StorageRilieviMaxiDettaglio
	@OneToMany(mappedBy="tipologFiliali")
	private List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios;

	//bi-directional many-to-one association to TipologUffici
	@OneToMany(mappedBy="tipologFiliali")
	private List<TipologUffici> tipologUfficis;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologFiliali")
	private List<StagingAggRilievi> stagingAggRilievis;

	public TipologFiliali() {
	}

	public Long getIdFiliale() {
		return this.idFiliale;
	}

	public void setIdFiliale(Long idFiliale) {
		this.idFiliale = idFiliale;
	}

	public String getCodiceFiliale() {
		return this.codiceFiliale;
	}

	public void setCodiceFiliale(String codiceFiliale) {
		this.codiceFiliale = codiceFiliale;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataInizioValidita() {
		return this.dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public String getDescrFiliale() {
		return this.descrFiliale;
	}

	public void setDescrFiliale(String descrFiliale) {
		this.descrFiliale = descrFiliale;
	}

	public String getProgr() {
		return this.progr;
	}

	public void setProgr(String progr) {
		this.progr = progr;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setTipologFiliali(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setTipologFiliali(null);

		return storageRilievi;
	}

	public List<StorageRilieviMaxiDettaglio> getStorageRilieviMaxiDettaglios() {
		return this.storageRilieviMaxiDettaglios;
	}

	public void setStorageRilieviMaxiDettaglios(List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios) {
		this.storageRilieviMaxiDettaglios = storageRilieviMaxiDettaglios;
	}

	public StorageRilieviMaxiDettaglio addStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().add(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologFiliali(this);

		return storageRilieviMaxiDettaglio;
	}

	public StorageRilieviMaxiDettaglio removeStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().remove(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologFiliali(null);

		return storageRilieviMaxiDettaglio;
	}

	public List<TipologUffici> getTipologUfficis() {
		return this.tipologUfficis;
	}

	public void setTipologUfficis(List<TipologUffici> tipologUfficis) {
		this.tipologUfficis = tipologUfficis;
	}

	public TipologUffici addTipologUffici(TipologUffici tipologUffici) {
		getTipologUfficis().add(tipologUffici);
		tipologUffici.setTipologFiliali(this);

		return tipologUffici;
	}

	public TipologUffici removeTipologUffici(TipologUffici tipologUffici) {
		getTipologUfficis().remove(tipologUffici);
		tipologUffici.setTipologFiliali(null);

		return tipologUffici;
	}

	public List<StagingAggRilievi> getStagingAggRilievis() {
		return this.stagingAggRilievis;
	}

	public void setStagingAggRilievis(List<StagingAggRilievi> stagingAggRilievis) {
		this.stagingAggRilievis = stagingAggRilievis;
	}

	public StagingAggRilievi addStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().add(stagingAggRilievi);
		stagingAggRilievi.setTipologFiliali(this);

		return stagingAggRilievi;
	}

	public StagingAggRilievi removeStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().remove(stagingAggRilievi);
		stagingAggRilievi.setTipologFiliali(null);

		return stagingAggRilievi;
	}

}