package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the staging_daco_escludere_maxi_rilievi database table.
 * 
 */
@Entity
@Table(name="staging_daco_escludere_maxi_rilievi")
@NamedQueries({
	@NamedQuery(name=StagingDacoEscludereMaxiRilievi.RemoveAll, query="Delete FROM StagingDacoEscludereMaxiRilievi s")
	})
public class StagingDacoEscludereMaxiRilievi implements Serializable {
	private static final long serialVersionUID = 1L;

	public final static String RemoveAll = "RemoveAllEscluded";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_STAG_VOCE_DACO_ESCL_MAXI_RILIEVI")
	private Long idStagVoceDacoEsclMaxiRilievi;

	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_CODICE_DACO")
	private TipologDaco tipologDaco;

	public StagingDacoEscludereMaxiRilievi() {
	}

	public Long getIdStagVoceDacoEsclMaxiRilievi() {
		return this.idStagVoceDacoEsclMaxiRilievi;
	}

	public void setIdStagVoceDacoEsclMaxiRilievi(Long idStagVoceDacoEsclMaxiRilievi) {
		this.idStagVoceDacoEsclMaxiRilievi = idStagVoceDacoEsclMaxiRilievi;
	}

	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

}