package it.poste.rilievi.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class Transaction {

	private String sysDate;

	private Integer operation;

	private Integer counter;

	public String getSysDate() {
		return sysDate;
	}

	public void setSysDate(String sysDate) {
		this.sysDate = sysDate;
	}

	public Integer getOperation() {
		return operation;
	}

	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
