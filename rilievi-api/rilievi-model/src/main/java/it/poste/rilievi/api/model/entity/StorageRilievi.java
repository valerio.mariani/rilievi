package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_rilievi database table.
 * 
 */
@Entity
@Table(name="storage_rilievi")
@NamedQueries({
@NamedQuery(name="StorageRilievi.findAll", query="SELECT s FROM StorageRilievi s"),
@NamedQuery(name=StorageRilievi.UpdateEvaluationById, query="UPDATE StorageRilievi SET dataRegolarizzazione = :dataRegolarizzazione, regolarizzatoDaUtente = :regolarizzatoDaUtente, flagRegolarizzato = :flagRegolarizzato WHERE idRilievi = :idRilievi")
})
public class StorageRilievi implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FLAG_ANNULLATO = "1";
	public static final String FLAG_NON_ANNULLATO = "0";
	
	public static final String FLAG_RIALLINEATO = "1";
	public static final String FLAG_NON_RIALLINEATO = "0";
	
	public static final String STATUS_RIALLINEATO = "0";
	public static final String STATUS_NON_RIALLINEATO = "1";
	
	public static final String FLAG_REGOLARIZZATO = "1";
	public static final String FLAG_NON_REGOLARIZZATO = "0";
	
	public static final String UpdateEvaluationById = "UpdateEvaluationById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RILIEVI")
	private Long idRilievi;

	@Column(name="CREDITI_EURO")
	private BigDecimal creditiEuro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ANNULLAMENTO")
	private Date dataAnnullamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COGE")
	private Date dataCoge;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COMPETENZA")
	private Date dataCompetenza;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INSERIMENTO")
	private Date dataInserimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INVIO_SRC")
	private Date dataInvioSrc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOL_CUMUL")
	private Date dataRegolCumul;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOLARIZZAZIONE")
	private Date dataRegolarizzazione;

	@Column(name="DEBITI_EURO")
	private BigDecimal debitiEuro;

	private String descrizione;

	@Column(name="FLAG_ANNULLATO")
	private String flagAnnullato;

	@Column(name="FLAG_REGOLARIZZATO")
	private String flagRegolarizzato;

	@Column(name="FLAG_RIALLINEATO")
	private String flagRiallineato;

	private String motivo;

	@Column(name="NUM_RILIEVO")
	private Integer numRilievo;

	@Column(name="REGOLARIZZATO_DA_UTENTE")
	private String regolarizzatoDaUtente;

	@Column(name="RIF_RILIEVO_FIGLIO")
	private Long rifRilievoFiglio;

	@Column(name="RIF_RILIEVO_PADRE")
	private Long rifRilievoPadre;

	@Column(name="STATUS_RIALLINEATO")
	private String statusRiallineato;

	@Column(name="UTENTE_INSERIMENTO")
	private String utenteInserimento;

	//bi-directional many-to-one association to StorageAllegati
	@OneToMany(mappedBy="storageRilievi", cascade = CascadeType.PERSIST)
	private List<StorageAllegati> storageAllegatis = new ArrayList<StorageAllegati>();

	//bi-directional many-to-one association to StorageNoteAggiuntive
	@OneToMany(mappedBy="storageRilievi")
	private List<StorageNoteAggiuntive> storageNoteAggiuntives = new ArrayList<StorageNoteAggiuntive>();

	//bi-directional many-to-one association to AnagProfili
	@ManyToOne
	@JoinColumn(name="RIF_PROFILO")
	private AnagProfili anagProfili;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE")
	private TipologCausale tipologCausale1;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE_PRECEDENTE")
	private TipologCausale tipologCausale2;

	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_VOCE_DACO")
	private TipologDaco tipologDaco;

	//bi-directional many-to-one association to TipologFiliali
	@ManyToOne
	@JoinColumn(name="RIF_FILIALE")
	private TipologFiliali tipologFiliali;

	//bi-directional many-to-one association to TipologServizi
	@ManyToOne
	@JoinColumn(name="RIF_SERVIZIO")
	private TipologServizi tipologServizi;

	//bi-directional many-to-one association to TipologStatoRilievo
	@ManyToOne
	@JoinColumn(name="RIF_STATO_RILIEVO")
	private TipologStatoRilievo tipologStatoRilievo;

	//bi-directional many-to-one association to TipologUffici
	@ManyToOne
	@JoinColumn(name="RIF_FRAZIONARIO")
	private TipologUffici tipologUffici;

	//bi-directional many-to-one association to StorageRilieviMaxiDettaglio
	@OneToMany(mappedBy="storageRilievi")
	private List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios;

	//bi-directional many-to-one association to StorageRilieviMaxiTotale
	@OneToMany(mappedBy="storageRilievi")
	private List<StorageRilieviMaxiTotale> storageRilieviMaxiTotales;


	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="storageRilievi2")
	private List<StagingAggRilievi> stagingAggRilievis2;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="storageRilievi3")
	private List<StagingAggRilievi> stagingAggRilievis3;

//	//bi-directional many-to-one association to StagingProcessiSdp
//	@OneToMany(mappedBy="storageRilievi")
//	private List<StagingProcessiSdp> stagingProcessiSdps;

	//bi-directional many-to-one association to StorageArchRilievi
	@OneToMany(mappedBy="storageRilievi1")
	private List<StorageArchRilievi> storageArchRilievis1;

	//bi-directional many-to-one association to StorageArchRilievi
	@OneToMany(mappedBy="storageRilievi2")
	private List<StorageArchRilievi> storageArchRilievis2;

	public StorageRilievi() {
	}

	public Long getIdRilievi() {
		return this.idRilievi;
	}

	public void setIdRilievi(Long idRilievi) {
		this.idRilievi = idRilievi;
	}

	public BigDecimal getCreditiEuro() {
		return this.creditiEuro;
	}

	public void setCreditiEuro(BigDecimal creditiEuro) {
		this.creditiEuro = creditiEuro;
	}

	public Date getDataAnnullamento() {
		return this.dataAnnullamento;
	}

	public void setDataAnnullamento(Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

	public Date getDataCoge() {
		return this.dataCoge;
	}

	public void setDataCoge(Date dataCoge) {
		this.dataCoge = dataCoge;
	}

	public Date getDataCompetenza() {
		return this.dataCompetenza;
	}

	public void setDataCompetenza(Date dataCompetenza) {
		this.dataCompetenza = dataCompetenza;
	}

	public Date getDataInserimento() {
		return this.dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataInvioSrc() {
		return this.dataInvioSrc;
	}

	public void setDataInvioSrc(Date dataInvioSrc) {
		this.dataInvioSrc = dataInvioSrc;
	}

	public Date getDataRegolCumul() {
		return this.dataRegolCumul;
	}

	public void setDataRegolCumul(Date dataRegolCumul) {
		this.dataRegolCumul = dataRegolCumul;
	}

	public Date getDataRegolarizzazione() {
		return this.dataRegolarizzazione;
	}

	public void setDataRegolarizzazione(Date dataRegolarizzazione) {
		this.dataRegolarizzazione = dataRegolarizzazione;
	}

	public BigDecimal getDebitiEuro() {
		return this.debitiEuro;
	}

	public void setDebitiEuro(BigDecimal debitiEuro) {
		this.debitiEuro = debitiEuro;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getFlagAnnullato() {
		return this.flagAnnullato;
	}

	public void setFlagAnnullato(String flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	public String getFlagRegolarizzato() {
		return this.flagRegolarizzato;
	}

	public void setFlagRegolarizzato(String flagRegolarizzato) {
		this.flagRegolarizzato = flagRegolarizzato;
	}

	public String getFlagRiallineato() {
		return this.flagRiallineato;
	}

	public void setFlagRiallineato(String flagRiallineato) {
		this.flagRiallineato = flagRiallineato;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Integer getNumRilievo() {
		return this.numRilievo;
	}

	public void setNumRilievo(Integer numRilievo) {
		this.numRilievo = numRilievo;
	}

	public String getRegolarizzatoDaUtente() {
		return this.regolarizzatoDaUtente;
	}

	public void setRegolarizzatoDaUtente(String regolarizzatoDaUtente) {
		this.regolarizzatoDaUtente = regolarizzatoDaUtente;
	}

	public Long getRifRilievoFiglio() {
		return this.rifRilievoFiglio;
	}

	public void setRifRilievoFiglio(Long rifRilievoFiglio) {
		this.rifRilievoFiglio = rifRilievoFiglio;
	}

	public Long getRifRilievoPadre() {
		return this.rifRilievoPadre;
	}

	public void setRifRilievoPadre(Long rifRilievoPadre) {
		this.rifRilievoPadre = rifRilievoPadre;
	}

	public String getStatusRiallineato() {
		return this.statusRiallineato;
	}

	public void setStatusRiallineato(String statusRiallineato) {
		this.statusRiallineato = statusRiallineato;
	}

	public String getUtenteInserimento() {
		return this.utenteInserimento;
	}

	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}

	public List<StorageAllegati> getStorageAllegatis() {
		return this.storageAllegatis;
	}

	public void setStorageAllegatis(List<StorageAllegati> storageAllegatis) {
		this.storageAllegatis = storageAllegatis;
	}

	public StorageAllegati addStorageAllegati(StorageAllegati storageAllegati) {
		getStorageAllegatis().add(storageAllegati);
		storageAllegati.setStorageRilievi(this);

		return storageAllegati;
	}

	public StorageAllegati removeStorageAllegati(StorageAllegati storageAllegati) {
		getStorageAllegatis().remove(storageAllegati);
		storageAllegati.setStorageRilievi(null);

		return storageAllegati;
	}

	public List<StorageNoteAggiuntive> getStorageNoteAggiuntives() {
		return this.storageNoteAggiuntives;
	}

	public void setStorageNoteAggiuntives(List<StorageNoteAggiuntive> storageNoteAggiuntives) {
		this.storageNoteAggiuntives = storageNoteAggiuntives;
	}

	public StorageNoteAggiuntive addStorageNoteAggiuntive(StorageNoteAggiuntive storageNoteAggiuntive) {
		getStorageNoteAggiuntives().add(storageNoteAggiuntive);
		storageNoteAggiuntive.setStorageRilievi(this);

		return storageNoteAggiuntive;
	}

	public StorageNoteAggiuntive removeStorageNoteAggiuntive(StorageNoteAggiuntive storageNoteAggiuntive) {
		getStorageNoteAggiuntives().remove(storageNoteAggiuntive);
		storageNoteAggiuntive.setStorageRilievi(null);

		return storageNoteAggiuntive;
	}

	public AnagProfili getAnagProfili() {
		return this.anagProfili;
	}

	public void setAnagProfili(AnagProfili anagProfili) {
		this.anagProfili = anagProfili;
	}

	public TipologCausale getTipologCausale1() {
		return this.tipologCausale1;
	}

	public void setTipologCausale1(TipologCausale tipologCausale1) {
		this.tipologCausale1 = tipologCausale1;
	}

	public TipologCausale getTipologCausale2() {
		return this.tipologCausale2;
	}

	public void setTipologCausale2(TipologCausale tipologCausale2) {
		this.tipologCausale2 = tipologCausale2;
	}

	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

	public TipologFiliali getTipologFiliali() {
		return this.tipologFiliali;
	}

	public void setTipologFiliali(TipologFiliali tipologFiliali) {
		this.tipologFiliali = tipologFiliali;
	}

	public TipologServizi getTipologServizi() {
		return this.tipologServizi;
	}

	public void setTipologServizi(TipologServizi tipologServizi) {
		this.tipologServizi = tipologServizi;
	}

	public TipologStatoRilievo getTipologStatoRilievo() {
		return this.tipologStatoRilievo;
	}

	public void setTipologStatoRilievo(TipologStatoRilievo tipologStatoRilievo) {
		this.tipologStatoRilievo = tipologStatoRilievo;
	}

	public TipologUffici getTipologUffici() {
		return this.tipologUffici;
	}

	public void setTipologUffici(TipologUffici tipologUffici) {
		this.tipologUffici = tipologUffici;
	}

	public List<StorageRilieviMaxiDettaglio> getStorageRilieviMaxiDettaglios() {
		return this.storageRilieviMaxiDettaglios;
	}

	public void setStorageRilieviMaxiDettaglios(List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios) {
		this.storageRilieviMaxiDettaglios = storageRilieviMaxiDettaglios;
	}

	public StorageRilieviMaxiDettaglio addStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().add(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setStorageRilievi(this);

		return storageRilieviMaxiDettaglio;
	}

	public StorageRilieviMaxiDettaglio removeStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().remove(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setStorageRilievi(null);

		return storageRilieviMaxiDettaglio;
	}

	public List<StorageRilieviMaxiTotale> getStorageRilieviMaxiTotales() {
		return this.storageRilieviMaxiTotales;
	}

	public void setStorageRilieviMaxiTotales(List<StorageRilieviMaxiTotale> storageRilieviMaxiTotales) {
		this.storageRilieviMaxiTotales = storageRilieviMaxiTotales;
	}

	public StorageRilieviMaxiTotale addStorageRilieviMaxiTotale(StorageRilieviMaxiTotale storageRilieviMaxiTotale) {
		getStorageRilieviMaxiTotales().add(storageRilieviMaxiTotale);
		storageRilieviMaxiTotale.setStorageRilievi(this);

		return storageRilieviMaxiTotale;
	}

	public StorageRilieviMaxiTotale removeStorageRilieviMaxiTotale(StorageRilieviMaxiTotale storageRilieviMaxiTotale) {
		getStorageRilieviMaxiTotales().remove(storageRilieviMaxiTotale);
		storageRilieviMaxiTotale.setStorageRilievi(null);

		return storageRilieviMaxiTotale;
	}

	public List<StagingAggRilievi> getStagingAggRilievis2() {
		return this.stagingAggRilievis2;
	}

	public void setStagingAggRilievis2(List<StagingAggRilievi> stagingAggRilievis2) {
		this.stagingAggRilievis2 = stagingAggRilievis2;
	}

	public StagingAggRilievi addStagingAggRilievis2(StagingAggRilievi stagingAggRilievis2) {
		getStagingAggRilievis2().add(stagingAggRilievis2);
		stagingAggRilievis2.setStorageRilievi2(this);

		return stagingAggRilievis2;
	}

	public StagingAggRilievi removeStagingAggRilievis2(StagingAggRilievi stagingAggRilievis2) {
		getStagingAggRilievis2().remove(stagingAggRilievis2);
		stagingAggRilievis2.setStorageRilievi2(null);

		return stagingAggRilievis2;
	}

	public List<StagingAggRilievi> getStagingAggRilievis3() {
		return this.stagingAggRilievis3;
	}

	public void setStagingAggRilievis3(List<StagingAggRilievi> stagingAggRilievis3) {
		this.stagingAggRilievis3 = stagingAggRilievis3;
	}

	public StagingAggRilievi addStagingAggRilievis3(StagingAggRilievi stagingAggRilievis3) {
		getStagingAggRilievis3().add(stagingAggRilievis3);
		stagingAggRilievis3.setStorageRilievi3(this);

		return stagingAggRilievis3;
	}

	public StagingAggRilievi removeStagingAggRilievis3(StagingAggRilievi stagingAggRilievis3) {
		getStagingAggRilievis3().remove(stagingAggRilievis3);
		stagingAggRilievis3.setStorageRilievi3(null);

		return stagingAggRilievis3;
	}


	public List<StorageArchRilievi> getStorageArchRilievis1() {
		return this.storageArchRilievis1;
	}

	public void setStorageArchRilievis1(List<StorageArchRilievi> storageArchRilievis1) {
		this.storageArchRilievis1 = storageArchRilievis1;
	}

	public StorageArchRilievi addStorageArchRilievis1(StorageArchRilievi storageArchRilievis1) {
		getStorageArchRilievis1().add(storageArchRilievis1);
		storageArchRilievis1.setStorageRilievi1(this);

		return storageArchRilievis1;
	}

	public StorageArchRilievi removeStorageArchRilievis1(StorageArchRilievi storageArchRilievis1) {
		getStorageArchRilievis1().remove(storageArchRilievis1);
		storageArchRilievis1.setStorageRilievi1(null);

		return storageArchRilievis1;
	}

	public List<StorageArchRilievi> getStorageArchRilievis2() {
		return this.storageArchRilievis2;
	}

	public void setStorageArchRilievis2(List<StorageArchRilievi> storageArchRilievis2) {
		this.storageArchRilievis2 = storageArchRilievis2;
	}

	public StorageArchRilievi addStorageArchRilievis2(StorageArchRilievi storageArchRilievis2) {
		getStorageArchRilievis2().add(storageArchRilievis2);
		storageArchRilievis2.setStorageRilievi2(this);

		return storageArchRilievis2;
	}

	public StorageArchRilievi removeStorageArchRilievis2(StorageArchRilievi storageArchRilievis2) {
		getStorageArchRilievis2().remove(storageArchRilievis2);
		storageArchRilievis2.setStorageRilievi2(null);

		return storageArchRilievis2;
	}

}