package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the storage_processi_sdp database table.
 * 
 */
@Entity
@Table(name="storage_processi_sdp")
@NamedQueries({
@NamedQuery(name=StorageProcessiSdp.ProcessByNumOpeFractionalDeskAccountingDate, query="SELECT s FROM StorageProcessiSdp s WHERE s.numOperazione = :numOpe AND s.frazionario = :node AND s.numSportello = :desk AND DATE_FORMAT(s.dataContabile,'%Y-%m-%d') = :accountingDate"),
@NamedQuery(name=StorageProcessiSdp.ProcessesByNumOpeFractionalDeskAccountingDate, query="SELECT s FROM StorageProcessiSdp s WHERE s.numOperazione > :numOpe AND s.frazionario = :node AND s.numSportello = :desk AND DATE_FORMAT(s.dataContabile,'%Y-%m-%d') = :accountingDate ORDER BY s.numOperazione DESC"),
@NamedQuery(name=StorageProcessiSdp.UpdateRegularization, query="UPDATE StorageProcessiSdp SET flagValidita = :flagValidita, dataFineValidita = : dataFineValidita WHERE idProcSdp <= :idProcSdp"),
@NamedQuery(name=StorageProcessiSdp.DeleteProcessByIdProcSdp, query="DELETE FROM StorageProcessiSdp s WHERE idProcSdp = :idProcSdp"),
@NamedQuery(name=StorageProcessiSdp.ProcessByNumOpe, query="SELECT s FROM StorageProcessiSdp s WHERE s.numOperazione = :numOpe "
		+ " AND s.frazionario= :fractional AND s.dataContabile = :contabilizationDate AND s.numSportello = :branchNumber  ")

})
public class StorageProcessiSdp implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String ProcessByNumOpe = "ProcessByNumOpe";
	public static final String ProcessByNumOpeFractionalDeskAccountingDate = "ProcessByNumOpeFractionalDeskAccountingDate";
	public static final String ProcessesByNumOpeFractionalDeskAccountingDate = "ProcessesByNumOpeFractionalDeskAccountingDate";
	public static final String DeleteProcessByIdProcSdp = "DeleteProcessByIdProcSdp";
	public static final String UpdateRegularization = "UpdateRegularization";
	
	public static final String FLAG_VALIDO = "1";
	
	public static final String FLAG_NON_VALIDO = "0";
	
	public static final String FLAG_OPERAZIONE_COMPLETA = "1";
	
	public static final String FLAG_OPERAZIONE_NON_COMPLETA = "0";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PROC_SDP")
	private Long idProcSdp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CONTABILE")
	private Date dataContabile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INTERVENTO")
	private Date dataIntervento;

	@Column(name="FLAG_INTERVENTO_COMPLETO")
	private String flagInterventoCompleto;

	@Column(name="FLAG_VALIDITA")
	private String flagValidita;

	private String frazionario;

	@Column(name="LIV_RESTART")
	private int livRestart;

	@Column(name="NUM_OPERAZIONE")
	private Integer numOperazione;

	@Column(name="NUM_OPERAZIONE_ORIG")
	private Integer numOperazioneOrig;

	@Column(name="NUM_SPORTELLO")
	private int numSportello;

	@Column(name="STATO_OPERAZIONE")
	private String statoOperazione;

	@Column(name="UTENTE_SDP")
	private String utenteSdp;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	public StorageProcessiSdp() {
	}

	public Long getIdProcSdp() {
		return this.idProcSdp;
	}

	public void setIdProcSdp(Long idProcSdp) {
		this.idProcSdp = idProcSdp;
	}

	public Date getDataContabile() {
		return this.dataContabile;
	}

	public void setDataContabile(Date dataContabile) {
		this.dataContabile = dataContabile;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataIntervento() {
		return this.dataIntervento;
	}

	public void setDataIntervento(Date dataIntervento) {
		this.dataIntervento = dataIntervento;
	}

	public String getFlagInterventoCompleto() {
		return this.flagInterventoCompleto;
	}

	public void setFlagInterventoCompleto(String flagInterventoCompleto) {
		this.flagInterventoCompleto = flagInterventoCompleto;
	}

	public String getFlagValidita() {
		return this.flagValidita;
	}

	public void setFlagValidita(String flagValidita) {
		this.flagValidita = flagValidita;
	}

	public String getFrazionario() {
		return this.frazionario;
	}

	public void setFrazionario(String frazionario) {
		this.frazionario = frazionario;
	}

	public int getLivRestart() {
		return this.livRestart;
	}

	public void setLivRestart(int livRestart) {
		this.livRestart = livRestart;
	}

	public Integer getNumOperazione() {
		return this.numOperazione;
	}

	public void setNumOperazione(Integer numOperazione) {
		this.numOperazione = numOperazione;
	}

	public Integer getNumOperazioneOrig() {
		return this.numOperazioneOrig;
	}

	public void setNumOperazioneOrig(Integer numOperazioneOrig) {
		this.numOperazioneOrig = numOperazioneOrig;
	}

	public int getNumSportello() {
		return this.numSportello;
	}

	public void setNumSportello(int numSportello) {
		this.numSportello = numSportello;
	}

	public String getStatoOperazione() {
		return this.statoOperazione;
	}

	public void setStatoOperazione(String statoOperazione) {
		this.statoOperazione = statoOperazione;
	}

	public String getUtenteSdp() {
		return this.utenteSdp;
	}

	public void setUtenteSdp(String utenteSdp) {
		this.utenteSdp = utenteSdp;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

}