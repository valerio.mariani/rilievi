package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class EvaluationNoteCriteriaDTO extends PaginableSearchCriteria{
	
	private Long idnote;
	private EvaluationDTO evaluation;
	private String note;
	private String user;
	private LocalDateTime dateCreation;
	
	
	public Long getIdnote() {
		return idnote;
	}
	public void setIdnote(Long idnote) {
		this.idnote = idnote;
	}
	public EvaluationDTO getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(EvaluationDTO evaluation) {
		this.evaluation = evaluation;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public LocalDateTime getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	@Override
	void populateMapDtoEntity() {
		getFieldsMap().put("idnote", "idNota");
		getFieldsMap().put("note", "nota");
		getFieldsMap().put("user.code", "utente");
		getFieldsMap().put("dateCreation", "dataInserimento");	
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
