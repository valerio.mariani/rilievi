package it.poste.rilievi.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AppUserDTO {
	
	private AppProfileDTO profile;
	
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AppProfileDTO getProfile() {
		return profile;
	}

	public void setProfile(AppProfileDTO profile) {
		this.profile = profile;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	
}
