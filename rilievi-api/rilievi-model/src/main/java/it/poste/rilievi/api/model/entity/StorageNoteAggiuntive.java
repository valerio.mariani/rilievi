package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_note_aggiuntive database table.
 * 
 */
@Entity
@Table(name="storage_note_aggiuntive")

@NamedQueries(value = {	
	@NamedQuery(name="StorageNoteAggiuntive.findAll", query="SELECT s FROM StorageNoteAggiuntive s"),
	@NamedQuery(name=StorageNoteAggiuntive.GetEvaluationNote, query="SELECT t "
		     																		+ "FROM StorageNoteAggiuntive t"
		     																			+ " WHERE t.storageRilievi.idRilievi= :id "),
		     })

public class StorageNoteAggiuntive implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GetEvaluationNote = "StorageNoteAggiuntive.getEvaluationNote";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_NOTA")
	private Long idNota;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INSERIMENTO")
	private Date dataInserimento;

	private String nota;

	private String utente;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	public StorageNoteAggiuntive() {
	}

	public Long getIdNota() {
		return this.idNota;
	}

	public void setIdNota(Long idNota) {
		this.idNota = idNota;
	}

	public Date getDataInserimento() {
		return this.dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getNota() {
		return this.nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getUtente() {
		return this.utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

}