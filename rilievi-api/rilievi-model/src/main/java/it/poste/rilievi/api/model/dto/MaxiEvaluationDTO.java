package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class MaxiEvaluationDTO {
		
	private Long id;
	
	private LocalDateTime regularizationDate;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime mxrDate;
	
	private ReasonDTO reason;
	
	private Double totalAmount;
	
	private Integer totalEvaluation;
	
	private Double amount;
	
	private EvaluationDTO evaluation;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public LocalDateTime getRegularizationDate() {
		return regularizationDate;
	}

	public void setRegularizationDate(LocalDateTime regularizationDate) {
		this.regularizationDate = regularizationDate;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getMxrDate() {
		return mxrDate;
	}

	public void setMxrDate(LocalDateTime mxrDate) {
		this.mxrDate = mxrDate;
	}

	public ReasonDTO getReason() {
		return reason;
	}

	public void setReason(ReasonDTO reason) {
		this.reason = reason;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public EvaluationDTO getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(EvaluationDTO evaluation) {
		this.evaluation = evaluation;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalEvaluation() {
		return totalEvaluation;
	}

	public void setTotalEvaluation(Integer totalEvaluation) {
		this.totalEvaluation = totalEvaluation;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
