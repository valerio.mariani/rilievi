package it.poste.rilievi.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class OfficeDTO {

	private Long id;

	private String fractionalDescription;

	private String email;

	private String fractional;

	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFractionalDescription() {
		return fractionalDescription;
	}

	public void setFractionalDescription(String fractionalDescription) {
		this.fractionalDescription = fractionalDescription;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFractional() {
		return fractional;
	}

	public void setFractional(String fractional) {
		this.fractional = fractional;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
