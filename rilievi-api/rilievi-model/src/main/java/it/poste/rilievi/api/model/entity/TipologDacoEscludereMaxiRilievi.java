package it.poste.rilievi.api.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_daco_escludere_maxi_rilievi database table.
 * 
 */
@Entity
@Table(name="tipolog_daco_escludere_maxi_rilievi")
@NamedQuery(name="TipologDacoEscludereMaxiRilievi.findAll", query="SELECT t FROM TipologDacoEscludereMaxiRilievi t")
public class TipologDacoEscludereMaxiRilievi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_VOCE_DACO_ESCL_MAXI_RILIEVI")
	private Long idVoceDacoEsclMaxiRilievi;

	@Column(name="RIF_CODICE_DACO")
	private int rifCodiceDaco;

	public TipologDacoEscludereMaxiRilievi() {
	}

	public Long getIdVoceDacoEsclMaxiRilievi() {
		return this.idVoceDacoEsclMaxiRilievi;
	}

	public void setIdVoceDacoEsclMaxiRilievi(Long idVoceDacoEsclMaxiRilievi) {
		this.idVoceDacoEsclMaxiRilievi = idVoceDacoEsclMaxiRilievi;
	}

	public int getRifCodiceDaco() {
		return this.rifCodiceDaco;
	}

	public void setRifCodiceDaco(int rifCodiceDaco) {
		this.rifCodiceDaco = rifCodiceDaco;
	}

}