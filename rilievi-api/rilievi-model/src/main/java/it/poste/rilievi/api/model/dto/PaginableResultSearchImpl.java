package it.poste.rilievi.api.model.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;



public class PaginableResultSearchImpl<T> implements IPaginableResultSearch<T> {
	
	private int totalRows;
	
	private int totalPages;
	
	private Integer pageNumber;
	
	private Integer pageSize;
	
	private List<T> items = new ArrayList<T>();

	
	public PaginableResultSearchImpl(PaginableSearchCriteria criteria) {
		this.setPageNumber(criteria.getPageNumber()==null || criteria.getPageNumber().intValue()==0 ? 1 : criteria.getPageNumber());
		this.setPageSize(criteria.getPageSize());
	}
	
	public PaginableResultSearchImpl() {
	}
	
	@Override
	public int getTotalRows() {
		return totalRows;
	}

	@Override
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
		setTotalPages();
		
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
		setTotalPages();
	}

	@Override
	public List<T> getItems() {
		return items;
	}

	@Override
	public void setItems(List<T> items) {
		this.items = items;
		
	}

	@Override
	public void addItem(T item) {
		if( this.items == null ) {
			this.items = new ArrayList<T>();
		}
		this.items.add(item);
	}

	@Override
	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages() {		
		if( totalRows == 0 ) {
			this.totalPages = 0;
		}else
			if(pageSize == null || pageSize <= 0 ) {
				this.totalPages = 1;
			}else{
				this.totalPages = totalRows%pageSize == 0 ? totalRows/pageSize : totalRows/pageSize +1;
			}
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
