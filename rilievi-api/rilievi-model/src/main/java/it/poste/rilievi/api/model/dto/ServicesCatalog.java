package it.poste.rilievi.api.model.dto;

import java.util.HashMap;
import java.util.Map;

public enum ServicesCatalog {
	NONE("Nessuna",0),
	NEW_EVALUATION("Nuovo Rilievo",1),
	NEW_DACO("Nuova Daco",2),
	UPD_DACO("Aggiorna Daco",3),
	SEARCH_DACO("Ricerca Daco",4),
	SEARCH_AUDIT_LOG("Ricerca Log",5),
	GET_EVALUATION("Leggi Rilievo",6),
	GET_OFFICES("Leggi Uffici",7),
	GET_BRANCHES("Leggi Filiali",8),
	NEW_EXTRACTIONDATES("Nuove Date Estrazione",9),
	SEARCH_EXTRACTIONDATES("Leggi Date Estrazione",10),
	NEW_FUNCTION("Nuova Funzione",11),
	UPD_FUNCTION("Aggiorna Funzione",12),
	SEARCH_FUNCTION("Ricerca Funzione",13),
	NEW_FUNCTION_PARAM("Nuovo Parametro Funzione",14),
	GET_EVALUATION_MAXI("Recupera rilievi per regolarizzazione maxi",15),
	GET_ALL_PROFILE("Leggi Profili",16),
	NEW_PROFILE("Nuovo Profilo",17),
	SEARCH_FUNCTION_PARAM("Leggi Lista Parametri Funzione",18),
	UPD_FUNCTION_PARAM("Aggiorna Parametro Funzione",19),
	SET_EVALUATION_MAXI("Imposta regolarizzazione maxi rilievi",20),
	GET_FUNCTION_BY_PROFILE("Leggi Funzioni Associate Al Profilo",21),
	GET_OTHER_FUNCTION_BY_PROFILE("Leggi Funzioni Non Associate Al Profilo",22),
	ADD_FUNCTION_PROFILE("Aggiungi Funzione Al Profilo",23),
	NEW_EVALUATION_NOTE("Aggiungi Nota Rilievo",24),
    GET_EVALUATION_NOTE("Leggi Note Rilievo",25),
    DELETE_EVALUATION_NOTE("Cancella Nota rilievo",26),
	GET_EVALUATION_ATTACHMENT("Leggi Allegati Rilievo",27),	
	DELETE_EVALUATION_ATTACHMENT("Cancella Allegato Rilievo",28),
	INSERT_EVALUATION_ATTACHMENT("Aggiungi Allegato Rilievo",29),
    CHANGE_EVALUATION_FRACTIONAL("Cambio Frazionario Rilievo",30),
    DELETE_EVALUATION("Elimina Rilievo",31),
    SET_LOSS_STATE("Connotazione Stato Perdita",32),
	UNDO_LOSS_STATE("Annnulla Connotazione Stato Perdita",33),
	UNDO_RC_REASON("Annulla Trasformazione causale RC",34),
	SET_RC_REASON("Trasforma in casusale RC",35),
	SET_EVALUATION_DESCRIPTION("Aggiorna Descrizione Rilievo",36),
	RECLASSIFICATION_DACO("Riclassificazione Daco",37),
	REGULARIZATION("Regolarizza Rilievo",38),
	GET_FUNCTION_BY_PROFILE_CODE("Leggi Funzioni Profilo",39),
	FILE_EXPORT("Esporta file excel",42),
	UNDO_REGULARIZATION("Annulla Regolarizzazione",44),
	GET_EVALUTATIONS("Leggi Lista Rilievi",46),
	UPDATE_EVALUTATION_DETAIL("Aggiornato Rilievo: Gestione Dettaglio Rilievo",47);

	private String description;
	
	private Long code;
	
	ServicesCatalog(String descr, long code) {
		this.description = descr;
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public Long getCode() {
		return code;
	}

	private static final Map<String,ServicesCatalog> map;
    static {
        map = new HashMap<String,ServicesCatalog>();
        for (ServicesCatalog v : ServicesCatalog.values()) {
            map.put(""+v.getCode(), v);
        }
    }
    public static ServicesCatalog findByCode(String code) {
        return map.get(code);
    }
}
