package it.poste.rilievi.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ReasonDTO {
	
	private Long id;
	
	private String code;
	
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
