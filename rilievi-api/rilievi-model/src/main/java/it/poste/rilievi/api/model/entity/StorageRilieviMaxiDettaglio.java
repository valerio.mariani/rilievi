package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_rilievi_maxi_dettaglio database table.
 * 
 */
@Entity
@Table(name="storage_rilievi_maxi_dettaglio")
@NamedQuery(name="StorageRilieviMaxiDettaglio.findAll", query="SELECT s FROM StorageRilieviMaxiDettaglio s")
public class StorageRilieviMaxiDettaglio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RILIEVI_MAXI_DETTAGLIO")
	private Long idRilieviMaxiDettaglio;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_LIM")
	private Date dataLim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_MXR")
	private Date dataMxr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REG")
	private Date dataReg;

	@Column(name="ID_RILIEVI_MAXI_TOTALE")
	private Long idRilieviMaxiTotale;

	private BigDecimal importo;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE")
	private TipologCausale tipologCausale;

	//bi-directional many-to-one association to TipologFiliali
	@ManyToOne
	@JoinColumn(name="RIF_FILIALE")
	private TipologFiliali tipologFiliali;

	//bi-directional many-to-one association to TipologUffici
	@ManyToOne
	@JoinColumn(name="RIF_UFFICIO")
	private TipologUffici tipologUffici;

	public StorageRilieviMaxiDettaglio() {
	}

	public Long getIdRilieviMaxiDettaglio() {
		return this.idRilieviMaxiDettaglio;
	}

	public void setIdRilieviMaxiDettaglio(Long idRilieviMaxiDettaglio) {
		this.idRilieviMaxiDettaglio = idRilieviMaxiDettaglio;
	}

	public Date getDataLim() {
		return this.dataLim;
	}

	public void setDataLim(Date dataLim) {
		this.dataLim = dataLim;
	}

	public Date getDataMxr() {
		return this.dataMxr;
	}

	public void setDataMxr(Date dataMxr) {
		this.dataMxr = dataMxr;
	}

	public Date getDataReg() {
		return this.dataReg;
	}

	public void setDataReg(Date dataReg) {
		this.dataReg = dataReg;
	}

	public Long getIdRilieviMaxiTotale() {
		return this.idRilieviMaxiTotale;
	}

	public void setIdRilieviMaxiTotale(Long idRilieviMaxiTotale) {
		this.idRilieviMaxiTotale = idRilieviMaxiTotale;
	}

	public BigDecimal getImporto() {
		return this.importo;
	}

	public void setImporto(BigDecimal importo) {
		this.importo = importo;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

	public TipologCausale getTipologCausale() {
		return this.tipologCausale;
	}

	public void setTipologCausale(TipologCausale tipologCausale) {
		this.tipologCausale = tipologCausale;
	}

	public TipologFiliali getTipologFiliali() {
		return this.tipologFiliali;
	}

	public void setTipologFiliali(TipologFiliali tipologFiliali) {
		this.tipologFiliali = tipologFiliali;
	}

	public TipologUffici getTipologUffici() {
		return this.tipologUffici;
	}

	public void setTipologUffici(TipologUffici tipologUffici) {
		this.tipologUffici = tipologUffici;
	}

}