package it.poste.rilievi.api.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the abbinamento_voci_daco_servizi database table.
 * 
 */
@Entity
@Table(name="abbinamento_voci_daco_servizi")
@NamedQuery(name=AbbinamentoVociDacoServizi.CheckDacoService, query="SELECT COUNT(*) FROM AbbinamentoVociDacoServizi a WHERE a.tipologServizi.idServizio= :idServizio AND a.tipologDaco.idVoceDaco= :idDaco" )
public class AbbinamentoVociDacoServizi implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String CheckDacoService = "CheckDacoService";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ABBINAMENTO")
	private Long idAbbinamento;

	//bi-directional many-to-one association to TipologServizi
	@ManyToOne
	@JoinColumn(name="RIF_SERVIZIO")
	private TipologServizi tipologServizi;

	//bi-directional many-to-one association to TipologDaco
	@ManyToOne
	@JoinColumn(name="RIF_VOCE_DACO")
	private TipologDaco tipologDaco;

	public AbbinamentoVociDacoServizi() {
	}

	public Long getIdAbbinamento() {
		return this.idAbbinamento;
	}

	public void setIdAbbinamento(Long idAbbinamento) {
		this.idAbbinamento = idAbbinamento;
	}

	public TipologServizi getTipologServizi() {
		return this.tipologServizi;
	}

	public void setTipologServizi(TipologServizi tipologServizi) {
		this.tipologServizi = tipologServizi;
	}

	public TipologDaco getTipologDaco() {
		return this.tipologDaco;
	}

	public void setTipologDaco(TipologDaco tipologDaco) {
		this.tipologDaco = tipologDaco;
	}

}