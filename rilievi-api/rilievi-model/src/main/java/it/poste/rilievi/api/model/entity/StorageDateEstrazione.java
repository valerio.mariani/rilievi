package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the storage_date_estrazione database table.
 * 
 */
//@Entity
@Table(name="storage_date_estrazione")

            
public class StorageDateEstrazione implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QueryGetAllExtractionDates = "StorageDateEstrazione.getAllExtractionDates";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID_DATE_ESTRAZIONE")
	private Long idDateEstrazione;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CHIUSURA")
	private LocalDate dataChiusura;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FISSA")
	private LocalDate dataFissa;
    
	public StorageDateEstrazione() {
	}

	public Long getIdDateEstrazione() {
		return idDateEstrazione;
	}

	public void setIdDateEstrazione(Long idDateEstrazione) {
		this.idDateEstrazione = idDateEstrazione;
	}

	public LocalDate getDataChiusura() {
		return this.dataChiusura;
	}

	public void setDataChiusura(LocalDate localDate) {
		this.dataChiusura = localDate;
	}

	public LocalDate getDataFissa() {
		return this.dataFissa;
	}

	public void setDataFissa(LocalDate localDate) {
		this.dataFissa = localDate;
	}

}