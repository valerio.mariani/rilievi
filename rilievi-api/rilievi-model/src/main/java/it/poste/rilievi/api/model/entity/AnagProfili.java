package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the anag_profili database table.
 * 
 */
@Entity
@Table(name="anag_profili")
@NamedQueries({
	@NamedQuery(name=AnagProfili.FindAnagProfileBycode, query="SELECT a FROM AnagProfili a where a.codiceIam = :code"),
	@NamedQuery(name=AnagProfili.findAll, query="SELECT a FROM AnagProfili a"),
	@NamedQuery(name=AnagProfili.findAnagProfileByName, query= "SELECT a FROM AnagProfili a where a.nome = :name"),
	@NamedQuery(name=AnagProfili.findAnagProfileById, query="SELECT a FROM AnagProfili a where a.idProfilo = :idProfilo"),
	@NamedQuery(name=AnagProfili.profileCount, query= " SELECT COUNT( * ) FROM AnagProfili a")

})
public class AnagProfili implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String findAll = "AnagProfili.findAll";
    public static final String FindAnagProfileBycode = "FindAnagProfileBycode";
    public static final String findAnagProfileByName = "findAnagProfileByName";
    public static final String findAnagProfileById = "findAnagProfileById";
    public static final String profileCount = "AnagProfili.profileCount";
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PROFILO")
	private Long idProfilo;
	@Column(name="CODICE_IAM")
	private String codiceIam;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione;

	private String descrizione;
	
	private String nome;

	@OneToMany(mappedBy="anagProfili")
	private List<StorageAbilitazioniDaco> abilitazioniDaco;
	
	//bi-directional many-to-one association to AnagProfiliAbilitazione
	@OneToMany(mappedBy="anagProfili")
	private List<AnagProfiliAbilitazione> anagProfiliAbilitaziones;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="anagProfili")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="anagProfili")
	private List<StagingAggRilievi> stagingAggRilievis;

	//bi-directional many-to-one association to StorageArchRilievi
/*	@OneToMany(mappedBy="anagProfili1")
	private List<StorageArchRilievi> storageArchRilievis1;*/

	//bi-directional many-to-one association to StorageArchRilievi
	@OneToMany(mappedBy="anagProfili2")
	private List<StorageArchRilievi> storageArchRilievis2;

	public AnagProfili() {
	}

	public Long getIdProfilo() {
		return this.idProfilo;
	}

	public void setIdProfilo(Long idProfilo) {
		this.idProfilo = idProfilo;
	}

	public String getCodiceIam() {
		return this.codiceIam;
	}

	public void setCodiceIam(String codiceIam) {
		this.codiceIam = codiceIam;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<AnagProfiliAbilitazione> getAnagProfiliAbilitaziones() {
		return this.anagProfiliAbilitaziones;
	}

	public void setAnagProfiliAbilitaziones(List<AnagProfiliAbilitazione> anagProfiliAbilitaziones) {
		this.anagProfiliAbilitaziones = anagProfiliAbilitaziones;
	}

	public AnagProfiliAbilitazione addAnagProfiliAbilitazione(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		getAnagProfiliAbilitaziones().add(anagProfiliAbilitazione);
		anagProfiliAbilitazione.setAnagProfili(this);

		return anagProfiliAbilitazione;
	}

	public AnagProfiliAbilitazione removeAnagProfiliAbilitazione(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		getAnagProfiliAbilitaziones().remove(anagProfiliAbilitazione);
		anagProfiliAbilitazione.setAnagProfili(null);

		return anagProfiliAbilitazione;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setAnagProfili(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setAnagProfili(null);

		return storageRilievi;
	}

	public List<StagingAggRilievi> getStagingAggRilievis() {
		return this.stagingAggRilievis;
	}

	public void setStagingAggRilievis(List<StagingAggRilievi> stagingAggRilievis) {
		this.stagingAggRilievis = stagingAggRilievis;
	}

	public StagingAggRilievi addStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().add(stagingAggRilievi);
		stagingAggRilievi.setAnagProfili(this);

		return stagingAggRilievi;
	}

	public StagingAggRilievi removeStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().remove(stagingAggRilievi);
		stagingAggRilievi.setAnagProfili(null);

		return stagingAggRilievi;
	}

	public List<StorageArchRilievi> getStorageArchRilievis2() {
		return this.storageArchRilievis2;
	}

	public void setStorageArchRilievis2(List<StorageArchRilievi> storageArchRilievis2) {
		this.storageArchRilievis2 = storageArchRilievis2;
	}

	public StorageArchRilievi addStorageArchRilievis2(StorageArchRilievi storageArchRilievis2) {
		getStorageArchRilievis2().add(storageArchRilievis2);
		storageArchRilievis2.setAnagProfili2(this);

		return storageArchRilievis2;
	}

	public StorageArchRilievi removeStorageArchRilievis2(StorageArchRilievi storageArchRilievis2) {
		getStorageArchRilievis2().remove(storageArchRilievis2);
		storageArchRilievis2.setAnagProfili2(null);

		return storageArchRilievis2;
	}

	public List<StorageAbilitazioniDaco> getAbilitazioniDaco() {
		return abilitazioniDaco;
	}

	public void setAbilitazioniDaco(List<StorageAbilitazioniDaco> abilitazioniDaco) {
		this.abilitazioniDaco = abilitazioniDaco;
	}



}