package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class MaxiEvaluationDetailDTO {
		
	private Long id;
	
	private LocalDateTime regularizationDate;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime mxrDate;
	
	private BranchDTO branch;
	
	private OfficeDTO office;
	
	private DacoDTO daco;
	
	private ReasonDTO reason;
	
	private Double amount;
	
	private EvaluationDTO evaluation;
	
	private MaxiEvaluationDTO maxiEvaluation;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public LocalDateTime getRegularizationDate() {
		return regularizationDate;
	}

	public void setRegularizationDate(LocalDateTime regularizationDate) {
		this.regularizationDate = regularizationDate;
	}
	
	public ReasonDTO getReason() {
		return reason;
	}

	
	public void setReason(ReasonDTO reason) {
		this.reason = reason;
		
	}

	public BranchDTO getBranch() {
		return branch;
	}

	public void setBranch(BranchDTO branch) {
		this.branch = branch;
	}

	public OfficeDTO getOffice() {
		return office;
	}

	public void setOffice(OfficeDTO office) {
		this.office = office;
	}

	public DacoDTO getDaco() {
		return daco;
	}

	public void setDaco(DacoDTO daco) {
		this.daco = daco;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getMxrDate() {
		return mxrDate;
	}

	public void setMxrDate(LocalDateTime mxrDate) {
		this.mxrDate = mxrDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public EvaluationDTO getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(EvaluationDTO evaluation) {
		this.evaluation = evaluation;
	}

	public MaxiEvaluationDTO getMaxiEvaluation() {
		return maxiEvaluation;
	}

	public void setMaxiEvaluation(MaxiEvaluationDTO maxiEvaluation) {
		this.maxiEvaluation = maxiEvaluation;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
