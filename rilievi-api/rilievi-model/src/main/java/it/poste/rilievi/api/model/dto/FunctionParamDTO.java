package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;



public class FunctionParamDTO {
	
	private Long id;
	private LocalDateTime dateCreation;
	private String description;
	private String nameParam;
	private String value; 
	private FunctionDTO function;


	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id= id;
	}
	public LocalDateTime getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNameparam() {
		return nameParam;
	}
	public void setNameparam(String nameParam) {
		this.nameParam = nameParam;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public FunctionDTO getFunction() {
		return function;
	}
	public void setFunction(FunctionDTO function) {
		this.function = function;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	

}
