package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_rilievi_maxi_totale database table.
 * 
 */
@Entity
@Table(name="storage_rilievi_maxi_totale")
@NamedQueries({
	@NamedQuery(name="StorageRilieviMaxiTotale.findAll", query="SELECT s FROM StorageRilieviMaxiTotale s"),
	@NamedQuery(name=StorageRilieviMaxiTotale.GetMaxiEvaluationRaised, query="SELECT s FROM StorageRilieviMaxiTotale s WHERE s.dataReg IS NULL ")
}
)


public class StorageRilieviMaxiTotale implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GetMaxiEvaluationRaised = "GetMaxiEvaluationRaised";
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RILIEVI_MAXI_TOTALE")
	private Long idRilieviMaxiTotale;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_LIM")
	private Date dataLim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_MXR")
	private Date dataMxr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REG")
	private Date dataReg;

	private BigDecimal importo;

	@Column(name="TOT_IMPORTO")
	private BigDecimal totImporto;

	@Column(name="TOT_RILIEVI")
	private Integer totRilievi;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	//bi-directional many-to-one association to TipologCausale
	@ManyToOne
	@JoinColumn(name="RIF_CAUSALE")
	private TipologCausale tipologCausale;

	public StorageRilieviMaxiTotale() {
	}

	public Long getIdRilieviMaxiTotale() {
		return this.idRilieviMaxiTotale;
	}

	public void setIdRilieviMaxiTotale(Long idRilieviMaxiTotale) {
		this.idRilieviMaxiTotale = idRilieviMaxiTotale;
	}

	public Date getDataLim() {
		return this.dataLim;
	}

	public void setDataLim(Date dataLim) {
		this.dataLim = dataLim;
	}

	public Date getDataMxr() {
		return this.dataMxr;
	}

	public void setDataMxr(Date dataMxr) {
		this.dataMxr = dataMxr;
	}

	public Date getDataReg() {
		return this.dataReg;
	}

	public void setDataReg(Date dataReg) {
		this.dataReg = dataReg;
	}

	public BigDecimal getImporto() {
		return this.importo;
	}

	public void setImporto(BigDecimal importo) {
		this.importo = importo;
	}

	public BigDecimal getTotImporto() {
		return this.totImporto;
	}

	public void setTotImporto(BigDecimal totImporto) {
		this.totImporto = totImporto;
	}

	public Integer getTotRilievi() {
		return this.totRilievi;
	}

	public void setTotRilievi(Integer totRilievi) {
		this.totRilievi = totRilievi;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

	public TipologCausale getTipologCausale() {
		return this.tipologCausale;
	}

	public void setTipologCausale(TipologCausale tipologCausale) {
		this.tipologCausale = tipologCausale;
	}

}