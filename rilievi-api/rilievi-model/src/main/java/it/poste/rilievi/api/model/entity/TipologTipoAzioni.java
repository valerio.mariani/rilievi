package it.poste.rilievi.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_tipo_azioni database table.
 * 
 */
@Entity
@Table(name="tipolog_tipo_azioni")
@NamedQueries({@NamedQuery(name = TipologTipoAzioni.GetTipoAzioneByAzione, query ="SELECT t FROM TipologTipoAzioni t where t.azione = :azione")})
public class TipologTipoAzioni implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String GetTipoAzioneByAzione = "GetTipoAzioneByAzione";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_AZIONE")
	private Long idTipoAzione;

	private String azione;

	//bi-directional many-to-one association to StorageLogAzioni
	@OneToMany(mappedBy="tipologTipoAzioni")
	private List<StorageLogAzioni> storageLogAzionis;

	public TipologTipoAzioni() {
	}

	public Long getIdTipoAzione() {
		return this.idTipoAzione;
	}

	public void setIdTipoAzione(Long idTipoAzione) {
		this.idTipoAzione = idTipoAzione;
	}

	public String getAzione() {
		return this.azione;
	}

	public void setAzione(String azione) {
		this.azione = azione;
	}

	public List<StorageLogAzioni> getStorageLogAzionis() {
		return this.storageLogAzionis;
	}

	public void setStorageLogAzionis(List<StorageLogAzioni> storageLogAzionis) {
		this.storageLogAzionis = storageLogAzionis;
	}

	public StorageLogAzioni addStorageLogAzioni(StorageLogAzioni storageLogAzioni) {
		getStorageLogAzionis().add(storageLogAzioni);
		storageLogAzioni.setTipologTipoAzioni(this);

		return storageLogAzioni;
	}

	public StorageLogAzioni removeStorageLogAzioni(StorageLogAzioni storageLogAzioni) {
		getStorageLogAzionis().remove(storageLogAzioni);
		storageLogAzioni.setTipologTipoAzioni(null);

		return storageLogAzioni;
	}

}