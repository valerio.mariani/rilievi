package it.poste.rilievi.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AppProfileCriteriaDTO extends PaginableSearchCriteria {
	
	private Long id;
	
	private String code;
	
	private String name;
	
	private String description;
	
	private LocalDateTime creationDate;

	

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public LocalDateTime getCreationDate() {
		return creationDate;
	}



	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}



	@Override
	void populateMapDtoEntity() {
		getFieldsMap().put("id", "idProfilo");
		getFieldsMap().put("creationDate", "dataCreazione");
		getFieldsMap().put("description", "descrizione");
		getFieldsMap().put("name", "nome");
		
		
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
