package it.poste.rilievi.api.model.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class EvaluationCriteriaDTO extends PaginableSearchCriteria{
	
	
	public enum SearchType{REGULARIZED, NOT_REGULARIZED, ALL};
	
	private FunctionEnum function;
	
	private LocalDate insertDateFrom;
	
	private LocalDate insertDateTo;
	
	private LocalDate competenceDateFrom;
	
	private LocalDate competenceDateTo;
	
	private Double amountTo;
	
	private Double amountFrom;
	
	private boolean showRemoved = true; //Default
	
	private DacoDTO daco;
	
	private List<DacoDTO> dacos;// = new ArrayList<DacoDTO>();
	
	private BranchDTO branch;
	
	private OfficeDTO office;	
	
	private ReasonDTO reason;
	
	private List<ReasonDTO> reasons = new ArrayList<ReasonDTO>();
	
	private EvaluationStatusDTO lossState;
	
	private String appUserCode;	
	
	private String profile;
	
	private SearchType type;

	private boolean showOnlySendedBridge;
	
	private boolean showOnlyAccounted;
	

	public FunctionEnum getFunction() {
		return function;
	}

	public void setFunction(FunctionEnum function) {
		this.function = function;
	}

	public String getAppUserCode() {
		return appUserCode;
	}

	public void setAppUserCode(String appUserCode) {
		this.appUserCode = appUserCode;
	}

	public LocalDate getInsertDateFrom() {
		return insertDateFrom;
	}

	public void setInsertDateFrom(LocalDate insertDateFrom) {
		this.insertDateFrom = insertDateFrom;
	}

	public LocalDate getInsertDateTo() {
		return insertDateTo;
	}

	public void setInsertDateTo(LocalDate insertDateTo) {
		this.insertDateTo = insertDateTo;
	}

	public LocalDate getCompetenceDateFrom() {
		return competenceDateFrom;
	}

	public void setCompetenceDateFrom(LocalDate competenceDateFrom) {
		this.competenceDateFrom = competenceDateFrom;
	}

	public LocalDate getCompetenceDateTo() {
		return competenceDateTo;
	}

	public void setCompetenceDateTo(LocalDate competenceDateTo) {
		this.competenceDateTo = competenceDateTo;
	}

	public boolean isShowRemoved() {
		return showRemoved;
	}

	public void setShowRemoved(boolean showRemoved) {
		this.showRemoved = showRemoved;
	}

	public Double getAmountTo() {
		return amountTo;
	}

	public void setAmountTo(Double amountTo) {
		this.amountTo = amountTo;
	}

	public Double getAmountFrom() {
		return amountFrom;
	}

	public void setAmountFrom(Double amountFrom) {
		this.amountFrom = amountFrom;
	}

	public DacoDTO getDaco() {
		return daco;
	}

	public void setDaco(DacoDTO daco) {
		this.daco = daco;
	}

	public BranchDTO getBranch() {
		return branch;
	}

	public void setBranch(BranchDTO branch) {
		this.branch = branch;
	}

	public OfficeDTO getOffice() {
		return office;
	}

	public void setOffice(OfficeDTO office) {
		this.office = office;
	}

	public ReasonDTO getReason() {
		return reason;
	}

	public void setReason(ReasonDTO reason) {
		this.reason = reason;
	}

	public List<ReasonDTO> getReasons() {
		if( reasons == null ) {
			reasons = new ArrayList<ReasonDTO>();
		}
		if( this.getReason() != null) {
			this.reasons.add(this.getReason());
		}
		
		return reasons;
	}

	public void setReasons(List<ReasonDTO> reasons) {
		this.reasons = reasons;
	}

	public EvaluationStatusDTO getLossState() {
		return lossState;
	}

	public void setLossState(EvaluationStatusDTO lossState) {
		this.lossState = lossState;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public SearchType getType() {
		return type;
	}

	public void setType(SearchType type) {
		this.type = type;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

	public List<DacoDTO> getDacos() {
		if( dacos == null ) {
			dacos = new ArrayList<DacoDTO>();
		}
		
		if(this.getDaco() != null) {
			this.dacos.add(getDaco());
		}
		
		return dacos;
	}

	public void setDacos(List<DacoDTO> dacos) {
		this.dacos = dacos;
	}
	
	public void addReason(ReasonDTO reason ) {
		this.getReasons().add(reason);
	}
	
	public void addDaco(DacoDTO daco ) {
		this.getDacos().add(daco);
	}
	
	public List<Long> getIdDaco(){
		List<Long> ret = new ArrayList<Long>();
		getDacos().forEach(daco ->{
			ret.add(daco.getId());
		});
		return ret;
	}
	
	public List<Long> getIdReason(){
		List<Long> ret = new ArrayList<Long>();
		getReasons().forEach(reason ->{
			ret.add(reason.getId());
		});
		return ret;
	}



	public boolean isShowOnlySendedBridge() {
		return showOnlySendedBridge;
	}

	public void setShowOnlySendedBridge(boolean showOnlySendedBridge) {
		this.showOnlySendedBridge = showOnlySendedBridge;
	}

	public boolean isShowOnlyAccounted() {
		return showOnlyAccounted;
	}

	public void setShowOnlyAccounted(boolean showOnlyAccounted) {
		this.showOnlyAccounted = showOnlyAccounted;
	}

	@Override
	void populateMapDtoEntity() {
		getFieldsMap().put("id", "idRilievi");
		getFieldsMap().put("insertDate", "dataInserimento");
		getFieldsMap().put("competenceDate", "dataCompetenza");
		getFieldsMap().put("idChild", "rifRilievoFiglio");
		getFieldsMap().put("number","numRilievo");
		getFieldsMap().put("office.fractional", "tipologUffici.frazionario");
		
		getFieldsMap().put("daco.codiceDaco", "tipologDaco.codiceDaco");
		getFieldsMap().put("daco.description", "tipologDaco.descrizione");
		getFieldsMap().put("daco.accountNumber", "tipologDaco.contoContabile");
		getFieldsMap().put("daco.accountDescription", "tipologDaco.contoContabileDesc");
		
		getFieldsMap().put("accountDate", "dataCoge");
		getFieldsMap().put("reason.description", "tipologCausale1.descCausale");
		getFieldsMap().put("creditAmount", "COALESCE(s.creditiEuro, s.debitiEuro)");
		
		getFieldsMap().put("description", "descrizione");
		
	}

}
