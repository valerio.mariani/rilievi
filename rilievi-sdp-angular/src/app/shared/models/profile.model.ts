import Utils from '../utily/utils';

export class Profile {

    id:number;
	
	code:string;
	
	name:string;
	
	description:string;
	
    creationDate: Date;
    
    static fromJson(json: Object): Profile {
        const profile = new Profile();
        profile.id = json['id'];
        profile.code = json['code'];
        profile.name = json['name'];
        profile.description = json['description'];
        profile.creationDate = Utils.parseDate(json['creationDate']);
        return profile;
    }

}