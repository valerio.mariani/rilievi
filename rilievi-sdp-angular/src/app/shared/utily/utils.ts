import { NgForm } from '@angular/forms';
import { Daco } from 'src/app/evaluation/model/daco.model';
import { ResponseError } from 'src/app/evaluation/model/responseError.model';
import { AlertScope } from '../alertscope.enum';
import { AlertService } from '../services/alert.service';


export default class Utils {


    static isDacoDebit(daco: Daco) {
        let val: string = daco.code;
        return val!=null && val.length>1 && val.charAt(1)=="D"; 
    }
  
    static isNotEmpty(val: any) {        
        return !this.isEmpty(val); 
    }

    static isEmpty(val: any) {        
        return  val == undefined || val == null || val == ''; 
    }

    static cloneDacoList(dacoList: Array<Daco>, excludedDaco: Daco, filterDebitCredit:boolean, filterDebit:boolean):Array<Daco>{
        let dacoListTmp: Array<Daco> = new Array<Daco>();        
        for(const daco of dacoList){
            if(excludedDaco != null && excludedDaco.id == daco.id){
                continue;
            }
            if(filterDebitCredit){
                if(filterDebit && !this.isDacoDebit(daco)){//prendo le voci a debito
                    continue;
                }
                if(!filterDebit && this.isDacoDebit(daco)){//prendo le voci a credito
                    continue;
                }
            }
          dacoListTmp.push(daco);
        }
        return dacoListTmp;
    }

    static parseDate(strDate:string):Date{
        if(!strDate){
            return null;
        }
        if(strDate.length==10){
            return new Date(Date.parse(strDate + 'T00:00:00'));
        }
        return new Date(Date.parse(strDate));
    }

    static isValidDate(d:any):boolean {
        return d instanceof Date && !isNaN(d.getTime());
    }

    static download(data:Response):void {
        let contentDisposition = data.headers.get("Content-Disposition");
        let fileName = contentDisposition.substring(contentDisposition.indexOf("filename=\"")+10,contentDisposition.length-1);
        let blob = data.body;
        if(window.navigator.msSaveBlob){
            window.navigator.msSaveBlob(blob, fileName);  
        }else{
          let blobURL = window.URL.createObjectURL(data.body);
          let anchor = document.createElement("a");
          document.body.appendChild(anchor);
          anchor.download = fileName;
          anchor.setAttribute('style', 'display: none');
          anchor.href = blobURL;
          anchor.click();
          anchor.remove();
        }
    }

    static showServerError(respErr:ResponseError, alertService:AlertService, currentForm?: NgForm, alertScope: AlertScope = AlertScope.PAGE ):void{
    
        let details: Array<string> = new Array<string>();   
        if(respErr.details && respErr.details.length > 0){
           details = details.concat(respErr.details);           
        }
        let haveFieldErrors:boolean = false;
        if(respErr.fields && currentForm){
             for (const key in respErr.fields) {
               if (respErr.fields.hasOwnProperty(key)) {                
                 if(currentForm.control.controls[key]){
                    haveFieldErrors = true;
                    currentForm.control.controls[key].setErrors({'error': respErr.fields[key]});
                 }else{
                    details = details.concat(respErr.fields[key]);
                 }
                
               }
             }
         }
         if(details.length >0){
           alertService.addErrorMessage("Errori restituiti dal sistema:",details, alertScope);
         }else
            if(haveFieldErrors){
                alertService.addErrorMessage("Errori restituiti dal sistema", null, alertScope);
            }else{
                    alertService.addErrorMessage("Sistema momentaneamente non disponibile!", null, alertScope);
                }
    }

    static serialize(obj) {
        var str = [];
        for (var p in obj)
          if (obj.hasOwnProperty(p) && obj[p] != null) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
        return str.join("&");
      }
}