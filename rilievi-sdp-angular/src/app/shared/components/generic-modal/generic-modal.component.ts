import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';

@Component({
  selector: 'modal-content',
  templateUrl: './generic-modal.component.html',
  styleUrls: ['./generic-modal.component.scss']
})
export class GenericModalComponent implements OnInit {
  public onClose: Subject<string>;
  title: string;
  message: string;
  closeBtnName: string;
 
  buttons: Array< { label: string,
            retValue: string,
            colorClass: string
  }>;
  constructor(public bsModalRef: BsModalRef) {}
  ngOnInit() {
    this.onClose = new Subject();
   
  }

  buttonClick(retValue: string){
    this.onClose.next(retValue);
    this.bsModalRef.hide();
  }

}

