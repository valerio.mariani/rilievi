import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PathRegistry } from '../../pathregistry.enum';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  message:string ='Errore Generico'

  constructor(public route: ActivatedRoute, public router: Router) { 
      this.message = this.route.snapshot.params['message'];
  }

  ngOnInit() {
    if( history.state['message'] ){
      this.message = history.state['message'];
    }
  }
}
