export enum AlertScope {
    PAGE = 'page',
    MODAL = 'modal'
}
