export enum PathRegistry {
  ROOT = '',
  UNKNOWN_PATH = '**',
  ERROR ="error", 
  SEARCH_MANAGE_SDP = "searchAndManageSdp",
  UNDO_REGULARIZATION_SDP = "undoRegularizationSdp"
}