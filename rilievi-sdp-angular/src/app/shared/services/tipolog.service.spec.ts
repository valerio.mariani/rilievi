import { TestBed } from '@angular/core/testing';

import { TipologService } from './tipolog.service';

describe('TipologService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipologService = TestBed.get(TipologService);
    expect(service).toBeTruthy();
  });
});
