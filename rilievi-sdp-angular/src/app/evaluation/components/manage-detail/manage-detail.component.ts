import { EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { Function } from 'src/app/core/model/function.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { UserService } from 'src/app/core/services/user.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { AlertService } from 'src/app/shared/services/alert.service';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import Utils from 'src/app/shared/utily/utils';
import { Evaluation } from '../../model/evaluation.model';
import { Office } from '../../model/office.model';
import { Status } from '../../model/status.model';
import { EvaluationService } from '../../services/evaluation.service';


export class ManageDetailComponent implements OnInit  {
functions: Array<Function>=[];
  @Input()
  model: Evaluation = new Evaluation();
  @Input()
  statusList: Array<Status> = [];
  @Output('back')
  emitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  backupModel: Evaluation;
  settings: EvaluationSettings;
  isLoaded: boolean = false;
  officeList: Array<Office> =[];
  @ViewChild('manageEvalFormRef', { read: "", static: true })
  currentForm: NgForm;
  
  modalRef: BsModalRef;

  totalNotes: number;
  totalAttachments: number;

  labelDebit:string = "DARE (DIMINUISCE)/DARE (AUMENTA)";
  labelCredit:string = "AVERE (AUMENTA)/AVERE (DIMINUISCE)";

  userProfile:string;
  profile:any;

  constructor(public route: ActivatedRoute, public modalService: BsModalService, 
    public evaluationService: EvaluationService, public previousUrlService : PreviousRouteService, 
    protected tipologService: TipologService, protected settingsService: SettingsService,
    public alertService: AlertService, protected userService: UserService) { 
      this.userProfile = userService.user.profile;
      this.profile = ProfileEnum;
    }
 
  ngOnInit() {
    this.settings = this.settingsService.getSettings();
    this.totalNotes = this.model.notes.length;
    this.totalAttachments = this.model.attachments.length;
    this.backupModel = Evaluation.fromJSON(JSON.parse(JSON.stringify(this.model)));
    this.tipologService.getOffices(this.model.branch.id).subscribe(res =>{
      this.officeList = res;
      this.isLoaded = true;
    });

    if(this.model.idChild || this.model.idParent){

      if( this.model.daco && Utils.isDacoDebit(this.model.daco) ){
        this.labelCredit = "AVERE (AUMENTA)";
        this.labelDebit = "DARE (DIMINUISCE)";
        }else{
          this.labelCredit = "AVERE (DIMINUISCE)";
          this.labelDebit = "DARE (AUMENTA)";
        }
    
    }else{
      this.labelDebit = "Debiti per Rilievi (13 B) RESTITUIRE SOMMA AL CLIENTE";
      this.labelCredit = "Crediti per Rilievi (5 B) RICHIESTA DAL CLIENTE";
    }
  }

  updateFranctional(): void{ 
      this.currentForm.controls['office'].enable();
      this.currentForm.controls['fractional'].enable();     
  }

  selectionOfficeChanged(event) {
    this.model.fractional = undefined;
    if (this.model.office != null) {
      this.model.fractional = this.model.office.fractional;
    }
  }
  
  fractionalChanged(event) {
    this.model.office = undefined;
    let model = this.model;
    let founded = false;
    this.officeList.forEach(function (office, index) {
      if (office.fractional == model.fractional) {
        model.office = office;
        founded = true;
        return;
      }
    });
    if (!founded) {
      this.model.office = undefined;
      if(this.model.fractional!= null && this.model.fractional !=''){
        this.currentForm.control.controls['fractional'].setErrors({ 'incorrect': false });
      }
    } 
  }

  btnAnnulla():void{
    this.currentForm.controls['office'].disable();
    this.currentForm.controls['fractional'].disable(); //btnUpdFractional
    this.model = Evaluation.fromJSON(JSON.parse(JSON.stringify(this.backupModel)));
  }

  btnDeleteEval():void{
    let msgConfirm = 'Confermi la cancellazione del rilievo?';
    if(this.model.idParent){
      msgConfirm = 'Verrà cancellato anche il rilievo n. ' + this.model.idParent 
                + ' in quanto con causale RA. Confermi la cancellazione dei rilievi?';
    }
    const confirmData = {
      title: 'Cancellazione Rilievo',
      message:  msgConfirm,
      buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
        this.evaluationService.deleteEvaluation(this.model.id).subscribe(res =>{
          this.model = res;
          this.alertService.addSuccessMessage("Rilievo cancellato con successo!");
          this.goToBack();
        }, err => {
          Utils.showServerError(err,this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    });
  }

  reasonToRc(): void {
    const confirmData = {
      title: 'Trasformazione Rilievo',
      message: 'Confermi la trasformazione del rilievo a RC?' ,
      buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
          this.evaluationService.transformToRcReason(this.model.id).subscribe(res =>{
            this.model.reason = res.reason;
            this.model.previousReason = res.previousReason;    
          }, err => {
            Utils.showServerError(err,this.alertService, this.currentForm);
          });
      } else {
        return;
      }
    });

  }

  reasonToRsRd(): void {
    const confirmData = {
      title: 'Trasformazione Rilievo',
      message: 'Confermi l\'annullo della trasformazione del rilievo a RC?' ,
      buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
          this.evaluationService.undoRcReason(this.model.id).subscribe(res =>{
            this.model.reason = res.reason;
            this.model.previousReason = res.previousReason;                
          }, err => {
            Utils.showServerError(err,this.alertService, this.currentForm);
          });
      } else {
        return;
      }
    });
  }

  openAttachmentsModal(template: TemplateRef<any>): void {  
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
  }

  openNoteModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
  }

  setLossState(template: TemplateRef<any>): void {
    this.modalRef =this.modalService.show(template);
  }
  updateLossState( status :Status): void {
    this.model.status = status;
  }
  undoLossState(): void {

    const confirmData = {
      title: 'Annullo connotazione a perdita',
      message: 'Confermi l\'annullamento della connotazione a perdita?' ,
      buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
        this.evaluationService.undoLossState(this.model.id).subscribe(res =>{
          // this.reload();
          this.model.status = this.settings.unLossState;
        }, err => {
          Utils.showServerError(err,this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    }); 
  }


  updateTotalNotes(totalNotes: number): void {
      this.totalNotes = totalNotes;
  }

  updateTotalAttachments(totalAttachments: number): void{
    this.totalAttachments = totalAttachments;
  }

  saveChange(): void{
     if( !this.currentForm.controls['office'].disabled ){

      const confirmData = {
        title: 'Modifica Frazionario',
        message: 'Confermi la modifica del frazionario?' ,
        buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
      };
  
      let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
      modalRefConf.content.onClose.subscribe(result => {
        if(result == "true"){
          this.evaluationService.updateEvaluationOffice(this.model.id, this.model.office).subscribe(res => {
            this.currentForm.controls['office'].disable();
            this.currentForm.controls['fractional'].disable();
            }, err =>{
              Utils.showServerError(err,this.alertService, this.currentForm);              
          });
        } else {
          return;
        }
      }); 

    

     }
     if( !this.currentForm.controls['description'].disabled ){
      this.evaluationService.setDescription(this.model.id, this.model.description).subscribe(res => {
          this.currentForm.form.reset(this.currentForm.form.getRawValue(), {emitEvent: false})
        }, err =>{
          Utils.showServerError(err,this.alertService, this.currentForm);
      });

 }
  }

  goToBack(){
    this.emitter.emit(true);
  }

}
