import { CommonModule, registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule, ModalModule } from 'ngx-bootstrap';
import { NgxCurrencyModule } from "ngx-currency";
import { GenericModalComponent } from '../shared/components/generic-modal/generic-modal.component';
import { SharedModule } from '../shared/shared.module';
import { NoteComponent } from './components/note/note.component';

registerLocaleData(localeIt, 'it');
@NgModule({

  declarations: [GenericModalComponent,NoteComponent],

  imports: [
    CommonModule,
    FormsModule, 
    NgSelectModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    DataTablesModule,
    NgxCurrencyModule
  ],
  exports : [
    NoteComponent 
  ], 
  entryComponents: [GenericModalComponent]
})
export class EvaluationModule { 
  constructor(){ 
  }
}
 