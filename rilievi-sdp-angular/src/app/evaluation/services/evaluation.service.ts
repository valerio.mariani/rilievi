import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RequestOptions } from 'src/app/shared/models/requestoptions.model';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import Utils from 'src/app/shared/utily/utils';
import { ParameterPair } from '../../shared/services/parameterPair';
import { Daco } from '../model/daco.model';
import { DataTablesRequest } from '../model/datatableRequest.model';
import { DataTablesResponse } from '../model/datatableResponse.model';
import { Evaluation } from '../model/evaluation.model';
import { Note } from '../model/note.model';
import { Office } from '../model/office.model';
import { SearchCriteria } from '../model/searchCritera.model';
import { Status } from '../model/status.model';
import { SDPRegularization } from '../model/sdpRegularization.model';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService extends BaseHttpService {


  buildQueryparams(parameterPairs: Array<ParameterPair>) {
    let queryParams = '';
    parameterPairs.map(r => {
      let parameterPairStr = null;
      if (r.name && r.value) {
        ///
        parameterPairStr = encodeURIComponent(r.name) + '=' + encodeURIComponent(r.value);
      }
      return parameterPairStr;
    }).forEach(r => {
      if (r) {
        queryParams += '&' + r;
      }
    });

    if (queryParams == '') {

    } else {
      queryParams = queryParams.substr(1);

    }
    return queryParams;
  } 


  getDailyEvaluations(request: DataTablesRequest): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    return this.get('/api/daily-evaluations?' + queryStringDt + '&appUserCode=' + this.userService.getUser().code, options)
      .pipe(
        map(data => {
          let list: Array<Evaluation> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Evaluation.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })
      );
  }

  exportDailyEvaluations(): Observable<Response> {
    let options: RequestOptions = new RequestOptions();
    return this.download('/api/daily-evaluations/export?appUserCode=' + this.userService.getUser().code, options);
  }

  exportAllEvaluations(criteria: SearchCriteria): Observable<Response> {
    let idChild = criteria.idChild != null ? criteria.idChild : null;
    let insertDateFrom = criteria._insertDateFrom != null ? criteria._insertDateFrom.toISOString().substring(0, 10) : null;
    let insertDateTo = criteria._insertDateTo != null ? criteria._insertDateTo.toISOString().substring(0, 10) : null;
    let competenceDateFrom = criteria._competenceDateFrom != null ? criteria._competenceDateFrom.toISOString().substring(0, 10) : null;
    let competenceDateTo = criteria._competenceDateTo != null ? criteria._competenceDateTo.toISOString().substring(0, 10) : null;
    let amountFrom = criteria.amountFrom != null ? criteria.amountFrom : null;
    let amountTo = criteria.amountTo != null ? criteria.amountTo : null;
    let office = criteria.office != null ? criteria.office.id : null;
    let fractional = criteria.fractional != null ? criteria.fractional : null;
    let daco = criteria.daco != null ? criteria.daco.id : null;
    let reason = criteria.reason != null ? criteria.reason.id : null;
    let appUserCode = criteria.appUserCode != null ? criteria.appUserCode : null;
    let status = criteria.status != null ? criteria.status.id : null;
    let type = criteria.type != null ? criteria.type : null;

    let options: RequestOptions = new RequestOptions();
    let queryParams = this.buildQueryparams([
      new ParameterPair('idChild', idChild),
      new ParameterPair('competenceDateFrom', competenceDateFrom),
      new ParameterPair('competenceDateTo', competenceDateTo),
      new ParameterPair('insertDateFrom', insertDateFrom),
      new ParameterPair('insertDateTo', insertDateTo),
      new ParameterPair('office.id', office),
      new ParameterPair('daco.id', daco),
      new ParameterPair('amountFrom', amountFrom),
      new ParameterPair('amountTo', amountTo),
      new ParameterPair('fractional', fractional),
      new ParameterPair('reason.id', reason),
      new ParameterPair('status.id', status),
      new ParameterPair('type', type),
      new ParameterPair('appUserCode', appUserCode)]);
    return this.download('/api/evaluations/export?' + queryParams, options)

  }

  checkDuplicate(evaluation: Evaluation): Observable<boolean> {
    let competenceDate = evaluation._competenceDate.toISOString().substring(0, 10);
    let amount: string = evaluation.debitAmount != null ? "&debitAmount=" + evaluation.debitAmount : "&creditAmount=" + evaluation.creditAmount;
    return this.get('/api/evaluations?competenceDateFrom=' + competenceDate + '&competenceDateTo=' + competenceDate + '&office.id='
      + evaluation.office.id + '&daco.id=' + evaluation.daco.id + amount)
      .pipe(
        map(data => {
          return (Array.isArray(data['items']) && Array.from(data['items']).length > 0);
        })
      );
  }

  searchEvaluation(criteria: SearchCriteria,request: DataTablesRequest): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    let insertDateFrom = criteria._insertDateFrom != null ? criteria._insertDateFrom.toISOString().substring(0, 10) : null;
    let insertDateTo = criteria._insertDateTo != null ? criteria._insertDateTo.toISOString().substring(0, 10) : null;
    let competenceDateFrom = criteria._competenceDateFrom != null ? criteria._competenceDateFrom.toISOString().substring(0, 10) : null;
    let competenceDateTo = criteria._competenceDateTo != null ? criteria._competenceDateTo.toISOString().substring(0, 10) : null;
    let amountFrom = criteria.amountFrom != null ? criteria.amountFrom : null;
    let amountTo = criteria.amountTo != null ? criteria.amountTo : null;
    let office = criteria.office != null ? criteria.office.id : null;
    let fractional = criteria.fractional != null ? criteria.fractional : null;
    let daco = criteria.daco != null ? criteria.daco.id : null;
    let reason = criteria.reason != null ? criteria.reason.id : null;
    let appUserCode = criteria.appUserCode != null ? criteria.appUserCode : null;
    let branch = criteria.branch!= null ? criteria.branch.id : null;
    let status = criteria.status != null ? criteria.status.id : null;
    let type = criteria.type != null ? criteria.type : null;

    let queryParams = this.buildQueryparams([
      new ParameterPair('type', type),
      new ParameterPair('branch.id', branch),
      new ParameterPair('competenceDateFrom', competenceDateFrom),
      new ParameterPair('competenceDateTo', competenceDateTo),
      new ParameterPair('insertDateFrom', insertDateFrom),
      new ParameterPair('insertDateTo', insertDateTo),
      new ParameterPair('office.id', office),
      new ParameterPair('daco.id', daco),
      new ParameterPair('amountFrom', amountFrom),
      new ParameterPair('amountTo', amountTo),
      new ParameterPair('fractional', fractional),
      new ParameterPair('reason.id', reason),
      new ParameterPair('status.id', status),

      new ParameterPair('showOnlySendedBridge', criteria.showOnlySendedBridge),
      new ParameterPair('showOnlyAccounted',  criteria.showOnlyAccounted),

      new ParameterPair('appUserCode', appUserCode)]);
    return this.get('/api/evaluations?' +queryStringDt+ '&'+ queryParams,options)
      .pipe(
        map(data => {

          let list: Array<Evaluation> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Evaluation.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })

      );
  }

  insertEvaluation(evaluation: Evaluation): Observable<Evaluation> {
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.post('/api/evaluation', evaluation, options)
      .pipe(
        map(data => {
          return Evaluation.fromJSON(data);
        })
      );
  }

  reclassificationEvaluation(evaluation: Evaluation): Observable<Evaluation> {
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.post('/api/evaluation/reclassification', evaluation, options)
      .pipe(
        map(data => {
          return Evaluation.fromJSON(data);
        })
      );
  }

  rollBackRegularizzation(sdpRegularization: SDPRegularization): Observable<SDPRegularization>{
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.put('/api/evaluation/undo', sdpRegularization, options)
    .pipe(
      map(data => {
        return SDPRegularization.fromJSON(data);
      })
    );
  }

  regularizzationEvaluationSDP(evaluation: SDPRegularization): Observable<SDPRegularization> {
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.put('/api/evaluation/regularization', evaluation, options)
      .pipe(
        map(data => {
          return SDPRegularization.fromJSON(data);
        })
      );
  }

  undoRegularizzationEvaluationSDP(evaluation: SDPRegularization): Observable<SDPRegularization> {
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.put('/api/evaluation/undoregularization', evaluation, options)
      .pipe(
        map(data => {
          return SDPRegularization.fromJSON(data);
        })
      );
  }

  getDacoItem(): Observable<Array<Daco>> {
    return this.get('/api/profile/dacos')
      .pipe(
        map(data => {
          let list: Array<Daco> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Daco.fromJSON(el));
          }
          return list;
        })
      );
  }

  getAllDaco(request: DataTablesRequest, dacoCriteria: Daco): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    let dacoVoice = dacoCriteria.id;
    let description = dacoCriteria.description;
    let accountNumber = dacoCriteria.accountNumber;
    let startDate = dacoCriteria.startDate;
    let endDate = dacoCriteria.endDate;
    let queryParams = this.buildQueryparams([
      new ParameterPair('id', dacoVoice),
      new ParameterPair('description', description),
      new ParameterPair('accountNumber', accountNumber),
      new ParameterPair('startDate', startDate),
      new ParameterPair('endDate', endDate),]);
      
    return this.get('/api/dacos?' + queryStringDt+ queryParams , options)
      .pipe(
        map(data => {
          let list: Array<Daco> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Daco.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;

        })
      );
  }

  insertDaco(dacoCriteria: Daco): Observable<Daco> {

    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.post('/api/daco?', dacoCriteria, options)
      .pipe(
        map(data => {
          return Daco.fromJSON(data);
        })
      );
  }

  setDaco(daco: Daco): Observable<Daco> {
    return this.put('/api/daco/', daco)
      .pipe(
        map(data => Daco.fromJSON(data))
      );
  }

  getEvaluation(id: number): Observable<Evaluation> {
    return this.get('/api/evaluation/' + id)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  deleteEvaluation(id: number): Observable<Evaluation> {
    return this.delete('/api/evaluation/' + id)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  getSysDate(): Observable<Date> {
    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");
    return this.get('/api/sysDate', options)
      .pipe(
        map(data => {
          return new Date(data['sysDate']);
        })
      );
  }

  setLossState(id: number, status: Status): Observable<Evaluation> {
    return this.put('/api/evaluation/' + id + '/lossstate', status)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  undoLossState(id: number): Observable<Evaluation> {
    return this.put('/api/evaluation/' + id + '/undolossstate', null)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  getEvaluationNotes(idEvaluation: number): Observable<DataTablesResponse> {
    return this.get('/api/evaluation/' + idEvaluation + '/notes')
      .pipe(
        map(data => {
          let list: Array<Note> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Note.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })
      );
  }

  addEvaluationNote(idEvaluation: number, note: Note): Observable<Note> {
    return this.post('/api/evaluation/' + idEvaluation + '/note', note)
      .pipe(
        map(data => {
          return Note.fromJSON(data);
        })
      );
  }

  updateEvaluationOffice(idEvaluation: number, office: Office): Observable<Evaluation> {
    return this.put('/api/evaluation/' + idEvaluation + '/office', office).pipe(
      map(data => {
        return Evaluation.fromJSON(data);
      })
    );
  }

  transformToRcReason(idEvaluation: number): Observable<Evaluation> {
    return this.put('/api/evaluation/' + idEvaluation + '/rcreason', null)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  undoRcReason(idEvaluation: number): Observable<Evaluation> {
    return this.put('/api/evaluation/' + idEvaluation + '/undorcreason', null)
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

  setDescription(idEvaluation: number, descr: string): Observable<Evaluation> {
    return this.put('/api/evaluation/' + idEvaluation + '/description', { description: descr })
      .pipe(
        map(data => Evaluation.fromJSON(data))
      );
  }

/*
  searchMaxiEvaluation(criteria: MaxiEvaluationCriteria): Observable<DataTablesResponse> {
      let param: Array<ParameterPair> = [
      new ParameterPair('pageSize', criteria.pageSize),
      new ParameterPair('pageNumber', criteria.pageNumber),
      new ParameterPair('insertDateFrom', criteria.dateFrom != null ? criteria.dateFrom.toISOString().substring(0, 10) : null),
      new ParameterPair('insertDateTo', criteria.dateTo != null ? criteria.dateTo.toISOString().substring(0, 10) : null),
      //    new ParameterPair('daco.id', daco),
      new ParameterPair('amountFrom', criteria.amountFrom),
      new ParameterPair('amountTo', criteria.amountTo)
    ];

    let i = 0;
    criteria.dacos.forEach(daco => {
      param.push(new ParameterPair('dacos[' + (i++) +'].id',daco.id));
    });
   
    let queryParams = this.buildQueryparams(param);
    return this.get('/api/maxi-evaluations?' + queryParams)
      .pipe(
        map(data => {
          let list: Array<Evaluation> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Evaluation.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })
      );
  }

  setMaxiEvaluationSettings(settings: MaxiEvaluationSettings): Observable<MaxiEvaluationSettings> {
    return this.post('/api/maxi-evaluations-settings', settings)
      .pipe(
        map(data => {
          return MaxiEvaluationSettings.fromJSON(data);
        })
      );
  }
*/


}