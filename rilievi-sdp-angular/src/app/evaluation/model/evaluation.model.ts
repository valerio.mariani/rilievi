import { Branch } from './branch.model';
import { Office } from './office.model';
import { Reason } from './reason.model';
import { Daco } from './daco.model';
import { Service } from './service.model';
import { Attachment } from './attachment.model';
import { Status } from './status.model';
import { Note } from './note.model';
import Utils from 'src/app/shared/utily/utils';


export class Evaluation {
 
    id: number;

    private insertDate: Date;

    private competenceDate: Date;

    regularizationDate: Date;

    sendedSrcDate: Date;

    accountDate: Date;

    branch: Branch;

    office: Office;

    daco: Daco;

    reason: Reason;

    number: number;

    description: string;

    previousReason: Reason;

    service: Service;

    debitAmount: number;

    creditAmount: number;

    accountNumber: string;

    fractional: string;

    attachments: Array<Attachment> = new Array<Attachment>();

    notes: Array<Note> = new Array<Note>();

    status: Status;

    generated: Evaluation;

    flagDeleted:string;

    deleteDate:Date;

    idParent: number;

    idChild: number;

    sdpOperationNumber:string;

    
    isDeleted():boolean{
        return this.deleteDate!=null;
    }


    get _insertDate() : Date {
        return this.insertDate;
    }

    set _insertDate(_insertDate : Date) {
        this.insertDate =  _insertDate && !isNaN(_insertDate.getTime()) ? _insertDate : void 0;    }

    get _competenceDate() : Date {
        return this.competenceDate;
    }

    set _competenceDate(_competenceDate : Date) {
        this.competenceDate =  _competenceDate && !isNaN(_competenceDate.getTime()) ? _competenceDate : void 0;    }

    static fromJSON(json: any): Evaluation {
        let evaluation
        if (json) {
            evaluation = new Evaluation();
            evaluation.id = json['id'];
            evaluation.branch = Branch.fromJSON(json['branch']);
            evaluation.office = Office.fromJSON(json['office']);
            evaluation.fractional = evaluation.office.fractional;
            evaluation.daco = Daco.fromJSON(json['daco']);
            evaluation.accountNumber = evaluation.daco.accountNumber;
            evaluation.reason = Reason.fromJSON(json['reason']);
            evaluation.insertDate = Utils.parseDate(json['insertDate']);
            evaluation.deleteDate =  Utils.parseDate(json['deleteDate']);
            evaluation.accountDate = Utils.parseDate(json['accountDate']);   
            evaluation.sendedSrcDate = Utils.parseDate(json['sendedSrcDate']);                            
            evaluation.regularizationDate = Utils.parseDate(json['regularizationDate']);
                     
            evaluation.number = json['number'];
            evaluation.competenceDate = Utils.parseDate(json['competenceDate']);
            evaluation.description = json['description'];
            if( json['previousReason']){
                evaluation.previousReason = json['previousReason'];
            }
           
            evaluation.service = Service.fromJSON(json['service']);
            evaluation.debitAmount = json['debitAmount'];
            evaluation.creditAmount = json['creditAmount'];

            if (json['attachments'] != null) {
                json['attachments'].forEach(element => {
                    evaluation.attachments.push(Attachment.fromJSON(element));
                });
            }

            if (json['notes'] != null) {
                json['notes'].forEach(element => {
                    evaluation.notes.push(Note.fromJSON(element));
                });
            }
            if(json['status']){
                evaluation.status = Status.fromJSON(json['status']);
            }
          
            evaluation.idChild = json['idChild'];
            evaluation.idParent = json['idParent'];
        }
        //evaluation.notes = json['notes'];
        //evaluation.attachments = json['attachments'];
        return evaluation;
    }
}