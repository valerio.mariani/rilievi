
export class Parameter{

    id:number;
    
    nameparam:string;

    description:string;

    value:string;

    static fromJSON(json: any): Parameter {
        let param;
        if (json) {   
            param= new Parameter();
            param.id=json['id'];
            param.nameparam= json['nameparam'];
            param.description= json['description'];
            param.value =json['value'];
        }
        return param;
    }

}