import { Office } from './office.model';

export class Branch {
    constructor() { }
    id: number;
    code: string;
    description: string;
    full: string;
    /*startDate: Date,
    "endDate": null,*/
    office: Array<Office>;

    static fromJSON(json: any): Branch {
        let branch;
        if(json){
        branch = new Branch();
        branch.id =  json['id'];
        branch.code= json['code'];
        branch.description= json['description'];
        if(json['office'] != null){
            branch.office = new Array<Office>();
            json['office'].forEach(element => {
                branch.office.push(Office.fromJSON(element));
            });
        }
        branch.full = '(' + branch.code + ')' + '   ' + branch.description;
    }
        return branch;
    }
}