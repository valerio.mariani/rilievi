import Utils from 'src/app/shared/utily/utils';

export class Note{

    id: number;
	note: string;
	user: string;
	dateCreation: Date;

    static fromJSON(json: any): Note {
        let  note;
        if (json) {
            note = new Note();
            note.id = json['id'];
            note.note = json['note'];          
            note.dateCreation = Utils.parseDate(json['dateCreation']);      
            note.user = json['user'];          
        }
        return note;
    }

   
}