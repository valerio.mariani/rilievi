export class Service {
    id: number;
    code: string;
    description: string;
    full:string;
    static fromJSON(json: any): Service {
        let service;
        if (json) {
            service = new Service();
            service.id = json['id'];
            service.code = json['code'];
            service.description = json['description'];
            service.full = '(' + service.code + ')' + '            ' + service.description;
        }
        return service;
    }
}