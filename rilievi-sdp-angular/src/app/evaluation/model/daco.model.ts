import { Service } from './service.model';

export class Daco {
    id: number;
    code: string;
    description: string;
    accountNumber: string;
    accountDescription: string;
    services: Array<Service>;
    accountNumberFull: string;
    startDate: Date;
    endDate: Date;
    full:string;

    static fromJSON(json: any): Daco {
        let daco;
        if (json) {
            daco = new Daco();
            daco.id = json['id'];
            daco.code = json['code'];
            daco.description = json['description'];
            daco.accountNumber = json['accountNumber'];
            daco.accountDescription = json['accountDescription'];
            if (json['services'] != null) {
                daco.services = new Array<Service>();
                json['services'].forEach(element => {
                    daco.services.push(Service.fromJSON(element));
                });
            }
        }
        daco.full = '(' + daco.code + ')' + '     ' + daco.description;
        daco.accountNumberFull = '(' + daco.accountNumber + ')' + ' ' + daco.accountDescription;;

        daco.startDate= json['startDate'];
        daco.endDate= json['endDate'];
        return daco;
    }
}