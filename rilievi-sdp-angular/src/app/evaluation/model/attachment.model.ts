import Utils from 'src/app/shared/utily/utils';

export class Attachment {

    id: number;

    name: string;

    size: number;

    content: string;

    mimeType: string;


    insertDate: Date;

    insertUser: string;

    static fromJSON(json: any): Attachment {
        const attachment = new Attachment();
        attachment.id = json['id'];
        attachment.name = json['name'];
        attachment.size = json['size'];
        attachment.mimeType = json['mimetype']; 
        attachment.content = json['content'];
        if(json['insertUser']){
            attachment.insertUser = json['insertUser']['code']; 
        }
        if (json['insertDate']) {
            attachment.insertDate = Utils.parseDate((json['insertDate']));
        }
        return attachment;
    }

}