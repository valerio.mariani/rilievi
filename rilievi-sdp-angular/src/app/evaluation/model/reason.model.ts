export class Reason {
    id: number;
    code: string;
    description: string;
    full: string;

    static fromJSON(json: any): Reason {
        let reason;
        if (json) {
            reason = new Reason();
            reason.id = json['id'];
            reason.code = json['code'];
            reason.description = json['description'];

            reason.full = '(' + reason.code + ')' + '            ' + reason.description;
        }
        return reason;
    }

}