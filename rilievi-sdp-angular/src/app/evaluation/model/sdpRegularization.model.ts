import { User } from 'src/app/core/model/user.model';
import { Office } from './office.model';
import { Evaluation } from './evaluation.model';

export class SDPRegularization{

    contabilizationDate:Date;

	office:Office;

	livRestart:number;

    operationNumber:string; 

    originalOperationNumber:string;

	branchNumber:string;

	userSdp:string;
	
    evaluationId:number;

    phase: string; 

    state:string; 
   
    restartLevel:string;

    channel:string;

    compliance:string;
 
    client:string;

    evaluation: Evaluation;

    static fromUser(user: User):SDPRegularization{
        let ret = new SDPRegularization();
        ret.contabilizationDate = user.contabilizationDate;
        ret.office = user.office;
        ret.operationNumber =  user.operationNumber;
        ret.originalOperationNumber = user.originalOperationNumber;
        ret.branchNumber = user.branchNumber;
        ret.userSdp = user.code;
        ret.phase = user.phase;
        ret.restartLevel = user.restartLevel;
        ret.channel = user.channel;
        ret.compliance = user.compliance;
        ret.client = user.client;
        return ret;
    }



    static fromJSON(json: any):SDPRegularization{
        let ret = new SDPRegularization();
        if(json){
            ret.contabilizationDate = json['contabilizationDate'];
            ret.office = json['office'];
            ret.operationNumber =  json['operationNumber'];
            ret.originalOperationNumber = json['originalOperationNumber'];
            ret.branchNumber = json['branchNumber'];
            ret.userSdp = json['code'];
            ret.phase = json['phase'];
            ret.restartLevel = json['restartLevel'];
            ret.channel = json['channel'];
            ret.compliance = json['compliance'];
            ret.client = json['client'];
            if(json['evaluation']){
                ret.evaluation = Evaluation.fromJSON(json['evaluation']);
            }
        }
        return ret;
    }
} 