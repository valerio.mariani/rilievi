export class SearchManageSdpLevel{
    public static Level_Start:number = 1;
    public static Level_Search:number = 2;
    public static Level_Open_Detail:number = 3;
    public static Level_Regularizzed_WEB:number = 4;
    public static Level_Regularizzed_SDP:number = 5;
    public static Level_Regularizzed_SDP_Scrittura_Contabile:number = 6;
    public static Level_Regularizzed_SDP_Scrittura_Giornale:number = 7;
    public static Level_Print_Regularizzed_SDP:number = 8;
}