import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchManageSdpComponent } from './search-manage-sdp.component';

describe('SearchManageSdpComponent', () => {
  let component: SearchManageSdpComponent;
  let fixture: ComponentFixture<SearchManageSdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchManageSdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchManageSdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
