import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, ElementRef, HostListener } from '@angular/core';
import { SearchManageComponent } from 'src/app/evaluation/components/search-manage/search-manage.component';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import { InquirySdpComponent } from '../inquiry-sdp/inquiry-sdp.component';
import { SearchCriteria } from 'src/app/evaluation/model/searchCritera.model';
import { SearchManageSdpLevel } from './search-manage-sdp-level';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { Evaluation } from 'src/app/evaluation/model/evaluation.model';
import { ManageDetailSdpComponent } from '../manage-detail-sdp/manage-detail-sdp.component';
import SDPUtils from '../../utils-sdp';
declare var SmistatoreJS:any;
@Component({
  selector: 'app-search-manage-sdp',
  templateUrl: './search-manage-sdp.component.html',
  styleUrls: ['./search-manage-sdp.component.scss']
})
export class SearchManageSdpComponent extends SearchManageComponent  implements OnInit, AfterViewInit {
  

 
  @ViewChild(InquirySdpComponent, {static:false})
  inquiryComponent;

  currentLevelRestart:number;

 @ViewChild(ManageDetailSdpComponent, {static:false})
  manageDetailComponent;

  objSmistatore:any; 

  modelCriteria: SearchCriteria = new SearchCriteria();

  constructor( tipologService: TipologService, private evalutService: EvaluationService, private cd: ChangeDetectorRef) { 
    super(tipologService);
    this.modelCriteria.showRemoved = true;
  }


  @HostListener('document:keydown.escape', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {  
    SDPUtils.confermaUscita(this.objSmistatore);
  }
  
  ngAfterViewInit(): void {
   
    // Inizializzazione documento xml
    let xmlDoc = this.objSmistatore.XML_Inizializza_Doc();
    let idEval;
    // Legge le informazioni di restart
    if (this.objSmistatore.Leggi_Restart(xmlDoc)) {
      // File di restart recuperato correttamente
      xmlDoc = this.objSmistatore.strXMLDocRitorno;
      let livelloRestart = this.objSmistatore.strLivelloRitorno;
      this.currentLevelRestart = livelloRestart;
      let urlRestart = this.objSmistatore.strURLRitorno;
   /*  if(livelloRestart > SearchManageSdpLevel.Level_Open_Detail){
        this.toggleDetail = true;
     }*/

      switch (livelloRestart) {
          case SearchManageSdpLevel.Level_Start:          
                break;       
          case SearchManageSdpLevel.Level_Search:
               this.openInquiry(xmlDoc);
               break;
          case SearchManageSdpLevel.Level_Open_Detail:
              this.openDetailEvalut(xmlDoc, livelloRestart);          
              break;

          case SearchManageSdpLevel.Level_Regularizzed_WEB:
              this.openDetailEvalut(xmlDoc, livelloRestart);
              break;
          case SearchManageSdpLevel.Level_Regularizzed_SDP:
              this.openDetailEvalut(xmlDoc, livelloRestart);           
              break;
          case SearchManageSdpLevel.Level_Regularizzed_SDP_Scrittura_Contabile:
              this.openDetailEvalut(xmlDoc, livelloRestart);          
              break;
          case SearchManageSdpLevel.Level_Regularizzed_SDP_Scrittura_Giornale:
              this.openDetailEvalut(xmlDoc, livelloRestart);         
              break;    
          case SearchManageSdpLevel.Level_Print_Regularizzed_SDP:
              SDPUtils.cancellaRestart(this.objSmistatore);
              break;
          default:
              break;
      }
      this.cd.detectChanges();
    } else {
        console.log('Nessun punto di Restart Presente');
    }
    
  }




  openInquiry(xmlDoc:any):void{
    this.modelCriteria =  SearchCriteria.fromJSON(SDPUtils.sdpGetRestartContent(xmlDoc),this.modelCriteria);
    this.inquiryComponent.modelCriteria= this.modelCriteria;
    this.showInquiry();
  }

 openDetailEvalut( xmlDoc:any, livelloRestart:number):void{
  this.modelCriteria =  SearchCriteria.fromJSON(SDPUtils.sdpGetRestartContent(xmlDoc),this.modelCriteria);
  this.inquiryComponent.modelCriteria= this.modelCriteria;
  let idEval = SDPUtils.sdpGetRestartIdEvaluation(xmlDoc);
  this.evalutService.getEvaluation(idEval).subscribe(res=>{
    this.showDetail(res);  
    
    switch (livelloRestart) {     
      case SearchManageSdpLevel.Level_Regularizzed_WEB:
          this.manageDetailComponent.regularizzationSdp();
          break;
      case SearchManageSdpLevel.Level_Regularizzed_SDP:
          this.manageDetailComponent.printSdp();        
          break;
      case SearchManageSdpLevel.Level_Regularizzed_SDP_Scrittura_Contabile:       
          this.manageDetailComponent.printSdp();        
          break;
      case SearchManageSdpLevel.Level_Regularizzed_SDP_Scrittura_Giornale:
          this.manageDetailComponent.printSdp();        
          break;    
      default:
          break;
    }



  });
 }


  ngOnInit() {
   // super.ngOnInit();
  //SDP
  this.objSmistatore = new SmistatoreJS(false,true);
  
  }

  showInquiry () {
    this.inquiryComponent.showResults=true;
    super.showInquiry(null);
  
  }
  showDetail($event: Evaluation) {
    this.evaluation=$event;
    //this.modelCriteria
    this.toggleDetail=true;
    this.cd.detectChanges();
  }
}
