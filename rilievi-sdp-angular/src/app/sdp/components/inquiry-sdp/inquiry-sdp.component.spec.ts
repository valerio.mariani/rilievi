import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquirySdpComponent } from './inquiry-sdp.component';

describe('InquirySdpComponent', () => {
  let component: InquirySdpComponent;
  let fixture: ComponentFixture<InquirySdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquirySdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquirySdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
