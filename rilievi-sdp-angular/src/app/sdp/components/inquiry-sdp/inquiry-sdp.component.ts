import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit, ViewContainerRef, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsLocaleService, BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SettingsService } from 'src/app/core/services/settings.service';
import { InquiryComponent } from 'src/app/evaluation/components/inquiry/inquiry.component';
import { NoteComponent } from 'src/app/evaluation/components/note/note.component';
import { Evaluation } from 'src/app/evaluation/model/evaluation.model';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import Utils from 'src/app/shared/utily/utils';
import { UserService } from 'src/app/core/services/user.service';
import { SearchManageSdpComponent } from '../search-manage-sdp/search-manage-sdp.component';
import { SearchManageSdpLevel } from '../search-manage-sdp/search-manage-sdp-level';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { SearchCriteria } from 'src/app/evaluation/model/searchCritera.model';
import SDPUtils from '../../utils-sdp';


@Component({
  selector: 'app-inquiry-sdp',
  templateUrl: './inquiry-sdp.component.html',
  styleUrls: ['./inquiry-sdp.component.scss']
})
export class InquirySdpComponent extends InquiryComponent implements OnInit, AfterViewInit {

  modalRef: BsModalRef;
  selectedEvaluationId: number;
  @Input('smistatore')
  objSmistatore:any;
  @Input('criteria')
  modelCriteria:SearchCriteria;

constructor(protected route: ActivatedRoute, protected router: Router, protected modalService: BsModalService, protected localeService: BsLocaleService,
  protected evaluationService: EvaluationService, protected alertService: AlertService, protected settingsService: SettingsService, 
  protected tilogService: TipologService, private _viewContainer: ViewContainerRef, private userService: UserService, private cd: ChangeDetectorRef) {

    super(route, router, modalService, localeService, evaluationService, alertService, settingsService, tilogService );
  }
  ngOnInit() {
    //super.ngOnInit();
    //Metodo Provvisorio per nasconder header e footer
    this.modelCriteria.type = 'REGULARIZED';
    this.modelCriteria.office = this.userService.user.office;
    this.initDataTable();
    jQuery('.content-footer').css('display','none');
    jQuery('.content-header').css('display','none');
    
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this.cd.detectChanges();
  }


  
  insertDateFromUpperBound(): Date {
    let limit = new Date();
    let ub = new Date();
    if (this.modelCriteria._insertDateFrom <= ub) {
      limit.setDate(ub.getDate());
    }
    return limit;
  }

  insertDateToUpperBound(): Date {
    let limit = new Date();
    if (this.modelCriteria._insertDateFrom != null) {

      limit.setDate(this.modelCriteria._insertDateFrom.getDate()-1);
    } else {
      limit = null;
    }

    return limit;

  }

  maxInsertDateToUpperBound(): Date {
    let limit = new Date();
    limit.setDate(limit.getDate());
    return limit
  }


  toggleResults() {
    if(this.scriviRestart(SearchManageSdpLevel.Level_Search)){
      super.toggleResults(); 
    }    
  }

  toggleSearch(){
    if(this.scriviRestart(SearchManageSdpLevel.Level_Start)){
      super.toggleSearch();
    }
  }

  scriviRestart(level):boolean{
   /* let xmlDoc = SDPUtils.sdpXmlTemplate(this.userService.getUser());
    xmlDoc.getElementsByTagName('Dati_Operazione')[0].appendChild(SDPUtils.sdpXmlRestartContent(this.modelCriteria ).firstChild);
    let xmlDocStr = new XMLSerializer().serializeToString(xmlDoc);*/
    let xmlDocStr = SDPUtils.sdpBuildXmlRestart(this.userService.getUser(),this.modelCriteria );
    alert("SDP - InquirySdpComponent->scriviRestart -> XML: " + xmlDocStr);
    alert("SDP - InquirySdpComponent->scriviRestart -> URL: " + window.location.href);
    alert("SDP - InquirySdpComponent->scriviRestart -> Livello: " + level);
    let retRest = this.objSmistatore.Scrivi_Restart(xmlDocStr, window.location.href, level);
    alert("retRest = this.objSmistatore.Scrivi_Restart(xmlDocStr, window.location.href, level):" + retRest);
    if(retRest){
      return true;
    }else{
      //SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
      return true;
    }
  }

  initDataTable(): void {
    let that = this;
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      scrollX:true,
      autoWidth:true,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      stateSave: true,
      ordering: true,
      info: true,
      order: [[2,"desc"]],
      infoCallback: function (settings, start, end, max, total, pre) {
        that.totale = total;
        
      if(that.totale>1){
        let buttonExp = '<button id="btnExport" #btnExport type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span> Export Excel</button>';        
        return " Totali: " + total+buttonExp;
      }else{
        return " Totali: " + total ;
      }

      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun rilievo',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      rowCallback: (row: Node, data:  Evaluation, index: number) => {
        let self = this;
        $('#note'+data.id, row).bind('click', () => {
          self.openMangeNote(data.id);
        });
        $('#regularizzation'+data.id, row).bind('click', () => {
          self.regularizzation(data);
        });

         
        let deleteIcon = " <span class='oi oi-trash icon-header' title='Annullato' style='padding-right: 15pt;color: red; " + (data.isDeleted() ? "" : "display:none;") + "' aria-hidden='true'></span>";
        jQuery('td', row)[0].innerHTML = deleteIcon ;
        return row;
      },
      drawCallback: (settings: DataTables.SettingsLegacy) => {
        if (this.totale>1){
        jQuery('#btnExport').bind('click', () => {
          that.exportExcel();
        });
        }
      },
      ajax: (dataTablesParameters: any, callback) => {
        if (that.showResults) {
            /*
  causale = ‘RC’
  data regolarizzazione non valorizzata
  data contabilizzazione valorizzata
	data invio verso SRC valorizzata
  stato rilievo non in contenzioso
  data inserimento minore della data odierna
  */
      this.modelCriteria.reason = this.settings.rcReason;
        if(this.modelCriteria.type == 'NOT_REGULARIZED'){//NOT_REGULARIZED equivale rilievi regolarizzabili           
            this.modelCriteria.status = this.settings.unLossState;
            this.modelCriteria.showOnlySendedBridge = true;
            this.modelCriteria.showOnlyAccounted= true;
          }
          this.modelCriteria.showRemoved = false;
          that.evaluationService.searchEvaluation(this.modelCriteria,DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
            resp => {
              if (this.totale > 15 && this.ExceedBoolean) {//TODO Parametro da passare da DB
                this.enoughEval();
                this.ExceedBoolean=false;
              } 
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            },
            err=> {
              callback({
                recordsTotal: 0,
                recordsFiltered: 0,
                data: [],
                draw: 0
              });
            }
          );

        }
      },
      columns: [
        {
        orderable: false,
        title: '',
        data: 'id',
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('text-align', 'left');
        }
       }, 
       {
        orderable: true,
        title: 'Nr.Rilievo',
        data: 'id'
      }, 
      {
        title: 'Nr.Generato',
        data: 'idChild'
      },      
      {
        title: 'Data Inserimento',
        data: 'insertDate',
        render: function (data, type, row, meta) {
          return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
        }
      }, 
      {
        title: 'Data Competenza',
        data: 'competenceDate',
        render: function (data, type, row, meta) {
          return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
        }
      }, 
      {
        title: 'Importo',
        data: 'creditAmount',
        render: function (data, type, row, meta) {
          let amount = row.debitAmount != null ? row.debitAmount : row.creditAmount;
          return (new CurrencyPipe('it-IT')).transform(amount, 'EUR', 'symbol');
        },
        createdCell: function (td, cellData, rowData, row, col) {
                     $(td).css('text-align', 'right');             
                    }
      }, 
      {
        title: 'Regolarizzazione',
        data: 'regularizationDate',
        render: function (data, type, row, meta) {
          if (data == null) {
            return '';
          } else {
            return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
          }

        }
      },
      {
        title: 'Voce Daco',
        data: 'daco.description',
        render: function (data, type, row, meta) {
          return row.daco.full;
        },
        createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('width', '400px');             
         }
      },
      {
        title: 'Conto Contabile',
        data: 'daco.accountNumber'
      },
      {
        orderable: false,
        title: '',
        data: 'id',
        render: function ( data, type, row, meta ) {
          let html = '';       
          html = "<a id=\"note"+ data +"\" ><span class=\"oi oi-plus icon-header\" style=\"cursor: pointer; padding-right: 15pt\" title=\"Aggiungi Nota\" aria-hidden=\"true\" ></span></a>";
          html +=  "<a  id=\"regularizzation"+ data +"\" ><span class=\"oi oi-pencil icon-header\" title=\"Regolarizza\"  style=\"cursor: pointer; padding-right: 15pt\" aria-hidden=\"true\" ></span></a>";
          return html;
         },
         createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('width', '80px');             
          }
      }
      ]
    };
    
  }

  regularizzation(evalut: Evaluation): void {
      this.emitter.emit(evalut);   
  }

  openMangeNote( idEval: number ): void {
    this._viewContainer;
    this.selectedEvaluationId = idEval;
    this.modalRef = this.modalService.show(NoteComponent,{class: 'modal-lg', initialState:{idEvaluation:idEval}});
    this.modalRef.content.modalRef = this.modalRef;
  }


  amountToChange(): void {
    if (this.modelCriteria.amountFrom && this.modelCriteria.amountTo < this.modelCriteria.amountFrom) {
      this.modelCriteria.amountTo = null;
    }
  }

  amountFromChange(): void {
    if (this.modelCriteria.amountTo && this.modelCriteria.amountTo < this.modelCriteria.amountFrom) {
      this.modelCriteria.amountFrom = null;
    }
  }
}
