import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ManageDetailComponent } from 'src/app/evaluation/components/manage-detail/manage-detail.component';
import { BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { UserService } from 'src/app/core/services/user.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import Utils from 'src/app/shared/utily/utils';
import { SearchManageSdpLevel } from '../search-manage-sdp/search-manage-sdp-level';
import { SearchCriteria } from 'src/app/evaluation/model/searchCritera.model';
import SDPUtils from '../../utils-sdp';
import { SDPRegularization } from 'src/app/evaluation/model/sdpRegularization.model';
declare var SmistatoreStampa:any;

@Component({
  selector: 'app-manage-detail-sdp',
  templateUrl: './manage-detail-sdp.component.html',
  styleUrls: ['./manage-detail-sdp.component.scss']
})
export class ManageDetailSdpComponent extends ManageDetailComponent   implements OnInit, AfterViewInit {

 

@Input('smistatore')
objSmistatore:any;

objSmistatoreStampa:any;

@Input('criteria')
modelCriteria: SearchCriteria;

@Input('levelRestart')
currentLevelRestart:number;

  constructor(public route: ActivatedRoute, public modalService: BsModalService, 
    protected router: Router,
    public evaluationService: EvaluationService, public previousUrlService : PreviousRouteService, 
    protected tipologService: TipologService, protected settingsService: SettingsService,
    public alertService: AlertService, protected userService: UserService) { 
      super( route,  modalService, evaluationService,  previousUrlService , tipologService,  settingsService, alertService,  userService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.objSmistatoreStampa = new SmistatoreStampa();
  }
  ngAfterViewInit(): void {
    if(this.currentLevelRestart <= SearchManageSdpLevel.Level_Open_Detail){
      this.scriviRestart(SearchManageSdpLevel.Level_Open_Detail);
    }
  }
  /*
  causale = ‘RC’
  data regolarizzazione non valorizzata
  data contabilizzazione valorizzata
	data invio verso SRC valorizzata
  stato rilievo non in contenzioso
  data inserimento minore della data odierna
  */
  enableRegularizzation() : boolean{
    let insertDate = new Date(this.model._insertDate);
    insertDate.setHours(0,0,0,0);
    let sysDate = new Date(this.settings.sysDate);
    sysDate.setHours(0,0,0,0);
    return !this.model.isDeleted() && this.model.reason.id == this.settings.rcReason.id 
          && this.model.regularizationDate == null && this.model.accountDate != null && this.model.sendedSrcDate != null 
          && this.model.status.id == this.settings.unLossState.id && insertDate < sysDate;
  }

  confirmRegularizzation(): void {
    let pulsanteSelezionato = this.objSmistatore.Visualizza_Msg("Confermi la Regolarizzaione del Rilievo","3");
    if(pulsanteSelezionato == 1){
      this.regularizzation();
    }
  }

  regularizzation(): void{ 
    let regularization = SDPRegularization.fromUser(this.userService.getUser());
    regularization.evaluationId = this.model.id;
    this.evaluationService.regularizzationEvaluationSDP(regularization).subscribe(res =>{
      this.model = res.evaluation;
    //  if(this.scriviRestart(SearchManageSdpLevel.Level_Regularizzed_WEB)){
        this.regularizzationSdp();
     // }
    }, err => {
       Utils.showServerError(err, this.alertService);
    });
  }

  regularizzationSdp():void{
    let xmlDoc = SDPUtils.sdpBuildXmlEseguiFase(this.model, this.userService.getUser())
    if(this.objSmistatore.EseguiFase(xmlDoc)){
      if(this.scriviRestart(SearchManageSdpLevel.Level_Regularizzed_SDP)){
          this.printSdp();
      }
    }else{

      let regularization = new SDPRegularization();
      regularization.contabilizationDate = this.userService.getUser().contabilizationDate;
      regularization.office = this.userService.getUser().office;
      regularization.branchNumber = this.userService.getUser().branchNumber;
      regularization.operationNumber = this.userService.getUser().operationNumber;
      this.evaluationService.rollBackRegularizzation(regularization).subscribe(res =>{
        SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
      },
        err =>{
          SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
        });     
    } 
  }

  printSdp():void{
    let xmlPrint = SDPUtils.sdpBuildXmlPrintRegularization(this.model, this.userService.getUser());
    console.log("SDP - ManageDetailSdpComponent->stampa_documenti -> XML: " + xmlPrint);
    if(this.objSmistatoreStampa.Stampa_Documento(xmlPrint)){
      SDPUtils.cancellaRestart(this.objSmistatore);
      SDPUtils.uscitaFaseOK();
    }else{
      SDPUtils.uscitaFaseNOK_SDP();
    }
  }


  scriviRestart(level:number):boolean{    
    /*let xmlDoc = SDPUtils.sdpXmlTemplate(this.userService.getUser());
    xmlDoc.getElementsByTagName('Dati_Operazione')[0].appendChild(SDPUtils.sdpXmlRestartContent(this.modelCriteria,this.model.id ).firstChild);
    let xmlDocStr = new XMLSerializer().serializeToString(xmlDoc);*/
    let xmlDocStr = SDPUtils.sdpBuildXmlRestart(this.userService.getUser(),this.modelCriteria, this.model.id );
    console.log("SDP - ManageDetailSdpComponent->scriviRestart -> XML: " + xmlDocStr);
    console.log("SDP - ManageDetailSdpComponent->scriviRestart -> URL: " + this.router.url);
    console.log("SDP - ManageDetailSdpComponent->scriviRestart -> Livello: " + level);
    let retRest = this.objSmistatore.Scrivi_Restart(xmlDocStr, window.location.href, level);
    if(retRest){
      return true;
    }else{
      SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
      return false;
    }
  }


}
