import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDetailSdpComponent } from './manage-detail-sdp.component';

describe('ManageDetailSdpComponent', () => {
  let component: ManageDetailSdpComponent;
  let fixture: ComponentFixture<ManageDetailSdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDetailSdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDetailSdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
