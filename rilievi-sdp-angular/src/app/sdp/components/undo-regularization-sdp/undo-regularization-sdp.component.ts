import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { Evaluation } from 'src/app/evaluation/model/evaluation.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsService } from 'src/app/core/services/settings.service';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';
import SDPUtils from '../../utils-sdp';
import { UserService } from 'src/app/core/services/user.service';
import { UndoRegularizationSdpLevel } from './undo-manage-sdp-level';
import { SDPRegularization } from 'src/app/evaluation/model/sdpRegularization.model';
declare var SmistatoreJS:any;
declare var SmistatoreStampa:any;

@Component({
  selector: 'app-cancel-regularization-sdp',
  templateUrl: './undo-regularization-sdp.component.html',
  styleUrls: ['./undo-regularization-sdp.component.scss']
})
export class UndoRegularizationSdpComponent implements OnInit, AfterViewInit {
 
  labelCredit:string;
  labelDebit:string;
  model:Evaluation;
  settings: EvaluationSettings;
  objSmistatore:any;
  objSmistatoreStampa:any;

  constructor(private evalutService: EvaluationService, private route: ActivatedRoute,
    protected settingsService: SettingsService, private alertService: AlertService,
    private userService: UserService, protected router: Router) { }


  @HostListener('document:keydown.escape', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {  
    SDPUtils.confermaUscita(this.objSmistatore);
  }


  ngOnInit() {
    this.model = this.route.snapshot.data.pageData;
    this.settings = this.settingsService.settings;
    this.objSmistatore = new SmistatoreJS(false,true);
    this.objSmistatoreStampa = new SmistatoreStampa();
  }

  ngAfterViewInit(): void {
       // Inizializzazione documento xml
       let xmlDoc = this.objSmistatore.XML_Inizializza_Doc();
       let idEval;
       // Legge le informazioni di restart
       if (this.objSmistatore.Leggi_Restart(xmlDoc)) {
         // File di restart recuperato correttamente
         xmlDoc = this.objSmistatore.strXMLDocRitorno;
         let livelloRestart = this.objSmistatore.strLivelloRitorno;
         let urlRestart = this.objSmistatore.strURLRitorno;
        
         switch (livelloRestart) {
             case UndoRegularizationSdpLevel.Level_Start:          
                   break; 
         
             case UndoRegularizationSdpLevel.Level_Confirm_Cancel:
                 this.cancelRegularizationWeb();
                  break;
   
             case UndoRegularizationSdpLevel.Level_Cancel_WEB:
                  this.cancelRegularizationSDP();    
                 break;
   
             case UndoRegularizationSdpLevel.Level_Cancel_SDP:
                this.printSdp()
                 break;
             case UndoRegularizationSdpLevel.Level_Cancel_SDP_Scrittura_Contabile:
                this.printSdp()
                 break;
             case UndoRegularizationSdpLevel.Level_Cancel_SDP_Scrittura_Giornale:
                this.printSdp()
                 break;
             case UndoRegularizationSdpLevel.Level_Print_Cancel_SDP:
                 SDPUtils.cancellaRestart(this.objSmistatore);
                 break;    
             
           default:
             break;
         }
       } else {
           console.log('Nessun punto di Restart Presente');
       }
  }

  disableCancelRegularization(): boolean{
    //return this.model.regularizationDate == null;
    let insertDate = new Date(this.model._insertDate);
    insertDate.setHours(0,0,0,0);
    let sysDate = new Date(this.settings.sysDate);
    sysDate.setHours(0,0,0,0);
    return this.model.isDeleted() || this.model.reason.id != this.settings.rcReason.id 
          || this.model.regularizationDate == null || this.model.accountDate == null || this.model.sendedSrcDate == null 
          || this.model.status.id != this.settings.unLossState.id || insertDate > sysDate;
  }


  confirmCancelRegularization():boolean{

    let pulsanteSelezionato = this.objSmistatore.Visualizza_Msg("Confermi l'annullamento della Regolarizzaione del Rilievo","3");
    if(pulsanteSelezionato == 1){
      if(this.scriviRestart(UndoRegularizationSdpLevel.Level_Confirm_Cancel)){
        this.cancelRegularizationWeb();
        return true;
      }
    }
   return false;
  }

  cancelRegularizationWeb():void{
    let undoRegularization = SDPRegularization.fromUser(this.userService.getUser());
    undoRegularization.evaluationId = this.model.id;
    this.evalutService.undoRegularizzationEvaluationSDP(undoRegularization).subscribe( res => {
    //    if(this.scriviRestart(UndoRegularizationSdpLevel.Level_Cancel_WEB)){
          this.cancelRegularizationSDP();
      //  }        
    }, err =>{
      Utils.showServerError(err, this.alertService);    
    });

  }


  cancelRegularizationSDP():void{
    let xmlDoc = SDPUtils.sdpBuildXmlEseguiFase(this.model, this.userService.getUser())
    if(this.objSmistatore.EseguiFase(xmlDoc)){
      if(this.scriviRestart(UndoRegularizationSdpLevel.Level_Cancel_SDP)){
          this.printSdp();
      }
    }else{
        //SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
        let regularization = new SDPRegularization();
        regularization.contabilizationDate = this.userService.getUser().contabilizationDate;
        regularization.office = this.userService.getUser().office;
        regularization.branchNumber = this.userService.getUser().branchNumber;
        regularization.operationNumber = this.userService.getUser().operationNumber;
        this.evalutService.rollBackRegularizzation(regularization).subscribe(res =>{
          SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
        },
          err =>{
            SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
          });   
    } 
  }

  printSdp():void{
    let xmlPrint = SDPUtils.sdpBuildXmlPrintUndoRegularization(this.model, this.userService.getUser());
    console.log("SDP - ManageDetailSdpComponent->stampa_documenti -> XML: " + xmlPrint);
    if(this.objSmistatoreStampa.Stampa_Documento()){
      SDPUtils.cancellaRestart(this.objSmistatore);
      SDPUtils.uscitaFaseOK();
    }else{
      SDPUtils.uscitaFaseNOK_SDP();
    }
  }

  scriviRestart(level:number):boolean{    
    /*let xmlDoc = SDPUtils.sdpXmlTemplate(this.userService.getUser());
    let xmlDocStr = new XMLSerializer().serializeToString(xmlDoc);*/
    let xmlDocStr = SDPUtils.sdpBuildXmlRestart(this.userService.getUser() );
    console.log("SDP - CancelRegularizationSdpComponent->scriviRestart -> XML: " + xmlDocStr);
    console.log("SDP - CancelRegularizationSdpComponent->scriviRestart -> URL: " + this.router.url);
    console.log("SDP - CancelRegularizationSdpComponent->scriviRestart -> Livello: " + level);
    let retRest = this.objSmistatore.Scrivi_Restart(xmlDocStr, this.router.url, level);
    if(retRest){
      return true;
    }else{
      SDPUtils.visualizzaErroreAbbandona(this.objSmistatore);
      return false;
    }
  }
}
