import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UndoRegularizationSdpComponent } from './undo-regularization-sdp.component';

describe('UndoRegularizationSdpComponent', () => {
  let component: UndoRegularizationSdpComponent;
  let fixture: ComponentFixture<UndoRegularizationSdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UndoRegularizationSdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UndoRegularizationSdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
