import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Evaluation } from 'src/app/evaluation/model/evaluation.model';
import { Observable } from 'rxjs';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
  })
export class UndoRegularizationSdpResolver implements Resolve<Evaluation>{

    constructor(private evaluationService: EvaluationService) { 

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Evaluation> {
       const evalId:number = route.queryParams['eval'];
       return this.evaluationService.getEvaluation(evalId);
    }
}
