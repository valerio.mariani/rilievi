export class UndoRegularizationSdpLevel{
    public static Level_Start:number = 1;
    public static Level_Confirm_Cancel:number = 2;
    public static Level_Cancel_WEB:number = 3;
    public static Level_Cancel_SDP:number = 4;
    public static Level_Cancel_SDP_Scrittura_Contabile:number = 5;
    public static Level_Cancel_SDP_Scrittura_Giornale:number = 6;
    public static Level_Print_Cancel_SDP:number = 7;
}