import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxCurrencyModule } from 'ngx-currency';
import localeIt from '@angular/common/locales/it';
import { InquirySdpComponent } from './components/inquiry-sdp/inquiry-sdp.component';
import { SearchManageSdpComponent } from './components/search-manage-sdp/search-manage-sdp.component';
import { NoteComponent } from '../evaluation/components/note/note.component';
import { EvaluationModule } from '../evaluation/evaluation.module';
import { ManageDetailSdpComponent } from './components/manage-detail-sdp/manage-detail-sdp.component';
import { UndoRegularizationSdpComponent } from './components/undo-regularization-sdp/undo-regularization-sdp.component';


registerLocaleData(localeIt, 'it');

@NgModule({
  declarations: [InquirySdpComponent, SearchManageSdpComponent, ManageDetailSdpComponent, UndoRegularizationSdpComponent],
  imports: [
    CommonModule,
    FormsModule, 
    NgSelectModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    DataTablesModule,
    NgxCurrencyModule,
    EvaluationModule
  ],
  exports: [InquirySdpComponent],
  entryComponents: [NoteComponent]

})
export class SdpModule { }
