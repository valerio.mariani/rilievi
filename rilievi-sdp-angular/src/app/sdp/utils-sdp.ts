import { User } from 'src/app/core/model/user.model';
import { Evaluation } from '../evaluation/model/evaluation.model';


export default class SDPUtils {

    static sdpXmlTemplate( user: User): Document{
        var xmlDoc = "<?xml version='1.0'?>"; 
        xmlDoc += "<Dati_Operazione>";
        xmlDoc += "<Ufficio>"; 
        xmlDoc += "<Frazionario NUM_UFF='00"+user.office.fractional.substring(0,2)+"' DEN_UFF='" + user.office.fractional.substring(2) + "'/>";
        xmlDoc += "<Data_Cont>" + SDPUtils.formatDate(user.contabilizationDate) + "</Data_Cont>";       
        xmlDoc += "</Ufficio>";
        xmlDoc += "<Sportello>"; 
        xmlDoc += "<Num_Spo>" + user.branchNumber + "</Num_Spo>";
        xmlDoc += "<Num_Ope>" + user.operationNumber + "</Num_Ope>";
        xmlDoc += "</Sportello>";       
        xmlDoc += "<Transazione>"; 
        xmlDoc += "<Cod_Fase>"+user.phase+"</Cod_Fase>";
        if( user.originalOperationNumber != null ){
            xmlDoc += "<Num_Ope_Originaria>" + user.originalOperationNumber + "</Num_Ope_Originaria>"; 
        }else{
            xmlDoc += "<Num_Ope_Originaria/>"; 
        }
        xmlDoc += "<Stat_Oper>"+user.state+"</Stat_Oper>";        
        xmlDoc += "</Transazione>";
        xmlDoc += "</Dati_Operazione>";
        return new DOMParser().parseFromString(xmlDoc, "text/xml");
       
    } 

    static sdpXmlRestartContent( model?: any, evalutId?:number): Document{
        var xmlDoc = "<?xml version='1.0'?>"; 
        xmlDoc += "<Dati_Restart>";
        if(model){
            xmlDoc += "<Model><![CDATA["; 
            xmlDoc += JSON.stringify(model);  
            xmlDoc += "]]></Model>";
        }
        if(evalutId){
            xmlDoc += "<idEvaluation>" + evalutId + "</idEvaluation>"; 
        }
        xmlDoc += "</Dati_Restart>";
        return new DOMParser().parseFromString(xmlDoc, "text/xml");
       
    }

    static sdpBuildXmlRestart(user: User,  model?: any, evalutId?:number):string{
        let xmlDoc = SDPUtils.sdpXmlTemplate(user);
        xmlDoc.getElementsByTagName('Dati_Operazione')[0].appendChild(SDPUtils.sdpXmlRestartContent(model,evalutId).firstChild);
        let xmlDocStr = new XMLSerializer().serializeToString(xmlDoc);
        return xmlDocStr;
    }


    static sdpGetRestartContent(xml:string): Object{
        let doc =  new DOMParser().parseFromString(xml, "text/xml");
        let content = doc.getElementsByTagName("Model")[0].childNodes[0].nodeValue;
        return JSON.parse(content);
    }

    static sdpGetRestartIdEvaluation(xml:string): number{
        let doc =  new DOMParser().parseFromString(xml, "text/xml");
        if(doc.getElementsByTagName("idEvaluation").length > 0 ){
            let content = parseInt(doc.getElementsByTagName("idEvaluation")[0].innerHTML);
            return content;
        }
       return null;
    }

    static uscitaFaseOK() {
        document.location.href = 'about:blank';
    }

    static uscitaFaseNOK_SDP() {
        document.location.href = 'jsp/exitnok.jsp';
    }

    static confermaUscita(objSmistatore: any):void{

        let pulsanteSelezionato = objSmistatore.Visualizza_Msg("Confermi l'uscita dalla Fase?'","3");
        if(pulsanteSelezionato == 1){
            SDPUtils.uscitaFaseOK();
        }
    }


    static visualizzaErroreAbbandona(objSmistatore: any):void{

        let docRet =  objSmistatore.strXMLDocRitorno;
        if( objSmistatore.Leggi_Errore( objSmistatore.strXMLDocRitorno)){
          var errorCode =  objSmistatore.strCodErrRitorno;
          var errorDescription =  objSmistatore.strDescErrRitorno;
          let ret = objSmistatore.Visualizza_Msg(errorCode+" - "+ errorDescription,"8");   
          if(ret== 8){
            SDPUtils.uscitaFaseNOK_SDP();
          } 		   
        }else{
          console.log('Impossibile leggere l\'errore. Abbandono.')
          SDPUtils.uscitaFaseNOK_SDP();
        }   		
    }
    

    static cancellaRestart(objSmistatore: any): void {
        // Inizializzazione documento xml
        let xmlDoc = objSmistatore.XML_Inizializza_Doc();
        // Cancella informazioni di restart
        if (objSmistatore.Cancella_Restart(xmlDoc)) {
              console.log('File di restart cancellato correttamente');
        }else{
            console.log('File di restart non cancellato correttamente');
        }
    
      }


    static sdpBuildXmlPrintRegularization(evalut:Evaluation, user:User):string{
        
        let contabilizationDate = SDPUtils.formatDate(user.contabilizationDate);
        let competenceDate = SDPUtils.formatDate(evalut._competenceDate);
        let fractionalDescr = evalut.office.fractionalDescription.toUpperCase();
        let type = evalut.debitAmount > 0 ?"DEBITO":"CREDITO";
        let amount:number = evalut.debitAmount > 0 ? evalut.debitAmount : evalut.creditAmount;
        let amountStr = "" + amount.toFixed(2).replace(".",",");
        let numRilevo = evalut.number;

        let colFirst = 53;
        let colHeader = colFirst + 15;
        let colBody = colFirst + 10;  Math.round
        let colSign = Math.round(colBody + (48-5)/2);

        var xmlDoc = "";
        xmlDoc += "<Stampante>";
        xmlDoc += " <Stampa NUM_PAGINE=\"1\">";
        xmlDoc += "  <Pagina_Stampa NUM_TIMBRI=\"1\" NUM_ITEMS=\"5\" MODELLO=\"Stampa Regolarizzazione Rilievo\" LEN_DOC=\"15\" TAGLIO=\"0\" ESP=\"NO\" INTERLINEA=\"6\" OBBLIGATORIA=\"NO\">";
        xmlDoc += "    <Item RIGA=\"3\" COLONNA=\"" + colHeader + "\" FONT=\"1\">" + fractionalDescr + " - Data : " + contabilizationDate + "</Item>";
        xmlDoc += "    <Item RIGA=\"5\" COLONNA=\"" + colBody + "\" FONT=\"1\">Regolarizzazione Rilievo a "+type+"    N. "+ numRilevo + "</Item>";
        xmlDoc += "    <Item RIGA=\"6\" COLONNA=\"" + colBody + "\" FONT=\"1\">Del  "+competenceDate+"  di EUR " + amountStr + "</Item>";
        xmlDoc += "    <Item RIGA=\"8\" COLONNA=\"" + colSign + "\" FONT=\"1\">Firma</Item>";
        xmlDoc += "    <Item RIGA=\"9\" COLONNA=\"" + colBody + "\" FONT=\"1\">................................................</Item>";
        xmlDoc += "    <Timbri RIGA1=\"12\" COLONNA1=\"84\">";
        xmlDoc += "      <Importo_Timb VALUTA=\"E\">" + (amount*100) + "</Importo_Timb>";
        xmlDoc += "      <Tassa_Timb VALUTA=\"E\"/>";
        xmlDoc += "      <Desc_Agg/>";
        xmlDoc += "      <Imp_Agg VALUTA=\"E\"/>";
        xmlDoc += "    </Timbri>";
        xmlDoc += "  </Pagina_Stampa>";
        xmlDoc += " </Stampa>";
        xmlDoc += "</Stampante>";
        
        let doc:Document = new DOMParser().parseFromString(xmlDoc, "text/xml");
        let xmlDocTempl = SDPUtils.sdpXmlTemplate(user);
        xmlDocTempl.getElementsByTagName('Dati_Operazione')[0].appendChild(doc.firstChild);
        let xmlDocStr = new XMLSerializer().serializeToString(xmlDocTempl);

        return xmlDocStr;
    }


    
    static sdpBuildXmlPrintUndoRegularization(evalut:Evaluation, user:User):string{

        let colFirst = 53;
        let colBody = colFirst + 10;  Math.round
        let annStr =  user.operationNumber + ' ANN ' + user.originalOperationNumber;
        var xmlDoc = "";
        xmlDoc += "<Stampante>";
        xmlDoc += " <Stampa NUM_PAGINE=\"1\">";
        xmlDoc += "  <Pagina_Stampa NUM_TIMBRI=\"0\" NUM_ITEMS=\"2\" MODELLO=\"Stampa Regolarizzazione Rilievo\" LEN_DOC=\"15\" TAGLIO=\"0\" ESP=\"NO\" INTERLINEA=\"6\" OBBLIGATORIA=\"NO\">";
        xmlDoc += "    <Item RIGA=\"12\" COLONNA=\"" + colBody + "\" FONT=\"1\">ANNULLATO</Item>";
        xmlDoc += "    <Item RIGA=\"13\" COLONNA=\"" + 84 + "\" FONT=\"1\">"+annStr+"</Item>";
        xmlDoc += "  </Pagina_Stampa>";
        xmlDoc += " </Stampa>";
        xmlDoc += "</Stampante>";
        
        let doc:Document = new DOMParser().parseFromString(xmlDoc, "text/xml");
        let xmlDocTempl = SDPUtils.sdpXmlTemplate(user);
        xmlDocTempl.getElementsByTagName('Dati_Operazione')[0].appendChild(doc.firstChild);
        let xmlDocStr = new XMLSerializer().serializeToString(xmlDocTempl);

        return xmlDocStr;
    }

    static sdpBuildXmlEseguiFase(evalut:Evaluation, user:User):string{
        
        let amount:number = evalut.debitAmount > 0 ? evalut.debitAmount : evalut.creditAmount;
        let amountStr = amount*100;
        let codFase = user.phase;
        let tipoOp = user.state;
      //  let sign =  evalut.debitAmount > 0 ? 'DARE':'AVERE';
        let VFT1Str = evalut.number;
        let VFT2Str = SDPUtils.formatDate(evalut._insertDate);
        var xmlDoc = "";
        xmlDoc += "<?xml version=\"1.0\"?>";
        xmlDoc += "<Dati_Operazione>";
        xmlDoc += "<serviceData name=\"RDYRConf\" codFase=\""+ codFase + "\" codFaseCont=\""+codFase+"\" tipoOpez=\""+tipoOp +"\" overrideFase=\"true\">";
        xmlDoc += "<sectionCont>";
        xmlDoc += "<impCont importo=\""+amountStr+"\" prodotto=\"02\"/>";
        xmlDoc += "</sectionCont>";
        xmlDoc += "<sectionGDFO>";
        xmlDoc += "<datiGDFO importo=\""+amountStr+"\" tassa=\"0\" commissione=\"0\">";
        xmlDoc += "<DatiRapportoDTO CFT1=\"NURI\" VFT1=\"" + VFT1Str + "\" CFT2=\"DATA\" VFT2=\"" + VFT2Str + "\"/>";
        xmlDoc += "</datiGDFO>";
        xmlDoc += "</sectionGDFO>";
        xmlDoc += "<sectionARAB>";
        xmlDoc += "<EFASOperazioneARAB segno=\"DARE\">";
        xmlDoc += "<importi>";
        xmlDoc += "<ImportoAppSvcDTO prodotto=\"IMP\" importo=\"0\" divisa=\"EUR\" discriminante=\"\"/>";
        xmlDoc += "</importi>";
        xmlDoc += "</EFASOperazioneARAB>";
        xmlDoc += "</sectionARAB>";
        xmlDoc += "</serviceData>";
        xmlDoc += "</Dati_Operazione>";
        return xmlDoc;
    }


      static formatDate(targetDate:Date):string{
          let day = targetDate.getDate() < 10 ? "0" + targetDate.getDate() : "" + targetDate.getDate();
          let month = (targetDate.getMonth()+1) < 10 ? "0" + (targetDate.getMonth()+1) : "" + (targetDate.getMonth()+1);
          return day + "/" + month + "/" + targetDate.getFullYear();
      }
}