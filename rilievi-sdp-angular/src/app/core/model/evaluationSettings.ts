import { Reason } from 'src/app/evaluation/model/reason.model';
import { Function } from './function.model';
import Utils from 'src/app/shared/utily/utils';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import { Status } from 'src/app/evaluation/model/status.model';


export class EvaluationSettings {

    sysDate: Date;

    rcReason: Reason;

    rsReason: Reason;

    rdReason: Reason;

    raReason: Reason;

    unLossState: Status; 

    functions: Array<Function> = new Array<Function>();

    static fromJSON(json: any): EvaluationSettings {
        let settings;
        if (json) {
            settings = new EvaluationSettings();
            settings.rcReason = Reason.fromJSON(json['rcReason']);
            settings.rsReason = Reason.fromJSON(json['rsReason']);
            settings.rdReason = Reason.fromJSON(json['rdReason']);
            settings.raReason = Reason.fromJSON(json['raReason']);
            settings.unLossState = Status.fromJSON(json['unLossState'])
            if(json['sysDate']){
                settings.sysDate = Utils.parseDate(json['sysDate']);
            }
           for( let func of json['functions']){
               let funcMod = Function.fromJSON(func);
               settings.functions.push(funcMod);
           }
        }
        return settings;
    }


    getParameter(functionCode:string, paramCode:string): Parameter{
        this.functions.forEach( func => {
            if(func.name == functionCode){
                return func.params[paramCode];
            }
        });

        return null;
    }

    getFunction(functionCode:string): Function{
        let ret:Function = null;
        this.functions.forEach( func => {
            if(func.name == functionCode){
                ret = func;
            }
        });

        return ret;
    }
}