import Utils from 'src/app/shared/utily/utils';
import { Parameter } from 'src/app/evaluation/model/parameter.model';

export class Function {

    id: number;

    name: string;
    
    extendedName: string;

    description: string;

    dateCreation: Date;

    params: Object= new Object();
 
    static fromJSON(json: any): Function {
        const func = new Function();
        func.id = json['id'];
        func.name = json['name'];
        func.description = json['description'];
        func.dateCreation = Utils.parseDate(json['dateCreation']);
        func.extendedName = json['extendedName'];
        if( json['params'] != null  ){
            for( let key in json['params']){
                func.params[key] = Parameter.fromJSON(json['params'][key]);
            }  
        }
        return func; 
    }

}