import { TestBed, async, inject } from '@angular/core/testing';

import { SdpGuard } from './sdp.guard';

describe('SdpGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SdpGuard]
    });
  });

  it('should ...', inject([SdpGuard], (guard: SdpGuard) => {
    expect(guard).toBeTruthy();
  }));
});
