import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
declare var SmistatoreJS:any;

@Injectable({
  providedIn: 'root'
})
export class SdpGuard implements  CanActivate{

  constructor(private router: Router) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    
    try{
      var objSmistatore = new SmistatoreJS();
     objSmistatore.XML_Inizializza_Doc();
     console.log("Sdp Session");
     return true;
    }catch(err){
      this.router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Fallita inizializzazione Smistatore SDP'}})
      console.log("Web Session");
      return false;
    }
  }
  
}
