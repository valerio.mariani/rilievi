import { Component, OnInit } from '@angular/core';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {

  constructor(private previousRouteService: PreviousRouteService) { }

  ngOnInit() {
  }

}
