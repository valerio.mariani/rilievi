import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BodyComponent } from './components/body/body.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ContentComponent } from './components/content/content.component';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ApiInterceptorService } from './services/api-interceptor.service';
import { SharedModule } from '../shared/shared.module';



export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptorService, multi: true },
];

@NgModule({
  declarations: [BodyComponent, SpinnerComponent, ContentComponent],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    NgxSpinnerModule,
    RouterModule
  ],
  providers : httpInterceptorProviders
})
export class CoreModule { }
