import { Injectable, Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { User } from '../model/user.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import { ServerError } from 'src/app/shared/models/servererror.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  http: HttpClient;

  constructor(private injector: Injector) {
    this.http = this.injector.get(HttpClient);
  }

   /**
   * Handle the server's response  
   * @param{any} response data object returned from server
   * */
  public static handleServerResponse(response: any): any {
    return response;
  }

  /**
   * Handle the error server response throwing a ServerError
   * @param{HttpErrorResponse} error from server
   * */
  public static handleServerError(error: HttpErrorResponse): any {
    return throwError(ServerError.fromJson(error.error));
  } 
  
  logout(): Observable<any> {
    return this.http.get('logout').pipe(
      map(response => UserService.handleServerResponse(response)),
      catchError((error) => UserService.handleServerError(error)),
    );  
  }
 
  loadUserInfo(): Promise<any> {
    return this.http.get('user?timestamp=' + Date.now()).toPromise();    
  }

  getUser(): User {
    return this.user;
  }
}
