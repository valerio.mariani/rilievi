import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './core/components/content/content.component';
import { CoreModule } from './core/core.module';
import { EvaluationModule } from './evaluation/evaluation.module';
import { SearchManageSdpComponent } from './sdp/components/search-manage-sdp/search-manage-sdp.component';
import { SdpModule } from './sdp/sdp.module';
import { PathRegistry } from './shared/pathregistry.enum';
import { UndoRegularizationSdpComponent } from './sdp/components/undo-regularization-sdp/undo-regularization-sdp.component';
import { UndoRegularizationSdpResolver } from './sdp/components/undo-regularization-sdp/undo-regularization-sdp-resolver';
import { ErrorComponent } from './shared/components/error/error.component';
import { SdpGuard } from './core/guard/sdp.guard';




const routes: Routes = [
//resolve:{pageData: MenuResolver},
  { path: PathRegistry.ROOT, component: ContentComponent, children:[
    { path: PathRegistry.SEARCH_MANAGE_SDP, component: SearchManageSdpComponent, canActivate:[SdpGuard]},
    { path: PathRegistry.UNDO_REGULARIZATION_SDP, component: UndoRegularizationSdpComponent, resolve:{pageData: UndoRegularizationSdpResolver}, canActivate:[SdpGuard]},
    { path: PathRegistry.ERROR, component: ErrorComponent}
  ]},
  { path: PathRegistry.UNKNOWN_PATH, redirectTo: PathRegistry.ERROR } //for unknown path

]; 

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation:  'reload'}),
            CoreModule,
            EvaluationModule,
            SdpModule],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }
