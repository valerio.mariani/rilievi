import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { ProfileService } from 'src/app/admin/services/profile.service';
import { Function } from 'src/app/core/model/function.model';
import { AlertScope } from 'src/app/shared/alertscope.enum';
import { Profile } from 'src/app/shared/models/profile.model';
import { AlertService } from 'src/app/shared/services/alert.service';
import Utils from 'src/app/shared/utily/utils';

@Component({
  selector: 'app-profile-functions',
  templateUrl: './profile-functions.component.html',
  styleUrls: ['./profile-functions.component.scss']
})
export class ProfileFunctionsComponent implements OnInit {

  profile:Profile;
  functionList: Array<Function> = new Array<Function>();
  AlertScope:any;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  modalRefAddFunc: BsModalRef;

  selectedFunction: Function;
  @ViewChild('FormAddFunction', { read: "", static: true })
  currentForm: NgForm;
  constructor(private  modalRef: BsModalRef, private profileService: ProfileService, 
    public alertService: AlertService, public modalService: BsModalService) { 
      this.AlertScope = AlertScope;
  }

  ngOnInit() {
    let that = this;
    this.dtOptions = {
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      ordering: false,
      stateSave:true,
      info: true,
      paging:false,
      infoCallback: function( settings, start, end, max, total, pre ) {         
        return  "  " ;
      },
      ajax: (dataTablesParameters: any, callback) => {
        let pageNumber = dataTablesParameters.start == 0 ?1 : Math.round(dataTablesParameters.start/dataTablesParameters.length+1);
        that.profileService.functionAssociated(that.profile).subscribe(
          resp =>{
            callback({
             // recordsTotal: resp.recordsTotal,
             // recordsFiltered: resp.recordsTotal,
              data: resp,
              draw: dataTablesParameters.draw
            });
          }, err =>{
            Utils.showServerError(err, this.alertService, this.currentForm);
        }
        /*
          err => {
            callback({
              recordsTotal: 0,
              recordsFiltered: 0,
              data:[],
              draw: 0,
              error: 'Sistema momentaneamente non disponibile!'
            });
          }
          */
        );

      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessuna Funzione Associata',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      drawCallback :( settings: DataTables.SettingsLegacy ) =>{  
        jQuery('.dataTables_empty').text('Nessuna Funzione Associata');
      }, 
      columns: [
        {
        title: 'Nome',
        data: 'name'
        }, 
        {
        title: 'Descrizione',
        data: 'description'
        }
    ]
    };

    this.profileService.functionNotAssociated(this.profile).subscribe(res =>{
      this.functionList = res;
     
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
    
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /*
  addFunction(template:TemplateRef<any>):void{
    this.modalRefAddFunc = this.modalService.show(template);
  }*/

  addFunction(): void{
    this.profileService.addFunction(this.profile, this.selectedFunction).subscribe(res =>{
      this.rerender();
      this.selectedFunction = null;
      this.currentForm.reset();
      this.profileService.functionNotAssociated(this.profile).subscribe(res =>{
        this.functionList = res;
      });
    }, err =>{
      Utils.showServerError(err, this.alertService ,this.currentForm, AlertScope.MODAL);
    });
  }
  close():void{
    if(this.modalRef){
      this.modalRef.hide();
    }
  }

}
