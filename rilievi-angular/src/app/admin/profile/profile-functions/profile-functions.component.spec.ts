import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFunctionsComponent } from './profile-functions.component';

describe('ProfileFunctionsComponent', () => {
  let component: ProfileFunctionsComponent;
  let fixture: ComponentFixture<ProfileFunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
