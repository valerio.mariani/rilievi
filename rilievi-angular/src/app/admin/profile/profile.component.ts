import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { Profile } from 'src/app/shared/models/profile.model';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { ProfileService } from '../services/profile.service';
import { InsertProfileComponent } from './insert/insert.component';
import { ProfileFunctionsComponent } from './profile-functions/profile-functions.component';
import { SettingsService } from 'src/app/core/services/settings.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ParamEnum } from '../manage-function/param.enum';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import Utils from 'src/app/shared/utily/utils';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit , OnDestroy, AfterViewInit {
  
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  
  modalRef: BsModalRef;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;
  
  constructor(public router: Router, private profileService: ProfileService, public modalService: BsModalService,protected settingsService: SettingsService,public alertService: AlertService) {    
  }

  ngOnInit() {
    this.initDataTable();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
    
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.MANAG_PROFILES);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 5;
    this.dtOptions = {
        pagingType: 'simple_numbers',
        pageLength: pagingSizeValue,
        serverSide: true,
        processing: false,
        searching: false,
        lengthChange: false,
        stateSave: false,
        ordering: true,
        info: true,
        infoCallback: function( settings, start, end, max, total, pre ) {         
          return  " Totali: " + total ;
        },
        language: {
          info: '',
          infoFiltered: '',
          emptyTable: 'Nessun profilo',
          paginate: {
            first: 'Inizio',
            last: 'Fine',
            next: 'Successivo',
            previous: 'Precedente'
          }
        },
        rowCallback: (row: Node, data:  Profile, index: number) => {

          $('#profile'+data.id, row).bind('click', () => {
            that.openShowFunction(data);
          });
          return row;
        },
       /* drawCallback :( settings: DataTables.SettingsLegacy ) =>{  
          jQuery('#btnExport').bind('click', () => {
            that.exportExcel();
          });
        },*/
        ajax: (dataTablesParameters: any, callback) => {
          
          
          that.profileService.getProfiles(DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
            resp =>{
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            }, err =>{
              Utils.showServerError(err, this.alertService, this.currentForm);
          }
          );

        },
          columns: [
           {
             title: 'Profilo',
             data: 'name'
           }, 
           {
             title: 'Descrizione',
             data: 'description'
           }, 
            {
             title: 'Data Creazione',
             data: 'creationDate',
             createdCell: function (td, cellData, rowData, row, col) {
                $(td).css('width', '220px');             
             },
             render: function ( data, type, row, meta ) {
                return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
             }
           }, 
           {
            title: '',
            orderable:false,
            data: 'id',
            render: function ( data, type, row, meta ) {
             let html = '';             
             html +=  "<a  id=\"profile"+ data +"\" ><span class=\"oi oi-magnifying-glass icon-header\" title=\"Funzioni\"  style=\"cursor: pointer; padding-right: 15pt\" aria-hidden=\"true\" ></span></a>";
             return html;
            },
            createdCell: function (td, cellData, rowData, row, col) {
               $(td).css('width', '80px');             
             }
          }                   
         ]
    };
  }

  openInsertProfile() {  
    this.modalRef = this.modalService.show(InsertProfileComponent,{class: 'modal-lg',backdrop:"static"});
    this.modalRef.content.onSaved.subscribe(result => {this.rerender();});
  }

  openShowFunction(profile:Profile):void{
    this.modalRef = this.modalService.show(ProfileFunctionsComponent, {initialState:{profile: profile},backdrop:"static"});

  }

  goToHome():void{
    this.router.navigateByUrl(PathRegistry.HOME);
  }
}
