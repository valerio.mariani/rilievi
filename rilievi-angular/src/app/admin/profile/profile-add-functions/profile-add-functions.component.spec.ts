import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAddFunctionsComponent } from './profile-add-functions.component';

describe('ProfileAddFunctionsComponent', () => {
  let component: ProfileAddFunctionsComponent;
  let fixture: ComponentFixture<ProfileAddFunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileAddFunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileAddFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
