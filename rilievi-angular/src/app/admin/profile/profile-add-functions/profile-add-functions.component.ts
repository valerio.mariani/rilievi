import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ProfileService } from 'src/app/admin/services/profile.service';
import { Function } from 'src/app/core/model/function.model';
import { AlertScope } from 'src/app/shared/alertscope.enum';
import { Profile } from 'src/app/shared/models/profile.model';
import Utils from 'src/app/shared/utily/utils';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-profile-add-functions',
  templateUrl: './profile-add-functions.component.html',
  styleUrls: ['./profile-add-functions.component.scss']
})
export class ProfileAddFunctionsComponent implements OnInit{
  @Input('function-list') 
  functionList: Function[];
  selectedFunction: Function;

  @Input('profile')
  profile: Profile;

  added: Boolean= false;
  AlertScope: any;
  
  @Input()
  modalRef: BsModalRef;

  @Output('function-added')
  emitter: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;

  constructor( private profileService: ProfileService,public alertService: AlertService){ 
    this.AlertScope = AlertScope;
  }

  ngOnInit() {
   
  }


  close():void{
    if(this.modalRef){
      this.modalRef.hide();
    }
  }

  addFunction(): void{
    this.profileService.addFunction(this.profile, this.selectedFunction).subscribe(res =>{
      this.emitter.emit(this.added);
      this.close();
    }, err =>{
      Utils.showServerError(err, this.alertService, this.currentForm);
  });
  }
}
