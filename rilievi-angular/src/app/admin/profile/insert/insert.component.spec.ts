import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertProfileComponent } from './insert.component';

describe('InsertProfileComponent', () => {
  let component: InsertProfileComponent;
  let fixture: ComponentFixture<InsertProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
