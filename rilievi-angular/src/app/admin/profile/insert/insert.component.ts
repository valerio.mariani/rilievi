import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { ProfileService } from 'src/app/admin/services/profile.service';
import { AlertScope } from 'src/app/shared/alertscope.enum';
import { Profile } from 'src/app/shared/models/profile.model';
import { AlertService } from 'src/app/shared/services/alert.service';
import Utils from 'src/app/shared/utily/utils';

@Component({
  selector: 'app-profile-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.scss']
})
export class InsertProfileComponent implements OnInit {


  public onSaved: Subject<boolean>;
  model: Profile;
  AlertScope: any;
  @ViewChild('FormInsertProfile', { read: "", static: true })
  currentForm: NgForm;

  constructor(private  modalRef: BsModalRef, private profileService: ProfileService, public alertService: AlertService) { 
    this.AlertScope = AlertScope;
  }

  ngOnInit() {
    this.model = new Profile();
    this.onSaved = new Subject();
  }


  
  close(): void {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  insertProfile(): void{
    this.profileService.insertProfile(this.model).subscribe(res =>{
      this.onSaved.next(true);
      this.close();
    }, err =>{
      Utils.showServerError(err, this.alertService ,this.currentForm, AlertScope.MODAL);
    });
  }
}
