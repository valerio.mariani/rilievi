import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtractionDatesComponent } from './extraction-dates.component';

describe('ExtractionDatesComponent', () => {
  let component: ExtractionDatesComponent;
  let fixture: ComponentFixture<ExtractionDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtractionDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtractionDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
