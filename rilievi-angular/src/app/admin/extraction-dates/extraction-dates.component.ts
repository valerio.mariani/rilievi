import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { itLocale } from 'ngx-bootstrap/locale';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { SettingsService } from 'src/app/core/services/settings.service';
import { UserService } from 'src/app/core/services/user.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { Evaluation } from '../../evaluation/model/evaluation.model';
import { ExtractionDate } from '../../evaluation/model/extractionDate.model';
import { SearchCriteria } from '../../evaluation/model/searchCritera.model';
import { AdminService } from '../services/admin.service';
import Utils from 'src/app/shared/utily/utils';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-extraction-dates',
  templateUrl: './extraction-dates.component.html',
  styleUrls: ['./extraction-dates.component.scss']
})
export class ExtractionDatesComponent implements OnInit {

  settings: EvaluationSettings;
  model = new Evaluation();
  modelCriteria = new SearchCriteria();
  extractionCriteria = new ExtractionDate();
  ShowResults: boolean;
  extractDateArr: ExtractionDate[];
  extractDate: ExtractionDate;
  userProfile: string;
  profile: any;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;

  constructor(private localeService: BsLocaleService, private settingsService: SettingsService, private route: ActivatedRoute, private router: Router, private modalService: BsModalService, public adminService: AdminService, private userService: UserService, public alertService: AlertService) {

    //setting the it locale for datepicker component
    itLocale.invalidDate = '';
    defineLocale('it', itLocale);
    this.localeService.use('it');
    //leggo le impostazioni
    this.settings = this.settingsService.settings;
    this.userProfile = userService.user.profile;
    this.profile = ProfileEnum;

  }



  ngOnInit() {

    this.getExtractDate();
  }

  extractionDateLowerBound(): Date {
    let ub = new Date();
    let limit = new Date();

    if (this.extractionCriteria.fixedDate != null && this.extractionCriteria.fixedDate > limit) {

      ub.setDate(this.extractionCriteria.fixedDate.getDate());

    } else {

      ub.setDate(limit.getDate() + 1);
    }

    return ub;
  }



  goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }

  toggleResults() {
    this.confirmData();
  }

  confirmData() {
    const confirmData = {
      title: 'Conferma Dati',
      message: 'Vuoi confermare le date inserite ?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "success" },
      { label: "NO", retValue: "false", colorClass: "danger" }
      ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.adminService.insertExtractionDate(this.extractionCriteria).subscribe(res => {
          this.getExtractDate();
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      }
    });

  }

  getExtractDate() {

    this.adminService.getExtractionDates(this.extractionCriteria).subscribe(res => {
      this.extractDateArr = res;
      if (this.extractDateArr.length) {
        this.extractDate = (<ExtractionDate>(this.extractDateArr[0]));
      }
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });
    return this.extractDate;
  }



}
