import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { itLocale } from 'ngx-bootstrap/locale';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { AdminService } from 'src/app/admin/services/admin.service';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { SettingsService } from 'src/app/core/services/settings.service';
import { UserService } from 'src/app/core/services/user.service';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import Utils from 'src/app/shared/utily/utils';
import { Evaluation } from '../../evaluation/model/evaluation.model';
import { LogCriteria } from '../../evaluation/model/logCriteria.model';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ParamEnum } from '../manage-function/param.enum';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';



@Component({
  selector: 'app-log-track',
  templateUrl: './log-track.component.html',
  styleUrls: ['./log-track.component.scss']
})
export class LogTrackComponent implements OnInit {
  settings:EvaluationSettings; 
  model = new Evaluation();
  modelCriteria = new LogCriteria();
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  now: Date;
  userProfile:string;
  profile:any;
  totale: number;
  showResults : number;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;

  constructor(private adminService:AdminService,private router: Router,private settingsService: SettingsService,private localeService: BsLocaleService, private modalService: BsModalService, private userService: UserService,public alertService: AlertService) {
      //setting the it locale for datepicker component
      itLocale.invalidDate = '';
      defineLocale('it', itLocale);
      this.localeService.use('it');
      //leggo le impostazioni
      this.settings = this.settingsService.settings;
      this.userProfile = userService.user.profile;
     this.profile = ProfileEnum;
   }

  ngOnInit() {
    this.showResults=0;
    this.initDataTable();
  }
  logDateFromUpperBound():Date {
    let ub = new Date();
    if (this.modelCriteria.logDateTo != null) {
      ub= new Date (this.modelCriteria.logDateTo.getTime());
    } 
    return ub;
  }
/*
  logDateToMaxUpperBound():Date {
    let ub = new Date();
    if (this.modelCriteria.logDateTo != null) {
      ub.setDate(this.modelCriteria.logDateTo.getDate());
    }
    return ub;
    
  }*/

  logDateToMinUpperBound():Date {
    let ub = new Date();
    if(this.modelCriteria.logDateFrom!=null){
      //ub.setDate(this.modelCriteria.logDateFrom.getDate());
    ub= new Date(this.modelCriteria.logDateFrom.getTime());
    }
    return ub;
  }

  toggleResults(){
this.showResults=1;
    this.rerender();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      dtInstance.state.clear();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.AUDIT_LOG);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 6;
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      scrollY: "500px",
      scrollCollapse: true,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      stateSave: true,
      autoWidth: true,
      ordering: true,
      info: true,
      infoCallback: function (settings, start, end, max, total, pre) {
        that.totale = total;
        
        if(that.totale>0){
          let buttonExp = '<button id="btnExport" #btnExport type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span> Export Excel</button>';        
          return " Totali: " + total+buttonExp;
        }else{
          return " Totali: " + total ;
        }
      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessuna traccia disponibile',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },

      drawCallback :( settings: DataTables.SettingsLegacy ) =>{ 
        jQuery('#btnExport').bind('click', () => {
         that.exportExcel();
        });
      },
      ajax: (dataTablesParameters: any, callback) => {
        if (that.showResults==1) {


          that.adminService.getLogs(this.modelCriteria,DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
            resp => {
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            }, 
             err =>{
              Utils.showServerError(err, this.alertService, this.currentForm);
          }
          );

        } else if(that.showResults == 0) {

          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: [],
            draw: 0
          });
        }

      },
      columns: [
        {
          "width":"5%",
          title: 'Esito',
          orderable:false,
          data: 'result',
          render:function (data, type, row, meta){
            if (data == null){
              return '';
          }else if(data==1){
            let html = '';             
         html +=  "<span class=\"oi oi-circle-check\" style=\"color:green\"></span>";
         return html;
          }else{
            let html = '';             
         html +=  "<span class=\"oi oi-circle-x\" style=\"color:red\"></span>";
         return html;
          }
          }
        }
          ,
          {
            "width":"10%",
        title: 'Utente',
        data: 'user',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return data; 
        }
        }
        
      }, {"width":"20%",
        title: 'Data Azione',
        data: 'dateTime',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return new DatePipe('it-IT').transform(data,'dd/MM/yyyy'+' '+'-'+' '+'HH:mm:ss');
          //return data.toISOString().substring(0, 10);
        }
      }
          
       
      }, {
        "width":"20%",
        title: 'Tipo Azione',
        data: 'actionDescr',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return data;
        }
        }
        
      }, {
        "width":"30%",
        title: 'Azione',
        orderable:false,
        data: 'action',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else if(data.length>50){
          let html = '';             
          html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data  + "\">"+data.substr( 0, 50 )+"..." +"</label>";
          return html;

        
        }else return data
        }
      },
      ]
    };
  }
  exportExcel(): void {
    this.adminService.exportAllLogs(this.modelCriteria).subscribe(data => {
      Utils.download(data);      
    });
   }
   ngAfterViewInit(): void {
    this.dtTrigger.next();

  }


  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }



  goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }
}


//TODO Has-error