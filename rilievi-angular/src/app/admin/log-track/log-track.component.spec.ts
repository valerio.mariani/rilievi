import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogTrackComponent } from './log-track.component';

describe('LogTrackComponent', () => {
  let component: LogTrackComponent;
  let fixture: ComponentFixture<LogTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
