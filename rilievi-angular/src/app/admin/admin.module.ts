import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { InsertDacoComponent } from './manage-daco/insert-daco/insert-daco.component';
import { EditDacoComponent } from './manage-daco/edit-daco/edit-daco.component';
import { ManageDacoComponent } from './manage-daco/manage-daco.component';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxCurrencyModule } from 'ngx-currency';
import localeIt from '@angular/common/locales/it';
import { LogTrackComponent } from './log-track/log-track.component';
import { GenericModalComponent } from '../shared/components/generic-modal/generic-modal.component';
import { ProfileFunctionsComponent } from './profile/profile-functions/profile-functions.component';
import { ProfileComponent } from './profile/profile.component';
import { InsertProfileComponent } from './profile/insert/insert.component';
import { ProfileAddFunctionsComponent } from './profile/profile-add-functions/profile-add-functions.component';
import { ManageParameterComponent } from './manage-function/manage-parameter/manage-parameter.component';
import { ChangeDescriptionComponent } from './manage-function/change-description/change-description.component';
import { ExtractionDatesComponent } from './extraction-dates/extraction-dates.component';
import { ManageFunctionComponent } from './manage-function/manage-function.component';
import { InsertFunctionComponent } from './manage-function/insert-function/insert-function.component';


registerLocaleData(localeIt, 'it');

@NgModule({
  declarations: [InsertDacoComponent,EditDacoComponent,ManageDacoComponent, LogTrackComponent, ProfileComponent,InsertProfileComponent, ProfileFunctionsComponent, ProfileAddFunctionsComponent,ManageFunctionComponent,InsertFunctionComponent, ManageParameterComponent, ChangeDescriptionComponent,ExtractionDatesComponent],
  imports: [
    CommonModule,
    FormsModule, 
    NgSelectModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    DataTablesModule,
    NgxCurrencyModule
  ],
  exports:[],
  entryComponents:[InsertDacoComponent,EditDacoComponent,GenericModalComponent,ProfileFunctionsComponent,ProfileFunctionsComponent, InsertProfileComponent,ManageParameterComponent,ChangeDescriptionComponent],
})
export class AdminModule {
  constructor(){ 
  }
 }
