import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageFunctionComponent } from './manage-function.component';

describe('ManageFunctionComponent', () => {
  let component: ManageFunctionComponent;
  let fixture: ComponentFixture<ManageFunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageFunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
