import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { itLocale } from 'ngx-bootstrap/locale';
import { SettingsService } from 'src/app/core/services/settings.service';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { BsModalService, BsModalRef, } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { UserService } from 'src/app/core/services/user.service';
import { FunctionCriteria } from '../../evaluation/model/functionCriteria.model';
import { Profile } from 'src/app/shared/models/profile.model';
import { ManageParameterComponent } from './manage-parameter/manage-parameter.component';
import { ChangeDescriptionComponent } from './change-description/change-description.component';
import { Router } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import { ParamEnum } from './param.enum';
import { DatePipe } from '@angular/common';
import Utils from 'src/app/shared/utily/utils';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-manage-function',
  templateUrl: './manage-function.component.html',
  styleUrls: ['./manage-function.component.scss']
})
export class ManageFunctionComponent implements OnInit {

  settings: EvaluationSettings;
  modalRef: BsModalRef;
  modelCriteria = new FunctionCriteria();

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  now: Date;
  userProfile: string;
  profile: any;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;


  constructor(private adminService: AdminService, private router: Router, private settingsService: SettingsService, private localeService: BsLocaleService, private modalService: BsModalService, private userService: UserService,public alertService: AlertService) {
    //setting the it locale for datepicker component
    itLocale.invalidDate = '';
    defineLocale('it', itLocale);
    this.localeService.use('it');
    //leggo le impostazioni
    this.settings = this.settingsService.settings;
    this.userProfile = userService.user.profile;
    this.profile = Profile;
  }

  ngOnInit() {
    this.initDataTable();
  }

  openInsertFunctionModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md', backdrop: "static" });
  }

  openChangeFunctionDescriptionModal(funct: FunctionCriteria): void {
    this.modalRef = this.modalService.show(ChangeDescriptionComponent, { initialState: { function: funct }, backdrop: "static",class: 'modal-md' });

  }

  openShowParameterModal(funct: FunctionCriteria): void {
    this.modalRef = this.modalService.show(ManageParameterComponent, { class: 'modal-lg', initialState: { function: funct }, backdrop: "static" });

  }

  goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.MANAG_FUNCTIONS);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue = pagingSize ? parseInt(pagingSize.value) : 5;

    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      scrollY:"300px",
      scrollCollapse:true,
      serverSide: true,
      processing: false,
      searching: false,
      autoWidth:true,
      lengthChange: false,
      stateSave: false,
      ordering: true,
      info: true,
      
      infoCallback: function (settings, start, end, max, total, pre) {


        return " Totali: " + total;
      },
      
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessuna Funzione',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },

      rowCallback: (row: Node, data: FunctionCriteria, index: number) => {

        $('#function' + data.id, row).bind('click', () => {
          that.openShowParameterModal(data);
        });
        $('#function2' + data.id, row).bind('click', () => {
          that.openChangeFunctionDescriptionModal(data);
        });
        return row;

      },
      ajax: (dataTablesParameters: any, callback) => {

        that.adminService.getManageFunction(this.modelCriteria,DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
          resp => {
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            }, err =>{
              Utils.showServerError(err, this.alertService, this.currentForm);
          }
          );

        err=> {

          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: [],
            draw: 0
          });
        }

      },
      columns: [{
        title: 'Nome',
        data: 'extendedName',
        orderable: false,
        render: function (data, type, row, meta) {
          if (data == null) {
            return '';
          } else {
            return data;
          }
        }

      }, {
        title: 'Descrizione',
        data: 'description',
        render: function (data, type, row, meta) {
          if (data == null) {
            return '';
          } else {
            return data;
          }
        }


      }, 
      {
        title: 'Data Creazione',
        data: 'dateCreation',
        render: function (data, type, row, meta) {
          if (data == null) {
            return '';
          } else {
            return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
          }
        }

      }, {
        title: 'Azione',
        data: 'id',
        orderable: false,
        render: function (data, type, row, meta) {
          let html = '';
          let html2 = '';
          html2 += "<a  id=\"function2" + data + "\" ><span class=\"oi oi-pencil icon-header\" title=\"Descrizione\"  style=\"cursor: pointer;\" aria-hidden=\"true\" ></span></a>";
          html += "<a  id=\"function" + data + "\" ><span class=\"oi oi-magnifying-glass icon-header\" title=\"Parametri\"  style=\"cursor: pointer;margin-right:10px; \" aria-hidden=\"true\" ></span></a>";
          return html + html2;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('width', '80px');
        }
      },
      ]
    };
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


}








