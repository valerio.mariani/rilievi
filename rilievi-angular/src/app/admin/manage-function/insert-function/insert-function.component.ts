import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FunctionCriteria } from '../../../evaluation/model/functionCriteria.model';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { AdminService } from '../../services/admin.service';
import { NgForm } from '@angular/forms';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-insert-function',
  templateUrl: './insert-function.component.html',
  styleUrls: ['./insert-function.component.scss']
})
export class InsertFunctionComponent implements OnInit {
  functionCriteria = new FunctionCriteria();
  @Input()
  modalRef: BsModalRef;
  @ViewChild('insertFormRef', { read: "", static: false })
  currentForm: NgForm;


  constructor(private modalService: BsModalService, public adminService: AdminService,public alertService: AlertService) { }

  ngOnInit() {
  }

  close(): void {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  toggleResults() {
    this.confirmData();
  }

  confirmData() {
   this.close();
  
    const confirmData = {
      title: 'Conferma Dati',
      message: 'Vuoi confermare la nuova Funzione ?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "success" },
      { label: "NO", retValue: "false", colorClass: "danger" }
      ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {

        this.adminService.insertFunction(this.functionCriteria).subscribe(res => {
          this.modalRef.hide();
          
        },
          err => {   
            
            Utils.showServerError(err, this.alertService, this.currentForm);
          });





      }
    });

  }

}
