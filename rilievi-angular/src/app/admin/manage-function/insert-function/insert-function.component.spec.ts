import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertFunctionComponent } from './insert-function.component';

describe('InsertFunctionComponent', () => {
  let component: InsertFunctionComponent;
  let fixture: ComponentFixture<InsertFunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertFunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
