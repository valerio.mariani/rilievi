import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { FunctionCriteria } from 'src/app/evaluation/model/functionCriteria.model';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { AdminService } from '../../services/admin.service';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ParamEnum } from '../param.enum';
import { NgForm } from '@angular/forms';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AlertScope } from 'src/app/shared/alertscope.enum';



@Component({
  selector: 'app-manage-parameter',
  templateUrl: './manage-parameter.component.html',
  styleUrls: ['./manage-parameter.component.scss']
})
export class ManageParameterComponent implements OnInit {

  showList: number;
  parameter: Parameter = new Parameter();
  function: FunctionCriteria;
  functionCriteria = new FunctionCriteria();

  AlertScope: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild('insertFormRef', { read: "", static: false })
  currentForm: NgForm;



  constructor(private modalService: BsModalService, private modalRef: BsModalRef, public adminService: AdminService, protected settingsService: SettingsService, public alertService: AlertService) {
    this.AlertScope = AlertScope;
   }

  ngOnInit() {
    this.functionCriteria = this.function;
    this.showList = 1;
    this.initDataTable();
  }


  insertswitch() {
    this.showList = 2;
    this.parameter.description = undefined;
    this.parameter.nameparam = undefined;
    this.parameter.value = undefined;
  }

  valueswitch() {
    this.showList = 3;
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.MANAG_PROFILES);
    let pagingSize: Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue = pagingSize ? parseInt(pagingSize.value) : 3;
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      stateSave: false,
      ordering: true,
      info: true,
      infoCallback: function (settings, start, end, max, total, pre) {
      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun Parametro della Funzione',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },

      drawCallback: (settings: DataTables.SettingsLegacy) => {
        jQuery('#btnExport').bind('click', () => {

        });
      },
      rowCallback: (row: Node, data: Parameter, index: number) => {

        $('#value' + data.id, row).bind('click', () => {
          this.parameter = data;
          that.valueswitch();

        });

        return row;
      },
      ajax: (dataTablesParameters: any, callback) => {

        that.adminService.getParameters(this.functionCriteria, DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
          resp => {
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsTotal,
              data: resp.data,
              draw: dataTablesParameters.draw
            });
          }, err => {
            Utils.showServerError(err, this.alertService, this.currentForm);
          }
        );

        err => {

          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: [],
            draw: 0
          });
        }

      },
      columns: [
        {
          title: 'Nome',
          data: 'nameParam',
          orderable:true,
        },
        {
          title: 'Descrizione',
          data: 'description'
        },
        {
          title: 'Valore',
          data: 'value'
        },
        {
          title: '',
          data: 'id',
          orderable: false,
          render: function (data, type, row, meta) {
            let html = '';

            html += "<a  id=\"value" + data + "\" ><span class=\"oi oi-pencil icon-header\" title=\"Valore\"  style=\"cursor: pointer;\" aria-hidden=\"true\" ></span></a>";

            return html;



          },
          createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('width', '80px');
          }
        },
      ]
    };

  }


  ngAfterViewInit(): void {
    this.dtTrigger.next();

  }


  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  close(): void {
    if (this.showList == 1 && this.modalRef) {
      this.modalRef.hide();
    } else if (this.modalRef) {
      this.showList = 1;
      this.rerender();
    }
  }


  toggleResults() {
    this.confirmData();
  }

  confirmData() {

    this.adminService.insertParameter(this.functionCriteria, this.parameter).subscribe(res => {
      this.close();

    },
      err => {
        Utils.showServerError(err, this.alertService, this.currentForm);
      }
    );

  }

  toggleResultsValue() {
    this.confirmDataValue();
  }

  confirmDataValue() {

    const confirmData = {
      title: 'Conferma Dati',
      message: 'Vuoi confermare il nuovo Valore ?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "success" },
      { label: "NO", retValue: "false", colorClass: "danger" }
      ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData, backdrop: "static" });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.adminService.changeParameterValue(this.functionCriteria.id, this.parameter).subscribe(res => {
          
          this.close();
          
        },
          err => {
            Utils.showServerError(err, this.alertService, this.currentForm, AlertScope.MODAL);
          });
      }
    });
  }

}




