import { Component, OnInit, ViewChild } from '@angular/core';
import { FunctionCriteria } from 'src/app/evaluation/model/functionCriteria.model';
import { BsModalRef } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { AdminService } from '../../services/admin.service';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-change-description',
  templateUrl: './change-description.component.html',
  styleUrls: ['./change-description.component.scss']
})
export class ChangeDescriptionComponent implements OnInit {
  function: FunctionCriteria;
  functionCriteria = new FunctionCriteria();

  @ViewChild('insertFormRef', { read: "", static: false })
  currentForm: NgForm;


  constructor(private router: Router, private modalRef: BsModalRef, public adminService: AdminService,public alertService: AlertService) { }

  ngOnInit() {
    this.functionCriteria = this.function;
  }

  close(): void {
    if (this.modalRef) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([PathRegistry.MANAGE_FUNCTION]);
      this.modalRef.hide();
    }
  }

  toggleResults() {
    this.confirmData();
  }

  confirmData() {
    this.close();
    this.adminService.changeFunctionDescription(this.functionCriteria).subscribe(res => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([PathRegistry.MANAGE_FUNCTION]);

    },
    err =>{  
      Utils.showServerError(err, this.alertService, this.currentForm);
    });

  }


}


