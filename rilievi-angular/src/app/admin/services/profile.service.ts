import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Function } from 'src/app/core/model/function.model';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { Profile } from 'src/app/shared/models/profile.model';
import { RequestOptions } from 'src/app/shared/models/requestoptions.model';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import Utils from 'src/app/shared/utily/utils';
import { DataTablesResponse } from '../../evaluation/model/datatableResponse.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService extends BaseHttpService{

  getProfiles(request: DataTablesRequest): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    return this.get('/api/profiles?'+ queryStringDt , options )
      .pipe(
        map(data => {
          let list: Array<Profile> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Profile.fromJson(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })
      );
  }

  insertProfile(profile: Profile): Observable<Profile> {

    return this.post('/api/profile',profile)
      .pipe(
        map( data => {
          return Profile.fromJson(data); 
        })
      );
  }

  functionNotAssociated(profile:Profile):Observable<Array<Function>>{
    return this.get('/api/profile/' + profile.id + '/otherfunctions')
    .pipe(
      map( data => {
        let list: Array<Function> = [];
        if (Array.isArray(data)) {
          list = Array.from(data).map(el => Function.fromJSON(el));
        }
        return list;
      })
    );
  }

  addFunction(profile: Profile, funct:Function):Observable<Function>{
    return this.post('/api/profile/' + profile.id + '/function/'+funct.id, null )
    .pipe(
      map( data => {        
        return  Function.fromJSON(data);
      })
    );
  }

  functionAssociated(profile:Profile):Observable<Array<Function>>{
    return this.get('/api/profile/' + profile.id + '/functions')
    .pipe(
      map( data => {
        let list: Array<Function> = [];
        if (Array.isArray(data)) {
          list = Array.from(data).map(el => Function.fromJSON(el));
        }
        return list;
      })
    );
  }

  getFuctionByProfileCode(profileCode:string):Promise<any>{
    return this.get('/api/profile/functions?code=' + profileCode).toPromise();
  }
}
