import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RequestOptions } from 'src/app/shared/models/requestoptions.model';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import { ParameterPair } from '../../shared/services/parameterPair';
import { DataTablesResponse } from '../../evaluation/model/datatableResponse.model';
import { LogCriteria } from '../../evaluation/model/logCriteria.model';
import { Injectable } from '@angular/core';
import { ExtractionDate } from 'src/app/evaluation/model/extractionDate.model';
import { FunctionCriteria } from 'src/app/evaluation/model/functionCriteria.model';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import Utils from 'src/app/shared/utily/utils';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { Function } from 'src/app/core/model/function.model';


@Injectable({
  providedIn: 'root'
})
export class AdminService extends BaseHttpService {

  buildQueryparams(parameterPairs: Array<ParameterPair>) {
    let queryParams = '';
    parameterPairs.map(r => {
      let parameterPairStr = null;
      if (r.name && r.value) {
        ///
        parameterPairStr = r.name + '=' + r.value;
      }
      return parameterPairStr;
    }).forEach(r => {
      if (r) {
        queryParams += '&' + r;
      }
    });

    if (queryParams == '') {

    } else {
      queryParams = queryParams.substr(1);

    }
    return queryParams;
  }
  ///////////////////////////////////////////////LOGS/////////////////////////////////////////////////////////////
  getLogs(log: LogCriteria,request: DataTablesRequest ): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    let logDateFrom = log.logDateFrom != null ? log.logDateFrom.toISOString().substring(0, 10) : null;
    let logDateTo = log.logDateTo != null ? log.logDateTo.toISOString().substring(0, 10) : null;
    let userCode = log.user != null ? log.user : null;
    let queryParams = this.buildQueryparams([
      new ParameterPair('user.code', userCode),
      new ParameterPair('startDate', logDateFrom), 
      new ParameterPair('endDate', logDateTo),]);
    return this.get('/api/auditlogs?' +queryStringDt + '&'+queryParams ,options )
      .pipe(
        map(data => {

          let list: Array<LogCriteria> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => LogCriteria.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })

      );

  }

  exportAllLogs(log: LogCriteria): Observable<Response> {
    let userCode = log.user != null ? log.user : null;
    let actionDate = log.actionDate != null ? log.actionDate : null;
    let actionType = log.actionType != null ? log.actionType : null;
    let action = log.action != null ? log.action : null;


    let options: RequestOptions = new RequestOptions();
    let queryParams = this.buildQueryparams([
      new ParameterPair('dateTime', actionDate),
      new ParameterPair('serviceCode', actionType),
      new ParameterPair('descrAction', action),
      new ParameterPair('user.code', userCode)]);
    return this.download('/api/auditlogs/export?' + queryParams, options)
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////EXTRACTION DATES//////////////////////////////////////////////////


  getExtractionDates(extractDate: ExtractionDate): Observable<Array<ExtractionDate>> {
    let fixedDate = extractDate.fixedDate != null ? extractDate.fixedDate.toISOString().substring(0, 10) : null;
    let closingDate = extractDate.closingDate != null ? extractDate.closingDate.toISOString().substring(0, 10) : null;

    let queryParams = this.buildQueryparams([
      new ParameterPair('closingDate', closingDate),
      new ParameterPair('fixedDate', fixedDate),]);

    return this.get('/api/extractionDates?' + queryParams)
      .pipe(
        map(data => {
          let list: Array<ExtractionDate> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => ExtractionDate.fromJSON(el));
          }
          return list;
        })
      );
  }

  insertExtractionDate(extractionDate: ExtractionDate): Observable<ExtractionDate> {

    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");

    return this.post('/api/extractionDates', extractionDate, options)
      .pipe(
        map(data => {
          return ExtractionDate.fromJSON(data);
        })
      );
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////MANAGE FUNCTION///////////////////////////////////////////////////


  
  getManageFunction(funct: FunctionCriteria,request: DataTablesRequest): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    let functionName = funct.name != null ? funct.name : null;
    let functionDescr = funct.description != null ? funct.description : null;
    let functionDateCreation = funct.dateCreation != null ? funct.dateCreation.toISOString().substring(0, 10) : null;
    let queryParams = this.buildQueryparams([
      new ParameterPair('extendedName', functionName),
      new ParameterPair('description', functionDescr),
      new ParameterPair('dateCreation', functionDateCreation)]);
    return this.get('/api/functions?' +queryStringDt + queryParams,options )
      .pipe(
        map(data => {
          let list: Array<Function> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Function.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })

      );

  }

  insertFunction(insertFunction: FunctionCriteria): Observable<FunctionCriteria> {

    let options: RequestOptions = new RequestOptions();
    options.addHeader("Content-Type", "application/json");

    return this.post('/api/function?', insertFunction, options)
      .pipe(
        map(data => {
          return FunctionCriteria.fromJSON(data);
        })
      );
  }

  insertParameter(funct: FunctionCriteria, insertParameter: Parameter, ): Observable<Parameter> {
    return this.post('/api/function/' + funct.id + '/param', insertParameter)
      .pipe(
        map(data => {
          return Parameter.fromJSON(data);
        })
      );
  }

  getParameters(funct: FunctionCriteria,request: DataTablesRequest): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);


    return this.get('/api/function/' + funct.id + '/params?' +queryStringDt,options )
      .pipe(
        map(data => {
          let list: Array<Parameter> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Parameter.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;

        })
      );

  }

  changeFunctionDescription(funct: FunctionCriteria): Observable<FunctionCriteria> {
    return this.put('/api/function/' + funct.id, funct)
      .pipe(
        map(data => FunctionCriteria.fromJSON(data))
      );
  }

  changeParameterValue(functionId:number, param: Parameter): Observable<Parameter> {
    return this.put('/api/function/' + functionId + '/param/' + param.id, param)
      .pipe(
        map(data => Parameter.fromJSON(data))
      );
  }

  getParameter(functionId:number, paramId: number): Observable<Parameter> {
    return this.get('/api/function/' + functionId + '/param/' + paramId)
      .pipe(
        map(data => { return Parameter.fromJSON(data);})
      );
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
