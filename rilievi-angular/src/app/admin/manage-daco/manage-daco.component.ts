import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Daco } from 'src/app/evaluation/model/daco.model';
import { DataTablesRequest } from 'src/app/evaluation/model/datatableRequest.model';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { EditDacoComponent } from './edit-daco/edit-daco.component';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { Parameter } from 'src/app/evaluation/model/parameter.model';
import { ParamEnum } from '../manage-function/param.enum';
import Utils from 'src/app/shared/utily/utils';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-manage-daco',
  templateUrl: './manage-daco.component.html',
  styleUrls: ['./manage-daco.component.scss']
})
export class ManageDacoComponent implements OnInit {
  settings:EvaluationSettings; 
  modalRef: BsModalRef;
  dacoCriteria = new Daco();
 
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  now: Date;
  userProfile:string;
  profile:any;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;


  constructor(private evaluationService: EvaluationService,private router: Router,private settingsService: SettingsService,private modalService: BsModalService,public alertService: AlertService) { }

  ngOnInit() {

    this.initDataTable();   
  }

  toggleResults(){
 
    this.rerender();
    
      }
 

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.MANAG_DACO);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 5;
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      scrollY: "500px",
      scrollCollapse: true,

      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      stateSave: false,
      ordering: true,
      info: true,
      infoCallback: function (settings, start, end, max, total, pre) {

        return " Totali: " + total;
      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun Daco',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      ajax: (dataTablesParameters: any, callback) => {

          that.evaluationService.getAllDaco(DataTablesRequest.fromDataTablesParameters(dataTablesParameters),this.dacoCriteria).subscribe(
            resp => {
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            }, err =>{
              Utils.showServerError(err, this.alertService, this.currentForm);
          }
          );

        

      },
      columns: [{
        title: 'Voce daco',
        data: 'code',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return data;
        }
        }
        
      }, {
        title: 'Descrizione',
        data: 'description',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else if(data.length>30){
          let html = '';             
          html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data  + "\">"+data.substr( 0, 30 )+"..." +"</label>";
          return html;

        
        }else return data
        }
          
       
      }, {
        title: 'Cocont',
        data: 'accountNumber',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return data;
        }
        }
        
      }, 
      {
        title: 'Descrizione Cocont',
        data: 'accountDescription',
        render:function (data, type, row, meta){
          if (data == null){
            return '';
        }else{
          return data;
        }
        }
        
      },
      {
        title: 'Data inizio',
        data: 'startDate',
        render: function ( data, type, row, meta ) {
          if(data==null){
          
          }else{
            return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');

          }
       }
      },
      {
        title: 'Data fine',
        data: 'endDate',
        render: function ( data, type, row, meta ) {
          return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
       }
      },
      {
        title: 'Azioni',
        data: 'id',
        orderable: false,
        render: function ( data, type, row, meta ) {
         let html = '';             
         html +=  "<a  id=\"edit"+ data +"\" ><span class=\"oi oi-pencil icon-header\" title=\"Modifica\"  style=\"cursor: pointer; \" aria-hidden=\"true\" ></span></a>";
         return html;
        },
        createdCell: function (td, cellData, rowData, row, col) {
           $(td).css('width', '80px');             
         }
      }
    
      ],
      rowCallback: (row: Node, data:  Daco, index: number) => {

        $('#edit'+data.id, row).bind('click', () => {
          that.openEditFunction(data);
        });
        return row;
      },
      
    };
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();

  }


  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  openEditFunction(daco:Daco):void{
    this.modalRef = this.modalService.show(EditDacoComponent, {initialState:{daco: daco},backdrop: "static"});

  }
  openAttachmentsModal(template: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(template,{class: 'modal-lg',backdrop: "static"},);
  }

  goToHome(){
    this.router.navigate([PathRegistry.HOME]);
  }
}
