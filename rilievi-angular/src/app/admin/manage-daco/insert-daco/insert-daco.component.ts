import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { Router } from '@angular/router';
import { SettingsService } from 'src/app/core/services/settings.service';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { Daco } from 'src/app/evaluation/model/daco.model';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import Utils from 'src/app/shared/utily/utils';

@Component({
  selector: 'app-insert-daco',
  templateUrl: './insert-daco.component.html',
  styleUrls: ['./insert-daco.component.scss']
})
export class InsertDacoComponent implements OnInit {
  dacoCriteria = new Daco();
  now= new Date();
  settings: EvaluationSettings;
  @Input()
  modalRef: BsModalRef;
  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;
  constructor(private evaluationService: EvaluationService,private router: Router,private settingsService: SettingsService,private modalService: BsModalService,public alertService: AlertService) { }

  ngOnInit() { 
    this.dacoCriteria.startDate = this.now;
  }
  confirmDaco(){
    this.close(); 
    const confirmData = {
      title: 'Conferma nuova Voce Daco',
      message: 'Vuoi confermare questa nuova Voce Daco?' ,
      buttons: [{  label: "SI",  retValue: "true", colorClass: "success" },
                {  label:"NO", retValue:"false", colorClass:"danger"}
              ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData,backdrop:"static" });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){

        this.evaluationService.insertDaco(this.dacoCriteria).subscribe(
          resp => {
            this.close();         
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate([PathRegistry.MANAGE_DACO]);
          }, 
          err =>{
            Utils.showServerError(err, this.alertService, this.currentForm);
        })

          
      }
    });

  }
  close():void{
    if(this.modalRef){
      this.modalRef.hide();
    }
  }
}
