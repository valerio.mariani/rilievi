import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertDacoComponent } from './insert-daco.component';

describe('InsertDacoComponent', () => {
  let component: InsertDacoComponent;
  let fixture: ComponentFixture<InsertDacoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertDacoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertDacoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
