import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDacoComponent } from './manage-daco.component';

describe('ManageDacoComponent', () => {
  let component: ManageDacoComponent;
  let fixture: ComponentFixture<ManageDacoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDacoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDacoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
