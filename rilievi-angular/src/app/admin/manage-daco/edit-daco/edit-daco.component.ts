import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsLocaleService, defineLocale, itLocale } from 'ngx-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Daco } from 'src/app/evaluation/model/daco.model';
import { EvaluationService } from 'src/app/evaluation/services/evaluation.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import Utils from 'src/app/shared/utily/utils';

@Component({
  selector: 'app-edit-daco',
  templateUrl: './edit-daco.component.html',
  styleUrls: ['./edit-daco.component.scss']
})
export class EditDacoComponent implements OnInit {

  daco: Daco;
  dacoCriteria = new Daco();
  @Input()
 
  settings: EvaluationSettings;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;
  constructor(private router: Router,private evaluationService: EvaluationService,private  modalRef: BsModalRef, private settingsService: SettingsService,private localeService: BsLocaleService,private modalService: BsModalService,public alertService: AlertService) { }

  ngOnInit() {
this.dacoCriteria=this.daco
 //setting the it locale for datepicker component
 itLocale.invalidDate = '';
 defineLocale('it', itLocale);
 this.localeService.use('it');
 //leggo le impostazioni
 this.settings = this.settingsService.settings;
    }
  close():void{
    if(this.modalRef){
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([PathRegistry.MANAGE_DACO]);
      this.modalRef.hide();
    }
  }

  confirmChange(){
    this.close();
    const modData = {
      title: 'Modifica Voce Daco ' + this.daco.code,
      message: 'Confermi i dati inseriti?' ,
      buttons: [{  label: "SI",  retValue: "true", colorClass: "success" },
                {  label:"NO", retValue:"false", colorClass:"danger"}
              ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: modData,backdrop:"static" });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
        this.evaluationService.setDaco(this.dacoCriteria).subscribe(data => {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([PathRegistry.MANAGE_DACO]);

          return data
        },
         err =>{
          Utils.showServerError(err, this.alertService, this.currentForm);
      });

      }
    });

  }
}
