import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDacoComponent } from './edit-daco.component';

describe('EditDacoComponent', () => {
  let component: EditDacoComponent;
  let fixture: ComponentFixture<EditDacoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDacoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDacoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
