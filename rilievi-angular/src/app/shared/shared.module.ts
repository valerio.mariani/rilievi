import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AlertComponent } from './components/alert/alert.component';
import { ErrorComponent } from './components/error/error.component';
import { AlertService } from './services/alert.service';
import { LogoutComponent } from './components/logout/logout.component';



@NgModule({
  declarations: [AlertComponent, ErrorComponent, LogoutComponent],
  imports: [
    CommonModule
  ],
  exports : [
    AlertComponent
  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [AlertService]
    };
  }
 }
