import { Component, OnInit, Input } from '@angular/core';
import { AlertService } from '../../services/alert.service';
import { AlertScope } from '../../alertscope.enum';
import { AlertStore } from '../../models/alertstore.model';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input()
  scope: AlertScope;

  private successStyleDiv: string = "alert-success";
  private errorStyleDiv: string = "alert-danger";
  private warnStyleDiv: string = "alert-warning";
  private infoStyleDiv: string = "alert-info";


  alertStore: AlertStore;

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {

    let currentScope = this.scope ? this.scope : AlertScope.PAGE;

    this.alertService.getAlerts().
      subscribe(alert => {
        if (alert.scope == currentScope) {
          if (alert.typeEmpty) {
            this.clear()
          } else {
            this.alertStore = alert;
          }
        }
      });
  }


  clear() {
    this.alertStore = null;
  }

  getStyle() {

    if ("success" == this.alertStore.level) {
      return this.successStyleDiv;
    } else if ("error" == this.alertStore.level) {
      return this.errorStyleDiv;
    } else if ("warn" == this.alertStore.level) {
      return this.warnStyleDiv;
    } else if ("info" == this.alertStore.level) {
      return this.infoStyleDiv;
    }

  }


}
