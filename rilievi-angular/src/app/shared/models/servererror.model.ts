export class ServerError {
    
    status: string;
    errorCode: string;
    message: string;
    details: Array<string>;
    timestamp: Date;
    path: string;
    fields : [string, Array<string>];

     /**
   * Conversion method JSON => RaiServerError
   * */
  static fromJson(json: Object): ServerError {
    const error = new ServerError();
    error.message = 'Unknown error';
    
    if (json) {
      error.status = json['status'];
      error.errorCode = json['error_code'];
      error.details = json['details'] || [];
      if(json['message']) {
        error.message =  json['message'];
      }
      error.timestamp = json['timestamp'] ? new Date(Date.parse(json['timestamp']+'Z')) : void 0;
      error.path = json['path'];
    }
    if( json['fields'] != null ){
      error.fields = json['fields'] || [];/*new Array<string>();
      Object.keys(json['fields']).forEach(function(key) {
          error.fields[key] = json['fields'][key] || [];/*new Array<string>();
          let errList = json['fields'][key]
          errList.array.forEach(element => {
              error.fields[key] = element;
          });
      }); */
    }
    return error;
  }

}