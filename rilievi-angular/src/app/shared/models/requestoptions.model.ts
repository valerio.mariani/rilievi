import {HttpHeaders, HttpParams} from '@angular/common/http';

/**
 * Typed object for options in HTTP request, based on Angular HttpClient module.
 * */
export class RequestOptions {

  /**
   * headers option of the HTTP request
   * */
  headers?: HttpHeaders;

  /**
   * observe option of the HTTP request; default is 'body'
   * */
  observe?: any = 'body';

  /**
   * Parameters of the HTTP request
   * */
  params?: HttpParams;

  /**
   * reportProgress option of the HTTP request
   * */
  reportProgress?: boolean;

  /**
   * responseType option of the HTTP request; default is 'json'
   * */
  responseType?: any = 'json';


  /**
   * withCredentials option of the HTTP request
   * */
  withCredentials?: boolean;

  /**
   * Create and add header to the outgoing HTTP request
   * @param{string} key header's key
   * @param{string} value header's value
   * */
  addHeader(key: string, value: string) {
    if (!this.headers) {
      this.headers = new HttpHeaders();
    }

    this.headers = this.headers.set(key, value);
  }

  /**
   * Remove a single header from the outgoing HTTP request
   * @param{string} key header's key to be removed
   * */
  removeHeader(key: string) {
    if (!this.headers) {
      this.headers = new HttpHeaders();
    }

    if (this.headers.has(key)) {
      this.headers = this.headers.delete(key);
    }
  }

  /**
   * Add a query string param to the outgoing HTTP request
   * @param{string} key param's key to be added
   * @param{string} value param's value to be added
   * */
  addParam(key: string, value: any) {
    if (!this.params) {
      this.params = new HttpParams();
    }

    this.params = this.params.set(key, value);
  }
}
