import { AlertScope } from '../alertscope.enum';

export class AlertStore {

    public title: string;
    messages: string[];
    scope: AlertScope;
    typeEmpty: boolean = false;

    constructor(public level?: string) {
    }

}