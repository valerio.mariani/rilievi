export enum FunctionEnum {

    INS_EVAL = "INS_EVAL", // "Inserimento Rilievo"
    
    MANAG_ATTACHMENT= "MANAG_ATTACHMENT", //"Gestione Allegato"
    
    READ_ATTACHMENT= "READ_ATTACHMENT",//"Leggi Allegato"
    
    RECLA_DACO= "RECLA_DACO",//"Riclassificazione Voce Daco"
    
    INQUIRY_EVAL= "INQUIRY_EVAL",//"Consultazione Rilievi"
    
    MANAG_NOTE= "MANAG_NOTE",//"Gestione Note Aggiuntive"
    
    DELETE_EVAL= "DELETE_EVAL",//"Cancellazione Rilievo"
    
    SETLOSS_EVAL= "SETLOSS_EVAL",//"Connotazione a perdita"
    
    CHANGE_EVAL_REASON= "CHANGE_EVAL_REASON",//"Modifica Casuale Rilievo"
    
    CHANGE_EVAL_FRACT= "CHANGE_EVAL_FRACT",//"Modifica Frazionario"
    
    AUDIT_LOG= "AUDIT_LOG",//"Monitoraggio log di audit"
    
    REPO_EVAL= "REPO_EVAL",//"Reportistica"
    
    REGO_MAXI_EVAL= "REGO_MAXI_EVAL",//"Regolarizzazione Maxi Rilievi"
    
    MANAG_DACO= "MANAG_DACO",//"Gestione Voci Daco"
    
    DATES_EXTRACT= "DATES_EXTRACT",//"Gestione Date Intervalli Estrazione"
    
    MANAG_PROFILES= "MANAG_PROFILES",//"Gestione profili utente"
    
    MANAG_FUNCTIONS= "MANAG_FUNCTIONS",//"Gestione funzioni",
    
    REG_EVAL= "REG_EVAL",//"Regolarizzazione Rilievo",
    
    UNDO_REG_EVAL= "UNDO_REG_EVAL",//"Annullo Regolarizzazione Rilievo"
    
}