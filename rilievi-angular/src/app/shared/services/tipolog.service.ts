import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { Branch } from 'src/app/evaluation/model/branch.model';
import { Reason } from 'src/app/evaluation/model/reason.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Office } from 'src/app/evaluation/model/office.model';
import { Status } from 'src/app/evaluation/model/status.model';
import { Daco } from 'src/app/evaluation/model/daco.model';

@Injectable({
  providedIn: 'root'
})
export class TipologService extends BaseHttpService{

  getBranches(): Observable<Array<Branch>> {

    return this.get('/api/branches')
      .pipe(
        map(data => {
          let list: Array<Branch> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Branch.fromJSON(el));
          }
          return list;
        })
      );
  }

  getOffices(branchId:number): Observable<Array<Office>> {
    return this.get('/api/branch/'+ branchId + '/offices')
      .pipe(
        map(data => {
          let list: Array<Office> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Office.fromJSON(el));
          }
          return list;
        })
      );
  }

  getReasonItem(): Observable<Array<Reason>> {
    return this.get('/api/reasons')
      .pipe(
        map(data => {
          let list: Array<Reason> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Reason.fromJSON(el));
          }
          return list;
        })
      );
  }

  getStatus(): Observable<Array<Status>> {
    return this.get('/api/status')
      .pipe(
        map(data => {
          let list: Array<Status> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Status.fromJSON(el));
          }
          return list;
        })
      );
  }

  getDacoItemMaxiEvaluation(): Observable<Array<Daco>> {
    return this.get('/api/dacos/maxi-evaluation')
      .pipe(
        map(data => {
          let list: Array<Daco> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Daco.fromJSON(el));
          }
          return list;
        })
      );
  }

  getDacoItemExcludedMaxiEvaluation(): Observable<Array<Daco>> {
    return this.get('/api/dacos/maxi-evaluation-excluded')
      .pipe(
        map(data => {
          let list: Array<Daco> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Daco.fromJSON(el));
          }
          return list;
        })
      );
  }
}
