import { Injectable, EventEmitter } from '@angular/core';
import { AlertStore } from '../models/alertstore.model';
import { AlertScope} from '../alertscope.enum';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  private emitter = new EventEmitter<AlertStore>();

  alertStore: AlertStore;
  private defaultErrorTitle: string = "error";
  private defaultWarningTitle: string = "warning";
  private defaultSuccessTitle: string = "success";
  private defaultInfoTitle: string = "info";



  public addErrorMessage(title: string, messages?: string[], scope?: AlertScope) {
    this.clear(scope);
    if (title == null) {
      title = this.defaultErrorTitle;
    }
    this.createMessage("error", messages, title, scope);
    this.emitter.emit(this.alertStore);
  }


  public addWarningMessage(title: string, messages?: string[], scope?: AlertScope) {
    this.clear(scope);
    if (title == null) {
      title = this.defaultWarningTitle;
    }
    this.createMessage("warn", messages, title, scope);
    this.emitter.emit(this.alertStore);
  }


  public addSuccessMessage(title: string, messages?: string[], scope?: AlertScope) {
    this.clear(scope);
    if (title == null) {
      title = this.defaultSuccessTitle;
    }
    this.createMessage("success", messages, title, scope);
    this.emitter.emit(this.alertStore);
  }


  public addInfoMessage(title: string, messages?: string[], scope?: AlertScope) {
    this.clear(scope);
    if (title == null) {
      title = this.defaultInfoTitle;
    }
    this.createMessage("info", messages, title, scope);
    this.emitter.emit(this.alertStore);
  }


  private createMessage(level: string, messages: string[], title: string, scope: AlertScope) {
    this.alertStore = new AlertStore(level);
    
    this.alertStore.messages = messages;
    this.alertStore.title = title;
    this.alertStore.scope = scope ? scope : AlertScope.PAGE;
  }

  public clear(scope?: AlertScope) {
    this.alertStore = new AlertStore();
    this.alertStore.scope=scope ? scope : AlertScope.PAGE;
    this.alertStore.typeEmpty=true;
    this.emitter.emit(this.alertStore);
  }

   public getAlerts(): EventEmitter<AlertStore> {
    return this.emitter;
  }
}
