import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Function } from 'src/app/core/model/function.model';

@Injectable({
  providedIn: 'root'
})
export class FunctionService extends BaseHttpService{


  getFunction(profileCode:string): Observable<Array<Function>> {

    return this.get('/profile/'+profileCode+'/functions')
      .pipe(
        map(data => {
          let list: Array<Function> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Function.fromJSON(el));
          }
          return list;
        })
      );
  }
}
