import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UserService } from 'src/app/core/services/user.service';
import { RequestOptions } from '../models/requestoptions.model';
import { ServerError } from '../models/servererror.model';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpService {


  constructor(protected http: HttpClient, protected userService: UserService) { 
  }

  /**
   * Add the user info  header to all server's request
   * @param{RequestOptions} options typed object requested by all the HttpClient's method
   * */
  protected addUserHeaders(options: RequestOptions): void {
    options.addHeader("USER_ID",this.userService.getUser().code);
    options.addHeader("USER_PROFILE",this.userService.getUser().profile);
  }
  /**
   * Handle the server's response  
   * @param{any} response data object returned from server
   * */
  protected static handleServerResponse(response: any): any {
    return response;
  }

  /**
   * Handle the error server response throwing a ServerError
   * @param{HttpErrorResponse} error from server
   * */
  protected static handleServerError(error: HttpErrorResponse): any {
    return throwError(ServerError.fromJson(error.error));
  }

  /**
   * Add the default content-type header to all server's request
   * @param{RequestOptions} options typed object requested by all the HttpClient's method
   * */
  private addHeaders(options: RequestOptions): void {
    options.addHeader('Content-Type', 'application/json');
  }

  /**
   * HTTP GET method
   * @param{string} url service endpoint
   * @param{RequestOptions} options typed object requested by the HttpClient GET method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public get<T = any>(url: string, options: RequestOptions = new RequestOptions()): Observable<T> {
    this.addHeaders(options);
    this.addUserHeaders(options);
    //Per evitare la lettura dalla cache
    url = url.indexOf('?') != -1? url +"&timestamp=" + Date.now(): url +"?timestamp=" + Date.now();
    return this.http.get<T>(url, options)
      .pipe(
        map(response => BaseHttpService.handleServerResponse(response)),
        catchError((error) => BaseHttpService.handleServerError(error)),
      );
  }


    /**
   * HTTP GET method
   * @param{string} url service endpoint
   * @param{RequestOptions} options typed object requested by the HttpClient GET method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public download<T = any>(url: string, options: RequestOptions = new RequestOptions()): Observable<Response> {
    this.addUserHeaders(options);
    options.responseType = 'blob';
    options.observe= 'response';
    return this.http.get<Response>(url, options);
  }
  /**
   * HTTP POST method
   * @param{string} url service endpoint
   * @param{any} body request body
   * @param{RequestOptions} options typed object requested by the HttpClient POST method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public post<T = any>(url: string, body: any, options: RequestOptions = new RequestOptions()): Observable<T> {
    this.addHeaders(options);
    this.addUserHeaders(options);
    return this.http.post<T>(url, body, options)
      .pipe(
        map(response => BaseHttpService.handleServerResponse(response)),
        catchError((error) => BaseHttpService.handleServerError(error)),
      );
  }

  /**
   * HTTP PUT method
   * @param{string} url service endpoint
   * @param{any} body request body
   * @param{RequestOptions} options typed object requested by the HttpClient PUT method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public put<T = any>(url: string, body: any , options: RequestOptions = new RequestOptions()): Observable<T> {
    this.addHeaders(options);    
    this.addUserHeaders(options);
    url = url.indexOf('?') != -1? url +"&timestamp=" + Date.now(): url +"?timestamp=" + Date.now();
    return this.http.put<T>(url, body, options)
      .pipe(
        map(response => BaseHttpService.handleServerResponse(response)),
        catchError((error) => BaseHttpService.handleServerError(error)),
      );
  }

  /**
   * HTTP PATCH method
   * @param{string} url service endpoint
   * @param{any} body request body
   * @param{RequestOptions} options typed object requested by the HttpClient PATCH method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public patch<T = any>(url: string, body: any, options: RequestOptions = new RequestOptions()): Observable<T> {
    this.addHeaders(options);
    this.addUserHeaders(options);
    return this.http.patch<T>(url, body, options)
      .pipe(
        map(response => BaseHttpService.handleServerResponse(response)),
        catchError((error) => BaseHttpService.handleServerError(error)),
      );
  }

  /**
   * HTTP DELETE method
   * @param{string} url service endpoint
   * @param{RequestOptions} options typed object requested by the HttpClient DELETE method
   * @returns{Observable<T>} observable of the type T specified (T = any if none is provided)
   * */
  public delete<T = any>(url: string, options: RequestOptions = new RequestOptions()): Observable<T> {
    this.addHeaders(options);
    this.addUserHeaders(options);
    return this.http.delete<T>(url, options)
      .pipe(
        map(response => BaseHttpService.handleServerResponse(response)),
        catchError((error) => BaseHttpService.handleServerError(error)),
      );
  }

}
