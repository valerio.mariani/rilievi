import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsertComponent } from './components/insert/insert.component';
import { FormsModule } from '@angular/forms';
import { ModalModule, BsDatepickerModule  } from 'ngx-bootstrap';
import { AttachmentComponent } from './components/attachment/attachment.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GenericModalComponent } from '../shared/components/generic-modal/generic-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import { NgxCurrencyModule } from "ngx-currency";
import { DacoReclassificationComponent } from './components/daco-reclassification/daco-reclassification.component';
import { DetailComponent } from './components/detail/detail.component';
import { InquiryComponent } from './components/inquiry/inquiry.component';
import { ManageDetailComponent } from './components/manage-detail/manage-detail.component';
import { NoteComponent } from './components/note/note.component';
import { ModalSelectStatusComponent } from './components/modal-select-status/modal-select-status.component';
import { ReportComponent } from './components/report/report.component';
import { MaxiEvaluationComponent } from './components/maxi-evaluation/maxi-evaluation.component';
import { SearchManageComponent } from './components/search-manage/search-manage.component';

registerLocaleData(localeIt, 'it');
@NgModule({

  declarations: [InsertComponent, AttachmentComponent,GenericModalComponent, DacoReclassificationComponent, DetailComponent, InquiryComponent, ManageDetailComponent, NoteComponent, ModalSelectStatusComponent, ReportComponent, MaxiEvaluationComponent,SearchManageComponent],

  imports: [
    CommonModule,
    FormsModule, 
    NgSelectModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    DataTablesModule,
    NgxCurrencyModule
  ],
  exports : [
    InsertComponent, NoteComponent 
  ], 
  entryComponents: [GenericModalComponent, AttachmentComponent]
})
export class EvaluationModule { 
  constructor(){ 
  }
}
 