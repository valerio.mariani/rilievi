import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RequestOptions } from 'src/app/shared/models/requestoptions.model';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import Utils from 'src/app/shared/utily/utils';
import { Attachment } from '../model/attachment.model';
import { DataTablesRequest } from '../model/datatableRequest.model';
import { DataTablesResponse } from '../model/datatableResponse.model';


@Injectable({
  providedIn: 'root'
})
export class AttachmentService extends BaseHttpService{


  getEvaluationAttachments(request: DataTablesRequest, evaluationId:number): Observable<DataTablesResponse> {
    let options: RequestOptions = new RequestOptions();
    let queryStringDt = Utils.serialize(request);
    return this.get('/api/evaluation/' + evaluationId + '/attachments?'+ queryStringDt , options  )
      .pipe(
        map(data => {
          let list: Array<Attachment> = [];
          if (Array.isArray(data['items'])) {
            list = Array.from(data['items']).map(el => Attachment.fromJSON(el));
          }
          let resp = new DataTablesResponse();
          resp.data = list;
          resp.recordsTotal = data['totalRows'];
          resp.totalPages = data['totalPages'];
          resp.pageNumber = data['pageNumber'];
          return resp;
        })
      );
  }
 /*
  getAttachment(evaluationId: number, attachmentId: number): Observable<HttpResponse<string>> {
    return this.get('/api/evaluation/' + evaluationId + '/attachment/'+attachmentId);
  }
 */
getAttachment(evaluationId: number, attachmentId: number): Observable<Attachment> {
  return this.get('/api/evaluation/' + evaluationId + '/attachment/'+attachmentId).pipe(
    map(data => {
      return Attachment.fromJSON(data);
    })
  );
}

  deleteAttachment(evaluationId: number, attachmentId: number): Observable<Attachment> {
    return this.delete('/api/evaluation/' + evaluationId + '/attachment/'+attachmentId)
      .pipe(
        map(data => {          
          let resp = Attachment.fromJSON(data);
          return resp;
        })
      );
  }

  uploadAttachments(evaluationId: number, attachment: Array<Attachment>): Observable<Array<Attachment>> {
    return this.post('/api/evaluation/' + evaluationId + '/attachments/',attachment)
      .pipe(
        map(data => {     
          let list: Array<Attachment> = [];
          if (Array.isArray(data)) {
            list = Array.from(data).map(el => Attachment.fromJSON(el));
          }     
          return list;
        })
      );
  }

  uploadAttachment(evaluationId: number, attachment: Attachment): Observable<Attachment> {
    return this.post('/api/evaluation/' + evaluationId + '/attachment/',attachment)
      .pipe(
        map(data => {     
          let resp = Attachment.fromJSON(data);
          return resp;
        })
      );
  }
}
