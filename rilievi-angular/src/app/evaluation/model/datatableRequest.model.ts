export class DataTablesRequest {
   
    pageSize: number;
    pageNumber: number;
    orderType: string;
    orderField: string;
    

    static fromDataTablesParameters(dataTablesParameters: any): DataTablesRequest{
      let req = new DataTablesRequest();
      if(dataTablesParameters){
        req.pageNumber = dataTablesParameters.start == 0 ?1 : Math.round(dataTablesParameters.start/dataTablesParameters.length+1);
        req.pageSize =  dataTablesParameters.length;
        if( dataTablesParameters.order && dataTablesParameters.order.length ){
          req.orderType = dataTablesParameters.order[0].dir;
          req.orderField = dataTablesParameters.columns[dataTablesParameters.order[0].column].data;
        }
       
      }
      return req;
    }

  
  }