
import Utils from 'src/app/shared/utily/utils';


export class LogCriteria{

logDateFrom:Date;

logDateTo:Date;
    
actionDate:Date;

actionType:string;

actionDescr: string;

user:string;

action:string;  

dateTime: Date;

result:number;


static fromJSON(json: any): LogCriteria {
    let log;
    if (json) {
        log = new LogCriteria();
        log.user = json['user']['code'];
        log.dateTime =Utils.parseDate( json['dateTime']);
        log.actionType = json['serviceCode'];
        log.action = json['descrAction'];
        log.result = json['result'];
        log.logDateFrom = json['startDate'];
        log.logDateTo = json['endDate'];
        log.actionDescr = json['serviceDescr'];
    }
    return log;
} 

}