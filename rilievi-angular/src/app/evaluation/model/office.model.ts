export class Office {
    id: number;
    fractionalDescription: string;
    email: string;
    fractional: string;
    type: string;
    full:string;

    static fromJSON(json: any): Office {
        let office;
        if(json){
        office = new Office();
        office.id = json['id'];
        office.fractionalDescription = json['fractionalDescription'];
        office.email = json['email'];
        office.fractional = json['fractional'];
        office.type = json['type'];
        }

        office.full = '(' + office.fractional + ')' + '  ' + office.fractionalDescription;
        return office;
    }
}
