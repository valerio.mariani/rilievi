export class Status{

    id:number;

    code: number;

    description: string;

    full:string;

    static fromJSON(json: any): Status {
        let status;
        if (json) {
            status = new Status();
            status.id = json['id'];
            status.code = json['code'];
            status.description = json['description'];
            status.full = '(' + status.code + ')' + '            ' + status.description;
        }
        return status;
    }

}