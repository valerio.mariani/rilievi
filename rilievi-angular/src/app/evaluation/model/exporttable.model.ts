export class ExportTable{

    fileName: string;
	
	content: string;
	
    contentType: string;
    
    static fromJSON(json: any): ExportTable {
        let exportTab;
        if (json) {
            exportTab = new ExportTable();
            exportTab.fileName = json['fileName'];
            exportTab.content = json['content'];
            exportTab.contentType = json['contentType'];
        }
        return exportTab;
    }
}