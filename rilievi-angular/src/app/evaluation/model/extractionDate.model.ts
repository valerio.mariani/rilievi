import Utils from 'src/app/shared/utily/utils';


export class ExtractionDate{

 fixedDate:Date;
 closingDate:Date;

 static fromJSON(json: any): ExtractionDate {
    let extractionDate;
    if (json) {
        extractionDate = new ExtractionDate();
        extractionDate.closingDate = Utils.parseDate( json['closingDate']);
        extractionDate.fixedDate =Utils.parseDate( json['fixedDate']);
        
    }
    return extractionDate;
}

}