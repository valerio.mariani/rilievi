
export class ResponseError {

    status: string;
	errorCode: string;
	message: string;
    details: Array<string>;
    fields : [string, Array<string>];
  //  [Key: string]: Array<string>;


  static fromJSON(json: any): ResponseError {
    const error = new ResponseError();
    error.status = json['status'];
    error.errorCode = json['error_code'];
    if( json['details'] != null ){
        error.details = new Array<string>();
        json['details'].array.forEach(element => {
            error.details.push(element);
        });
    }
    if( json['fields'] != null ){
        for( let key of json['fields']){
            error.fields[key] = new Array<string>();
            let errList = json['fields'][key]
            errList.array.forEach(element => {
                error.fields[key] = element;
            });
        } 
    }
    return error;
}
}