import { Daco } from './daco.model';

export class MaxiEvaluationCriteria{

  dateFrom: Date;

  dateTo: Date;

  amountFrom: number;

  amountTo: number;

  dacos: Array<Daco> = new Array<Daco>();

  pageSize: number;

  pageNumber: number;

}