import Utils from 'src/app/shared/utily/utils';

export class FunctionCriteria{

    id:number;

    dateCreation:Date;

    name:string;

    description:string;


    static fromJSON(json: any): FunctionCriteria {
        let funct;
        if (json) {
            funct= new FunctionCriteria();
            funct.id=json['id'];
            funct.name= json['name'];
            funct.description= json['description'];
            funct.dateCreation = Utils.parseDate( json['dateCreation']);

        }
        return funct;
    }

}