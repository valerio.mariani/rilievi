import { Daco } from './daco.model';
import Utils from 'src/app/shared/utily/utils';

export class MaxiEvaluationSettings{

    excludedDacos: Array<Daco> = new Array<Daco>();
    limitDate:Date;


    static fromJSON(json:any):MaxiEvaluationSettings{
        let ret:MaxiEvaluationSettings;
        if(json){
            ret = new MaxiEvaluationSettings();
            ret.limitDate = Utils.parseDate(json['limitDate']);
            if(json['excludedDacos']){
                json['excludedDacos'].forEach(element => {
                    ret.excludedDacos.push(Daco.fromJSON(element));
                });
            }
        }

        return ret;
    }



}