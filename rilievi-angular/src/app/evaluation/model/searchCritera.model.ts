import Utils from 'src/app/shared/utily/utils';
import { Branch } from './branch.model';
import { Daco } from "./daco.model";
import { Office } from './office.model';
import { Reason } from './reason.model';
import { Status } from './status.model';



export class SearchCriteria {
    
    private insertDateFrom: Date;

    private insertDateTo: Date;

    private competenceDateFrom: Date;

    private competenceDateTo: Date;

    amountTo: number;

    amountFrom: number;

    showRemoved: boolean;

    daco: Daco;

    branch: Branch;

    office: Office;

    fractional: string;

    reason: Reason;

    status:Status;
    
    appUserCode: string;

    searchStatus: string;

    filename:string;
    
    type: string;
    
    idChild: number;

    showOnlySendedBridge:boolean;
	
	showOnlyAccounted:boolean;

    get _insertDateFrom() : Date {
        return this.insertDateFrom;
    }

    set _insertDateFrom(_insertDateFrom : Date) {
        this.insertDateFrom =  _insertDateFrom && !isNaN(_insertDateFrom.getTime()) ? _insertDateFrom : void 0;
    }
    get _insertDateTo() : Date {
        return this.insertDateTo;
    }

    set _insertDateTo(_insertDateTo : Date) {
        this.insertDateTo =  _insertDateTo && !isNaN(_insertDateTo.getTime()) ? _insertDateTo : void 0;
    }
    get _competenceDateFrom() : Date {
        return this.competenceDateFrom;
    }

    set _competenceDateFrom(_competenceDateFrom : Date) {
        this.competenceDateFrom =  _competenceDateFrom && !isNaN(_competenceDateFrom.getTime()) ? _competenceDateFrom : void 0;
    }
    get _competenceDateTo() : Date {
        return this.competenceDateTo;
    }

    set _competenceDateTo(_competenceDateTo : Date) {
        this.competenceDateTo =  _competenceDateTo && !isNaN(_competenceDateTo.getTime()) ? _competenceDateTo : void 0;
    }

    static fromJSON(json: any, ret:SearchCriteria = new SearchCriteria()): SearchCriteria {
      
        if(json){
          
            ret._insertDateFrom = Utils.parseDate(json['insertDateFrom']);
            ret._insertDateTo = Utils.parseDate(json['insertDateTo']);
            ret._competenceDateFrom = Utils.parseDate(json['competenceDateFrom']);
            ret._competenceDateTo = Utils.parseDate(json['competenceDateTo']);
            ret.type = json['type'];
            ret.amountFrom = json['amountFrom'];
            ret.amountTo = json['amountTo'];
        }

        return ret;
    }
}