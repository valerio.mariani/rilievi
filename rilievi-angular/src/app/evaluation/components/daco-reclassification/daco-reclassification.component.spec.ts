import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DacoReclassificationComponent } from './daco-reclassification.component';

describe('DacoReclassificationComponent', () => {
  let component: DacoReclassificationComponent;
  let fixture: ComponentFixture<DacoReclassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DacoReclassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DacoReclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
