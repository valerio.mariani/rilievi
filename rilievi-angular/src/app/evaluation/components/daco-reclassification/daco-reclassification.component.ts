import { Component, OnInit } from '@angular/core';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { InsertComponent } from '../insert/insert.component';
import { Evaluation } from '../../model/evaluation.model';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { Daco } from '../../model/daco.model';
import Utils from 'src/app/shared/utily/utils';
import { Function } from 'src/app/core/model/function.model';
import { TabHeadingDirective } from 'ngx-bootstrap';

@Component({
  selector: 'daco-reclassification',
  templateUrl: './daco-reclassification.component.html',
  styleUrls: ['./daco-reclassification.component.scss']
})
export class DacoReclassificationComponent extends InsertComponent implements OnInit {

  evalGenerated: Evaluation = new Evaluation();
  dacoListSource: Array<Daco>;
  dacoListGenerated: Array<Daco>;

  labelDebit: string = "DARE (DIMINUISCE)/DARE (AUMENTA)";
  labelCredit: string = "AVERE (AUMENTA)/AVERE (DIMINUISCE)";
  dynamicLabel: string = "";
  ngOnInit() {
    super.ngOnInit();
    this.model.regularizationDate = this.model._insertDate;
    this.model.reason = this.settings.raReason;
    this.model.generated = this.evalGenerated;


  }

  afterDacoListLoaded(): void {
    this.dacoListSource = Utils.cloneDacoList(this.dacoList, null, null, null);
    this.dacoListGenerated = Utils.cloneDacoList(this.dacoList, null, null, null);
  }

  switchLabel() {
    if (this.model.debitAmount) {
      this.dynamicLabel = "AVERE (AUMENTA)";
    } 
    if (this.model.creditAmount) {
      this.dynamicLabel = "DARE (DIMINUISCE)";
    }
  
  }
  initDataTable() {
    super.initDataTable();
    this.dtOptions.scrollCollapse = true;
    this.dtOptions.scrollX = true;
    this.dtOptions.autoWidth = false;
  }

  filterDacoOnchange(isSource: boolean) {
    let isDebit: boolean;
    if (isSource) {
      if (this.model.daco) {
        this.dacoListGenerated = Utils.cloneDacoList(this.dacoList, this.model.daco, true, Utils.isDacoDebit(this.model.daco))
      } else {
        this.dacoListGenerated = Utils.cloneDacoList(this.dacoList, null, null, null);
      }

    } else {
      if (this.evalGenerated.daco) {
        this.dacoListSource = Utils.cloneDacoList(this.dacoList, this.evalGenerated.daco, true, Utils.isDacoDebit(this.evalGenerated.daco))
      } else {
        this.dacoListSource = Utils.cloneDacoList(this.dacoList, null, null, null);
      }
    }
    //Cambio Label
    if (!this.model.daco && !this.evalGenerated.daco) {
      this.labelDebit = "DARE (DIMINUISCE)/DARE (AUMENTA)";
      this.labelCredit = "AVERE (AUMENTA)/AVERE (DIMINUISCE)";
    } else if ((this.model.daco && Utils.isDacoDebit(this.model.daco)) || (this.evalGenerated.daco && Utils.isDacoDebit(this.evalGenerated.daco))) {
      this.labelCredit = "AVERE (AUMENTA)";
      this.labelDebit = "DARE (DIMINUISCE)";
    } else {
      this.labelCredit = "AVERE (DIMINUISCE)";
      this.labelDebit = "DARE (AUMENTA)";
    }

  }

  checkDacoEquals() {
    if (this.model.daco && this.evalGenerated.daco && this.model.daco.id == this.evalGenerated.daco.id) {
      this.currentForm.control.controls['daco'].setErrors({ 'incorrect': false });
    }
  }
  submitData() {
    this.model.attachments = this.attachmentToUpload;
    this.evalGenerated._competenceDate = this.model._competenceDate;
    this.evalGenerated._insertDate = this.model._insertDate;
    this.evalGenerated.regularizationDate = this.model.regularizationDate;
    this.evalGenerated.attachments = this.model.attachments;
    this.evalGenerated.branch = this.model.branch;
    this.evalGenerated.office = this.model.office;
    this.evalGenerated.creditAmount = this.model.creditAmount;
    this.evalGenerated.debitAmount = this.model.debitAmount;
    this.evalGenerated.reason = this.model.reason;
    this.evalGenerated.description = this.model.description;
    // this.model.description = "RA – Origine " + this.model.description;
    // this.evalGenerated.description = "RA – Generato in automatico da Riclassifica " + this.evalGenerated.description;
    this.model.generated = this.evalGenerated;
    this.evaluationService.reclassificationEvaluation(this.model).subscribe(res => {
      this.submitedData();
      this.submitted = true;
    },
      err => {
        Utils.showServerError(err, this.alertService, this.currentForm);
      });
  }

  submitedData() {
    const confirmData = {
      title: 'Inserimento Rilievo',
      message: 'Si vuole procedere con l\' inserimento di un altro Rilievo?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([PathRegistry.RECLASSIFICATION_DACO]);
      } else {
        this.router.navigateByUrl(PathRegistry.HOME);
      }
    });

  }
}
