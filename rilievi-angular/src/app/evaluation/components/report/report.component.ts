
import { Component } from '@angular/core';
import { Evaluation } from '../../model/evaluation.model';
import { InquiryComponent } from '../inquiry/inquiry.component';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { Parameter } from '../../model/parameter.model';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';
import { DataTablesRequest } from '../../model/datatableRequest.model';
import Utils from 'src/app/shared/utily/utils';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent extends InquiryComponent {


/*  constructor(private route: ActivatedRoute, private router: Router, private modalService: BsModalService, private localeService: BsLocaleService,
    private evaluationService: EvaluationService, private alertService: AlertService, private settingsService: SettingsService, private tilogService: TipologService) {

    super(route, router, modalService, localeService, evaluationService, alertService, settingsService, tilogService);

  }*/

  ngOnInit() {
    super.ngOnInit();
    this.dtOptions.rowCallback = (row: Node, data:  Evaluation, index: number) => {
      let deleteIcon = " <span class='oi oi-trash icon-header' title='Annullato' style='padding-right: 15pt;color: red; " + (data.isDeleted() ? "" : "display:none;") + "' aria-hidden='true'></span>";
      jQuery('td', row)[0].innerHTML = deleteIcon ;

      return row;
    };
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    super.ngOnDestroy();
  }
initDataTable(){
  this.totale = 0;
  let that = this;
  let function1 = this.settingsService.settings.getFunction(FunctionEnum.REPO_EVAL);
  let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
  let exportSize:Parameter= function1.params[ParamEnum.EXPORT_SIZE];
  let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 5;
  let exportSizeValue=exportSize ? parseInt(exportSize.value) : 15;
  console.log(that.totale);
  this.dtOptions = {
    pagingType: 'simple_numbers',
    pageLength: pagingSizeValue,
    serverSide: true,
    processing: false,
    searching: false,
    lengthChange: false,
    stateSave: true,
    ordering: true,
    order: [[1,"desc"]],
    info: true,
    infoCallback: function (settings, start, end, max, total, pre) {
      that.totale = 0;
      that.totale = total;
      if (that.totale > exportSizeValue && that.ExceedBoolean) {//TODO Parametro da passare da DB
        that.enoughEval();//TODO stampa sempre i rilievi regolarizzati all'inizio
        that.ExceedBoolean = false;
      }
      if (that.totale > 1) {
        let buttonExp = '<button id="btnExport" #btnExport type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span> Export Excel</button>';

        return " Totali: " + total + buttonExp;
      } {
        return " Totali: " + total;
      }

    },
    language: {
      info: '',
      infoFiltered: '',
      emptyTable: 'Nessun rilievo',
      paginate: {
        first: 'Inizio',
        last: 'Fine',
        next: 'Successivo',
        previous: 'Precedente'
      }
    },
    rowCallback: (row: Node, data: Evaluation, index: number) => {
      const self = this;
      $(row).css('cursor', 'pointer')
      $('td', row).unbind('click');
      $('td', row).bind('click', () => {
        //self.router.navigateByUrl(PathRegistry.MANAGE_DETAIL.replace(':id',''+data.id) );
        self.emitter.emit(data);
      });

      let deleteIcon = " <span class='oi oi-trash icon-header' title='Annullato' style='padding-right: 15pt;color: red; " + (data.isDeleted() ? "" : "display:none;") + "' aria-hidden='true'></span>";
      jQuery('td', row)[0].innerHTML = deleteIcon ;

      return row;
    },
    drawCallback: (settings: DataTables.SettingsLegacy) => {

      if (this.totale >= 1) {

        jQuery('#btnExport').bind('click', () => {
          that.exportExcel();

        });
      }
    },
    ajax: (dataTablesParameters: any, callback) => {
        if(that.showList==1){

      that.evaluationService.searchEvaluation(this.modelCriteria, DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
        resp => {

          callback({

            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsTotal,
            data: resp.data,
            draw: dataTablesParameters.draw
          });
        }, 
         err =>{
          Utils.showServerError(err, this.alertService, this.currentForm);
      }
      );
    } else if(that.showList == 0) {

      callback({
        recordsTotal: 0,
        recordsFiltered: 0,
        data: [],
        draw: 0
      });
    }
    },
    columns: [
      {
        orderable: false,
        title: '',
        data: 'id',
        createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('text-align', 'left');
          }
      },
    {
      title: 'Nr.Rilievo',
      data: 'id',
    },
    {
      title: 'Nr.Generato',
      data: 'idChild',
      width: '10%',
    },
    {
      title: 'Data Inserimento',
      data: 'insertDate',
      render: function (data, type, row, meta) {
        return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
      }
    }, {
      title: 'Data Competenza',
      data: 'competenceDate',
      render: function (data, type, row, meta) {
        return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
      }
    },
    {
      title: 'Causale',
      data: 'reason',
      render:function (data, type, row, meta){
        if (data == null){
          return '';
      }else{
        let html = '';             
        html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
        return html;

      
      }
      }
   },
   {
    title: 'Filiale',
    data: 'branch',
    render:function (data, type, row, meta){
      if (data == null){
        return '';
    }else{
      let html = '';             
      html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
      return html;

    
    }
    }
 },
 {
  title: 'Frazionario Up',
  data: 'office',
  render:function (data, type, row, meta){
   if (data == null){
     return '';
 }else{
   let html = '';             
   html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.fractionalDescription  + "\">"+data.fractional+"</label>";
   return html;

 
 }
 }
},
    {
      title: 'Credito',
      data: 'creditAmount',
      render: function ( data, type, row, meta ) {
                 let creditAmount = row.creditAmount;              
                  return  (new CurrencyPipe('it-IT')).transform(creditAmount, 'EUR', true);
              },
      createdCell: function (td, cellData, rowData, row, col) {
                   $(td).css('text-align', 'right');             
                 }
   },
   {
     title: 'Debito',
     data: 'debitAmount',
     render: function ( data, type, row, meta ) {
                let debitAmount = row.debitAmount;              
                 return  (new CurrencyPipe('it-IT')).transform(debitAmount, 'EUR', true);
             },
     createdCell: function (td, cellData, rowData, row, col) {
                  $(td).css('text-align', 'right');             
                }
  },
  {
    width:'100px',
     title: 'Voce Daco',
     data: 'daco',
     render:function (data, type, row, meta){
      if (data == null){
        return '';
    }else{
      let html = '';             
      html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
      return html;

    
    }
    }
  },
  {
    
    title: ' Data Contabilizzazione',
    data: 'accountDate',
    width: '10%',
    render: function (data, type, row, meta) {
      if (data == null) {
        return '';
      } else {
        return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
      }

    }
  },
  {
      title: ' Data Regolarizzazione',
      data: 'regularizationDate',
      width: '10%',
      render: function (data, type, row, meta) {
        if (data == null) {
          return '';
        } else {
          return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
        }

      }
    },
    {
      title: 'Servizio',
      data: 'service',
      render:function (data, type, row, meta){
        if (data == null){
          return '';
      }else{
        let html = '';             
        html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
        return html;

      
      }
      }
   },
   {
    title: 'Connotazione a perdita',
    data: 'status',
    render:function (data, type, row, meta){
      if (data == null){
        return '';
    }else{
      let html = '';             
      html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
      return html;

    
    }
    }
 },
   
    ]
  };
//  this.dtOptions.scrollCollapse=true;
  this.dtOptions.scrollX=true;
  this.dtOptions.ajax;
}

exportExcel(){
  super.exportExcel();
}

}

