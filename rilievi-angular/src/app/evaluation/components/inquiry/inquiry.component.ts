import { CurrencyPipe, DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { itLocale } from 'ngx-bootstrap/locale';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { SettingsService } from 'src/app/core/services/settings.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { AlertService } from 'src/app/shared/services/alert.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import Utils from 'src/app/shared/utily/utils';
import { Branch } from '../../model/branch.model';
import { Daco } from '../../model/daco.model';
import { Evaluation } from '../../model/evaluation.model';
import { Reason } from '../../model/reason.model';
import { SearchCriteria } from '../../model/searchCritera.model';
import { Status } from '../../model/status.model';
import { EvaluationService } from '../../services/evaluation.service';
import { DataTablesRequest } from '../../model/datatableRequest.model';
import { Parameter } from '../../model/parameter.model';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { Profile } from 'src/app/shared/models/profile.model';



@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss']
})
export class InquiryComponent implements OnInit, OnDestroy, AfterViewInit {

  @Output('detail')
  emitter: EventEmitter<Evaluation> = new EventEmitter<Evaluation>();
  now: Date;
  model = new Evaluation();
  modelCriteria = new SearchCriteria();
  profileModel = new Profile();
  dacoList: Array<Daco>;
  reasonList: Array<Reason>;
  branchOptions: Array<Branch>;
  settings: EvaluationSettings;
  showResults: boolean;
  currentForm: NgForm;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  statusList: Array<Status>;
  totale: number;
  ExceedBoolean = true;
  messageToShow: string;
  state: any;
  showList: number;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm2: NgForm;

  constructor(protected route: ActivatedRoute, protected router: Router, protected modalService: BsModalService, protected localeService: BsLocaleService,
    protected evaluationService: EvaluationService, protected alertService: AlertService, protected settingsService: SettingsService, protected tilogService: TipologService) {

    //setting the it locale for datepicker component
    itLocale.invalidDate = '';
    defineLocale('it', itLocale);
    this.localeService.use('it');
    //leggo le impostazioni
    this.settings = this.settingsService.settings;
  }

  ngOnInit() {
    this.showList = 0;
    this.modelCriteria.type = 'REGULARIZED';
    this.tilogService.getStatus().subscribe(res => {
      this.statusList = res;
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });

    this.evaluationService.getDacoItem().subscribe(res => {
      this.dacoList = res;
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });

    this.tilogService.getReasonItem().subscribe(res => {
      this.reasonList = res;
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });

    this.tilogService.getBranches().subscribe(res => {
      this.branchOptions = res;
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });

    this.initDataTable();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(clearState: boolean): void {
    this.totale = 0;
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      this.totale = 0;
      dtInstance.destroy();
      if (clearState) {
        dtInstance.state.clear();
      }

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  insertDateFromUpperBound(): Date {
    let limit = new Date();
    let ub = new Date();
    if (this.modelCriteria._insertDateFrom <= ub) {
      limit.setDate(ub.getDate());
    }
    return limit;
  }

  insertDateToLowerBound(): Date {
    // let limit = new Date();
    if (this.modelCriteria._insertDateFrom != null) {
      return this.modelCriteria._insertDateFrom;
    } else {
      return null;
    }
    // return limit;
  }

  maxInsertDateToUpperBound(): Date {
    let limit = new Date();
    limit.setDate(limit.getDate());
    return limit
  }
  competenceDateFromUpperBound(): Date {
    let limit = new Date();
    let ub = new Date();
    if (this.modelCriteria._competenceDateFrom <= ub) {
      limit.setDate(ub.getDate());
    }
    return limit;

  }

  competenceDateToUpperBound(): Date {

    if (this.modelCriteria._competenceDateFrom != null) {
      return this.modelCriteria._competenceDateFrom;
    } else {
      return null;
    }

  }

  maxCompetenceDateToUpperBound(): Date {
    let limit = new Date();
    limit.setDate(limit.getDate());
    return limit

  }

  fractionalChanged(event) {
    this.model.office = undefined;
    this.model.branch = undefined;
    let model = this.modelCriteria;
    let founded = false;
    this.branchOptions.forEach(function (branch, index) {
      if (founded) {
        return;
      }
      if (branch.office != null) {
        branch.office.forEach(function (office, index) {
          if (office.fractional == model.fractional) {
            model.office = office;
            model.branch = branch;
            founded = true;
            return;
          }
        });
      }
    });
    if (!founded) {
      this.modelCriteria.office = undefined;
      this.modelCriteria.branch = undefined;
      if (this.modelCriteria.fractional != null && this.modelCriteria.fractional != '') {
        this.currentForm.control.controls['Frazionario'].setErrors({ 'incorrect': false });
      }
    }
  }
  selectionBranchChanged(event) {
    this.modelCriteria.office = undefined;
    this.modelCriteria.fractional = undefined;
  }

  selectionOfficeChanged(event) {
    this.modelCriteria.fractional = undefined;
    if (this.modelCriteria.office != null) {
      this.modelCriteria.fractional = this.modelCriteria.office.fractional;
    }
  }

  selectionDacoChanged(event) {
    if (this.modelCriteria.daco != null) {
      return
    }
  }



  initDataTable(): void {
    this.totale = 0;
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.INQUIRY_EVAL);
    let pagingSize: Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let exportSize: Parameter = function1.params[ParamEnum.EXPORT_SIZE];
    let pagingSizeValue = pagingSize ? parseInt(pagingSize.value) : 5;
    let exportSizeValue = exportSize ? parseInt(exportSize.value) : 15;
    console.log(that.totale);
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      stateSave: true,
      ordering: true,
      order: [[1, "desc"]],
      info: true,
      infoCallback: function (settings, start, end, max, total, pre) {
        that.totale = 0;
        that.totale = total;
        if (that.totale > exportSizeValue && that.ExceedBoolean) {//TODO Parametro da passare da DB
          that.enoughEval();//TODO stampa sempre i rilievi regolarizzati all'inizio
          that.ExceedBoolean = false;
        }
        if (that.totale >= 1) {
          let buttonExp = '<button id="btnExport" #btnExport type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span> Export Excel</button>';
          return " Totali: " + total + buttonExp;
        } {
          return " Totali: " + total;
        }

      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun rilievo',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      rowCallback: (row: Node, data: Evaluation, index: number) => {
        const self = this;
        $(row).css('cursor', 'pointer')
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          //self.router.navigateByUrl(PathRegistry.MANAGE_DETAIL.replace(':id',''+data.id) );
          self.emitter.emit(data);
        });


        let deleteIcon = " <span class='oi oi-trash icon-header' title='Annullato' style='padding-right: 15pt;color: red; " + (data.isDeleted() ? "" : "display:none;") + "' aria-hidden='true'></span>";
        jQuery('td', row)[0].innerHTML = deleteIcon;

        return row;
      },
      drawCallback: (settings: DataTables.SettingsLegacy) => {

        if (this.totale > 1) {

          jQuery('#btnExport').bind('click', () => {
            that.exportExcel();

          });
        }
      },
      ajax: (dataTablesParameters: any, callback) => {
        if (that.showList == 1) {

          that.evaluationService.searchEvaluation(this.modelCriteria, DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
            resp => {

              callback({

                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            },
            err => {
              Utils.showServerError(err, this.alertService, this.currentForm);
            }
          );
        } else if (that.showList == 0) {

          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: [],
            draw: 0
          });
        }



      },

      columns: [
        {
          orderable: false,
          title: '',
          data: 'id',
          createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('text-align', 'left');
          }
        },
        {
          title: 'Nr.Rilievo',
          data: 'id',
        },
        {
          title: 'Nr.Generato',
          data: 'idChild',
          width: '10%',
        },
        {
          title: 'Data Inserimento',
          data: 'insertDate',
          render: function (data, type, row, meta) {
            return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
          }
        }, {
          title: 'Data Competenza',
          data: 'competenceDate',
          render: function (data, type, row, meta) {
            return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
          }
        },
        {
          title: 'Causale',
          data: 'reason',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.description + "\">" + data.code + "</label>";
              return html;


            }
          }
        },
        {
          title: 'Filiale',
          data: 'branch',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.description + "\">" + data.code + "</label>";
              return html;


            }
          }
        },
        {
          title: 'Frazionario Up',
          data: 'office',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.fractionalDescription + "\">" + data.fractional + "</label>";
              return html;


            }
          }
        },
        {
          title: 'Credito',
          data: 'creditAmount',
          render: function (data, type, row, meta) {
            let creditAmount = row.creditAmount;
            return (new CurrencyPipe('it-IT')).transform(creditAmount, 'EUR', true);
          },
          createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('text-align', 'right');
          }
        },
        {
          title: 'Debito',
          data: 'debitAmount',
          render: function (data, type, row, meta) {
            let debitAmount = row.debitAmount;
            return (new CurrencyPipe('it-IT')).transform(debitAmount, 'EUR', true);
          },
          createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('text-align', 'right');
          }
        },
        {
          width: '100px',
          title: 'Voce Daco',
          data: 'daco',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.description + "\">" + data.code + "</label>";
              return html;


            }
          }
        },
        {

          title: ' Data Contabilizzazione',
          data: 'accountDate',
          width: '10%',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
            }

          }
        },
        {
          title: ' Data Regolarizzazione',
          data: 'regularizationDate',
          width: '10%',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
            }

          }
        },
        {
          title: 'Servizio',
          data: 'service',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.description + "\">" + data.code + "</label>";
              return html;


            }
          }
        },
        {
          title: 'Connotazione a perdita',
          data: 'status',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              let html = '';
              html += "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + data.description + "\">" + data.code + "</label>";
              return html;


            }
          }
        },
        {
          title: 'Data Cancellazione',
          data: 'deleteDate',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
            }
          }
        },
        {
          title: 'Utente',
          data: 'insertUser.code',
          render: function (data, type, row, meta) {
            if (data == null) {
              return '';
            } else {
              return data;
            }
          }
        },
      ]
    };
    //this.dtOptions.scrollCollapse=true;
    this.dtOptions.scrollX = true;
    this.dtOptions.ajax;
  }


  exportExcel(): void {
    this.evaluationService.exportAllEvaluations(this.modelCriteria).subscribe(data => {
      Utils.download(data);
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });
  }


  checkAmountDigit(c) {
    if (!((c.keyCode > 95 && c.keyCode < 106)
      || (c.keyCode > 47 && c.keyCode < 58)
      || c.keyCode == 8 || c.keyCode == 190 || c.keyCode == 9 || c.keyCode == 110 || c.keyCode == 37 || c.keyCode == 38 || c.keyCode == 39 || c.keyCode == 40
    )) {
      return false;
    }
  }
  goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }


  enoughEval() {
    const enoughEvaluation = {
      title: 'Numero Eccessivo Rilievi',
      message: 'Sono stati generati ' + '' + this.totale + ' ' + 'Rilievi, si proceda con la stampa del risultato in excel.',
      buttons: [{ label: "Esporta", retValue: "true", colorClass: "success" }, { label: "Annulla", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: enoughEvaluation, backdrop: "static" });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.exportExcel();
        this.showResults = !this.showResults;
      } else {
        return;
      }
    });

  }
  toggleResults() {
    this.showList = 1;
    this.showResults = !this.showResults;
    this.rerender(true);
  }

  toggleSearch() {
    this.totale = 0;
    this.ExceedBoolean = true;
    this.showResults = !this.showResults;

  }
}
