import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Status } from '../../model/status.model';
import { EvaluationService } from '../../services/evaluation.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertScope } from 'src/app/shared/alertscope.enum';

@Component({
  selector: 'app-modal-select-status',
  templateUrl: './modal-select-status.component.html',
  styleUrls: ['./modal-select-status.component.scss']
})
export class ModalSelectStatusComponent implements OnInit {

  @Input()
  statusList: Array<Status> = [];

  @Input('evaluation-id')
  idEvaluation: number;

  @Input()
  selectedStatus: Status;
  AlertScope: any;
  updated: Boolean= false;

  @Input()
  modalRef: BsModalRef;
  @Output('statusChanged')
  emitter: EventEmitter<Status> = new EventEmitter<Status>();

  constructor(private evalutService: EvaluationService,  public alertService: AlertService) { 

    this.AlertScope = AlertScope;
  }

  ngOnInit() {
  }

  updateStatus():void {

    this.evalutService.setLossState(this.idEvaluation, this.selectedStatus).subscribe(res =>{       
        this.updated = true;
        this.emitter.emit(this.selectedStatus);
        this.close();
    },err => {
        this.alertService.addErrorMessage("Sistema momentaneamente non disponibile!",null, AlertScope.MODAL);
    });
  }

  close() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
