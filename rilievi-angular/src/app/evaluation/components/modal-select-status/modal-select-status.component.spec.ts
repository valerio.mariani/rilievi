import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSelectStatusComponent } from './modal-select-status.component';

describe('ModalSelectStatusComponent', () => {
  let component: ModalSelectStatusComponent;
  let fixture: ComponentFixture<ModalSelectStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSelectStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSelectStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
