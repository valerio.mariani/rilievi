import { CurrencyPipe, DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { itLocale } from 'ngx-bootstrap/locale';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { Function } from 'src/app/core/model/function.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { AlertService } from 'src/app/shared/services/alert.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import Utils from 'src/app/shared/utily/utils';
import { Attachment } from '../../model/attachment.model';
import { Branch } from '../../model/branch.model';
import { Daco } from '../../model/daco.model';
import { Evaluation } from '../../model/evaluation.model';
import { EvaluationService } from '../../services/evaluation.service';
import { DataTablesRequest } from '../../model/datatableRequest.model';
import { Parameter } from '../../model/parameter.model';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.scss']
})
export class InsertComponent implements OnInit, OnDestroy, AfterViewInit {
  minParam:number;
  settings: EvaluationSettings;
  model = new Evaluation();
  submitted = false;
  modalRef: BsModalRef;
  dacoList: Array<Daco>;
  branchOptions: Array<Branch>;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
 
  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;
  attachmentToUpload: Array<Attachment> = new Array<Attachment>();
  totalAttachments: number;
  
  function1:Function;
  minAmountThreshold:Parameter 
  

  
  constructor(  public router: Router, public modalService: BsModalService, private localeService: BsLocaleService, 
                public evaluationService: EvaluationService, public alertService: AlertService, private settingsService: SettingsService, 
                private tipologService: TipologService,public route: ActivatedRoute) {

    //setting the it locale for datepicker component
    itLocale.invalidDate = '';
    defineLocale('it', itLocale);
    this.localeService.use('it');
     //leggo le impostazioni
     this.settings = this.settingsService.settings;
  }


  ngOnInit() {

    this.function1 = this.settingsService.settings.getFunction(FunctionEnum.INS_EVAL);
    let minAmountThreshold:Parameter = this.function1.params[ParamEnum.MIN_AMOUNT_THRESHOLD];
    this.minParam = parseInt(minAmountThreshold.value);
    this.initDataTable(); 
    //valore reason di default
    this.model.reason = this.settings.rcReason;
    this.model._insertDate = this.settings.sysDate;
   
    
    
    this.evaluationService.getDacoItem().subscribe(res => {
      this.dacoList = res;
      this.afterDacoListLoaded();
    }, err =>{
      Utils.showServerError(err, this.alertService, this.currentForm);
  });

    this.tipologService.getBranches().subscribe(res => {
      this.branchOptions = res;
    }, err =>{
      Utils.showServerError(err, this.alertService, this.currentForm);
  });
  }

  afterDacoListLoaded():void{

  }

  

  ngAfterViewInit(): void {
    this.dtTrigger.next();
    
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  initDataTable(): void {
    let that = this;    
    let pagingSize:Parameter = this.function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 5;
    this.dtOptions = {
        pagingType: 'simple_numbers',
        pageLength: pagingSizeValue,
        scrollCollapse:true,
        scrollX:true,
        autoWidth:true,
        serverSide: true,
        processing: false,
        searching: false,
        lengthChange: false,
        stateSave: true,
        ordering: true,
        order: [[1,"desc"]],
        info: true,
        infoCallback: function( settings, start, end, max, total, pre ) {         
          let buttonExp = total == 0 ? '' :  '<button id="btnExport" type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span> Export Excel</button>';
          return  " Totali: " + total + buttonExp;
        },
        language: {
          info: '',
          infoFiltered: '',
          emptyTable: 'Nessun rilievo',
          paginate: {
            first: 'Inizio',
            last: 'Fine',
            next: 'Successivo',
            previous: 'Precedente'
          }
        },
        rowCallback: (row: Node, data:  Evaluation, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          jQuery(row).css('cursor','pointer')
          jQuery('td', row).unbind('click');
          jQuery('td', row).bind('click', () => {
            self.router.navigateByUrl(PathRegistry.DETAIL.replace(':id',''+data.number) );
          });
          let deleteIcon =" <span class='oi oi-trash icon-header' title='Annullato' style='padding-right: 15pt;color: red; " +(data.isDeleted()?"":"display:none;") + "' aria-hidden='true'></span>";
          jQuery('td', row)[0].innerHTML= deleteIcon   ;         
          return row;
        },
        drawCallback :( settings: DataTables.SettingsLegacy ) =>{  
          jQuery('#btnExport').bind('click', () => {
            that.exportExcel();
          });
        },
        ajax: (dataTablesParameters: any, callback) => {
        
          that.evaluationService.getDailyEvaluations(DataTablesRequest.fromDataTablesParameters(dataTablesParameters)).subscribe(
            resp =>{
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            },
            err => {
              callback({
                recordsTotal: 0,
                recordsFiltered: 0,
                data:[],
                draw: 0,
                error: 'Sistema momentaneamente non disponibile!'
              });
            }
          );

        },
        columns: [
          {
            title: '',
            data: 'id',
            createdCell: function (td, cellData, rowData, row, col) {
              $(td).css('text-align', 'center');             
            },
            orderable: false
          },
          {
            title: 'Nr. Rilievo',
            data: 'number',
            
          }, {
             title: 'Nr. Generato',
             data: 'idChild'
          }, {
             title: 'Data Inserimento',
             data: 'insertDate',
             render: function ( data, type, row, meta ) {
                     return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
                    }
          }, {
             title: 'Data Competenza',
             data: 'competenceDate',
             render: function ( data, type, row, meta ) {
                      return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
                    }
          },
          {
            title: 'Causale',
            data: 'reason',
            render:function (data, type, row, meta){
              if (data == null){
                return '';
            }else{
              let html = '';             
              html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
              return html;
    
            
            }
            }
         },
         {
          title: 'Filiale',
          data: 'branch',
          render:function (data, type, row, meta){
            if (data == null){
              return '';
          }else{
            let html = '';             
            html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
            return html;
  
          
          }
          }
       }, {
             title: 'Frazionario Up',
             data: 'office',
             render:function (data, type, row, meta){
              if (data == null){
                return '';
            }else{
              let html = '';             
              html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.fractionalDescription  + "\">"+data.fractional+"</label>";
              return html;
    
            
            }
            }
          }, {
            width:'100px',
             title: 'Voce Daco',
             data: 'daco',
             render:function (data, type, row, meta){
              if (data == null){
                return '';
            }else{
              let html = '';             
              html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
              return html;
    
            
            }
            }
          },
          {
            title: 'Servizio',
            data: 'service',
            render:function (data, type, row, meta){
              if (data == null){
                return '';
            }else{
              let html = '';             
              html +=  "<label data-toggle=\"tooltip\" data-placement=\"bottom\" title=\""  +  data.description  + "\">"+data.code+"</label>";
              return html;
    
            
            }
            }
         },
         
         {
             title: 'Credito',
             data: 'creditAmount',
             render: function ( data, type, row, meta ) {
                        let creditAmount = row.creditAmount;              
                         return  (new CurrencyPipe('it-IT')).transform(creditAmount, 'EUR', true);
                     },
             createdCell: function (td, cellData, rowData, row, col) {
                          $(td).css('text-align', 'right');             
                        }
          },
          {
            title: 'Debito',
            data: 'debitAmount',
            render: function ( data, type, row, meta ) {
                       let debitAmount = row.debitAmount;              
                        return  (new CurrencyPipe('it-IT')).transform(debitAmount, 'EUR', true);
                    },
            createdCell: function (td, cellData, rowData, row, col) {
                         $(td).css('text-align', 'right');             
                       }
         },
         {
          title: 'Data Regolarizzazione',
          data: 'regularizationDate',
          render: function ( data, type, row, meta ) {
                   return new DatePipe('it-IT').transform(data,'dd/MM/yyyy');
                 }
       },
       {
        title: 'Descrizione',
        data: 'description'
     }
      ]
    };
  }

  updateTotalAttachments(totalAttachments: number): void{
    this.totalAttachments = totalAttachments;
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  exportExcel(): void {    
   this.evaluationService.exportDailyEvaluations().subscribe(data => {
       Utils.download(data);
   }, err =>{
    Utils.showServerError(err, this.alertService, this.currentForm);
   });
  }

  selectionBranchChanged(event) {
    this.model.office = undefined;
    this.model.fractional = undefined;
  }

  selectionOfficeChanged(event) {
    this.model.fractional = undefined;
    if (this.model.office != null) {
      this.model.fractional = this.model.office.fractional;
    }
  }
  
  fractionalChanged(event) {
    this.model.office = undefined;
    this.model.branch = undefined;
    let model = this.model;
    let founded = false;
    this.branchOptions.forEach(function (branch, index) {
      if (founded) {
        return;
      }
      if (branch.office != null) {
        branch.office.forEach(function (office, index) {
          if (office.fractional == model.fractional) {
            model.office = office;
            model.branch = branch;
            founded = true;
            return;
          }
        });
      }
    });
    if (!founded) {
      this.model.office = undefined;
      this.model.branch = undefined;
      if(this.model.fractional!= null && this.model.fractional !=''){
        this.currentForm.controls['fractional'].setErrors({ 'error': 'Codice Frazionario non valido' });
      }
    } 
  }

  selectionDacoChanged(event, evalut:Evaluation) {
    evalut.accountNumber = undefined;
    evalut.service = undefined;
    if (evalut.daco != null) {
      evalut.accountNumber = evalut.daco.accountNumberFull;
    }
  }

  accountNumberChanged(dacoList:Array<Daco>, evalut:Evaluation, accountNumberFieldname: string) {
    evalut.daco = undefined;
    evalut.service = undefined;
    let founded = false;
    dacoList.forEach(function (dacoItem, index) {
      if (founded) {
        return;
      }
      if (dacoItem.accountNumber == evalut.accountNumber) {
        evalut.daco = dacoItem;
        founded = true;
        return;
      }
    });
    if (!founded) {
      evalut.daco = undefined;
      if(evalut.accountNumber != null && evalut.accountNumber != '' ){
        this.currentForm.control.controls[accountNumberFieldname].setErrors({ 'error': 'Codice contabile non valido' });//{'incorrect':false}
      }
    }
  }


  checkAmountDigit(c) {
    if (!((c.keyCode > 95 && c.keyCode < 106)
      || (c.keyCode > 47 && c.keyCode < 58)
      || c.keyCode == 8 || c.keyCode == 190 || c.keyCode == 9 || c.keyCode == 110 || c.keyCode == 37 || c.keyCode == 38 || c.keyCode == 39 || c.keyCode == 40
    )) {
      return false;
    }
  }

  attachmentsToUpload():Array<Attachment>{
    const attList = new Array<Attachment>();
    this.model.attachments.forEach(att => {
      if(att.id == null){
        attList.push(Attachment.fromJSON(JSON.stringify(att)));
      }
    });
    return attList;
  }

  openAttachmentsModal(template: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(template,{class: 'modal-lg',backdrop: "static"});
  }

  getFiles(files: Array<Attachment>) {
    this.attachmentToUpload = files;
  }

  onSubmit() {
    this.confirmEvaluationData();
  }

  competenceDateUpperBound(): Date {
    let ub = new Date();
    if (this.model._insertDate != null) {
      ub.setDate(this.model._insertDate.getDate() - 1);
    }
    return ub;
  }

  cleanAmount(debit?: boolean) {
    if (debit && this.model.debitAmount) {
      this.model.debitAmount = null;
    } else if (!debit && this.model.creditAmount) {
      this.model.creditAmount = null;
    }
  }

  checkValidAmount(debit: boolean): void {
    if( Utils.isEmpty(this.model.creditAmount)  && Utils.isEmpty(this.model.debitAmount) ){
       this.currentForm.control.controls[debit?'debitAmount':'creditAmount'].setErrors({'error': 'Valorizzare un importo'});
     }
  }

  confirmEvaluationData() {
    const confirmData = {
      title: 'Inserimento Rilievo',
      message: 'Si vuole procedere con l\'inserimento del rilievo?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false",  colorClass: "danger" } ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {        
        this.checkEvalutionDuplicate();
      } else {
        return;
      }
    });
  }

 checkEvalutionDuplicate(){
  this.evaluationService.checkDuplicate(this.model).subscribe(res =>{
    if(res){
      this.confirmInsertDuplicate();
    }else{
      this.submitData();
    }
  }, err =>{
        Utils.showServerError(err, this.alertService, this.currentForm);
    });
 }


  confirmInsertDuplicate() {
    const confirmData = {
      title: 'Inserimento Rilievo',
      message: 'Esiste un rilievo in archivio con stessa voce daco, data competenza e importo.Si vuole procedere con l\'inserimento?' ,
      buttons: [{label: "SI", retValue: "true", colorClass: "primary"}, {label: "NO", retValue: "false", colorClass: "danger"  }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
        this.submitData();
      } else {
        return;
      }
    });
  }

  submitData(){
    this.model.attachments = this.attachmentToUpload;
    this.evaluationService.insertEvaluation(this.model).subscribe(res =>{
      this.submitedData();
      this.submitted = true;
    },
    err =>{  
      Utils.showServerError(err, this.alertService, this.currentForm);
    } );
  }


  submitedData(){
    const confirmData = {
      title: 'Inserimento Rilievo',
      message: 'Si vuole procedere con l\'inserimento di un altro rilievo?' ,
      buttons: [{  label: "SI",  retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" } ]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if(result == "true"){
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([PathRegistry.DO_EVAL]);
      } else {
        this.router.navigateByUrl(PathRegistry.HOME);
      }
    });

  }

  creditRChange() {
    const confirmCreditChange = {
      title: 'Conferma Cambio Causale',
      message: 'Si sta modificando il tipo di Causale. \n Mantenere la causale di tipo RS o modificare in tipo RC?',
      buttons: [{ label: "RC", retValue: "1", colorClass: "success" }, { label: "RS", retValue: "0", colorClass: "danger" } ]
    };
    if (this.model.creditAmount <= this.minParam && this.model.reason.id != this.settings.rsReason.id) {
      this.modalRef = this.modalService.show(GenericModalComponent, { initialState: confirmCreditChange });
      this.modalRef.content.onClose.subscribe(result => {
        if (result == "1") {
          this.model.reason = this.settings.rcReason;
        } else if (result == "0") {
          this.model.reason = this.settings.rsReason;
        }
      });
      console.log("test");
    } else if (this.model.creditAmount > this.minParam) {
      this.model.reason = this.settings.rcReason;
    }

  }

  debitRChange() {
    const confirmDebitChange = {
      title: 'Conferma Cambio Causale',
      message: 'Si sta modificando il tipo di Causale. \n Mantenere la causale di tipo RD o modificare in tipo RC?',
      buttons: [{label: "RC", retValue: "1", colorClass: "success" }, { label: "RD", retValue: "0", colorClass: "danger" }]
    };
    if (this.model.debitAmount > 0 && this.model.reason.id != this.settings.rdReason.id) {
      this.modalRef = this.modalService.show(GenericModalComponent, { initialState: confirmDebitChange });
      this.modalRef.content.onClose.subscribe(result => {
        if (result == "1") {
          this.model.reason = this.settings.rcReason;
        } else if (result == "0") {
          this.model.reason = this.settings.rdReason;
        }
      });
    }
  }
  public goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }
}


