import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { AuthorizationService } from 'src/app/core/services/authorization.service';
import { UserService } from 'src/app/core/services/user.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { Attachment } from '../../model/attachment.model';
import { DataTablesRequest } from '../../model/datatableRequest.model';
import { AttachmentService } from '../../services/attachment.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Parameter } from '../../model/parameter.model';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {

  @Input()
  modalRef: BsModalRef;
  @Output('update')
  emitter: EventEmitter<Array<Attachment>> = new EventEmitter<Array<Attachment>>();
  @Input('evaluation-id')
  evaluationId: number;
  @Input('attachments')
  attachments: Array<Attachment> = [];

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  //Numero di allegati totali
  totalAttachments: number;
  @Output('numberAttachmentsChanged')
  emitterTotal: EventEmitter<number> = new EventEmitter<number>();
  userProfile: string;
  profile: any;

  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;
  maxSizeAttachment: number = 65535;
  maxSizeError:boolean=false;

  constructor(private attachmentService: AttachmentService, private userService: UserService, protected settingsService: SettingsService, protected authService: AuthorizationService, public alertService: AlertService) {
    this.userProfile = userService.user.profile;
    this.profile = ProfileEnum;
  }

  ngOnInit() {
    this.initDataTable();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.READ_ATTACHMENT);
    let pagingSize: Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue = pagingSize ? parseInt(pagingSize.value) : 2;
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: pagingSizeValue,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      ordering: true,
      stateSave: true,
      info: true,
      infoCallback: function (settings, start, end, max, total, pre) {
        return " Totali: " + total;
      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun Allegato.',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      ajax: (dataTablesParameters: any, callback) => {


        that.attachmentService.getEvaluationAttachments(DataTablesRequest.fromDataTablesParameters(dataTablesParameters), this.evaluationId).subscribe(
          resp => {
            that.totalAttachments = resp.recordsTotal;
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsTotal,
              data: resp.data,
              draw: dataTablesParameters.draw
            });
            that.emitterTotal.emit(that.totalAttachments);
          }, err => {
            Utils.showServerError(err, this.alertService, this.currentForm);
          }
        );
      },
      columns: [
        /* {
         title: 'Nr. Allegato',
         data: 'id'
         }, */
        {
          title: 'Allegato',
          data: 'name'
        },
        {
          title: 'Data Inserimento',
          data: 'insertDate',
          render: function (data, type, row, meta) {
            return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
          }

        },
        {
          title: 'Utente',
          data: 'insertUser'
        },
        {
          title: '',
          orderable: false,
          data: 'id',
          render: function (data, type, row, meta) {
            let html = '';
            if (that.isManageAttachmentsEnabled()) {
              html = "<a id=\"download" + data + "\" ><span class=\"oi oi-data-transfer-download icon-header\" style=\"cursor: pointer; padding-right: 15pt\" title=\"Download\" aria-hidden=\"true\" > </span></a>";
              html += "<a  id=\"delete" + data + "\" ><span class=\"oi oi-trash icon-header\"  title=\"Cancella\"  style=\"cursor: pointer; padding-right: 15pt\" aria-hidden=\"true\" ></span></a>";
              return html;
            } else if (that.isReadAttachmentsEnabled()) {
              html += "<a id=\"download" + data + "\" ><span class=\"oi oi-data-transfer-download icon-header\" style=\"cursor: pointer; padding-right: 15pt\" title=\"Download\" aria-hidden=\"true\" ></span></a>";
              return html;
            }

          },
          createdCell: function (td, cellData, rowData, row, col) {
            $(td).css('width', '80px');
          }
        }

      ],
      rowCallback: (row: Node, data: Attachment, index: number) => {
        const self = this;
        $('#delete' + data.id, row).bind('click', () => {
          self.delete(data.id);
          this.maxSizeError=false;
        });
        $('#download' + data.id, row).bind('click', () => {
          self.download(data.id);
          this.maxSizeError=false;
        });
        return row;
      }
    };
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }



  clear(file: Attachment) {
    this.maxSizeError=false;
    const index: number = this.attachments.indexOf(file);
    if (index !== -1) {
      this.attachments.splice(index, 1);
    }
  }

  uploadFiles() {
    if (this.attachments && this.attachments.length > 0) {
      if (!this.evaluationId) {
        if (this.modalRef) {
          this.modalRef.hide();
        }
        this.emitter.emit(this.attachments);
      } else {
        this.attachmentService.uploadAttachments(this.evaluationId, this.attachments).subscribe(res => {
          this.emitter.emit(this.attachments);
          this.attachments.splice(0, this.attachments.length);
          this.rerender();
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      }

    }
  }

  b64toBlob(b64Data, contentType='',sliceSize=512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
  
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
  
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  download(attachmentId: number) {
    this.attachmentService.getAttachment(this.evaluationId, attachmentId).subscribe(res => {
      console.log('start download:', res);
      let blob: Blob = this.b64toBlob(res.content, 'application/octet-stream');
      const url = window.URL.createObjectURL(blob);
      let fileName=res.name;
      if(window.navigator.msSaveBlob){
        window.navigator.msSaveBlob(blob, fileName);  
    }else{
      var anchor = document.createElement("a");
      anchor.download = res.name;
      anchor.href = url;
      anchor.click();
    }
      err => {
      }
    });
  }

  delete(attachmentId: number) {
    this.attachmentService.deleteAttachment(this.evaluationId, attachmentId).subscribe(res => {
      this.rerender();
    },
      err => {
      });
  }

  close() {
   /* if (this.attachments) {
      this.attachments.splice(0, this.attachments.length);
    }*/
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
  addFile(files: FileList) {
    const toBase64 = file => new Promise<any>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
    if (!this.attachments) {
      this.attachments = new Array<Attachment>();
    }
    for (let i = 0; i < files.length; i++) {
      let file = files.item(i);
        toBase64(file).then(f => {
          let attach = new Attachment();
          attach.mimeType = file.type;
          attach.name = file.name;
          attach.size = file.size;
          let content = f;
          attach.content =  content.substring(content.indexOf("base64,") + 7);
          this.attachments.push(attach);
        }
        );
    }

  }

  isManageAttachmentsEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.MANAG_ATTACHMENT);
  }
  isReadAttachmentsEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.READ_ATTACHMENT);
  }
}
