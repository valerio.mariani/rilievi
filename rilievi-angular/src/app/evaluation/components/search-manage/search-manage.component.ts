import { Component, OnInit, ViewChild } from '@angular/core';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import { Evaluation } from '../../model/evaluation.model';
import { Status } from '../../model/status.model';
import { InquiryComponent } from '../inquiry/inquiry.component';

@Component({
  selector: 'app-search-manage',
  templateUrl: './search-manage.component.html',
  styleUrls: ['./search-manage.component.scss']
})
export class SearchManageComponent implements OnInit {

  toggleDetail: boolean = false;
  evaluation: Evaluation;
  statusList: Array<Status> = [];
  @ViewChild(InquiryComponent, {static:false})
  inquiryComponent;

  constructor(protected tipologService: TipologService) { }

  ngOnInit() {
    this.tipologService.getStatus()
      .subscribe(res=> this.statusList = res);
  }

  showDetail($event: Evaluation) {
    this.evaluation=$event;
    this.toggleDetail=true;
  }

  showInquiry ($event) {
    this.toggleDetail = false;
    this.inquiryComponent.rerender();
  }

}
