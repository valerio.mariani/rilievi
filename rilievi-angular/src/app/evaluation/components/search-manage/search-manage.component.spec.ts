import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchManageComponent } from './search-manage.component';

describe('SearchManageComponent', () => {
  let component: SearchManageComponent;
  let fixture: ComponentFixture<SearchManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
