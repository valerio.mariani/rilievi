import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaxiEvaluationComponent } from './maxi-evaluation.component';

describe('MaxiEvaluationComponent', () => {
  let component: MaxiEvaluationComponent;
  let fixture: ComponentFixture<MaxiEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaxiEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaxiEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
