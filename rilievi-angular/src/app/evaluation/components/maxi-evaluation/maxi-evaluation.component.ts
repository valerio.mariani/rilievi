import { CurrencyPipe, DatePipe } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService, defineLocale, BsLocaleService } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { itLocale } from 'ngx-bootstrap/locale';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Daco } from '../../model/daco.model';
import { MaxiEvaluationCriteria } from '../../model/maxiEvaluationCriteria.model';
import { Parameter } from '../../model/parameter.model';
import { EvaluationService } from '../../services/evaluation.service';
import Utils from 'src/app/shared/utily/utils';
import { AlertService } from 'src/app/shared/services/alert.service';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';
import { AdminService } from 'src/app/admin/services/admin.service';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { MaxiEvaluationSettings } from '../../model/maxiEvaluationSettings.model';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { NgForm } from '@angular/forms';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';

@Component({
  selector: 'app-maxi-evaluation',
  templateUrl: './maxi-evaluation.component.html',
  styleUrls: ['./maxi-evaluation.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MaxiEvaluationComponent implements OnInit, OnDestroy, AfterViewInit {

  dacoList: Array<Daco>;
  dacoExcludedList: Array<Daco>;

  excludedSelectedDaco: Array<Daco> = new Array<Daco>();
  includedSelectedDaco: Array<Daco> = new Array<Daco>();

  criteria: MaxiEvaluationCriteria;

  searchStarted: boolean = false;

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  settings: EvaluationSettings;
  lastProcessDate: Date;
  deltaDays: number;
  endDate: Date;

  deltaDaysParam: Parameter;
  lastElaborationParam: Parameter;
  
  disabledForm:boolean=false;
  @ViewChild('insertFormRef', { read: "", static: true })
  currentForm: NgForm;

  constructor(private cd: ChangeDetectorRef, private settingsService: SettingsService, private localeService: BsLocaleService,  private evaluationService: EvaluationService,
    public route: ActivatedRoute, public router: Router, public modalService: BsModalService, private alertServ: AlertService,
    private adminService: AdminService,public alertService: AlertService) {
    this.criteria = new MaxiEvaluationCriteria();
     //setting the it locale for datepicker component
     itLocale.invalidDate = '';
     defineLocale('it', itLocale);
     this.localeService.use('it');

  }


  ngOnInit() {
    this.checkPreFetchData();
    this.initDataTable();
  }

  checkPreFetchData(): void {

    if(!this.route.snapshot.data.pageData.error){
      try{
        this.deltaDaysParam = this.route.snapshot.data.pageData.deltaDays;
        this.lastElaborationParam = this.route.snapshot.data.pageData.lastElaboration;
        this.lastProcessDate = Utils.parseDate(this.lastElaborationParam.value);
        this.endDate = new Date(this.lastProcessDate.getTime());
        if(!Utils.isValidDate(this.endDate)){
          throw new Error('Invalid Date!');
        }
        this.deltaDays= parseInt(this.deltaDaysParam.value);
        this.dacoList = this.route.snapshot.data.pageData.dacoList;
        this.dacoExcludedList = this.route.snapshot.data.pageData.dacoExcludedList;
      } catch (er) {
        this.router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Errore Inizializzazione funzione. Verificare il formato dei parametri: ' + ParamEnum.DELTA_LIMIT_DATE + ',' + ParamEnum.LIMIT_DATE}})
      }
    } else {
        this.router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Errore Inizializzazione funzione. Verificare la valorizzazione dei parametri: ' + ParamEnum.DELTA_LIMIT_DATE + ',' + ParamEnum.LIMIT_DATE}}) 
   }

  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();

  }


  addInExcludedList(): void {
    this.includedSelectedDaco.forEach(daco => {
      this.dacoExcludedList.push(daco);
      this.dacoList.splice(this.dacoList.indexOf(daco), 1);
    });
    this.includedSelectedDaco = new Array<Daco>();
    this.dacoExcludedList = this.dacoExcludedList.slice(0);
    this.dacoList = this.dacoList.slice(0);
  }

  addInIncludedList(): void {
    this.excludedSelectedDaco.forEach(daco => {
      this.dacoList.push(daco);
      this.dacoExcludedList.splice(this.dacoExcludedList.indexOf(daco), 1);
    });
    this.excludedSelectedDaco = new Array<Daco>();
    this.dacoExcludedList = this.dacoExcludedList.slice(0);
    this.dacoList = this.dacoList.slice(0);
  }

  dateToMin(): Date {
    let ub = new Date;
    if (this.criteria.dateFrom != null) {
      ub.setDate(this.criteria.dateFrom.getTime());
    } else {
      ub.setDate(this.settingsService.settings.sysDate.getTime());
    }
    return ub;
  }

  dateFromMax(): Date {
    return new Date(this.settingsService.settings.sysDate.getTime());
  }

  public goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.REGO_MAXI_EVAL);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 5;
    this.dtOptions = {
        pagingType: 'simple_numbers',
        pageLength: pagingSizeValue,
        scrollX:true,
        autoWidth:false,
        scrollCollapse: false,        
        serverSide: true,
        processing: false,
        searching: false,
        lengthChange: false,
        stateSave: true,
        ordering: false,
        info: true,
        infoCallback: function( settings, start, end, max, total, pre ) {
          let buttonExp = total == 0 ? '' : '<button id="btnExport" type="button" class="btn btn-sm btn-primary size" style="float: right; margin-left: 20px; margin-top: -5px"><span class="oi oi-document"></span>Export Excel</button>'; 
          return  " Totali: " + total + buttonExp;
      },
      language: {
        info: '',
        infoFiltered: '',
        emptyTable: 'Nessun rilievo',
        paginate: {
          first: 'Inizio',
          last: 'Fine',
          next: 'Successivo',
          previous: 'Precedente'
        }
      },
      drawCallback: (settings: DataTables.SettingsLegacy) => {
        jQuery('#btnExport').bind('click', () => {
          that.exportExcel();
        });
      },
      ajax: (dataTablesParameters: any, callback) => {
        if (!this.searchStarted) {
          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: [],
            draw: 0
          });
        } else {
          let pageNumber = dataTablesParameters.start == 0 ? 1 : Math.round(dataTablesParameters.start / dataTablesParameters.length + 1);
          this.criteria.pageNumber = pageNumber;
          this.criteria.pageSize = dataTablesParameters.length;
          this.criteria.dacos = this.dacoList;
          that.evaluationService.searchMaxiEvaluation(this.criteria).subscribe(
            resp => {
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: resp.data,
                draw: dataTablesParameters.draw
              });
            }, err =>{
              Utils.showServerError(err, this.alertService, this.currentForm);
            }
          );
        }


      },
      columns: [{
        title: 'Nr. Rilievo',
        data: 'id',
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('max-width', '80px');
        }
      },
      {
        title: 'Data Inserimento',
        data: 'insertDate',
        render: function (data, type, row, meta) {
          return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('max-width', '150px');
        }
      },
      {
        title: 'Data Competenza',
        data: 'competenceDate',
        render: function (data, type, row, meta) {
          return new DatePipe('it-IT').transform(data, 'dd/MM/yyyy');
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('max-width', '150px');
        }
      },
      {
        title: 'Filiale',
        data: 'branch.description'
      },
      {
        title: 'Frazionario',
        data: 'office.fractional'
      },
      {
        title: 'Voce Daco',
        data: 'daco.description',
        render: function (data, type, row, meta) {
          return row.daco.full;
        }
      },
      {
        title: 'Causale',
        data: 'reason.description',
        render: function (data, type, row, meta) {
          return data;
        },
      },
      {
        title: 'Debito',
        data: 'debitAmount',
        render: function (data, type, row, meta) {
          return row.debitAmount != null ? (new CurrencyPipe('it-IT')).transform(row.debitAmount, 'EUR', true) : "";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('text-align', 'right');
          $(td).css('max-width', '150px');
        }
      },
      {
        title: 'Credito',
        data: 'creditAmount',
        render: function (data, type, row, meta) {
          return row.creditAmount != null ? (new CurrencyPipe('it-IT')).transform(row.creditAmount, 'EUR', true) : "";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('text-align', 'right');
          $(td).css('max-width', '150px');
        }
      }
      ]
    };
  }

  search(): void {
    this.searchStarted = true;
    this.rerender();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      dtInstance.state.clear();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }


  amountToChange(): void {
    if (this.criteria.amountFrom && this.criteria.amountTo < this.criteria.amountFrom) {
      this.criteria.amountTo = null;
    }
  }

  amountFromChange(): void {
    if (this.criteria.amountTo && this.criteria.amountTo < this.criteria.amountFrom) {
      this.criteria.amountFrom = null;
    }
  }

  endDateMaxDate():Date{
    let maxDate = new Date(this.lastProcessDate.getTime());
    maxDate.setDate(maxDate.getDate() - this.deltaDays);
    return maxDate;
  }

  regularization(): void {
    let request: MaxiEvaluationSettings = new MaxiEvaluationSettings();
    request.limitDate = this.endDate
    request.excludedDacos = this.dacoExcludedList;

    this.evaluationService.setMaxiEvaluationSettings(request).subscribe(res => {
      this.alertServ.addSuccessMessage('Regolarizzazione avviata correttamente!');
    }, err =>{
      Utils.showServerError(err, this.alertService, this.currentForm);
  });

  }
  exportExcel(): void {
    this.criteria.dacos = this.dacoList;
    this.evaluationService.exportMaxiEvaluations(this.criteria).subscribe(data => {
      Utils.download(data);
    }, err =>{
      Utils.showServerError(err, this.alertService, this.currentForm);
  });
  }
}
