import { TestBed } from '@angular/core/testing';

import { MaxiEvaluationResolver } from './maxi-evaluation.resolver';

describe('MaxiEvaluationResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaxiEvaluationResolver = TestBed.get(MaxiEvaluationResolver);
    expect(service).toBeTruthy();
  });
});
