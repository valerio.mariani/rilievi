import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable,  of, forkJoin } from 'rxjs';
import { AdminService } from 'src/app/admin/services/admin.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';
import { Parameter } from '../../model/parameter.model';
import { map, catchError } from 'rxjs/operators';
import { Daco } from '../../model/daco.model';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';


interface IReturnMaxiEvaluation {
  deltaDays: Parameter;
  lastElaboration: Parameter;
  dacoList:Array<Daco>;
  dacoExcludedList:Array<Daco>;
  error:any;
}

@Injectable({
  providedIn: 'root'
})
export class MaxiEvaluationResolver implements Resolve<IReturnMaxiEvaluation>{
  
  
  constructor(private tipologService: TipologService,
              private adminService: AdminService, private settingsService: SettingsService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):  Observable<IReturnMaxiEvaluation> {
    try{
      let functionMaxi = this.settingsService.settings.getFunction(FunctionEnum.REGO_MAXI_EVAL);
      let deltaDate:Parameter = functionMaxi.params[ParamEnum.DELTA_LIMIT_DATE];
      let lastElaboration:Parameter = functionMaxi.params[ParamEnum.LIMIT_DATE];
      let ret:IReturnMaxiEvaluation;
      return forkJoin([
        this.adminService.getParameter(functionMaxi.id,deltaDate.id),
        this.adminService.getParameter(functionMaxi.id,lastElaboration.id),
        this.tipologService.getDacoItemMaxiEvaluation(),
        this.tipologService.getDacoItemExcludedMaxiEvaluation()
      ] ).pipe(
        map(results => ( {
                      deltaDays : results[0],
                      lastElaboration : results[1],
                      dacoList: results[2],
                      dacoExcludedList: results[3],
                      error: null
                        }),
            catchError( error=>{
                      throw Observable.throw(error);
             })
        ));

    }catch(error){
      return of({
        deltaDays : null,
        lastElaboration :null,
        dacoList:null,
        dacoExcludedList: null,
        error: error
          });
    }

  }

 



  
}
