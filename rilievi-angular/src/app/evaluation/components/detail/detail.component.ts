import { Component, Input, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal/';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';
import Utils from 'src/app/shared/utily/utils';
import { Evaluation } from '../../model/evaluation.model';
import { EvaluationService } from '../../services/evaluation.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailComponent implements OnInit {
  @Input()
  title: string = "Consultazione Dettaglio Rilievo";
  
  modalRef: BsModalRef;
  model = new Evaluation();
  branchOptions: any;
  totalAttachments: number;

  
  labelDebit:string = "DARE (DIMINUISCE)/DARE (AUMENTA)";
  labelCredit:string = "AVERE (AUMENTA)/AVERE (DIMINUISCE)";

  constructor(public route: ActivatedRoute, public router: Router, public modalService: BsModalService, 
              public evaluationService: EvaluationService, public previousUrlService : PreviousRouteService) { }
  submitted = false;

  ngOnInit() {
    const idEval=this.route.snapshot.params['id'];
    if(!isNaN(idEval)) {
      this.evaluationService.getEvaluation(idEval as number)
      .subscribe(res => {
        this.model=res;
        this.totalAttachments = this.model.attachments.length;
        
        if(this.model.idChild || this.model.idParent){

          if( this.model.daco && Utils.isDacoDebit(this.model.daco) ){
            this.labelCredit = "AVERE (AUMENTA)";
            this.labelDebit = "DARE (DIMINUISCE)";
            }else{
              this.labelCredit = "AVERE (DIMINUISCE)";
              this.labelDebit = "DARE (AUMENTA)";
            }
        
        }else{
          this.labelDebit = "Rilievo a DEBITO (EA13F)";
          this.labelCredit = "Rilievo a CREDITO (ED05F)";
        }
      }, err => {  //TODO 
        console.log(err);
        });
      }

  }
  
  public goToHome() {
    this.router.navigateByUrl(PathRegistry.HOME);
  }

  openAttachmentsModal(template: TemplateRef<any>) {  
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
  }
  onSubmit() {
    this.confirmPrintEvaluation();
    this.submitted = true;
  }
  confirmPrintEvaluation() {
    const confirmData = {
      title: 'Conferma Stampa Rilievo ',
      message: 'Si vuole stampare questo rilievo?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {        
        this.modalRef.hide();        
      } else {
        return;
      }
    });
  }

  public goToBack() {
    this.router.navigateByUrl(this.previousUrlService.getPreviousUrl());
  }

  print(){
    window.print();
  }

  updateTotalAttachments(totalAttachments: number): void{
    this.totalAttachments = totalAttachments;
  }

}
