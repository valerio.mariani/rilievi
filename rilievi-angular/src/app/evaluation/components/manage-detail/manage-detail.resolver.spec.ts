import { TestBed } from '@angular/core/testing';

import { ManageDetailResolver
 } from './manage-detail.resolver';

describe('ManageDetailResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageDetailResolver = TestBed.get(ManageDetailResolver);
    expect(service).toBeTruthy();
  });
});
