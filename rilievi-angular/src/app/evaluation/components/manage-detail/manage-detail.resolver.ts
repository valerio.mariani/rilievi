import { Injectable } from '@angular/core';
import { Evaluation } from '../../model/evaluation.model';
import { Office } from '../../model/office.model';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { EvaluationService } from '../../services/evaluation.service';
import { map, catchError } from 'rxjs/operators';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import { Status } from '../../model/status.model';


interface IReturnManageDetail {
  evaluation: Evaluation;
  statusList: Array<Status>;
}

@Injectable({
  providedIn: 'root'
})
export class ManageDetailResolver implements Resolve<IReturnManageDetail>{
  

  constructor(private evaluationService: EvaluationService, private tipologService: TipologService) { 

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IReturnManageDetail | Observable<IReturnManageDetail> | Promise<IReturnManageDetail> {
    const evalId:number = route.params['id'];
    return forkJoin ([
         this.evaluationService.getEvaluation(evalId),
         this.tipologService.getStatus()
       ])
       .pipe(
               map(results => ( {
                                evaluation : results[0],
                                statusList : results[1]
                               }),
                   catchError(error=>{
                             throw Observable.throw(error);
                    })
       ));
   
  }

  /*resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Evaluation | Observable<Evaluation> | Promise<Evaluation> {
    const evalId:number = route.params['id'];
    return  this.evaluationService.getEvaluation(evalId);

   
  }*/
}
