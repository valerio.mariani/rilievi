import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService, AlertComponent } from 'ngx-bootstrap';
import { EvaluationSettings } from 'src/app/core/model/evaluationSettings';
import { Function } from 'src/app/core/model/function.model';
import { AuthorizationService } from 'src/app/core/services/authorization.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { UserService } from 'src/app/core/services/user.service';
import { GenericModalComponent } from 'src/app/shared/components/generic-modal/generic-modal.component';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { AlertService } from 'src/app/shared/services/alert.service';
import { PreviousRouteService } from 'src/app/shared/services/previous-route.service';
import { TipologService } from 'src/app/shared/services/tipolog.service';
import Utils from 'src/app/shared/utily/utils';
import { Evaluation } from '../../model/evaluation.model';
import { Office } from '../../model/office.model';
import { Status } from '../../model/status.model';
import { EvaluationService } from '../../services/evaluation.service';
import { Daco } from '../../model/daco.model';
import { Branch } from '../../model/branch.model';

@Component({
  selector: 'app-manage-detail',
  templateUrl: './manage-detail.component.html',
  styleUrls: ['./manage-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ManageDetailComponent implements OnInit {
  evalGenerated: Evaluation = new Evaluation();
  dacoListSource: Array<Daco>;
  branchOptions: Array<Branch>;
  dacoListGenerated: Array<Daco>;
  functions: Array<Function> = [];
  @Input()
  model: Evaluation = new Evaluation();
  dacoList: Array<Daco>;
  @Input()
  statusList: Array<Status> = [];
  @Output('back')
  emitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  backupModel: Evaluation;
  settings: EvaluationSettings;
  isLoaded: boolean = false;
  officeList: Array<Office> = [];
  @ViewChild('manageEvalFormRef', { read: "", static: true })
  currentForm: NgForm;

  modalRef: BsModalRef;

  totalNotes: number;
  totalAttachments: number;

  labelDebit: string = "DARE (DIMINUISCE)/DARE (AUMENTA)";
  labelCredit: string = "AVERE (AUMENTA)/AVERE (DIMINUISCE)";

  userProfile: string;
  profile: any;
  switchSaveChange:boolean =false;
  constructor(public route: ActivatedRoute, public modalService: BsModalService,
    public evaluationService: EvaluationService, public previousUrlService: PreviousRouteService,
    protected tipologService: TipologService, protected settingsService: SettingsService,
    public alertService: AlertService, protected userService: UserService, protected authService: AuthorizationService) {
    this.userProfile = userService.user.profile;
    this.profile = ProfileEnum;
    
  }

  ngOnInit() {
    this.settings = this.settingsService.getSettings();
    this.totalNotes = this.model.notes.length;
    this.totalAttachments = this.model.attachments.length;
    this.backupModel = Evaluation.fromJSON(JSON.parse(JSON.stringify(this.model)));
    this.tipologService.getOffices(this.model.branch.id).subscribe(res => {
      this.officeList = res;
      this.isLoaded = true;
    });

    if (this.model.idChild || this.model.idParent) {

      if (this.model.daco && Utils.isDacoDebit(this.model.daco)) {
        this.labelCredit = "AVERE (AUMENTA)";
        this.labelDebit = "DARE (DIMINUISCE)";
      } else {
        this.labelCredit = "AVERE (DIMINUISCE)";
        this.labelDebit = "DARE (AUMENTA)";
      }

    } else {
      this.labelDebit = "Rilievo a DEBITO (EA13F)";
      this.labelCredit = "Rilievo a CREDITO (ED05F)";
    }
    this.tipologService.getBranches().subscribe(res => {
      this.branchOptions = res;
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });
    this.evaluationService.getDacoItem().subscribe(res => {
      this.dacoList = res;
      this.afterDacoListLoaded();
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });
  }

  afterDacoListLoaded(): void {

  }

  updateFranctional(): void {
    this.currentForm.controls['office'].enable();
    this.currentForm.controls['fractional'].enable();
  }

  selectionBranchChanged(event) {
    this.model.office = undefined;
    this.model.fractional = undefined;
  }

  selectionOfficeChanged(event) {
    this.model.fractional = undefined;
    if (this.model.office != null) {
      this.model.fractional = this.model.office.fractional;
    }
  }

  fractionalChanged(event) {
    this.model.office = undefined;
    this.model.branch = undefined;
    let model = this.model;
    let founded = false;
    this.branchOptions.forEach(function (branch, index) {
      if (founded) {
        return;
      }
      if (branch.office != null) {
        branch.office.forEach(function (office, index) {
          if (office.fractional == model.fractional) {
            model.office = office;
            model.branch = branch;
            founded = true;
            return;
          }
        });
      }
    });
    if (!founded) {
      this.model.office = undefined;
      this.model.branch = undefined;
      if (this.model.fractional != null && this.model.fractional != '') {
        this.currentForm.controls['fractional'].setErrors({ 'error': 'Codice Frazionario non valido' });
      }
    }
  }

  selectionDacoChanged(event, evalut: Evaluation) {
    evalut.accountNumber = undefined;
    evalut.service = undefined;
    if (evalut.daco != null) {
      evalut.accountNumber = evalut.daco.accountNumberFull;
    }
  }

  accountNumberChanged(dacoList: Array<Daco>, evalut: Evaluation, accountNumberFieldname: string) {
    evalut.daco = undefined;
    evalut.service = undefined;
    let founded = false;
    dacoList.forEach(function (dacoItem, index) {
      if (founded) {
        return;
      }
      if (dacoItem.accountNumber == evalut.accountNumber) {
        evalut.daco = dacoItem;
        founded = true;
        return;
      }
    });
    if (!founded) {
      evalut.daco = undefined;
      if (evalut.accountNumber != null && evalut.accountNumber != '') {
        this.currentForm.control.controls[accountNumberFieldname].setErrors({ 'error': 'Codice contabile non valido' });//{'incorrect':false}
      }
    }
  }

  btnAnnulla(): void {
    this.currentForm.controls['office'].disable();
    this.currentForm.controls['fractional'].disable(); //btnUpdFractional
    this.model = Evaluation.fromJSON(JSON.parse(JSON.stringify(this.backupModel)));
  }

  btnDeleteEval(): void {
    let msgConfirm = 'Confermi la cancellazione del rilievo?';
    if (this.model.idParent) {
      msgConfirm = 'Verrà cancellato anche il rilievo n. ' + this.model.idParent
        + ' in quanto con causale RA. Confermi la cancellazione dei rilievi?';
    }
    const confirmData = {
      title: 'Cancellazione Rilievo',
      message: msgConfirm,
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.evaluationService.deleteEvaluation(this.model.id).subscribe(res => {
          this.model = res;
          this.alertService.addSuccessMessage("Rilievo cancellato con successo!");
          this.goToBack();
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    });
  }

  reasonToRc(): void {
    const confirmData = {
      title: 'Trasformazione Rilievo',
      message: 'Confermi la trasformazione del rilievo a RC?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.evaluationService.transformToRcReason(this.model.id).subscribe(res => {
          this.model.reason = res.reason;
          this.model.previousReason = res.previousReason;
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    });

  }
  checkAmountDigit(c) {
    if (!((c.keyCode > 95 && c.keyCode < 106)
      || (c.keyCode > 47 && c.keyCode < 58)
      || c.keyCode == 8 || c.keyCode == 190 || c.keyCode == 9 || c.keyCode == 110 || c.keyCode == 37 || c.keyCode == 38 || c.keyCode == 39 || c.keyCode == 40
    )) {
      return false;
    }
  }
  reasonToRsRd(): void {
    const confirmData = {
      title: 'Trasformazione Rilievo',
      message: 'Confermi l\'annullo della trasformazione del rilievo a RC?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.evaluationService.undoRcReason(this.model.id).subscribe(res => {
          this.model.reason = res.reason;
          this.model.previousReason = res.previousReason;
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    });
  }

  openAttachmentsModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  openNoteModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  setLossState(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }
  updateLossState(status: Status): void {
    this.model.status = status;
  }
  undoLossState(): void {

    const confirmData = {
      title: 'Annullo connotazione a perdita',
      message: 'Confermi l\'annullamento della connotazione a perdita?',
      buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
    };

    let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
    modalRefConf.content.onClose.subscribe(result => {
      if (result == "true") {
        this.evaluationService.undoLossState(this.model.id).subscribe(res => {
          this.alertService.addSuccessMessage("Annullo connotazione a perdita effettuato con successo");
          // this.reload();
          this.model.status = this.settings.unLossState;
        }, err => {
          Utils.showServerError(err, this.alertService, this.currentForm);
        });
      } else {
        return;
      }
    });
  }


  updateTotalNotes(totalNotes: number): void {
    this.totalNotes = totalNotes;
  }

  updateTotalAttachments(totalAttachments: number): void {
    this.totalAttachments = totalAttachments;
  }
  competenceDateUpperBound(): Date {
    let ub = new Date();
    if (this.model._insertDate != null) {
      ub.setDate(this.model._insertDate.getDate() - 1);
    }
    return ub;
  }
  saveChange(): void {
    
    if (!this.currentForm.controls['office'].disabled) {

      const confirmData = {
        title: 'Modifica Frazionario',
        message: 'Confermi la modifica del frazionario?',
        buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
      };

      let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
      modalRefConf.content.onClose.subscribe(result => {
        this.switchSaveChange==true;
        if (result == "true") {
          
          this.evaluationService.updateEvaluationOffice(this.model.id, this.model.office).subscribe(res => {
            this.currentForm.controls['office'].disable();
            this.currentForm.controls['fractional'].disable();
            
          }, err => {
            Utils.showServerError(err, this.alertService, this.currentForm);
          });
          
        } else {
          return;
        }
      });



    }
    if (this.switchSaveChange==true) {
      return 
    }else{
      const confirmData = {
        title: 'Conferma modifica valori ',
        message: 'Confermi la modifica dei seguenti campi?',
        buttons: [{ label: "SI", retValue: "true", colorClass: "primary" }, { label: "NO", retValue: "false", colorClass: "danger" }]
      };
      let modalRefConf = this.modalService.show(GenericModalComponent, { initialState: confirmData });
      modalRefConf.content.onClose.subscribe(result => {
        if (result == "true") {
          this.evaluationService.updateEvaluationDetail(this.model.id, this.model).subscribe(res => {
            this.currentForm.controls['competenceDate'].disable();
            this.currentForm.controls['daco'].disable();
            this.currentForm.controls['description'].disable();
            this.currentForm.controls['creditAmount'].disable();
            this.currentForm.controls['debitAmount'].disable();
            this.currentForm.controls['service'].disable();
            this.alertService.addSuccessMessage("Modifica dei valori effettuata con successo");
          }
          )
        }
        
      });
      
    }
    if (!this.currentForm.controls['description'].disabled) {
      this.evaluationService.setDescription(this.model.id, this.model.description).subscribe(res => {
        this.currentForm.form.reset(this.currentForm.form.getRawValue(), { emitEvent: false })
      }, err => {
        Utils.showServerError(err, this.alertService, this.currentForm);
      });

    }
  }

  
  cleanAmount(debit?: boolean) {
    if (debit && this.model.debitAmount) {
      this.model.debitAmount = null;
    } else if (!debit && this.model.creditAmount) {
      this.model.creditAmount = null;
    }
  }

  checkValidAmount(debit: boolean): void {
    if( Utils.isEmpty(this.model.creditAmount)  && Utils.isEmpty(this.model.debitAmount) ){
       this.currentForm.control.controls[debit?'debitAmount':'creditAmount'].setErrors({'error': 'Valorizzare un importo'});
     }
  }
  
  goToBack() {
    this.alertService.clear();
    this.emitter.emit(true);

  }

  isUndoEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.DELETE_EVAL);
  }

  isSetLossEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.SETLOSS_EVAL);
  }

  isChangeReasonEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.CHANGE_EVAL_REASON);
  }

  isChangeFractionalEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.CHANGE_EVAL_FRACT);
  }
}
