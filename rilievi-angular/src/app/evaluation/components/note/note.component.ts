import { Component, OnInit, Input, ViewChild, OnDestroy, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { EvaluationService } from '../../services/evaluation.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { Note } from '../../model/note.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { UserService } from 'src/app/core/services/user.service';
import { AlertScope } from 'src/app/shared/alertscope.enum';
import { NgForm } from '@angular/forms';
import Utils from 'src/app/shared/utily/utils';
import { AuthorizationService } from 'src/app/core/services/authorization.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { Parameter } from '../../model/parameter.model';
import { ParamEnum } from 'src/app/admin/manage-function/param.enum';


@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnDestroy, AfterViewInit  {

  @Input()
  modalRef: BsModalRef;

  @Input('evaluation-id')
  idEvaluation: number;

  notes: Array<Note> = new Array<Note>();
  newNote:Note; 

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtNoteOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  AlertScope:any;

  //Numero di note totali
  totalNotes : number;
  @Output('numberNotesChanged')
  emitter: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('InserNoteForm', { read: "", static: true })
  currentForm: NgForm;

  userProfile:string;
  profile:any;

  constructor(private evalutService: EvaluationService,  public alertService: AlertService, 
              public settingsService: SettingsService, private userService: UserService, protected authService:AuthorizationService){
    this.userProfile = userService.user.profile;
    this.profile = ProfileEnum;
    this.AlertScope = AlertScope;
  }

  ngOnInit(){
    this.newNote = new Note(); 
    this.newNote.user= this.userService.user.code;
    this.newNote.dateCreation= this.settingsService.settings.sysDate;
    this.newNote.note = "";

    this.initDataTable();
  }

    
  addNote(): void{ 
    this.evalutService.addEvaluationNote(this.idEvaluation,this.newNote).subscribe(res=>{
      this.rerender();
      this.newNote.note = "";
    }, err => {
      Utils.showServerError(err, this.alertService, this.currentForm);
    });
  
  }
 rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  close() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();    
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
//
  initDataTable(): void {
    let that = this;
    let function1 = this.settingsService.settings.getFunction(FunctionEnum.MANAG_NOTE);
    let pagingSize:Parameter = function1.params[ParamEnum.PAGING_SIZE];
    let pagingSizeValue=pagingSize ? parseInt(pagingSize.value) : 10;
    this.dtNoteOptions = {
        pagingType: 'numbers',
        pageLength: pagingSizeValue,
        serverSide: true,
        processing: false,
        searching: false,
        paging: true,
        lengthChange: false,
        stateSave: false,
        ordering: false,
        info: true,
        infoCallback: function( settings, start, end, max, total, pre ) {
          return  " ";
        },
        language: {
          info: '',
          infoFiltered: '',
          emptyTable: 'Nessuna nota inserita',
          paginate: {
            first: 'Inizio',
            last: 'Fine',
            next: 'Successivo',
            previous: 'Precedente'
          },
          zeroRecords: ''
        },
        ajax: (dataTablesParameters: any, callback) => {
          let pageNumber = dataTablesParameters.start == 0 ?1 : Math.round(dataTablesParameters.start/dataTablesParameters.length+1);
          that.evalutService.getEvaluationNotes(that.idEvaluation).subscribe(
            resp =>{
              that.notes = resp.data;
              that.totalNotes = resp.data.length;
              that.emitter.emit(that.totalNotes);
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsTotal,
                data: []
              });
            },err =>{
              callback({
                recordsTotal:0,
                recordsFiltered: 0,
                data: [],
                error: 'Sistema momentaneamente non disponibile!'
              });
            }
          );

        },
        drawCallback :( settings: DataTables.SettingsLegacy ) =>{  
          if(that.totalNotes <= settings._iDisplayLength){
            jQuery('#EvaluationNotes_paginate').css('display','none');
          }
          
          
        }
    };
  }
  isManageNoteEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.MANAG_NOTE);
  }
}
