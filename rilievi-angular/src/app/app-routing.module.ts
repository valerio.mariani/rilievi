import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminModule } from './admin/admin.module';
import { ExtractionDatesComponent } from './admin/extraction-dates/extraction-dates.component';
import { LogTrackComponent } from './admin/log-track/log-track.component';
import { ManageDacoComponent } from './admin/manage-daco/manage-daco.component';
import { ManageFunctionComponent } from './admin/manage-function/manage-function.component';
import { ProfileComponent } from './admin/profile/profile.component';
import { ContentComponent } from './core/components/content/content.component';
import { MenuComponent } from './core/components/menu/menu.component';
import { CoreModule } from './core/core.module';
import { AuthorizationGuard } from './core/guard/authorization.guard';
import { DacoReclassificationComponent } from './evaluation/components/daco-reclassification/daco-reclassification.component';
import { DetailComponent } from './evaluation/components/detail/detail.component';
import { InsertComponent } from './evaluation/components/insert/insert.component';
import { MaxiEvaluationComponent } from './evaluation/components/maxi-evaluation/maxi-evaluation.component';
import { MaxiEvaluationResolver } from './evaluation/components/maxi-evaluation/maxi-evaluation.resolver';
import { ReportComponent } from './evaluation/components/report/report.component';
import { SearchManageComponent } from './evaluation/components/search-manage/search-manage.component';
import { EvaluationModule } from './evaluation/evaluation.module';
import { PathRegistry } from './shared/pathregistry.enum';
import { ErrorComponent } from './shared/components/error/error.component';
import { LogoutComponent } from './shared/components/logout/logout.component';





const routes: Routes = [
//resolve:{pageData: MenuResolver},
    { path: PathRegistry.ROOT, component: ContentComponent, children:[
    { path: PathRegistry.ROOT, redirectTo: PathRegistry.HOME, pathMatch: "full"},
    { path: PathRegistry.HOME, component: MenuComponent},
    { path: PathRegistry.DO_EVAL, component: InsertComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.REPORT, component: ReportComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.DETAIL, component: DetailComponent},//TODO
    { path: PathRegistry.SEARCH_MANAGE, component: SearchManageComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.RECLASSIFICATION_DACO, component: DacoReclassificationComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.MAXI_EVALUATION, component: MaxiEvaluationComponent, resolve:{pageData: MaxiEvaluationResolver}, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.EXTRACTION_DATES, component: ExtractionDatesComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.TRACK_LOG, component: LogTrackComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.PROFILE, component: ProfileComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.MANAGE_DACO, component: ManageDacoComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.MANAGE_FUNCTION, component: ManageFunctionComponent, canActivate:[AuthorizationGuard]},
    { path: PathRegistry.ERROR, component: ErrorComponent},
    { path: PathRegistry.LOGOUT, component: LogoutComponent}
  ]},
  { path: PathRegistry.UNKNOWN_PATH, redirectTo: PathRegistry.HOME } //for unknown path

]; 

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation:  'reload'}),
            CoreModule,
            AdminModule,
            EvaluationModule],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }
