import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { User } from './core/model/user.model';
import { AuthorizationService } from './core/services/authorization.service';
import { SettingsService } from './core/services/settings.service';
import { UserService } from './core/services/user.service';
import { PathRegistry } from './shared/pathregistry.enum';
import { Router } from '@angular/router';


/**
 * Function used to load User and Settings from backend servlet
 * */
export  function appLoadSettingsFn(userService: UserService, settingsService: SettingsService, 
                            authorizationService:AuthorizationService, injector:Injector) {
  
  return (): Promise<any> => {
      return userService.loadUserInfo().then(data => {
        return userService.user = User.fromJSON(data);
      }).catch(error => {
        console.warn('Error load User Info');
        const router = injector.get(Router);
        router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Errore lettura informazioni utente.'}})
      }).then(userInfo => {
          return Promise.all([settingsService.loadSettings(),authorizationService.loadAuthorizations()]);            
        }).catch(err =>{
          console.warn('Error load Settings');
          const router = injector.get(Router);
          router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Errore lettura configurazioni Applicazione'}})
        
        });

  };   
}
 
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    SettingsService,
    AuthorizationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appLoadSettingsFn,
      multi: true,
      deps: [UserService, SettingsService, AuthorizationService, Injector],
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
