import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { User } from '../model/user.model';
import { AuthorizationService } from '../services/authorization.service';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
  userProfile: string;
  user = new User();

  noPaths: Object = new Object();

  constructor(private userService: UserService, private router: Router, private authService: AuthorizationService) {
    this.userProfile = userService.user.profile;
    this.user = this.userService.user;

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let targetPath = route.routeConfig.path;

    if (this.authService.isAuthorizated(targetPath)) {
      return true;
    } else {
      this.router.navigateByUrl(PathRegistry.ERROR,{state:{message:'L\'Utente non è abilitato all\'utilizzo di questa funzione'}})
      return false;
    }
  }

}
