import { Router } from '@angular/router';
import { BaseHttpService } from 'src/app/shared/services/basehttp.service';
import { EvaluationSettings } from '../model/evaluationSettings';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService extends BaseHttpService{

  settings: EvaluationSettings = null;
 // http: HttpClient;
  //router: Router;

  private _loadSettings(): Promise<any> {
    return this.get('/api/config').toPromise();
  }

  loadSettings(): Promise<any> {
    let that = this;
    return this._loadSettings().then(data => {
      that.settings = EvaluationSettings.fromJSON(data); 
    })
    .catch(error => {
      console.warn('Error load application settings');
      throw new Error('Error load application settings');
    }); 
  }

 
  getSettings():EvaluationSettings{
    return this.settings;
  }

}
