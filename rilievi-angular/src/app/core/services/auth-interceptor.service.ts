import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
 
  constructor(private router: Router, private userService: UserService) { 

  }


  intercept(req: import("@angular/common/http").HttpRequest<any>, 
            next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
   


              return next.handle(req).pipe(
                tap(
                  event => {},
                  error => {
                    //logging the http response to browser's console in case of a failuer
                    if (event instanceof ProgressEvent) {
                      console.log('ResponseError');
                      let resp = <XMLHttpRequest>event.currentTarget;
                      if(resp.status == 401){
                        if(this.userService.getUser() && this.userService.getUser().branchNumber ){
                          this.router.navigateByUrl(PathRegistry.ERROR,{state:{message:'Sessione Scaduta'}});
                        }else{
                          window.location.href = '/init';
                        }
                      }
                    
                    }
                  }
                )
              );
  }

 
}
