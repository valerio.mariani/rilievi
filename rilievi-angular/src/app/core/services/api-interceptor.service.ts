import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiInterceptorService implements HttpInterceptor {

  private pendingRequest: number = 0;

  constructor(private spinner: NgxSpinnerService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.pendingRequest++;

    if (this.pendingRequest != 0) {
      //show the spinner
      this.spinner.show();

      return next.handle(req).pipe(
        finalize(() => {
          this.pendingRequest--;
          if (this.pendingRequest == 0) {
            //hide the spinner
            this.spinner.hide();
          }
        })
      );
    }
  }
}
