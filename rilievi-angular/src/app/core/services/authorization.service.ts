import { Injectable } from '@angular/core';
import { ProfileService } from 'src/app/admin/services/profile.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { Function } from '../model/function.model';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationService {

    availablePaths: Object = new Object();
    availableActions: Object = new Object();
    functions: Array<Function>;
    userProfile: string;

    constructor(private profileService: ProfileService, private userService: UserService) {
        this.initAvailablePaths();
    }

    loadAuthorizations(): Promise<any> {
        let that = this;
        return this.profileService.getFuctionByProfileCode(this.userService.user.profile)
            .then(
                data => {
                    let list: Array<Function> = [];
                    if (Array.isArray(data)) {
                        list = Array.from(data).map(el => 
                             {
                                 let f = Function.fromJSON(el);
                                 that.availableActions[f.name] = true;
                                return f;
                             });
                    }
                    that.functions = list;
                })
            .catch(error => {
                console.warn('Error load authorization settings');
                throw new Error('Error load authorization settings');
            });
    }

    isAuthorizated(targetPath: string) {
        let paths = this.functions.map(item => {
            let value = this.availablePaths[item.name];
            return value ? value[0] : void 0;
        });

        if (paths.lastIndexOf(targetPath) != -1) {
            return true
        } else {
            return false;
        }
    }

    isEnabled(f: FunctionEnum): boolean {
        return this.availableActions[FunctionEnum[f]];
    }

    private initAvailablePaths(): void {
        this.availablePaths[FunctionEnum[FunctionEnum.INS_EVAL]] = [PathRegistry.DO_EVAL];
        this.availablePaths[FunctionEnum[FunctionEnum.RECLA_DACO]] = [PathRegistry.RECLASSIFICATION_DACO];
        this.availablePaths[FunctionEnum[FunctionEnum.INQUIRY_EVAL]] = [PathRegistry.SEARCH_MANAGE];
        this.availablePaths[FunctionEnum[FunctionEnum.AUDIT_LOG]] = [PathRegistry.TRACK_LOG];
        this.availablePaths[FunctionEnum[FunctionEnum.REPO_EVAL]] = [PathRegistry.REPORT];
        this.availablePaths[FunctionEnum[FunctionEnum.MANAG_DACO]] = [PathRegistry.MANAGE_DACO];
        this.availablePaths[FunctionEnum[FunctionEnum.DATES_EXTRACT]] = [PathRegistry.EXTRACTION_DATES];
        this.availablePaths[FunctionEnum[FunctionEnum.MANAG_PROFILES]] = [PathRegistry.PROFILE];
        this.availablePaths[FunctionEnum[FunctionEnum.MANAG_FUNCTIONS]] = [PathRegistry.MANAGE_FUNCTION];
        this.availablePaths[FunctionEnum[FunctionEnum.REGO_MAXI_EVAL]] = [PathRegistry.MAXI_EVALUATION];
        this.availablePaths[FunctionEnum[FunctionEnum.REGO_MAXI_EVAL]] = [PathRegistry.MAXI_EVALUATION];
    }

}