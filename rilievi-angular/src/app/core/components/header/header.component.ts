import { Component, OnInit } from '@angular/core';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  booleanLogout:boolean;
  userCode:string;
  homelink = '/' + PathRegistry.HOME;

  constructor(private userService : UserService, public route: ActivatedRoute, public router: Router) {
   }

  ngOnInit() {
    this.userCode = this.userService.user.code;

  }



  exit() {
    this.userService.logout().subscribe( res =>{
     this.router.navigateByUrl(PathRegistry.LOGOUT)+ "?timestamp=" + Date.now();
      this.booleanLogout=true;
    }, err =>{

    })
  }

}
