import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PathRegistry } from 'src/app/shared/pathregistry.enum';
import { UserService } from '../../services/user.service';
import { ProfileEnum } from 'src/app/shared/profile.enum';
import { AuthorizationService } from '../../services/authorization.service';
import { FunctionEnum } from 'src/app/shared/fuction.enum';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  functionList: Array<Function>;
  userProfile: string;
  profile: any;
  constructor(private router: Router, public route: ActivatedRoute, protected userService: UserService, private authService: AuthorizationService) {
    this.userProfile = userService.user.profile;
    this.profile = ProfileEnum;
  }

  ngOnInit() {
    //this.functionList = this.route.snapshot.data.pageData; 

  }

  public goToDoEval() {
    this.router.navigateByUrl(PathRegistry.DO_EVAL);
  }

  public goToDaco() {
    this.router.navigateByUrl(PathRegistry.RECLASSIFICATION_DACO);
  }
  public goToConsultation() {
    this.router.navigateByUrl(PathRegistry.SEARCH_MANAGE);
  }

  public goToReport() {
    this.router.navigateByUrl(PathRegistry.REPORT);
  }

  public goToMaxiEvaluation() {
    this.router.navigateByUrl(PathRegistry.MAXI_EVALUATION);
  }

  public goToProfile() {
    this.router.navigateByUrl(PathRegistry.PROFILE);
  }

  public goToExtractionDates() {
    this.router.navigateByUrl(PathRegistry.EXTRACTION_DATES);
  }

  public goToManageDaco() {
    this.router.navigateByUrl(PathRegistry.MANAGE_DACO);
  }

  public goToTrackLog() {
    this.router.navigateByUrl(PathRegistry.TRACK_LOG);
  }

  public goToManageFunction() {
    this.router.navigateByUrl(PathRegistry.MANAGE_FUNCTION);
  }

  ///////////////////////////////////////////////////////////////////////////
  insEvalEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.INS_EVAL);
  }

  reclaDacoEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.RECLA_DACO);
  }

  inquiryEvalEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.INQUIRY_EVAL);
  }

  maxiEvalEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.REGO_MAXI_EVAL);
  }
  repoEvalEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.REPO_EVAL);
  }
  profilesEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.MANAG_PROFILES);
  }

  extractionDatesEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.DATES_EXTRACT);
  }

  manageDacoEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.MANAG_DACO);
  }
  trackLogEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.AUDIT_LOG);
  }
  manageFunctionEnabled(): boolean {
    return this.authService.isEnabled(FunctionEnum.MANAG_FUNCTIONS);
  }



}
