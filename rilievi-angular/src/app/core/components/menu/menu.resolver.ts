import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { FunctionService } from 'src/app/shared/services/function.service';
import { UserService } from '../../services/user.service';
import { Function } from '../../model/function.model';

interface IReturnMenu {
  functionList: Array<Function>;
}

@Injectable({
  providedIn: 'root'
})
export class MenuResolver implements Resolve<Array<Function>>{
  
  profile:string;
  constructor(private functionService:  FunctionService, userService: UserService ) { 
    this.profile = userService.user.profile;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Array<Function>> {
    return this.functionService.getFunction(this.profile);  
  }

}
