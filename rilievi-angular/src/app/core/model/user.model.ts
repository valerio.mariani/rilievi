import Utils from 'src/app/shared/utily/utils';
import { Office } from 'src/app/evaluation/model/office.model';

 export class User{
    code: string;
    profile: string;
    office: Office; 
    contabilizationDate:Date; 
    branchNumber:string; 
    operationNumber:string; 
    phase: string; 
    state:string; 
    originalOperationNumber:string;
    restartLevel:string;
    channel:string;
    compliance:string;
    client:string;

    static fromJSON(json: any): User {
        const user = new User();
        user.code = json['userId'];
        user.profile = json['role'][0]; 
      
        if(json['office']){
            user.office = Office.fromJSON(json['office']);
        }       
        user.contabilizationDate = Utils.parseDate(json['contabilizationDate']);
        user.branchNumber =json['branchNumber'];
        user.operationNumber =json['operationNumber'];
        user.phase = json['phase'];
        user.state = json['state'];
        user.originalOperationNumber = json['originalOperationNumber'];
        user.restartLevel = json['restartLevel'];
        user.channel = json['channel'];
        user.compliance = json['compliance'];
        user.client = json['client'];
        return user;
    }
}