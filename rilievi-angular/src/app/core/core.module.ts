import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContentComponent } from './components/content/content.component';
import { MenuComponent } from './components/menu/menu.component';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ApiInterceptorService } from './services/api-interceptor.service';
import { SharedModule } from '../shared/shared.module';
import { AuthInterceptorService } from './services/auth-interceptor.service';


export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptorService, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
];

@NgModule({
  declarations: [HeaderComponent, BodyComponent, SpinnerComponent, FooterComponent, ContentComponent, MenuComponent],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    NgxSpinnerModule,
    RouterModule
  ],
  providers : httpInterceptorProviders
})
export class CoreModule { }
