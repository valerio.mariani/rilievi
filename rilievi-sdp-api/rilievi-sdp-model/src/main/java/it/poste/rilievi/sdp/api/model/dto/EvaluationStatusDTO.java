package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class EvaluationStatusDTO {
	
    private Long id;
	
	private int code;
	
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
