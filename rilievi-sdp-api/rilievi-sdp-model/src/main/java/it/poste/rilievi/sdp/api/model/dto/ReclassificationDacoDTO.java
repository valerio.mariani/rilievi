package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ReclassificationDacoDTO {
	
	private EvaluationDTO source;
	
	private EvaluationDTO generated;

	public EvaluationDTO getSource() {
		return source;
	}

	public void setSource(EvaluationDTO source) {
		this.source = source;
	}

	public EvaluationDTO getGenerated() {
		return generated;
	}

	public void setGenerated(EvaluationDTO generated) {
		this.generated = generated;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	
}
