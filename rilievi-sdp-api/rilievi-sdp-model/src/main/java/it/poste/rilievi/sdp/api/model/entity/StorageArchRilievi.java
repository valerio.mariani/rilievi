package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_arch_rilievi database table.
 * 
 */
@Entity
@Table(name="storage_arch_rilievi")
@NamedQuery(name="StorageArchRilievi.findAll", query="SELECT s FROM StorageArchRilievi s")
public class StorageArchRilievi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RILIEVI")
	private Long idRilievi;

	private byte blocco;

	@Column(name="CODICE_CAUSALE")
	private String codiceCausale;

	@Column(name="CODICE_FILIALE")
	private String codiceFiliale;

	@Column(name="CODICE_STATO_RILIEVO")
	private int codiceStatoRilievo;

	@Column(name="CREDITI_DTV")
	private BigDecimal creditiDtv;

	@Column(name="CREDITI_EURO")
	private BigDecimal creditiEuro;

	@Column(name="CREDITI_EURO_CTV")
	private BigDecimal creditiEuroCtv;

	@Column(name="CREDITI_LIRE")
	private BigDecimal creditiLire;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COGE")
	private Date dataCoge;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_COMPETENZA")
	private Date dataCompetenza;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INSERIMENTO")
	private Date dataInserimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INVIO_SRC")
	private Date dataInvioSrc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOL_CUMUL")
	private Date dataRegolCumul;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REGOLARIZZAZIONE")
	private Date dataRegolarizzazione;

	@Column(name="DEBITI_CTV")
	private BigDecimal debitiCtv;

	@Column(name="DEBITI_EURO")
	private BigDecimal debitiEuro;

	@Column(name="DEBITI_EURO_CTV")
	private BigDecimal debitiEuroCtv;

	@Column(name="DEBITI_LIRE")
	private BigDecimal debitiLire;

	private String decade;

	@Column(name="DESCR_CAUSALE")
	private String descrCausale;

	@Column(name="DESCR_FILIALE")
	private String descrFiliale;

	@Column(name="DESCR_STATO_RILIEVO")
	private String descrStatoRilievo;

	@Column(name="FILIALE_EM")
	private String filialeEm;

	@Column(name="FLAG_REGOLARIZZATO")
	private String flagRegolarizzato;

	@Column(name="FLAG_RIALLINEATO")
	private String flagRiallineato;

	private String motivo;

	private String note;

	@Column(name="NUM_RIL_EM")
	private BigDecimal numRilEm;

	@Column(name="NUM_RILIEVO")
	private int numRilievo;

	@Column(name="REGOLARIZZATO_DA_UTENTE")
	private String regolarizzatoDaUtente;

	@Column(name="STATUS_RIALLINEATO")
	private String statusRiallineato;

	private String tipo;

	@Column(name="TIPO_CHIUSURA")
	private String tipoChiusura;

	private byte trasm;

	@Column(name="VOCE_DACO")
	private String voceDaco;

	//bi-directional many-to-one association to AnagProfili
	//@ManyToOne
	//@JoinColumn(name="RIF_PROFILO")
	//private AnagProfili anagProfili1;

	//bi-directional many-to-one association to AnagProfili
	@ManyToOne
	@JoinColumn(name="RIF_PROFILO")
	private AnagProfili anagProfili2;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVO_PADRE")
	private StorageRilievi storageRilievi1;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVO_FIGLIO")
	private StorageRilievi storageRilievi2;

	//bi-directional many-to-one association to TipologUffici
	@ManyToOne
	@JoinColumn(name="RIF_FRAZIONARIO")
	private TipologUffici tipologUffici;

	public StorageArchRilievi() {
	}

	public Long getIdRilievi() {
		return this.idRilievi;
	}

	public void setIdRilievi(Long idRilievi) {
		this.idRilievi = idRilievi;
	}

	public byte getBlocco() {
		return this.blocco;
	}

	public void setBlocco(byte blocco) {
		this.blocco = blocco;
	}

	public String getCodiceCausale() {
		return this.codiceCausale;
	}

	public void setCodiceCausale(String codiceCausale) {
		this.codiceCausale = codiceCausale;
	}

	public String getCodiceFiliale() {
		return this.codiceFiliale;
	}

	public void setCodiceFiliale(String codiceFiliale) {
		this.codiceFiliale = codiceFiliale;
	}

	public int getCodiceStatoRilievo() {
		return this.codiceStatoRilievo;
	}

	public void setCodiceStatoRilievo(int codiceStatoRilievo) {
		this.codiceStatoRilievo = codiceStatoRilievo;
	}

	public BigDecimal getCreditiDtv() {
		return this.creditiDtv;
	}

	public void setCreditiDtv(BigDecimal creditiDtv) {
		this.creditiDtv = creditiDtv;
	}

	public BigDecimal getCreditiEuro() {
		return this.creditiEuro;
	}

	public void setCreditiEuro(BigDecimal creditiEuro) {
		this.creditiEuro = creditiEuro;
	}

	public BigDecimal getCreditiEuroCtv() {
		return this.creditiEuroCtv;
	}

	public void setCreditiEuroCtv(BigDecimal creditiEuroCtv) {
		this.creditiEuroCtv = creditiEuroCtv;
	}

	public BigDecimal getCreditiLire() {
		return this.creditiLire;
	}

	public void setCreditiLire(BigDecimal creditiLire) {
		this.creditiLire = creditiLire;
	}

	public Date getDataCoge() {
		return this.dataCoge;
	}

	public void setDataCoge(Date dataCoge) {
		this.dataCoge = dataCoge;
	}

	public Date getDataCompetenza() {
		return this.dataCompetenza;
	}

	public void setDataCompetenza(Date dataCompetenza) {
		this.dataCompetenza = dataCompetenza;
	}

	public Date getDataInserimento() {
		return this.dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataInvioSrc() {
		return this.dataInvioSrc;
	}

	public void setDataInvioSrc(Date dataInvioSrc) {
		this.dataInvioSrc = dataInvioSrc;
	}

	public Date getDataRegolCumul() {
		return this.dataRegolCumul;
	}

	public void setDataRegolCumul(Date dataRegolCumul) {
		this.dataRegolCumul = dataRegolCumul;
	}

	public Date getDataRegolarizzazione() {
		return this.dataRegolarizzazione;
	}

	public void setDataRegolarizzazione(Date dataRegolarizzazione) {
		this.dataRegolarizzazione = dataRegolarizzazione;
	}

	public BigDecimal getDebitiCtv() {
		return this.debitiCtv;
	}

	public void setDebitiCtv(BigDecimal debitiCtv) {
		this.debitiCtv = debitiCtv;
	}

	public BigDecimal getDebitiEuro() {
		return this.debitiEuro;
	}

	public void setDebitiEuro(BigDecimal debitiEuro) {
		this.debitiEuro = debitiEuro;
	}

	public BigDecimal getDebitiEuroCtv() {
		return this.debitiEuroCtv;
	}

	public void setDebitiEuroCtv(BigDecimal debitiEuroCtv) {
		this.debitiEuroCtv = debitiEuroCtv;
	}

	public BigDecimal getDebitiLire() {
		return this.debitiLire;
	}

	public void setDebitiLire(BigDecimal debitiLire) {
		this.debitiLire = debitiLire;
	}

	public String getDecade() {
		return this.decade;
	}

	public void setDecade(String decade) {
		this.decade = decade;
	}

	public String getDescrCausale() {
		return this.descrCausale;
	}

	public void setDescrCausale(String descrCausale) {
		this.descrCausale = descrCausale;
	}

	public String getDescrFiliale() {
		return this.descrFiliale;
	}

	public void setDescrFiliale(String descrFiliale) {
		this.descrFiliale = descrFiliale;
	}

	public String getDescrStatoRilievo() {
		return this.descrStatoRilievo;
	}

	public void setDescrStatoRilievo(String descrStatoRilievo) {
		this.descrStatoRilievo = descrStatoRilievo;
	}

	public String getFilialeEm() {
		return this.filialeEm;
	}

	public void setFilialeEm(String filialeEm) {
		this.filialeEm = filialeEm;
	}

	public String getFlagRegolarizzato() {
		return this.flagRegolarizzato;
	}

	public void setFlagRegolarizzato(String flagRegolarizzato) {
		this.flagRegolarizzato = flagRegolarizzato;
	}

	public String getFlagRiallineato() {
		return this.flagRiallineato;
	}

	public void setFlagRiallineato(String flagRiallineato) {
		this.flagRiallineato = flagRiallineato;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public BigDecimal getNumRilEm() {
		return this.numRilEm;
	}

	public void setNumRilEm(BigDecimal numRilEm) {
		this.numRilEm = numRilEm;
	}

	public int getNumRilievo() {
		return this.numRilievo;
	}

	public void setNumRilievo(int numRilievo) {
		this.numRilievo = numRilievo;
	}

	public String getRegolarizzatoDaUtente() {
		return this.regolarizzatoDaUtente;
	}

	public void setRegolarizzatoDaUtente(String regolarizzatoDaUtente) {
		this.regolarizzatoDaUtente = regolarizzatoDaUtente;
	}

	public String getStatusRiallineato() {
		return this.statusRiallineato;
	}

	public void setStatusRiallineato(String statusRiallineato) {
		this.statusRiallineato = statusRiallineato;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoChiusura() {
		return this.tipoChiusura;
	}

	public void setTipoChiusura(String tipoChiusura) {
		this.tipoChiusura = tipoChiusura;
	}

	public byte getTrasm() {
		return this.trasm;
	}

	public void setTrasm(byte trasm) {
		this.trasm = trasm;
	}

	public String getVoceDaco() {
		return this.voceDaco;
	}

	public void setVoceDaco(String voceDaco) {
		this.voceDaco = voceDaco;
	}

/*	public AnagProfili getAnagProfili1() {
		return this.anagProfili1;
	}

	public void setAnagProfili1(AnagProfili anagProfili1) {
		this.anagProfili1 = anagProfili1;
	}*/

	public AnagProfili getAnagProfili2() {
		return this.anagProfili2;
	}

	public void setAnagProfili2(AnagProfili anagProfili2) {
		this.anagProfili2 = anagProfili2;
	}

	public StorageRilievi getStorageRilievi1() {
		return this.storageRilievi1;
	}

	public void setStorageRilievi1(StorageRilievi storageRilievi1) {
		this.storageRilievi1 = storageRilievi1;
	}

	public StorageRilievi getStorageRilievi2() {
		return this.storageRilievi2;
	}

	public void setStorageRilievi2(StorageRilievi storageRilievi2) {
		this.storageRilievi2 = storageRilievi2;
	}

	public TipologUffici getTipologUffici() {
		return this.tipologUffici;
	}

	public void setTipologUffici(TipologUffici tipologUffici) {
		this.tipologUffici = tipologUffici;
	}

}