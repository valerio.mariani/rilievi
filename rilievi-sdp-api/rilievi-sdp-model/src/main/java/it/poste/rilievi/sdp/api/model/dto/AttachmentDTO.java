package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AttachmentDTO {
	
	private Long id;
	
	private String name;
	
	private long size;
	
	private String content;
	
	private String mimeType;
	
	private AppUserDTO insertUser;
	
	private LocalDateTime insertDate;
	
	private LocalDateTime deleteDate;
	
	private boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public AppUserDTO getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(AppUserDTO insertUser) {
		this.insertUser = insertUser;
	}

	public LocalDateTime getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}

	public LocalDateTime getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(LocalDateTime deleteDate) {
		this.deleteDate = deleteDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
