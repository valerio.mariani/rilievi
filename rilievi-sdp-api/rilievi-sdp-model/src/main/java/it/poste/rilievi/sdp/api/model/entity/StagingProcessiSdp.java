package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the staging_processi_sdp database table.
 * 
 */
@Entity
@Table(name="staging_processi_sdp")
@NamedQuery(name="StagingProcessiSdp.findAll", query="SELECT s FROM StagingProcessiSdp s")
public class StagingProcessiSdp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PROC_SDP")
	private Long idProcSdp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CONTABILE")
	private Date dataContabile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INTERVENTO")
	private Date dataIntervento;

	@Column(name="FLAG_INTERVENTO_COMPLETO")
	private String flagInterventoCompleto;

	private String frazionario;

	@Column(name="LIV_RESTART")
	private int livRestart;

	@Column(name="NUM_OPERAZIONE")
	private int numOperazione;

	@Column(name="NUM_SPORTELLO")
	private int numSportello;

	@Column(name="UTENTE_SDP")
	private String utenteSdp;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	public StagingProcessiSdp() {
	}

	public Long getIdProcSdp() {
		return this.idProcSdp;
	}

	public void setIdProcSdp(Long idProcSdp) {
		this.idProcSdp = idProcSdp;
	}

	public Date getDataContabile() {
		return this.dataContabile;
	}

	public void setDataContabile(Date dataContabile) {
		this.dataContabile = dataContabile;
	}

	public Date getDataIntervento() {
		return this.dataIntervento;
	}

	public void setDataIntervento(Date dataIntervento) {
		this.dataIntervento = dataIntervento;
	}

	public String getFlagInterventoCompleto() {
		return this.flagInterventoCompleto;
	}

	public void setFlagInterventoCompleto(String flagInterventoCompleto) {
		this.flagInterventoCompleto = flagInterventoCompleto;
	}

	public String getFrazionario() {
		return this.frazionario;
	}

	public void setFrazionario(String frazionario) {
		this.frazionario = frazionario;
	}

	public int getLivRestart() {
		return this.livRestart;
	}

	public void setLivRestart(int livRestart) {
		this.livRestart = livRestart;
	}

	public int getNumOperazione() {
		return this.numOperazione;
	}

	public void setNumOperazione(int numOperazione) {
		this.numOperazione = numOperazione;
	}

	public int getNumSportello() {
		return this.numSportello;
	}

	public void setNumSportello(int numSportello) {
		this.numSportello = numSportello;
	}

	public String getUtenteSdp() {
		return this.utenteSdp;
	}

	public void setUtenteSdp(String utenteSdp) {
		this.utenteSdp = utenteSdp;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

}