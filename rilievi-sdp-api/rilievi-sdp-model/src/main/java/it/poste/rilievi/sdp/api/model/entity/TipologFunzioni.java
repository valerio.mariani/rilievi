package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tipolog_funzioni database table.
 * 
 */
@Entity
@Table(name="tipolog_funzioni")
//@NamedQuery(name="TipologFunzioni.findAll", query="SELECT t FROM TipologFunzioni t")
@NamedQueries(value = {	
		@NamedQuery(name=TipologFunzioni.GetAllFunction,query="SELECT t FROM TipologFunzioni t"),
		@NamedQuery(name=TipologFunzioni.findFunctionProfile, query="SELECT t FROM TipologFunzioni t WHERE idFunzione = ANY (SELECT tipologFunzioni FROM AnagProfiliAbilitazione WHERE anagProfili.idProfilo = :profile )"),
		@NamedQuery(name=TipologFunzioni.findOtherFunctionProfile, query="SELECT t FROM TipologFunzioni t WHERE idFunzione <> ALL (SELECT tipologFunzioni FROM AnagProfiliAbilitazione WHERE anagProfili.idProfilo = :profile )"),
		@NamedQuery(name=TipologFunzioni.checkExistFunctionID, query="SELECT a FROM TipologFunzioni a where a.idFunzione = :functionID"),
		@NamedQuery(name=TipologFunzioni.GetFunctionByNames, query="SELECT a FROM TipologFunzioni a where a.nomeFunzione IN ( :names )"),
		@NamedQuery(name=TipologFunzioni.findFunctionProfileCode, query="SELECT t FROM TipologFunzioni t WHERE idFunzione = ANY (SELECT tipologFunzioni FROM AnagProfiliAbilitazione WHERE anagProfili.codiceIam = :profile )")
})

public class TipologFunzioni implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GetAllFunction = "TipologFunzioni.getAllFunction";
	public static final String findFunctionProfile= "TipologFunzioni.findFunctionProfile";
	public static final String findFunctionProfileCode= "TipologFunzioni.findFunctionProfileCode";
	public static final String findOtherFunctionProfile= "TipologFunzioni.findOtherFunctionProfile";
	public static final String checkExistFunctionID = "TipologFunzioni.checkExistFunctionID";
	public static final String GetFunctionByNames= "TipologFunzioni.GetFunctionByNames";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_FUNZIONE")
	private Long idFunzione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione;

	@Column(name="DESCR_FUNZIONE")
	private String descrFunzione;

	@Column(name="NOME_FUNZIONE")
	private String nomeFunzione;

	//bi-directional many-to-one association to AnagProfiliAbilitazione
	@OneToMany(mappedBy="tipologFunzioni")
	private List<AnagProfiliAbilitazione> anagProfiliAbilitaziones;

	//bi-directional many-to-one association to TipologParamFunzioni
	@OneToMany(mappedBy="tipologFunzioni")
	private List<TipologParamFunzioni> tipologParamFunzionis;

	public TipologFunzioni() {
	}

	public Long getIdFunzione() {
		return this.idFunzione;
	}

	public void setIdFunzione(Long idFunzione) {
		this.idFunzione = idFunzione;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getDescrFunzione() {
		return this.descrFunzione;
	}

	public void setDescrFunzione(String descrFunzione) {
		this.descrFunzione = descrFunzione;
	}

	public String getNomeFunzione() {
		return this.nomeFunzione;
	}

	public void setNomeFunzione(String nomeFunzione) {
		this.nomeFunzione = nomeFunzione;
	}

	public List<AnagProfiliAbilitazione> getAnagProfiliAbilitaziones() {
		return this.anagProfiliAbilitaziones;
	}

	public void setAnagProfiliAbilitaziones(List<AnagProfiliAbilitazione> anagProfiliAbilitaziones) {
		this.anagProfiliAbilitaziones = anagProfiliAbilitaziones;
	}

	public AnagProfiliAbilitazione addAnagProfiliAbilitazione(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		getAnagProfiliAbilitaziones().add(anagProfiliAbilitazione);
		anagProfiliAbilitazione.setTipologFunzioni(this);

		return anagProfiliAbilitazione;
	}

	public AnagProfiliAbilitazione removeAnagProfiliAbilitazione(AnagProfiliAbilitazione anagProfiliAbilitazione) {
		getAnagProfiliAbilitaziones().remove(anagProfiliAbilitazione);
		anagProfiliAbilitazione.setTipologFunzioni(null);

		return anagProfiliAbilitazione;
	}

	public List<TipologParamFunzioni> getTipologParamFunzionis() {
		return this.tipologParamFunzionis;
	}

	public void setTipologParamFunzionis(List<TipologParamFunzioni> tipologParamFunzionis) {
		this.tipologParamFunzionis = tipologParamFunzionis;
	}

	public TipologParamFunzioni addTipologParamFunzioni(TipologParamFunzioni tipologParamFunzioni) {
		getTipologParamFunzionis().add(tipologParamFunzioni);
		tipologParamFunzioni.setTipologFunzioni(this);

		return tipologParamFunzioni;
	}

	public TipologParamFunzioni removeTipologParamFunzioni(TipologParamFunzioni tipologParamFunzioni) {
		getTipologParamFunzionis().remove(tipologParamFunzioni);
		tipologParamFunzioni.setTipologFunzioni(null);

		return tipologParamFunzioni;
	}

}