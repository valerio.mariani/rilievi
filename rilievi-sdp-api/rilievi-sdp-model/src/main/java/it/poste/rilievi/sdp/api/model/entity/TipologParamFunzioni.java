package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tipolog_param_funzioni database table.
 * 
 */
@Entity
@Table(name="tipolog_param_funzioni")
@NamedQueries(value = {	
		     @NamedQuery(name="TipologParamFunzioni.findAll", query="SELECT t FROM TipologParamFunzioni t"),
		     @NamedQuery(name=TipologParamFunzioni.GetAllFunctionParamById, query="SELECT t "
		     																		+ "FROM TipologParamFunzioni t"
		     																			+ " WHERE t.tipologFunzioni.idFunzione= :id "),
		     })

public class TipologParamFunzioni implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String GetAllFunctionParamById = "TipologParamFunzioni.getAllFunctionParamById";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PARAMETRO")
	private Long idParametro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione;

	@Column(name="DESCR_PARAMETRO")
	private String descrParametro;

	private String parametro;

	private String valore;

	//bi-directional many-to-one association to TipologFunzioni
	@ManyToOne
	@JoinColumn(name="RIF_FUNZIONE")
	private TipologFunzioni tipologFunzioni;

	public TipologParamFunzioni() {
	}

	public Long getIdParametro() {
		return this.idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getDescrParametro() {
		return this.descrParametro;
	}

	public void setDescrParametro(String descrParametro) {
		this.descrParametro = descrParametro;
	}

	public String getParametro() {
		return this.parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getValore() {
		return this.valore;
	}

	public void setValore(String valore) {
		this.valore = valore;
	}

	public TipologFunzioni getTipologFunzioni() {
		return this.tipologFunzioni;
	}

	public void setTipologFunzioni(TipologFunzioni tipologFunzioni) {
		this.tipologFunzioni = tipologFunzioni;
	}

}