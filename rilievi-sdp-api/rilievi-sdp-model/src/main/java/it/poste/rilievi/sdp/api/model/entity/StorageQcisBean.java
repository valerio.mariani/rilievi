package it.poste.rilievi.sdp.api.model.entity;

import java.math.BigDecimal;

public class StorageQcisBean {
	
	private int section;
	private String operationCode;
	private String reasonGiving;
	private String descriptionGiving;
	private String reasonToHave;
	private String descriptionToHave;
	private BigDecimal numberGivingMovementsEuro;
	private BigDecimal amountGivingMovementsEuro;
	private BigDecimal numberMovementsToHaveEuro;
	private BigDecimal amountMovementsToHaveEuro;
	private String numberGivingMovementsLire;
	private BigDecimal amountGivingMovementsLire;
	private String numberMovementsToHaveLire;
	private BigDecimal amountMovementsToHaveLire;
	private String operationNumber;
	private String rifEvaluation;
	
	public int getSection() {
		return section;
	}
	public void setSection(int section) {
		this.section = section;
	}
	public String getOperationCode() {
		return operationCode;
	}
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}
	public String getReasonGiving() {
		return reasonGiving;
	}
	public void setReasonGiving(String reasonGiving) {
		this.reasonGiving = reasonGiving;
	}
	public String getDescriptionGiving() {
		return descriptionGiving;
	}
	public void setDescriptionGiving(String descriptionGiving) {
		this.descriptionGiving = descriptionGiving;
	}
	
	public String getReasonToHave() {
		return reasonToHave;
	}
	public void setReasonToHave(String reasonToHave) {
		this.reasonToHave = reasonToHave;
	}
	public String getDescriptionToHave() {
		return descriptionToHave;
	}
	public void setDescriptionToHave(String descriptionToHave) {
		this.descriptionToHave = descriptionToHave;
	}
	public BigDecimal getNumberGivingMovementsEuro() {
		return numberGivingMovementsEuro;
	}
	public void setNumberGivingMovementsEuro(BigDecimal numberGivingMovementsEuro) {
		this.numberGivingMovementsEuro = numberGivingMovementsEuro;
	}
	public BigDecimal getAmountGivingMovementsEuro() {
		return amountGivingMovementsEuro;
	}
	public void setAmountGivingMovementsEuro(BigDecimal amountGivingMovementsEuro) {
		this.amountGivingMovementsEuro = amountGivingMovementsEuro;
	}
	public BigDecimal getNumberMovementsToHaveEuro() {
		return numberMovementsToHaveEuro;
	}
	public void setNumberMovementsToHaveEuro(BigDecimal numberMovementsToHaveEuro) {
		this.numberMovementsToHaveEuro = numberMovementsToHaveEuro;
	}
	public BigDecimal getAmountMovementsToHaveEuro() {
		return amountMovementsToHaveEuro;
	}
	public void setAmountMovementsToHaveEuro(BigDecimal amountMovementsToHaveEuro) {
		this.amountMovementsToHaveEuro = amountMovementsToHaveEuro;
	}
	public String getNumberGivingMovementsLire() {
		return numberGivingMovementsLire;
	}
	public void setNumberGivingMovementsLire(String numberGivingMovementsLire) {
		this.numberGivingMovementsLire = numberGivingMovementsLire;
	}
	public BigDecimal getAmountGivingMovementsLire() {
		return amountGivingMovementsLire;
	}
	public void setAmountGivingMovementsLire(BigDecimal amountGivingMovementsLire) {
		this.amountGivingMovementsLire = amountGivingMovementsLire;
	}
	public String getNumberMovementsToHaveLire() {
		return numberMovementsToHaveLire;
	}
	public void setNumberMovementsToHaveLire(String numberMovementsToHaveLire) {
		this.numberMovementsToHaveLire = numberMovementsToHaveLire;
	}
	public BigDecimal getAmountMovementsToHaveLire() {
		return amountMovementsToHaveLire;
	}
	public void setAmountMovementsToHaveLire(BigDecimal amountMovementsToHaveLire) {
		this.amountMovementsToHaveLire = amountMovementsToHaveLire;
	}
	public String getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getRifEvaluation() {
		return rifEvaluation;
	}
	public void setRifEvaluation(String rifEvaluation) {
		this.rifEvaluation = rifEvaluation;
	}
}
