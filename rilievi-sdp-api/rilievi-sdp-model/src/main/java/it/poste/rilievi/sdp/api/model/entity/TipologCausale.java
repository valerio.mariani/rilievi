package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_causale database table.
 * 
 */
@Entity
@Table(name="tipolog_causale")
@NamedQuery(name="TipologCausale.getAllReason", query="SELECT t FROM TipologCausale t")

public class TipologCausale implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QueryGetAllReason = "TipologCausale.getAllReason";
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CAUSALE")
	private Long idCausale;

	@Column(name="CODICE_CAUSALE")
	private String codiceCausale;

	@Column(name="DESCR_CAUSALE")
	private String descrCausale;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologCausale1")
	private List<StorageRilievi> storageRilievis1;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologCausale2")
	private List<StorageRilievi> storageRilievis2;

	//bi-directional many-to-one association to StorageRilieviMaxiDettaglio
	@OneToMany(mappedBy="tipologCausale")
	private List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios;

	//bi-directional many-to-one association to StorageRilieviMaxiTotale
	@OneToMany(mappedBy="tipologCausale")
	private List<StorageRilieviMaxiTotale> storageRilieviMaxiTotales;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologCausale1")
	private List<StagingAggRilievi> stagingAggRilievis1;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologCausale2")
	private List<StagingAggRilievi> stagingAggRilievis2;

	public TipologCausale() {
	}

	public Long getIdCausale() {
		return this.idCausale;
	}

	public void setIdCausale(Long idCausale) {
		this.idCausale = idCausale;
	}

	public String getCodiceCausale() {
		return this.codiceCausale;
	}

	public void setCodiceCausale(String codiceCausale) {
		this.codiceCausale = codiceCausale;
	}

	public String getDescrCausale() {
		return this.descrCausale;
	}

	public void setDescrCausale(String descrCausale) {
		this.descrCausale = descrCausale;
	}

	public List<StorageRilievi> getStorageRilievis1() {
		return this.storageRilievis1;
	}

	public void setStorageRilievis1(List<StorageRilievi> storageRilievis1) {
		this.storageRilievis1 = storageRilievis1;
	}

	public StorageRilievi addStorageRilievis1(StorageRilievi storageRilievis1) {
		getStorageRilievis1().add(storageRilievis1);
		storageRilievis1.setTipologCausale1(this);

		return storageRilievis1;
	}

	public StorageRilievi removeStorageRilievis1(StorageRilievi storageRilievis1) {
		getStorageRilievis1().remove(storageRilievis1);
		storageRilievis1.setTipologCausale1(null);

		return storageRilievis1;
	}

	public List<StorageRilievi> getStorageRilievis2() {
		return this.storageRilievis2;
	}

	public void setStorageRilievis2(List<StorageRilievi> storageRilievis2) {
		this.storageRilievis2 = storageRilievis2;
	}

	public StorageRilievi addStorageRilievis2(StorageRilievi storageRilievis2) {
		getStorageRilievis2().add(storageRilievis2);
		storageRilievis2.setTipologCausale2(this);

		return storageRilievis2;
	}

	public StorageRilievi removeStorageRilievis2(StorageRilievi storageRilievis2) {
		getStorageRilievis2().remove(storageRilievis2);
		storageRilievis2.setTipologCausale2(null);

		return storageRilievis2;
	}

	public List<StorageRilieviMaxiDettaglio> getStorageRilieviMaxiDettaglios() {
		return this.storageRilieviMaxiDettaglios;
	}

	public void setStorageRilieviMaxiDettaglios(List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios) {
		this.storageRilieviMaxiDettaglios = storageRilieviMaxiDettaglios;
	}

	public StorageRilieviMaxiDettaglio addStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().add(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologCausale(this);

		return storageRilieviMaxiDettaglio;
	}

	public StorageRilieviMaxiDettaglio removeStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().remove(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologCausale(null);

		return storageRilieviMaxiDettaglio;
	}

	public List<StorageRilieviMaxiTotale> getStorageRilieviMaxiTotales() {
		return this.storageRilieviMaxiTotales;
	}

	public void setStorageRilieviMaxiTotales(List<StorageRilieviMaxiTotale> storageRilieviMaxiTotales) {
		this.storageRilieviMaxiTotales = storageRilieviMaxiTotales;
	}

	public StorageRilieviMaxiTotale addStorageRilieviMaxiTotale(StorageRilieviMaxiTotale storageRilieviMaxiTotale) {
		getStorageRilieviMaxiTotales().add(storageRilieviMaxiTotale);
		storageRilieviMaxiTotale.setTipologCausale(this);

		return storageRilieviMaxiTotale;
	}

	public StorageRilieviMaxiTotale removeStorageRilieviMaxiTotale(StorageRilieviMaxiTotale storageRilieviMaxiTotale) {
		getStorageRilieviMaxiTotales().remove(storageRilieviMaxiTotale);
		storageRilieviMaxiTotale.setTipologCausale(null);

		return storageRilieviMaxiTotale;
	}

	public List<StagingAggRilievi> getStagingAggRilievis1() {
		return this.stagingAggRilievis1;
	}

	public void setStagingAggRilievis1(List<StagingAggRilievi> stagingAggRilievis1) {
		this.stagingAggRilievis1 = stagingAggRilievis1;
	}

	public StagingAggRilievi addStagingAggRilievis1(StagingAggRilievi stagingAggRilievis1) {
		getStagingAggRilievis1().add(stagingAggRilievis1);
		stagingAggRilievis1.setTipologCausale1(this);

		return stagingAggRilievis1;
	}

	public StagingAggRilievi removeStagingAggRilievis1(StagingAggRilievi stagingAggRilievis1) {
		getStagingAggRilievis1().remove(stagingAggRilievis1);
		stagingAggRilievis1.setTipologCausale1(null);

		return stagingAggRilievis1;
	}

	public List<StagingAggRilievi> getStagingAggRilievis2() {
		return this.stagingAggRilievis2;
	}

	public void setStagingAggRilievis2(List<StagingAggRilievi> stagingAggRilievis2) {
		this.stagingAggRilievis2 = stagingAggRilievis2;
	}

	public StagingAggRilievi addStagingAggRilievis2(StagingAggRilievi stagingAggRilievis2) {
		getStagingAggRilievis2().add(stagingAggRilievis2);
		stagingAggRilievis2.setTipologCausale2(this);

		return stagingAggRilievis2;
	}

	public StagingAggRilievi removeStagingAggRilievis2(StagingAggRilievi stagingAggRilievis2) {
		getStagingAggRilievis2().remove(stagingAggRilievis2);
		stagingAggRilievis2.setTipologCausale2(null);

		return stagingAggRilievis2;
	}

}