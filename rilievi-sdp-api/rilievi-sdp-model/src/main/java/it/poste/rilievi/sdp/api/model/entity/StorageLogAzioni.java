package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_log_azioni database table.
 * 
 */
@Entity
@Table(name="storage_log_azioni")
@NamedQueries(value = {	
		@NamedQuery(name=StorageLogAzioni.findAll, query="SELECT s FROM StorageLogAzioni s")
})

public class StorageLogAzioni implements Serializable {
	public static final String findAll = "StorageLogAzioni.findAll";
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_AZIONE")
	private Long idAzione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_AZIONE")
	private Date dataAzione;

	@Column(name="DESCR_AZIONE")
	private String descrAzione;

	@Column(name="ESITO_AZIONE")
	private String esitoAzione;

	private String utente;

	//bi-directional many-to-one association to TipologTipoAzioni
	@ManyToOne
	@JoinColumn(name="RIF_TIPO_AZIONE")
	private TipologTipoAzioni tipologTipoAzioni;

	public StorageLogAzioni() {
	}

	public Long getIdAzione() {
		return this.idAzione;
	}

	public void setIdAzione(Long idAzione) {
		this.idAzione = idAzione;
	}

	public Date getDataAzione() {
		return this.dataAzione;
	}

	public void setDataAzione(Date dataAzione) {
		this.dataAzione = dataAzione;
	}

	public String getDescrAzione() {
		return this.descrAzione;
	}

	public void setDescrAzione(String descrAzione) {
		this.descrAzione = descrAzione;
	}

	public String getEsitoAzione() {
		return this.esitoAzione;
	}

	public void setEsitoAzione(String esitoAzione) {
		this.esitoAzione = esitoAzione;
	}

	public String getUtente() {
		return this.utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public TipologTipoAzioni getTipologTipoAzioni() {
		return this.tipologTipoAzioni;
	}

	public void setTipologTipoAzioni(TipologTipoAzioni tipologTipoAzioni) {
		this.tipologTipoAzioni = tipologTipoAzioni;
	}

}