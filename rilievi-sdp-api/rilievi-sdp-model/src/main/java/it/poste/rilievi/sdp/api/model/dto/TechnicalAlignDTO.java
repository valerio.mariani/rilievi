package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class TechnicalAlignDTO {

	private String channel;

	private String node;

	private Integer desk;
	
	private AccountingDate accountingDate;
	
	private Transaction lastTransaction;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public Integer getDesk() {
		return desk;
	}

	public void setDesk(Integer desk) {
		this.desk = desk;
	}

	public AccountingDate getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(AccountingDate accountingDate) {
		this.accountingDate = accountingDate;
	}

	public Transaction getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(Transaction lastTransaction) {
		this.lastTransaction = lastTransaction;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
