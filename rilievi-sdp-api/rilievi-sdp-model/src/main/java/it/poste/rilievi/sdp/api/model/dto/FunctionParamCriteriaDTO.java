package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDate;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class FunctionParamCriteriaDTO extends PaginableSearchCriteria {
	private Long id;
	private LocalDate dateCreation;
	private String description;
	private String nameParam;
	private String value; 
	private FunctionDTO function;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id= id;
	}
	public LocalDate getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNameparam() {
		return nameParam;
	}
	public void setNameparam(String nameParam) {
		this.nameParam = nameParam;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public FunctionDTO getFunction() {
		return function;
	}
	public void setFunction(FunctionDTO function) {
		this.function = function;
	}
	
	
	
	@Override
	void populateMapDtoEntity() {
		
		getFieldsMap().put("id","idParametro");
		getFieldsMap().put("nameParam","parametro");
		getFieldsMap().put("description","descrParametro");
		getFieldsMap().put("value","valore");
		
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
