package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class BranchDTO {
	
	private Long id;
	
	private String code;
	
	private String description;
	
	private LocalDate startDate;
	
	private LocalDate endDate;
	
	private List<OfficeDTO> office;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<OfficeDTO> getOffice() {
		if( office == null ) {
			 office = new ArrayList<OfficeDTO>();
		}
		return office;
	}

	public void setOffice(List<OfficeDTO> office) {
		this.office = office;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
