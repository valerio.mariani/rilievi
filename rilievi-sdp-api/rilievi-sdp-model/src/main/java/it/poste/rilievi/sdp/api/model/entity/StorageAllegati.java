package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the storage_allegati database table.
 * 
 */
@Entity
@Table(name="storage_allegati")
@NamedQueries(value = {	
			@NamedQuery(name="StorageAllegati.findAll", query="SELECT s FROM StorageAllegati s"),
			@NamedQuery(name=StorageAllegati.findAttachmentById, query="SELECT s FROM StorageAllegati s WHERE storageRilievi.idRilievi = :evaluationId AND s.attivo = '" + StorageAllegati.ACTIVE + "'"),
			@NamedQuery(name=StorageAllegati.countAttachmentById, query="SELECT count(*) FROM StorageAllegati s WHERE storageRilievi.idRilievi = :evaluationId AND s.attivo = '" + StorageAllegati.ACTIVE + "'"),
			@NamedQuery(name=StorageAllegati.findAttachmentByNameAndID, query="SELECT s FROM StorageAllegati s WHERE nomeAllegato = :name AND storageRilievi.idRilievi = :evaluationID")

})
public class StorageAllegati implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String findAttachmentById="StorageAllegati.findAttachmentById";
	public static final String countAttachmentById="StorageAllegati.countAttachmentById";
	public static final String findAttachmentByNameAndID="StorageAllegati.findAttachmentByNameAndID";
	
	public static final String ACTIVE = "1";
	public static final String NOT_ACTIVE = "0";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ALLEGATI")
	private Long idAllegati;

	private String attivo;

	@Lob
	private byte[] contenuto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INSERIMENTO")
	private Date dataInserimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_RIMOZIONE")
	private Date dataRimozione;

	@Column(name="NOME_ALLEGATO")
	private String nomeAllegato;

	@Column(name="UTENTE_INSERIMENTO")
	private String utenteInserimento;

	@Column(name="UTENTE_RIMOZIONE")
	private String utenteRimozione;

	//bi-directional many-to-one association to StorageRilievi
	@ManyToOne
	@JoinColumn(name="RIF_RILIEVI")
	private StorageRilievi storageRilievi;

	public StorageAllegati() {
	}

	public Long getIdAllegati() {
		return this.idAllegati;
	}

	public void setIdAllegati(Long idAllegati) {
		this.idAllegati = idAllegati;
	}

	public String getAttivo() {
		return this.attivo;
	}

	public void setAttivo(String attivo) {
		this.attivo = attivo;
	}

	public byte[] getContenuto() {
		return this.contenuto;
	}

	public void setContenuto(byte[] contenuto) {
		this.contenuto = contenuto;
	}

	public Date getDataInserimento() {
		return this.dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataRimozione() {
		return this.dataRimozione;
	}

	public void setDataRimozione(Date dataRimozione) {
		this.dataRimozione = dataRimozione;
	}

	public String getNomeAllegato() {
		return this.nomeAllegato;
	}

	public void setNomeAllegato(String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}

	public String getUtenteInserimento() {
		return this.utenteInserimento;
	}

	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}

	public String getUtenteRimozione() {
		return this.utenteRimozione;
	}

	public void setUtenteRimozione(String utenteRimozione) {
		this.utenteRimozione = utenteRimozione;
	}

	public StorageRilievi getStorageRilievi() {
		return this.storageRilievi;
	}

	public void setStorageRilievi(StorageRilievi storageRilievi) {
		this.storageRilievi = storageRilievi;
	}

}