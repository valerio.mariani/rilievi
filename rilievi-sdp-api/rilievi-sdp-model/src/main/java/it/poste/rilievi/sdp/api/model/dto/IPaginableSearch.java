package it.poste.rilievi.sdp.api.model.dto;

public interface IPaginableSearch {

	public Integer getPageNumber();
	
	public void setPageNumber(Integer pageNumber);
	
	public Integer getPageSize();
	
	public void setPageSize(Integer pageSize);
	
	public Integer getFirstRow();
	
	
}
