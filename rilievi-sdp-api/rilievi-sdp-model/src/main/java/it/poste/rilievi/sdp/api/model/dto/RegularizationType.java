package it.poste.rilievi.sdp.api.model.dto;

public enum RegularizationType {
	
	U,//utente manuale
	S,//per regolarizzazione tramite SDP
	R,// (Rilievi) 
	B,// (Batch) per regolarizzazione tramite mondo Rilievi.

}
