package it.poste.rilievi.sdp.api.model.dto;

public enum FunctionEnum {
	
	INS_EVAL("Inserimento Rilievo"),
    RECLA_DACO("Riclassificazione Daco"),
    DETAIL_EVAL("Dettaglio Rilievo"),
    REPO_EVAL("Report Rilievi"),
    REGO_MAXI_EVAL("Maxi Regolarizzazione Rilievi"),
    INQUIRY_EVAL("Consulta Rilievi"),
    MANAG_ATTACHMENT("Gestione Allegato"), //"Gestione Allegato"    
    READ_ATTACHMENT("Leggi Allegato"),//"Leggi Allegato"    
    MANAG_NOTE("Gestione Note Aggiuntive"),//"Gestione Note Aggiuntive"  
    DELETE_EVAL("Cancellazione Rilievo"),//"Cancellazione Rilievo"  
    SETLOSS_EVAL("Connotazione a perdita"),//"Connotazione a perdita"   
    CHANGE_EVAL_REASON("Trasformazione Causale"),//"Modifica stato Rilievo"   
    CHANGE_EVAL_FRACT("Modifica Frazionario"),//"Modifica Frazionario" 
    AUDIT_LOG("Monitoraggio log di audit"),//"Monitoraggio log di audit"
    MANAG_DACO("Gestione Voci Daco"),//"Gestione Voci Daco"    
    DATES_EXTRACT("Gestione Date Intervalli Estrazione"),//"Gestione Date Intervalli Estrazione"   
    MANAG_PROFILES("Gestione profili utente"),//"Gestione profili utente"    
    MANAG_FUNCTIONS("Gestione funzioni"),//"Gestione funzioni",    
    REG_EVAL("Regolarizzazione Rilievo"),//"Regolarizzazione Rilievo",    
    UNDO_REG_EVAL("Annullo Regolarizzazione Rilievo");

	
	private String description;
	
	FunctionEnum(String descr) {
		this.description = descr;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description =description;
	}
}
