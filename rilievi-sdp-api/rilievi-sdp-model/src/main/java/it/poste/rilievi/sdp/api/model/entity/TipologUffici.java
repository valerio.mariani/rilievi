package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_uffici database table.
 * 
 */
@Entity
@Table(name="tipolog_uffici")
@NamedQueries({
		@NamedQuery(name=TipologUffici.GetBranchOffices, query="SELECT t FROM TipologUffici t where t.tipologFiliali.idFiliale = :rifBranch"),
		@NamedQuery(name=TipologUffici.checkOfficeInBranch, query="SELECT t FROM TipologUffici t where t.idUfficio = :rifOffice AND t.tipologFiliali.idFiliale = :rifBranch"),
		@NamedQuery(name=TipologUffici.GetOfficeByFractional, query="SELECT t FROM TipologUffici t where t.frazionario = :frazionario"),
		

})
public class TipologUffici implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GetBranchOffices = "GetBranchOffices";
	public static final String GetOfficeByFractional = "GetOfficeByFractional";
	public static final String checkOfficeInBranch = "checkOfficeInBranch";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_UFFICIO")
	private Long idUfficio;

	@Column(name="DESCR_FRAZIONARIO")
	private String descrFrazionario;

	private String email;

	private String frazionario;

	private String tipo;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologUffici")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to StorageRilieviMaxiDettaglio
	@OneToMany(mappedBy="tipologUffici")
	private List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios;

	//bi-directional many-to-one association to TipologFiliali
	@ManyToOne
	@JoinColumn(name="RIF_FILIALE")
	private TipologFiliali tipologFiliali;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologUffici")
	private List<StagingAggRilievi> stagingAggRilievis;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologUffici")
	private List<StorageArchRilievi> storageArchRilievis;

	public TipologUffici() {
	}

	public Long getIdUfficio() {
		return this.idUfficio;
	}

	public void setIdUfficio(Long idUfficio) {
		this.idUfficio = idUfficio;
	}

	public String getDescrFrazionario() {
		return this.descrFrazionario;
	}

	public void setDescrFrazionario(String descrFrazionario) {
		this.descrFrazionario = descrFrazionario;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFrazionario() {
		return this.frazionario;
	}

	public void setFrazionario(String frazionario) {
		this.frazionario = frazionario;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setTipologUffici(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setTipologUffici(null);

		return storageRilievi;
	}

	public List<StorageRilieviMaxiDettaglio> getStorageRilieviMaxiDettaglios() {
		return this.storageRilieviMaxiDettaglios;
	}

	public void setStorageRilieviMaxiDettaglios(List<StorageRilieviMaxiDettaglio> storageRilieviMaxiDettaglios) {
		this.storageRilieviMaxiDettaglios = storageRilieviMaxiDettaglios;
	}

	public StorageRilieviMaxiDettaglio addStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().add(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologUffici(this);

		return storageRilieviMaxiDettaglio;
	}

	public StorageRilieviMaxiDettaglio removeStorageRilieviMaxiDettaglio(StorageRilieviMaxiDettaglio storageRilieviMaxiDettaglio) {
		getStorageRilieviMaxiDettaglios().remove(storageRilieviMaxiDettaglio);
		storageRilieviMaxiDettaglio.setTipologUffici(null);

		return storageRilieviMaxiDettaglio;
	}

	public TipologFiliali getTipologFiliali() {
		return this.tipologFiliali;
	}

	public void setTipologFiliali(TipologFiliali tipologFiliali) {
		this.tipologFiliali = tipologFiliali;
	}

	public List<StagingAggRilievi> getStagingAggRilievis() {
		return this.stagingAggRilievis;
	}

	public void setStagingAggRilievis(List<StagingAggRilievi> stagingAggRilievis) {
		this.stagingAggRilievis = stagingAggRilievis;
	}

	public StagingAggRilievi addStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().add(stagingAggRilievi);
		stagingAggRilievi.setTipologUffici(this);

		return stagingAggRilievi;
	}

	public StagingAggRilievi removeStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().remove(stagingAggRilievi);
		stagingAggRilievi.setTipologUffici(null);

		return stagingAggRilievi;
	}

	public List<StorageArchRilievi> getStorageArchRilievis() {
		return this.storageArchRilievis;
	}

	public void setStorageArchRilievis(List<StorageArchRilievi> storageArchRilievis) {
		this.storageArchRilievis = storageArchRilievis;
	}

	public StorageArchRilievi addStorageArchRilievi(StorageArchRilievi storageArchRilievi) {
		getStorageArchRilievis().add(storageArchRilievi);
		storageArchRilievi.setTipologUffici(this);

		return storageArchRilievi;
	}

	public StorageArchRilievi removeStorageArchRilievi(StorageArchRilievi storageArchRilievi) {
		getStorageArchRilievis().remove(storageArchRilievi);
		storageArchRilievi.setTipologUffici(null);

		return storageArchRilievi;
	}

}