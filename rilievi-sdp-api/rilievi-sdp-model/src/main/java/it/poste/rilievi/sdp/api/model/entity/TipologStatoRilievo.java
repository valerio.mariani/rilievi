package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_stato_rilievo database table.
 * 
 */
@Entity
@Table(name="tipolog_stato_rilievo")
@NamedQueries({
	@NamedQuery(name="TipologStatoRilievo.getAllEvaluationStatus", query="SELECT t FROM TipologStatoRilievo t"),
	@NamedQuery(name=TipologStatoRilievo.QueryGetEvaluationStatusByCode, query="SELECT t FROM TipologStatoRilievo t WHERE t.codiceStatoRilievo = :code")
	})

public class TipologStatoRilievo implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QueryGetAllEvaluationStatus = "TipologStatoRilievo.getAllEvaluationStatus";
	public static final String QueryGetEvaluationStatusByCode = "TipologStatoRilievo.QueryGetEvaluationStatusByCode";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_STATO_RILIEVO")
	private Long idStatoRilievo;

	@Column(name="CODICE_STATO_RILIEVO")
	private int codiceStatoRilievo;

	private String descrizione;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologStatoRilievo")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to StagingAggRilievi
	@OneToMany(mappedBy="tipologStatoRilievo")
	private List<StagingAggRilievi> stagingAggRilievis;

	public TipologStatoRilievo() {
	}

	public Long getIdStatoRilievo() {
		return this.idStatoRilievo;
	}

	public void setIdStatoRilievo(Long idStatoRilievo) {
		this.idStatoRilievo = idStatoRilievo;
	}

	public int getCodiceStatoRilievo() {
		return this.codiceStatoRilievo;
	}

	public void setCodiceStatoRilievo(int codiceStatoRilievo) {
		this.codiceStatoRilievo = codiceStatoRilievo;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setTipologStatoRilievo(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setTipologStatoRilievo(null);

		return storageRilievi;
	}

	public List<StagingAggRilievi> getStagingAggRilievis() {
		return this.stagingAggRilievis;
	}

	public void setStagingAggRilievis(List<StagingAggRilievi> stagingAggRilievis) {
		this.stagingAggRilievis = stagingAggRilievis;
	}

	public StagingAggRilievi addStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().add(stagingAggRilievi);
		stagingAggRilievi.setTipologStatoRilievo(this);

		return stagingAggRilievi;
	}

	public StagingAggRilievi removeStagingAggRilievi(StagingAggRilievi stagingAggRilievi) {
		getStagingAggRilievis().remove(stagingAggRilievi);
		stagingAggRilievi.setTipologStatoRilievo(null);

		return stagingAggRilievi;
	}

}