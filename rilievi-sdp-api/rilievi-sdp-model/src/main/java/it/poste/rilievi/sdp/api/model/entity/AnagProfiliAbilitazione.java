package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the anag_profili_abilitazione database table.
 * 
 */
@Entity
@Table(name="anag_profili_abilitazione")
@NamedQueries(value = {	
		@NamedQuery(name="AnagProfiliAbilitazione.findAll", query="SELECT a FROM AnagProfiliAbilitazione a"),
		@NamedQuery(name="AnagProfiliAbilitazione.FindFunctionProfileExist", query="SELECT a FROM AnagProfiliAbilitazione a WHERE anagProfili.idProfilo= :idProfile AND tipologFunzioni.idFunzione= :idFunction")
})
public class AnagProfiliAbilitazione implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FindFunctionProfileExist = "AnagProfiliAbilitazione.FindFunctionProfileExist";
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ANAG_PROFILI_ABILITAZIONE")
	private Long idAnagProfiliAbilitazione;

	//bi-directional many-to-one association to AnagProfili
	@ManyToOne
	@JoinColumn(name="RIF_PROFILO")
	private AnagProfili anagProfili;

	//bi-directional many-to-one association to TipologFunzioni
	@ManyToOne
	@JoinColumn(name="RIF_FUNZIONE")
	private TipologFunzioni tipologFunzioni;

	//bi-directional many-to-one association to TipologUtentiServizi
	@OneToMany(mappedBy="anagProfiliAbilitazione")
	private List<TipologUtentiServizi> tipologUtentiServizis;

	public AnagProfiliAbilitazione() {
	}

	public Long getIdAnagProfiliAbilitazione() {
		return this.idAnagProfiliAbilitazione;
	}

	public void setIdAnagProfiliAbilitazione(Long idAnagProfiliAbilitazione) {
		this.idAnagProfiliAbilitazione = idAnagProfiliAbilitazione;
	}

	public AnagProfili getAnagProfili() {
		return this.anagProfili;
	}

	public void setAnagProfili(AnagProfili anagProfili) {
		this.anagProfili = anagProfili;
	}

	public TipologFunzioni getTipologFunzioni() {
		return this.tipologFunzioni;
	}

	public void setTipologFunzioni(TipologFunzioni tipologFunzioni) {
		this.tipologFunzioni = tipologFunzioni;
	}

	public List<TipologUtentiServizi> getTipologUtentiServizis() {
		return this.tipologUtentiServizis;
	}

	public void setTipologUtentiServizis(List<TipologUtentiServizi> tipologUtentiServizis) {
		this.tipologUtentiServizis = tipologUtentiServizis;
	}

	public TipologUtentiServizi addTipologUtentiServizi(TipologUtentiServizi tipologUtentiServizi) {
		getTipologUtentiServizis().add(tipologUtentiServizi);
		tipologUtentiServizi.setAnagProfiliAbilitazione(this);

		return tipologUtentiServizi;
	}

	public TipologUtentiServizi removeTipologUtentiServizi(TipologUtentiServizi tipologUtentiServizi) {
		getTipologUtentiServizis().remove(tipologUtentiServizi);
		tipologUtentiServizi.setAnagProfiliAbilitazione(null);

		return tipologUtentiServizi;
	}

}