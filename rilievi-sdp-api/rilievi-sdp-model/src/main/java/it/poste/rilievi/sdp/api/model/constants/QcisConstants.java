package it.poste.rilievi.sdp.api.model.constants;

public class QcisConstants {
	
	public static final String ESECUZIONE_CORRETTA = "00";
	public static final String LUNGHEZZA_AREA_PRIMO_LIVELLO = "00155";
	public static final String LUNGHEZZA_AREA_SECONDO_LIVELLO = "00287";
	public static final String NUMERO_AREE_PRIMO_LIVELLO = "001";
	public static final String NUMERO_AREE_SECONDO_LIVELLO = "002";
	public static final String ERRORE_AGENZIA_INESISTENTE = "02";
	public static final String ERRORE_CODICE_OPERAZIONE_INESISTENTE = "03";
	public static final String ERRORE_SEQUENZA_ERRATA = "04";
	public static final String ERRORE_ASSENZA_MOVIMENTI = "05";
	public static final String ERRORE_GENERICO_QCIS = "99";
	public static final String LUNGHEZZA_AREA_ERRORE = "00000";
	public static final String NUMERO_AREE_ERRORE = "000";
	public static final String CODICE_OPERAZIONE_VUOTO = "00000000";
	public static final String CAUSALE = "0000000000";
	public static final String DESCRIZIONE_CAUSALE_PRIMO_LIVELLO = "RILIEVI CONTABILI";
	public static final String DESCRIZIONE_CAUSALE_DEBITO_SECONDO_LIVELLO = "REGOLARIZ. RILIEVI A DEBITO";
	public static final String DESCRIZIONE_CAUSALE_CREDITO_SECONDO_LIVELLO = "REGOLARIZ. RILIEVI A CREDITO";
	public static final String NUMERO_MOVIMENTI_VUOTO = "00000";
	public static final String IMPORTO_MOVIMENTI_VUOTO = "000000000000000";
	public static final String EMPTY_THREE = "   ";
	public static final String EUR = "EUR";
	public static final String HEADER ="NUM.OP NUM.RILIEVO        DIVISA          IMPORTO ORIG.                IMPORTO";
	public static final int LUNGHEZZA_ITEM_QUARTO_LIVELLO = 78;
	public static final int DATI_CONTABILI_MAX_LENGTH_QUARTO_LIVELLO = 303; //(23760 : 78)-1
	public static final String DATA_CONTABILE_PATTERN = "yyyyMMdd";
}
