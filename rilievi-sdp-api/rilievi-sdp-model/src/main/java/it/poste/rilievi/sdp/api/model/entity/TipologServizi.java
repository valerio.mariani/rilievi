package it.poste.rilievi.sdp.api.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipolog_servizi database table.
 * 
 */
@Entity
@Table(name="tipolog_servizi")
@NamedQuery(name=TipologServizi.GetDacoServices, query="SELECT t FROM TipologServizi t JOIN t.abbinamentoVociDacoServizis a JOIN a.tipologDaco where a.tipologDaco.idVoceDaco = :idVoceDaco")
public class TipologServizi implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String GetDacoServices = "GetDacoServices"; 

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_SERVIZIO")
	private Long idServizio;

	@Column(name="CODICE_SERVIZIO")
	private String codiceServizio;

	private String descrizione;

	//bi-directional many-to-one association to StorageRilievi
	@OneToMany(mappedBy="tipologServizi")
	private List<StorageRilievi> storageRilievis;

	//bi-directional many-to-one association to AbbinamentoVociDacoServizi
	@OneToMany(mappedBy="tipologServizi")
	private List<AbbinamentoVociDacoServizi> abbinamentoVociDacoServizis;

	public TipologServizi() {
	}

	public Long getIdServizio() {
		return this.idServizio;
	}

	public void setIdServizio(Long idServizio) {
		this.idServizio = idServizio;
	}

	public String getCodiceServizio() {
		return this.codiceServizio;
	}

	public void setCodiceServizio(String codiceServizio) {
		this.codiceServizio = codiceServizio;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<StorageRilievi> getStorageRilievis() {
		return this.storageRilievis;
	}

	public void setStorageRilievis(List<StorageRilievi> storageRilievis) {
		this.storageRilievis = storageRilievis;
	}

	public StorageRilievi addStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().add(storageRilievi);
		storageRilievi.setTipologServizi(this);

		return storageRilievi;
	}

	public StorageRilievi removeStorageRilievi(StorageRilievi storageRilievi) {
		getStorageRilievis().remove(storageRilievi);
		storageRilievi.setTipologServizi(null);

		return storageRilievi;
	}

	public List<AbbinamentoVociDacoServizi> getAbbinamentoVociDacoServizis() {
		return this.abbinamentoVociDacoServizis;
	}

	public void setAbbinamentoVociDacoServizis(List<AbbinamentoVociDacoServizi> abbinamentoVociDacoServizis) {
		this.abbinamentoVociDacoServizis = abbinamentoVociDacoServizis;
	}

	public AbbinamentoVociDacoServizi addAbbinamentoVociDacoServizi(AbbinamentoVociDacoServizi abbinamentoVociDacoServizi) {
		getAbbinamentoVociDacoServizis().add(abbinamentoVociDacoServizi);
		abbinamentoVociDacoServizi.setTipologServizi(this);

		return abbinamentoVociDacoServizi;
	}

	public AbbinamentoVociDacoServizi removeAbbinamentoVociDacoServizi(AbbinamentoVociDacoServizi abbinamentoVociDacoServizi) {
		getAbbinamentoVociDacoServizis().remove(abbinamentoVociDacoServizi);
		abbinamentoVociDacoServizi.setTipologServizi(null);

		return abbinamentoVociDacoServizi;
	}

}