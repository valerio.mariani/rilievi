package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ExportTableDTO {
	
	
	public enum ExportType {UserDailyEvaluation, Evaluations, MaxiEvaluation}
	
	private String fileName;
	
	private String content;
	
	private String contentType;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
