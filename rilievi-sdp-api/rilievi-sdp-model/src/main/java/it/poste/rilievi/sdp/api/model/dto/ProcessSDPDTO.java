package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDate;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ProcessSDPDTO {
	
	private Long id;
	
	private LocalDate contabilizationDate;

	private OfficeDTO office;

	private int livRestart;

    private String operationNumber; 

    private String originalOperationNumber;

    private String branchNumber;

    private String userSdp;
	
    private Long evaluationId;

    private String phase; 

    private String state; 
   
    private String restartLevel;

    private String channel;

    private String compliance;
 
    private String client;
    
    private String flagValid;
	
	private LocalDate validDate;
	
	private LocalDate operationDate;

	private String flagOperationComplete;
	
	private EvaluationDTO evaluation;
	
	
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public LocalDate getContabilizationDate() {
		return contabilizationDate;
	}



	public void setContabilizationDate(LocalDate contabilizationDate) {
		this.contabilizationDate = contabilizationDate;
	}



	public OfficeDTO getOffice() {
		return office;
	}



	public void setOffice(OfficeDTO office) {
		this.office = office;
	}



	public int getLivRestart() {
		return livRestart;
	}



	public void setLivRestart(int livRestart) {
		this.livRestart = livRestart;
	}



	public String getOperationNumber() {
		return operationNumber;
	}



	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}



	public String getOriginalOperationNumber() {
		return originalOperationNumber;
	}



	public void setOriginalOperationNumber(String originalOperationNumber) {
		this.originalOperationNumber = originalOperationNumber;
	}



	public String getBranchNumber() {
		return branchNumber;
	}



	public void setBranchNumber(String branchNumber) {
		this.branchNumber = branchNumber;
	}



	public String getUserSdp() {
		return userSdp;
	}



	public void setUserSdp(String userSdp) {
		this.userSdp = userSdp;
	}



	public Long getEvaluationId() {
		return evaluationId;
	}



	public void setEvaluationId(Long evaluationId) {
		this.evaluationId = evaluationId;
	}



	public String getPhase() {
		return phase;
	}



	public void setPhase(String phase) {
		this.phase = phase;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getRestartLevel() {
		return restartLevel;
	}



	public void setRestartLevel(String restartLevel) {
		this.restartLevel = restartLevel;
	}



	public String getChannel() {
		return channel;
	}



	public void setChannel(String channel) {
		this.channel = channel;
	}



	public String getCompliance() {
		return compliance;
	}



	public void setCompliance(String compliance) {
		this.compliance = compliance;
	}



	public String getClient() {
		return client;
	}



	public EvaluationDTO getEvaluation() {
		return evaluation;
	}



	public void setEvaluation(EvaluationDTO evaluation) {
		this.evaluation = evaluation;
	}



	public void setClient(String client) {
		this.client = client;
	}



	public String getFlagValid() {
		return flagValid;
	}



	public void setFlagValid(String flagValid) {
		this.flagValid = flagValid;
	}



	public LocalDate getValidDate() {
		return validDate;
	}



	public void setValidDate(LocalDate validDate) {
		this.validDate = validDate;
	}



	public LocalDate getOperationDate() {
		return operationDate;
	}



	public void setOperationDate(LocalDate operationDate) {
		this.operationDate = operationDate;
	}



	public String getFlagOperationComplete() {
		return flagOperationComplete;
	}



	public void setFlagOperationComplete(String flagOperationComplete) {
		this.flagOperationComplete = flagOperationComplete;
	}



	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
