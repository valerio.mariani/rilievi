package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class DacoDTO {

	private Long id;
	
	private String code;
	
	private String description;
	
	private String accountNumber;
	
	private String accountDescription;
	
	private List<DacoServiceDTO> services = new ArrayList<DacoServiceDTO>();
	
	private LocalDate startDate;
	
	private LocalDate endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public List<DacoServiceDTO> getServices() {
		return services;
	}

	public void setServices(List<DacoServiceDTO> services) {
		this.services = services;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	
}
