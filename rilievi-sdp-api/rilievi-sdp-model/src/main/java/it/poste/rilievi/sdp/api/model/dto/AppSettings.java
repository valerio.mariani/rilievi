package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AppSettings  {
	

	private LocalDateTime sysDate;
	
	private ReasonDTO rcReason;
	
	private ReasonDTO rdReason;
	
	private ReasonDTO rsReason;
	
	private ReasonDTO raReason;
	
	private EvaluationStatusDTO unLossState;
	
	private List<FunctionDTO> functions = new ArrayList<FunctionDTO>();
	
	
	public LocalDateTime getSysDate() {
		return sysDate;
	}

	public void setSysDate(LocalDateTime sysDate) {
		this.sysDate = sysDate;
	}

	public ReasonDTO getRcReason() {
		return rcReason;
	}

	public void setRcReason(ReasonDTO rcReason) {
		this.rcReason = rcReason;
	}

	public ReasonDTO getRdReason() {
		return rdReason;
	}

	public void setRdReason(ReasonDTO rdReason) {
		this.rdReason = rdReason;
	}

	public ReasonDTO getRsReason() {
		return rsReason;
	}

	public void setRsReason(ReasonDTO rsReason) {
		this.rsReason = rsReason;
	}

	public ReasonDTO getRaReason() {
		return raReason;
	}

	public void setRaReason(ReasonDTO raReason) {
		this.raReason = raReason;
	}
	
	public List<FunctionDTO> getFunctions() {
		return functions;
	}

	public void setFunctions(List<FunctionDTO> functions) {
		this.functions = functions;
	}

	
	public FunctionDTO getFunction(FunctionEnum func) {		 
		 for(FunctionDTO fun : this.functions ) {
			 if(func.name().equalsIgnoreCase(fun.getName())) {
				 return fun;
			 }
		 }
		 return null;
	}

	
	public EvaluationStatusDTO getUnLossState() {
		return unLossState;
	}

	public void setUnLossState(EvaluationStatusDTO unLossState) {
		this.unLossState = unLossState;
	}

	public FunctionParamDTO getParam(FunctionEnum func, ParamEnum par) {		 
		FunctionDTO function = this.getFunction(func);
		if(function != null && function.getParams().containsKey(par.name())) {			
			return function.getParams().get(par.name());
		}
		 return null;
		 
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
	
}
