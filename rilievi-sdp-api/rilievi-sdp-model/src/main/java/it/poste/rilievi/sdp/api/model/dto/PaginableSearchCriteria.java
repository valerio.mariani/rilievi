package it.poste.rilievi.sdp.api.model.dto;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public abstract class PaginableSearchCriteria implements IPaginableSearch {

	private Integer pageNumber;
	
	private Integer pageSize;
	
	private String orderType;
	
	private String orderField;
	
	abstract void populateMapDtoEntity();
	
	private HashMap<String, String> fieldsMap = new HashMap<String, String>();
	
	
	public String getDBOrderField() {
		populateMapDtoEntity();
		if(StringUtils.isNotEmpty(getOrderField()) && fieldsMap.containsKey(getOrderField())) {
			return fieldsMap.get(getOrderField());
		}
		return null;
	}
	
	@Override
	public Integer getPageNumber() {
		return pageNumber;
	}

	@Override
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	@Override
	public Integer getPageSize() {
		return pageSize;
	}

	@Override
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;

	}

	@Override
	public Integer getFirstRow() {
		try {
			return this.getPageNumber() <= 1 ? 0 : ((this.getPageNumber()-1)*this.getPageSize());
		}catch (Exception e) {
			return 1;
		}
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public HashMap<String, String> getFieldsMap() {
		return fieldsMap;
	}

	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
