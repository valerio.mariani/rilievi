package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class TechnicalResponseDTO {

	private String channel;

	private Result result;

	public static class Result {
		
		private Integer errorCode;

		private String errorDescritpion;

		public Integer getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(Integer errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorDescritpion() {
			return errorDescritpion;
		}

		public void setErrorDescritpion(String errorDescritpion) {
			this.errorDescritpion = errorDescritpion;
		}

	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}
}
