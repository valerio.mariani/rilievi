package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class AuditLogCriteriaDTO extends PaginableSearchCriteria {
	
	private ServicesCatalog serviceCode;
	
	private LocalDateTime dateTime;
	
	private LocalDate startDate;
	
	private LocalDate endDate;
	
	private AppUserDTO user;
	
	private Long id;
	
	private String descrAction;
	
	
	
	
	public ServicesCatalog getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(ServicesCatalog serviceCode) {
		this.serviceCode = serviceCode;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescrAction() {
		return descrAction;
	}

	public void setDescrAction(String descrAction) {
		this.descrAction = descrAction;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public AppUserDTO getUser() {
		return user;
	}

	public void setUser(AppUserDTO user) {
		this.user = user;
	}

	@Override
	void populateMapDtoEntity() {
		
			getFieldsMap().put("id", "idAzione");
			getFieldsMap().put("dateTime", "dataAzione");
			getFieldsMap().put("user.code", "utente");			
			getFieldsMap().put("actionType","tipologTipoAzioni.azione");
			
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
