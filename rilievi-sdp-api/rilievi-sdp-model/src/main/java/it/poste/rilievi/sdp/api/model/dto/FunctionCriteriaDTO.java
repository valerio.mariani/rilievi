package it.poste.rilievi.sdp.api.model.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class FunctionCriteriaDTO extends PaginableSearchCriteria{
	
	private Long id;
	private String name;
	private String description;
	private LocalDateTime dateCreation;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	@Override
	void populateMapDtoEntity() {
		
		getFieldsMap().put("id", "idFunzione");
		getFieldsMap().put("name", "nomeFunzione");
		getFieldsMap().put("description", "descrFunzione");
		getFieldsMap().put("dateCreation", "dataCreazione");		

	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}	

}
