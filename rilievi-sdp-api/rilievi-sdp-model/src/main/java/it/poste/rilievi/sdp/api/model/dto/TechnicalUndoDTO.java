package it.poste.rilievi.sdp.api.model.dto;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class TechnicalUndoDTO {

	private String channel;

	private String node;

	private Integer desk;
	
	private AccountingDate accountingDate;
	
	private TransactionInfo toBeDeleted;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public Integer getDesk() {
		return desk;
	}

	public void setDesk(Integer desk) {
		this.desk = desk;
	}

	public AccountingDate getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(AccountingDate accountingDate) {
		this.accountingDate = accountingDate;
	}

	public TransactionInfo getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(TransactionInfo toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}
	
	@Override
	public String toString() {		
		return ReflectionToStringBuilder.toString(this);
	}

}
