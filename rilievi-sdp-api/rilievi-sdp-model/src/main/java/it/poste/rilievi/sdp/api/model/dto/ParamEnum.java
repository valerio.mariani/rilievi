package it.poste.rilievi.sdp.api.model.dto;

public enum ParamEnum {

	MIN_AMOUNT_THRESHOLD("Importo Soglia Minima"),
	LIMIT_DATE("Data Ultima Elaborazione"),
	PAGING_SIZE("Lunghezza Pagina"),
	EXPORT_SIZE("Lunghezza Export"),
	FLAG_SET_EXCLUDED_DACO(""),
	DELTA_LIMIT_DATE("");
		
	
	private String description;
	
	ParamEnum(String description){
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

}
