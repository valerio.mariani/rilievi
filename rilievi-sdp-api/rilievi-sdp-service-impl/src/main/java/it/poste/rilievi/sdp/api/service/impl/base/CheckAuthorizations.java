package it.poste.rilievi.sdp.api.service.impl.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckAuthorizations {
	
	ServicesCatalog[] services();

}
