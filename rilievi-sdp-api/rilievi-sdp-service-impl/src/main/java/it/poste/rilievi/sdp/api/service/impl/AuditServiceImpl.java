package it.poste.rilievi.sdp.api.service.impl;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import it.poste.rilievi.sdp.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.AuditLogDTO;
import it.poste.rilievi.sdp.api.model.dto.ExportTableDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.StorageLogAzioni;
import it.poste.rilievi.sdp.api.model.entity.TipologTipoAzioni;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IActionLogDao;
import it.poste.rilievi.sdp.api.repository.IActionTypeDao;
import it.poste.rilievi.sdp.api.service.api.IAuditService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class AuditServiceImpl extends ServiceBase implements IAuditService{

	private static Logger logger = LoggerFactory.getLogger(AuditServiceImpl.class);
	
	@Autowired
	IActionLogDao logAzioniDao;
	
	@Autowired
	IActionTypeDao tipoAzioniDao;
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AuditLogDTO insertAudit(AuditLogDTO audit) {
		try {
			audit.setDateTime(getSysDate());
			StorageLogAzioni azione = DTOConverter.toEntity(audit);
			TipologTipoAzioni tipoAzione = tipoAzioniDao.findById(audit.getServiceCode().getCode(), TipologTipoAzioni.class);
			if( tipoAzione == null ) {
				throw new ServiceException(ServiceException.AuditActionNotFound);
			}
			azione.setTipologTipoAzioni(tipoAzione);
			azione = logAzioniDao.insertActionLog(azione);			
			logger.info("inserted audit trace for action: " + tipoAzione.getAzione());
			return  DTOConverter.toDto(azione);
		}catch (ServiceException e) {
			logger.error("Error insertAudit ServiceException: " + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error insertAudit Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Valid(value = AuditValidator.class, operation = ServicesCatalog.SEARCH_AUDIT_LOG)
	public PaginableResultSearchImpl<AuditLogDTO> getAuditLogs(AuditLogCriteriaDTO request) {
		try {
			int totalRows = logAzioniDao.searchAuditLogCount(request);
			List<StorageLogAzioni> logItems = logAzioniDao.getAuditLog(request);
			PaginableResultSearchImpl<AuditLogDTO> ret  = new PaginableResultSearchImpl<AuditLogDTO>(request);
			logItems.stream().forEach(storageLogEnt -> {
				AuditLogDTO log = DTOConverter.toDto(storageLogEnt);
				ret.addItem(log);
			});
			
			ret.setTotalRows(totalRows);
			ret.setPageNumber(request.getPageNumber());
			ret.setPageSize(request.getPageSize());
			logger.info("found {} audit trace, returned page {}, with size {}", totalRows, request.getPageNumber(), request.getPageSize());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getAuditLogItems ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getAuditLogItems Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
	
	@Override
	@Auditable(operation = ServicesCatalog.FILE_EXPORT,
			trace = "Export dei log di audit su file")
	public ExportTableDTO exportLogs(AuditLogCriteriaDTO auditLogCriteria) {

	try {
		String FILE_NAME = "log.xlsx";

		PaginableResultSearchImpl<AuditLogDTO> logList = getAuditLogs(auditLogCriteria);

		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Logs");

        CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));

        int rowNum = 0;
        Row row = sheet.createRow(rowNum++);
        int colNum = 0;

        Cell cell = row.createCell(colNum++);

         cell.setCellValue("Esito");
		 cell = row.createCell(colNum++);
         cell.setCellValue("Utente");
		 cell = row.createCell(colNum++);
		 cell.setCellValue("Data Azione");
		 cell = row.createCell(colNum++);
		 cell.setCellValue("Tipo Azione");
		 cell = row.createCell(colNum++);
		 cell.setCellValue("Descrizione Azione");

         for (AuditLogDTO log : logList.getItems()) {
        	   	row = sheet.createRow(rowNum++);
                colNum = 0;

                cell = row.createCell(colNum++);
				if (log.getResult().equals("1")) {
					cell.setCellValue("Positivo");
                }
				if (log.getResult().equals("0")) {
					cell.setCellValue("Negativo");
				}

                cell = row.createCell(colNum++);
                cell.setCellValue(log.getUser().getCode());
                cell = row.createCell(colNum++);
                cell.setCellValue(Utils.localDateTimeToDate(log.getDateTime()));
                cell.setCellStyle(cellStyle);
                cell = row.createCell(colNum++);
                cell.setCellValue(log.getServiceCode().getDescription());
                cell = row.createCell(colNum++);
                cell.setCellValue(log.getDescrAction());
        }

			for (int i = 0; i < 4; i++) {
           sheet.autoSizeColumn(i);
       }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        workbook.write(outputStream);
        workbook.close();

        ExportTableDTO export = new ExportTableDTO();
		export.setFileName(FILE_NAME);
		export.setContent(new String(Base64Utils.encode(outputStream.toByteArray())));
	    export.setContentType("application/vnd.ms-excel");

        return export;

	} catch (ServiceException e) {
		logger.error("Error getLogsExport ServiceException:" + e.toString());
		throw e;
	} catch (Exception e) {
		logger.error("Error getLogsExport Generic Exception...", e);
		throw new ServiceException(e);
	}

	}
	
}
