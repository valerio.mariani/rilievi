package it.poste.rilievi.sdp.api.service.impl.base;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.impl.util.AOPUtils;

@Component
@Aspect
public class ValidAspect implements Ordered, ApplicationContextAware{
	private static final Logger logger = LoggerFactory.getLogger(ValidAspect.class);
	

	private ApplicationContext applicationContext;
	
	@Around("@annotation(it.poste.rilievi.sdp.api.service.impl.base.Valid)")
	public Object validAspect(ProceedingJoinPoint joinPoint) throws Throwable {
		
		Valid valAnnotation = AOPUtils.getAnnotation(joinPoint, Valid.class);			
		 if (valAnnotation != null) {
			
			ServicesCatalog operation = valAnnotation.operation();	
			ValidationError errors = new ValidationError();
			for( Class<? extends IValidator> validatorClass : valAnnotation.value() ) {
				IValidator val = (IValidator) applicationContext.getBean(validatorClass);
				val.validate(errors, operation, joinPoint.getArgs());
			}
			
			if(errors.hasErrors()) {
				logger.error("Validation error. Handle ServiceException");
				throw errors.toServiceException();
			}
				
			logger.debug("Operation '" + operation.name() + "' successfully validated!");
		 }

		return joinPoint.proceed();
	}

	@Override
	public int getOrder() {
		return 9;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;		
	}
}
