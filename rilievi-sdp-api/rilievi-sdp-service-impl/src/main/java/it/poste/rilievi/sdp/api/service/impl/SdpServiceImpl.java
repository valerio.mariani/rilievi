package it.poste.rilievi.sdp.api.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.sdp.api.model.dto.RegularizationType;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.dto.TechnicalAlignDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalResponseDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalUndoDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalResponseDTO.Result;
import it.poste.rilievi.sdp.api.model.entity.StorageProcessiSdp;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IEvaluationDao;
import it.poste.rilievi.sdp.api.repository.IProcessSdpDao;
import it.poste.rilievi.sdp.api.service.api.IEvaluationService;
import it.poste.rilievi.sdp.api.service.api.IProcessSdpService;
import it.poste.rilievi.sdp.api.service.api.ISdpService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;

@Service
@Transactional
public class SdpServiceImpl extends ServiceBase implements ISdpService {
	
private static Logger logger = LoggerFactory.getLogger(SdpServiceImpl.class);
	
	@Autowired
	IProcessSdpService processService;
	
	@Autowired
	IEvaluationService evaluationService;
	
	@Autowired
	IProcessSdpDao processDao;
	
	@Autowired
	IEvaluationDao evaluationDao;

	
	@Override
	@Transactional
	public ProcessSDPDTO regularization(ProcessSDPDTO process) {
		try {	
			logger.info("start regularization: " + process.toString());
			process.setState("N");
			process.setFlagValid(StorageProcessiSdp.FLAG_VALIDO);
			process.setFlagOperationComplete(StorageProcessiSdp.FLAG_OPERAZIONE_COMPLETA);
			process.setOperationDate(process.getContabilizationDate());
			evaluationService.setRegularization(process.getEvaluationId(), process.getContabilizationDate(), RegularizationType.S);
			ProcessSDPDTO ret = processService.insertProcess(process);
			ret.setEvaluation(evaluationService.getEvaluation(process.getEvaluationId()));
			return ret;
		}catch (ServiceException e) {
			logger.error("Error  regularization ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error  regularization Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public ProcessSDPDTO undoRegularization(ProcessSDPDTO process) {
		try {	
			logger.info("start undo regularization: " + process.toString());
			process.setState("A");
			process.setFlagValid(StorageProcessiSdp.FLAG_VALIDO);
			process.setFlagOperationComplete(StorageProcessiSdp.FLAG_OPERAZIONE_COMPLETA);
			process.setOperationDate(getSysDate().toLocalDate());
			ProcessSDPDTO ret = processService.insertProcess(process);
			process.setOffice(ret.getOffice());
			processService.invalidateProcess(process.getOriginalOperationNumber(), process.getOffice().getFractional(),
																			process.getBranchNumber(), process.getContabilizationDate());			
			evaluationService.undoRegularization(process.getEvaluationId());		
			
			ret.setEvaluation(evaluationService.getEvaluation(process.getEvaluationId()));
			logger.info("End undoRegularization");
			return ret;
		}catch (ServiceException e) {
			logger.error("Error  undoRegularization ServiceException:" + e.getMessage());
			throw e;
		}catch (Exception e) {
			logger.error("Error  undoRegularization Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.SDP_TECHNICAL_UNDO_OPERATION,
		trace = "Annullamento tecnico dell'operazione SDP con NumOpe=${(request.toBeDeleted.operation)!'?'}")
	public TechnicalResponseDTO technicalUndo(TechnicalUndoDTO request) {

		Result result = new Result();
		
		LocalDate accountingDate = Utils.convertToLocalDate(request.getAccountingDate());
		String node = request.getNode();
		Integer desk = request.getDesk();

		if (StringUtils.isBlank(node)) {
			result = this.buildResponse(1, "node non valido");
		} else if (desk==null) {
			result = this.buildResponse(1, "desk non valido");
		} else if (request.getToBeDeleted() == null || request.getToBeDeleted().getOperation() == null) {
			result = this.buildResponse(1, "operation non valida");
		} else if (accountingDate == null) {
			result = this.buildResponse(1, "accountingDate non valida");
		} else {
			try {
				Integer operation = request.getToBeDeleted().getOperation();
				StorageProcessiSdp storageProcessiSdp = processDao.getProcessByNumOpeFractionalDeskAccountingDate(operation, node, desk, accountingDate);
				
				if (storageProcessiSdp == null) {
					result = this.buildResponse(-1, "operazione da annullare non trovata");
				} else {
					if (StringUtils.equals(storageProcessiSdp.getStatoOperazione(), "N")) {
						//delete dirty record
						processDao.deleteProcessByIdProcSdp(storageProcessiSdp.getIdProcSdp());
						//set the evaluation as NOT REGULARIZED
						evaluationDao.updateEvaluationById(null, null, "0", storageProcessiSdp.getStorageRilievi().getIdRilievi());
						result = this.buildResponse(0, "OK");
					} else if (StringUtils.equals(storageProcessiSdp.getStatoOperazione(), "A")) {
						//delete dirty record
						processDao.deleteProcessByIdProcSdp(storageProcessiSdp.getIdProcSdp());
						//search the original record
						StorageProcessiSdp storageProcessiSdpNumOpOrig = processDao.getProcessByNumOpeFractionalDeskAccountingDate(storageProcessiSdp.getNumOperazioneOrig(), node, desk, accountingDate);
						//set the record as valid
						processDao.updateRegularization(StorageProcessiSdp.FLAG_VALIDO, null, storageProcessiSdpNumOpOrig.getIdProcSdp());
						//set the evaluation as REGULARIZED
						evaluationDao.updateEvaluationById(Utils.dateToLocalDate(storageProcessiSdpNumOpOrig.getDataIntervento()), "S", StorageRilievi.FLAG_REGOLARIZZATO, storageProcessiSdpNumOpOrig.getStorageRilievi().getIdRilievi());
						result = this.buildResponse(0, "OK");
					} else {
						result = this.buildResponse(-1, "operazione da annullare non valida");
					}
				}
			} catch (Exception e) {
				logger.error("generic error during technical undo operation", e);
				result = this.buildResponse(2, "generic error");
			}
		}

		//build the response
		TechnicalResponseDTO ret = new TechnicalResponseDTO();
		ret.setResult(result);
		ret.setChannel(request.getChannel());
		return ret;
	}

	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.SDP_TECHNICAL_ALIGN_OPERATION,
    	trace = "Allineamento tecnico a partire dall'operazione SDP con NumOpe=${(request.lastTransaction.operation)!'?'}")
	public TechnicalResponseDTO technicalAlign(TechnicalAlignDTO request) {

		Result result = new Result();

		LocalDate accountingDate = Utils.convertToLocalDate(request.getAccountingDate());
		String node = request.getNode();
		Integer desk = request.getDesk();
		

		if (StringUtils.isBlank(node)) {
			result = this.buildResponse(1, "node non valido");
		} else if (desk==null) {
			result = this.buildResponse(1, "desk non valido");
		} else if (request.getLastTransaction() == null || request.getLastTransaction().getOperation() == null) {
			result = this.buildResponse(1, "operation non valida");
		} else if (accountingDate == null) {
			result = this.buildResponse(1, "accountingDate non valida");
		} else {
			
			try {
				Integer operation = request.getLastTransaction().getOperation();
				//search all the invalid records
				List<StorageProcessiSdp> storageProcessiSdp = processDao.getProcessesByNumOpeFractionalDeskAccountingDate(operation, node, desk, accountingDate);
				if (storageProcessiSdp.isEmpty()) {
					result = this.buildResponse(-1, "operazioni da annullare non trovate");
				} else {
					
					boolean fullAlign = true;
					for (StorageProcessiSdp item : storageProcessiSdp) {
						
						if (StringUtils.equals(item.getStatoOperazione(), "N")) {
							//delete dirty record
							processDao.deleteProcessByIdProcSdp(item.getIdProcSdp());
							//set the evaluation as NOT REGULARIZED
							evaluationDao.updateEvaluationById(null, null, "0", item.getStorageRilievi().getIdRilievi());
						} else if (StringUtils.equals(item.getStatoOperazione(), "A")) {
							//delete dirty record
							processDao.deleteProcessByIdProcSdp(item.getIdProcSdp());
							//search the original record
							StorageProcessiSdp storageProcessiSdpNumOpOrig = processDao.getProcessByNumOpeFractionalDeskAccountingDate(item.getNumOperazioneOrig(), node, desk, accountingDate);
							//set the record as valid
							processDao.updateRegularization(StorageProcessiSdp.FLAG_VALIDO, null, storageProcessiSdpNumOpOrig.getIdProcSdp());
							//set the evaluation as REGULARIZED
							evaluationDao.updateEvaluationById(Utils.dateToLocalDate(storageProcessiSdpNumOpOrig.getDataIntervento()), "S", "1", storageProcessiSdpNumOpOrig.getStorageRilievi().getIdRilievi());
	
						} else {
							fullAlign = false;
							continue;
						}
					}
					
					if(fullAlign) {
						result = this.buildResponse(0, "OK");
					} else {
						result = this.buildResponse(3, "allineamento parziale delle operazioni");
					}
				}
			} catch (Exception e) {
				logger.error("generic error during technical align operation", e);
				result = this.buildResponse(2, "generic error");
			}
		}
		
		//build the response
		TechnicalResponseDTO ret = new TechnicalResponseDTO();
		ret.setResult(result);
		ret.setChannel(request.getChannel());
		
		return ret;
	}
	
	
	private Result buildResponse(Integer errorCode, String erroDescription) {
		Result result = new Result();
		result.setErrorCode(errorCode);
		result.setErrorDescritpion(erroDescription);
		logger.info("return response with errorCode: {} and errorDescription; {}", errorCode, erroDescription);
		return result;
	}

	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.SDP_UNDO_OPERATION_FE,
    	trace = "Annullamento (FE) dell'operazione SDP con NumOpe=${(request.operationNumber)!'?'}")
	@Valid(value = ProcessSDPValidator.class, operation = ServicesCatalog.SDP_UNDO_OPERATION_FE)
	public void undoOperation(ProcessSDPDTO request) {
		try {

			StorageProcessiSdp storageProcessiSdp = processDao.getProcessByNumOpeFractionalDeskAccountingDate(Integer.valueOf(request.getOperationNumber()), request.getOffice().getFractional(), Integer.valueOf(request.getBranchNumber()), request.getContabilizationDate());
			if (storageProcessiSdp != null) {
				if (StringUtils.equals(storageProcessiSdp.getStatoOperazione(), "N")) {
					// delete dirty record
					processDao.deleteProcessByIdProcSdp(storageProcessiSdp.getIdProcSdp());
					// set the evaluation as NOT REGULARIZED
					evaluationDao.updateEvaluationById(null, null, "0", storageProcessiSdp.getStorageRilievi().getIdRilievi());

				} else if (StringUtils.equals(storageProcessiSdp.getStatoOperazione(), "A")) {
					// delete dirty record
					processDao.deleteProcessByIdProcSdp(storageProcessiSdp.getIdProcSdp());
					// search the original record
					StorageProcessiSdp storageProcessiSdpNumOpOrig = processDao.getProcessByNumOpeFractionalDeskAccountingDate(
									storageProcessiSdp.getNumOperazioneOrig(),
									request.getOffice().getFractional(), Integer.valueOf(request.getBranchNumber()),
									request.getContabilizationDate());
					// set the record as valid
					processDao.updateRegularization(StorageProcessiSdp.FLAG_VALIDO, null, storageProcessiSdpNumOpOrig.getIdProcSdp());
					// set the evaluation as REGULARIZED
					evaluationDao.updateEvaluationById(
							Utils.dateToLocalDate(storageProcessiSdpNumOpOrig.getDataIntervento()), "S",
							StorageRilievi.FLAG_REGOLARIZZATO,
							storageProcessiSdpNumOpOrig.getStorageRilievi().getIdRilievi());
				}
			} else {
				logger.info("operazione da annullare non trovata");
			}
		} catch (ServiceException e) {
			logger.error("Error undoOperation ServiceException:" + e.getMessage());
			throw e;
		}catch (Exception e) {
			logger.error("Error undoOperation Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
}

