package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.OfficeDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.TipologUffici;
import it.poste.rilievi.sdp.api.repository.IOfficeDao;
import it.poste.rilievi.sdp.api.service.api.IOfficeService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class OfficeServiceImpl extends ServiceBase implements IOfficeService{
	
	private static Logger logger = LoggerFactory.getLogger(OfficeServiceImpl.class);
	
	@Autowired
	IOfficeDao officeDao;
	

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_OFFICES,
			trace = "Recuperata lista uffici della filiale con Id=${(idBranch)!'?'}")
	public List<OfficeDTO> getOffices(Long idBranch) {
		try {
			List<OfficeDTO> ret  = new ArrayList<OfficeDTO>();
			List<TipologUffici> officeList = officeDao.getOffices(idBranch);
			officeList.stream().forEach(dacoEnt -> {
				ret.add(DTOConverter.toDto(dacoEnt));
			});
			logger.info("found and returned {} offices by idBranch {}", ret.size(), idBranch );
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getOffices ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getOffices Generic Exception...",e);
			throw new ServiceException(e);
		}
	}


	@Override
	@Transactional(readOnly = true)
	public OfficeDTO getOfficeByFractional(String fractional) {
		try {
			TipologUffici office = officeDao.getOfficeByFractional(fractional);
			if(office == null) {
				return null;
			}
			OfficeDTO ret  = DTOConverter.toDto(office);
			logger.info("found an office for the following fractional: " + ret.toString());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getOfficeByFractional ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getOfficeByFractional Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
}
