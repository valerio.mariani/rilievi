package it.poste.rilievi.sdp.api.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.ExtractionDatesDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.api.IExtractionDatesService;
import it.poste.rilievi.sdp.api.service.impl.base.IValidator;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationError;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.sdp.api.service.impl.base.ValidatorBase;

@Component
public class ExtractionDatesValidator extends ValidatorBase implements IValidator {
	
	@Autowired
	IExtractionDatesService service;
	
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {
		ExtractionDatesDTO extractionDates = (ExtractionDatesDTO)target[0];
		
		switch (operation) {
			case NEW_EXTRACTIONDATES:
				validateInsert(errors, extractionDates);
				break;
			default:
				break;
		}
	}
	
	
	
	
	private void validateInsert(ValidationError errors, ExtractionDatesDTO extractionDates) {
		
		//caso null 
 		ValidationUtils.rejectIfNull(extractionDates.getClosingDate(),"closingDate" , resolveMessage("extractionDates.closingDate.null") ,errors);
 		ValidationUtils.rejectIfNull(extractionDates.getFixedDate(),"fixedDate" , resolveMessage("extractionDates.fixedDate.null") ,errors);
 		
		// closingDate > fixedDate
 		ValidationUtils.rejectIfDateIsBefore(extractionDates.getClosingDate(),extractionDates.getFixedDate(),"closingDate", resolveMessage("extractionDates.closingDate/fixedDate.min"),errors);
		// closingDate > SysDate
 		ValidationUtils.rejectIfDateIsBefore(extractionDates.getClosingDate(),service.getSysDate().toLocalDate(), "closingDate", resolveMessage("extractionDates.closingDate/SysDate.min"), errors);
		
	}

}
