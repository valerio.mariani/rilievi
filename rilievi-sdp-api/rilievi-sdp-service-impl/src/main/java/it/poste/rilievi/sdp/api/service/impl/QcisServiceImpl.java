package it.poste.rilievi.sdp.api.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.constants.QcisConstants;
import it.poste.rilievi.sdp.api.model.entity.StorageQcisBean;
import it.poste.rilievi.sdp.api.model.entity.TipologUffici;
import it.poste.rilievi.sdp.api.repository.IOfficeDao;
import it.poste.rilievi.sdp.api.repository.IQcisDao;
import it.poste.rilievi.sdp.api.service.api.IQcisService;

@Service
public class QcisServiceImpl extends ServiceBase implements IQcisService{
	
	private static Logger logger = LoggerFactory.getLogger(QcisServiceImpl.class);
	
	private final static String LEVEL_1="1";
	private final static String LEVEL_2="2";
	private final static String LEVEL_4="4";

	@Value("${poste.rilievi.codice.sottosistema}")
	private String codSubSystem;
	
	@Value("${poste.rilievi.codice.addebito}")
	private String codDebit;
	
	@Value("${poste.rilievi.codice.accredito}")
	private String codCredit;
	
	@Autowired
	IQcisDao qcisDao;
	
	@Autowired
	IOfficeDao officeDao;


	@Override
	public String getQcis(String ufficio, String sezione, String data, String livello, String codeOp) {
		
		if (isValidDate(data)) {
			
			if(!officeExist(ufficio)) {
				return buildErrorResponse(QcisConstants.ERRORE_AGENZIA_INESISTENTE, data);
			}
			
			try {
				String level = livello != null ? livello : "";
				switch (level) {
					case LEVEL_1:
						StorageQcisBean storageQcisBeanFirstLevel = qcisDao.getDataForFirstAndSecondLevel(ufficio, sezione, data);
						
						if (storageQcisBeanFirstLevel == null) {
							return buildErrorResponse(QcisConstants.ERRORE_ASSENZA_MOVIMENTI, data);
						} else {
							return getQcisStringFirstLevel(storageQcisBeanFirstLevel, sezione, data);
						}
					case LEVEL_2:
						StorageQcisBean storageQcisBeanSecondLevel = qcisDao.getDataForFirstAndSecondLevel(ufficio, sezione, data);
						
						if (storageQcisBeanSecondLevel == null) {
							return buildErrorResponse(QcisConstants.ERRORE_ASSENZA_MOVIMENTI, data);
						} else {
							return getQcisStringSecondLevel(storageQcisBeanSecondLevel, sezione, data);
						}
						
					case LEVEL_4:
						if (StringUtils.equals(codCredit, codeOp) || StringUtils.equals(codDebit, codeOp)) {
							
							List<StorageQcisBean> storageQcisBeanFourthLevel = qcisDao.getDataForFourthLevel(ufficio, sezione, data, codeOp);
							
							if (storageQcisBeanFourthLevel.isEmpty()) {
								return buildErrorResponse(QcisConstants.ERRORE_ASSENZA_MOVIMENTI, data);
							} else {
								return getQcisStringFourthLevelLevel(storageQcisBeanFourthLevel, sezione, data);
							}
							
						} else {
							return buildErrorResponse(QcisConstants.ERRORE_CODICE_OPERAZIONE_INESISTENTE, data);
						}
					default:
						return buildErrorResponse(QcisConstants.ERRORE_SEQUENZA_ERRATA, data);
				}
			} catch (Exception e) {
				logger.error("Error during QCIS response elaboration", e);
				return buildErrorResponse(QcisConstants.ERRORE_GENERICO_QCIS, data);
			}

		} else {
			logger.error("uffcio o data non validi");
			return new StringBuffer().append(QcisConstants.ERRORE_GENERICO_QCIS).append(QcisConstants.LUNGHEZZA_AREA_ERRORE)
					.append(codSubSystem).append(data).append(QcisConstants.NUMERO_AREE_ERRORE).toString();
		}
	}
	
	
	private String getQcisStringFirstLevel(StorageQcisBean storageQcisBean, String sezione, String dataContabile) {

		StringBuffer res = new StringBuffer();
			
		res.append(QcisConstants.ESECUZIONE_CORRETTA)
			.append(QcisConstants.LUNGHEZZA_AREA_PRIMO_LIVELLO)
			.append(codSubSystem)
			.append(dataContabile)
			.append(QcisConstants.NUMERO_AREE_PRIMO_LIVELLO)
			.append(StringUtils.isNotBlank(sezione) ? StringUtils.leftPad(sezione, 4, "0") : "0000")
			.append(QcisConstants.CODICE_OPERAZIONE_VUOTO)
			.append(QcisConstants.CAUSALE)
			.append(StringUtils.rightPad(QcisConstants.DESCRIZIONE_CAUSALE_PRIMO_LIVELLO, 30, " "))
			.append(StringUtils.leftPad(String.valueOf(storageQcisBean.getNumberGivingMovementsEuro().toBigInteger().toString()), 5, "0"))
			.append(StringUtils.leftPad(getValue(storageQcisBean.getAmountGivingMovementsEuro()), 15, "0"))
			.append(StringUtils.leftPad(String.valueOf(storageQcisBean.getNumberMovementsToHaveEuro().toBigInteger()), 5, "0"))
			.append(StringUtils.leftPad(getValue(storageQcisBean.getAmountMovementsToHaveEuro()), 15, "0"))
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO);
		
		return res.toString();
	}

	private String getQcisStringSecondLevel(StorageQcisBean storageQcisBean, String sezione, String dataContabile) {

		StringBuffer res_1 = new StringBuffer();

		res_1.append(QcisConstants.ESECUZIONE_CORRETTA)
		    .append(QcisConstants.LUNGHEZZA_AREA_SECONDO_LIVELLO)
			.append(codSubSystem)
			.append(dataContabile)
			.append(QcisConstants.NUMERO_AREE_SECONDO_LIVELLO)
			.append(StringUtils.isNotBlank(sezione) ? StringUtils.leftPad(sezione, 4, "0") : "0000")
			.append(codCredit).append("  ")
			.append(QcisConstants.CAUSALE)
			.append(StringUtils.rightPad(QcisConstants.DESCRIZIONE_CAUSALE_CREDITO_SECONDO_LIVELLO, 30, " "))
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			.append(StringUtils.leftPad(String.valueOf(storageQcisBean.getNumberMovementsToHaveEuro().toBigInteger()), 5, "0"))
			.append(StringUtils.leftPad(getValue(storageQcisBean.getAmountMovementsToHaveEuro()), 15, "0"))
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			//start second item
			.append(StringUtils.isNotBlank(sezione) ? StringUtils.leftPad(sezione, 4, "0") : "0000")
			.append(codDebit).append("  ")
			.append(QcisConstants.CAUSALE)
			.append(StringUtils.rightPad(QcisConstants.DESCRIZIONE_CAUSALE_DEBITO_SECONDO_LIVELLO, 30, " "))
			.append(StringUtils.leftPad(String.valueOf(storageQcisBean.getNumberGivingMovementsEuro().toBigInteger()), 5, "0"))
			.append(StringUtils.leftPad(getValue(storageQcisBean.getAmountGivingMovementsEuro()), 15, "0"))
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO)
			.append(QcisConstants.NUMERO_MOVIMENTI_VUOTO)
			.append(QcisConstants.IMPORTO_MOVIMENTI_VUOTO);

		return res_1.toString();
	}
	
	private String getQcisStringFourthLevelLevel(List<StorageQcisBean> storageQcisBeanList, String sezione, String dataContabile) throws IllegalArgumentException, IllegalAccessException {
		StringBuffer res_1 = new StringBuffer();
		
		int listSize = storageQcisBeanList.size();
		
		res_1.append(QcisConstants.ESECUZIONE_CORRETTA)
			.append(StringUtils.leftPad(String.valueOf((QcisConstants.LUNGHEZZA_ITEM_QUARTO_LIVELLO * listSize) + 28), 5, "0"))
			.append(codSubSystem)
			.append(dataContabile)
			.append(StringUtils.leftPad(String.valueOf(QcisConstants.LUNGHEZZA_ITEM_QUARTO_LIVELLO), 3, "0"))
			.append(StringUtils.leftPad(listSize+"", 5, "0"));
			
		res_1.append(QcisConstants.HEADER);
		
		int counter=1;
		
		for (StorageQcisBean storageQcisBean : storageQcisBeanList) {
			
			if (counter <= QcisConstants.DATI_CONTABILI_MAX_LENGTH_QUARTO_LIVELLO) {
				
				res_1.append(StringUtils.leftPad(storageQcisBean.getOperationNumber(), 4, "0"))
					 .append(QcisConstants.EMPTY_THREE)
					 .append(StringUtils.rightPad(storageQcisBean.getRifEvaluation(), 19, " "))
					 .append(QcisConstants.EUR);
				String importo = "";
				if (storageQcisBean.getAmountGivingMovementsEuro() != null) {
					importo = getDecimalFormat(storageQcisBean.getAmountGivingMovementsEuro());
				} else if(storageQcisBean.getAmountMovementsToHaveEuro() != null){
					importo = getDecimalFormat(storageQcisBean.getAmountMovementsToHaveEuro());
				}
				res_1.append(StringUtils.leftPad(importo + " ", 49, " "));
			} else {
				break;
			}
			
			counter++;
		}

		return res_1.toString();
	}
	
	
	private String buildErrorResponse(String resultCode, String accountingDate) {
		StringBuilder sb = new StringBuilder();
		sb.append(resultCode)
		.append(QcisConstants.LUNGHEZZA_AREA_ERRORE)
		.append(codSubSystem)
		.append(accountingDate)
		.append(QcisConstants.NUMERO_AREE_ERRORE);
		return sb.toString();
	}
	
	private String getValue (BigDecimal value) {
		BigDecimal ret = value.multiply(new BigDecimal(100));
		return String.valueOf(ret.intValue());
		
	}
	
	private String getDecimalFormat (BigDecimal value) {
		DecimalFormat df = new DecimalFormat("#,###.00");
		return df.format(value);
	}
	
	
	private boolean isValidDate(String date) {
		boolean value = false;
		
		if(date != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(QcisConstants.DATA_CONTABILE_PATTERN);
			
			try {
				LocalDate.parse(date, formatter);
				value = true;
			} catch (DateTimeParseException ex) {
				logger.error("error during formatting date: {}", date);
			}
		}
		return value;
	}
	
	private boolean officeExist(String fractional) {
		boolean value = false;
		TipologUffici office = officeDao.getOfficeByFractional(fractional);
		if (office != null) {
			value = true;
		}
		return value;
	}
	
}
