package it.poste.rilievi.sdp.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.api.IAuditService;
import it.poste.rilievi.sdp.api.service.impl.base.IValidator;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationError;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.sdp.api.service.impl.base.ValidatorBase;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;

@Component
public class AuditValidator extends ValidatorBase implements IValidator {

	@Autowired
	IAuditService service;
	
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException {
		AuditLogCriteriaDTO criteria = (AuditLogCriteriaDTO) obj[0];

		switch (operation) {
			case SEARCH_AUDIT_LOG:
				validateSearch(errors, criteria);
				break;
			default:
				break;
		}
	}

	private void validateSearch(ValidationError errors, AuditLogCriteriaDTO criteria) {
		ValidationUtils.rejectIfDateIsBefore(criteria.getEndDate(), criteria.getStartDate(), "endDate", resolveMessage("auditlog.criteria.endDate.min"), errors);
	}

}
