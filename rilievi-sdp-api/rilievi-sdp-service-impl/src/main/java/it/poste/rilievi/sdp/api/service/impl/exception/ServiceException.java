package it.poste.rilievi.sdp.api.service.impl.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 97841404024024136L;
	public static final ApplicationError NotAuthorized = new ApplicationError("001", "User not Authoried");
	public static final ApplicationError AuditActionNotFound = new ApplicationError("002", "Action not found");

	private List<ApplicationError> errors = new ArrayList<ApplicationError>();
	private HashMap<String, List<String>> fieldsErrors = new HashMap<String, List<String>>();

	public ServiceException() {
		super();
	}

	public ServiceException(ApplicationError error) {
		super(error.getMessage());
		this.errors.add(error);
	}

	public ServiceException(String errorMessage) {
		super(errorMessage);
		this.errors.add(new ApplicationError("000", errorMessage));
	}

	public ServiceException(Exception error) {
		super(error);
		this.errors.add(new ApplicationError("000", error.getMessage()));
	}

	public HashMap<String, List<String>> getFieldsErrors() {
		return fieldsErrors;
	}

	public void setFieldsErrors(HashMap<String, List<String>> fieldsErrors) {
		this.fieldsErrors = fieldsErrors;
	}

	public void addFieldError(String field, String message) {
		if (!fieldsErrors.containsKey(field)) {
			fieldsErrors.put(field, new ArrayList<String>());
		}
		fieldsErrors.get(field).add(message);
	}

	public List<String> getFieldErrors(String field) {
		if (!fieldsErrors.containsKey(field)) {
			fieldsErrors.put(field, new ArrayList<String>());
		}
		return fieldsErrors.get(field);
	}

	public List<ApplicationError> getErrors() {
		return errors;
	}

	public void setErrors(List<ApplicationError> errors) {
		this.errors = errors;
	}

	public String toString() {
		return "";
	}
}
