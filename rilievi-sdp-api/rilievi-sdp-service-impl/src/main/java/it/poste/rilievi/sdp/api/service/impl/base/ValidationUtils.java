package it.poste.rilievi.sdp.api.service.impl.base;

import java.time.LocalDate;

import org.apache.commons.lang.StringUtils;

public class ValidationUtils {

	public static boolean rejectIfEmpty(String value, String field, String message, ValidationError errors ) {
		if( StringUtils.isEmpty(value) ) {
			errors.addFieldError(field, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfBlank(String value, String field, String message, ValidationError errors ) {
		if( StringUtils.isBlank(value) ) {
			errors.addFieldError(field, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfNull(Object value, String field, String message, ValidationError errors ) {
		if( value == null ) {
			errors.addFieldError(field, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfDateIsBefore(LocalDate checkDate, LocalDate targetDate, String field, String message, ValidationError errors ) {
		if(checkDate != null && checkDate.isBefore(targetDate)) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}

	public static boolean rejectIfEmpty(String value, String message, ValidationError errors ) {

		if( StringUtils.isEmpty(value) ) {
			errors.addApplicationError(null, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfBlank(String value, String message, ValidationError errors ) {

		if( StringUtils.isBlank(value) ) {
			errors.addApplicationError(null, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfNull(Object value, String message, ValidationError errors ) {
		if( value == null ) {
			errors.addApplicationError(null, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfDateIsAfter(LocalDate checkDate, LocalDate targetDate, String field, String message, ValidationError errors ) {
		if(checkDate != null && checkDate.isAfter(targetDate)) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	
	public static boolean rejectIfDateIsAfterOrEqual(LocalDate checkDate, LocalDate targetDate, String field, String message, ValidationError errors ) {
		if(checkDate != null && (checkDate.isAfter(targetDate) || checkDate.equals(targetDate))) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	
	public static boolean rejectIfDateIsAfterOrEqual(LocalDate checkDate, LocalDate targetDate,  String message, ValidationError errors ) {
		if(checkDate != null && (checkDate.isAfter(targetDate) || checkDate.equals(targetDate))) {
			errors.addApplicationError(null, message);
			return true;
		 }
		return false;
	}
	
	public static boolean rejectIfDateIsBeforeOrEqual(LocalDate checkDate, LocalDate targetDate, String field, String message, ValidationError errors ) {
		if(checkDate != null && ( checkDate.isBefore(targetDate) || checkDate.equals(targetDate) )) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	
	public static boolean rejectIfDoubleIsLess(Double checkNumber, Double targetNumber, String field, String message, ValidationError errors ) {
		if(checkNumber != null &&  checkNumber < targetNumber ) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	public static boolean rejectIfDoubleIsLessOrEqual(Double checkNumber, Double targetNumber, String field, String message, ValidationError errors ) {
		if(checkNumber != null && checkNumber <= targetNumber  ) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	
	public static boolean rejectIfDoubleIsGreater(Double checkNumber, Double targetNumber, String field, String message, ValidationError errors ) {
		if(checkNumber != null &&  checkNumber > targetNumber ) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}
	public static boolean rejectIfDoubleIsGreaterOrEqual(Double checkNumber, Double targetNumber, String field, String message, ValidationError errors ) {
		if(checkNumber != null &&  checkNumber >= targetNumber ) {
			errors.addFieldError(field, message);
			return true;
		 }
		return false;
	}

	
	public static boolean rejectIfFalse(boolean value, String field, String message, ValidationError errors ) {
		if( !value ) {
			errors.addFieldError(field, message);
			return true;
		}
		return false;
	}
	public static boolean rejectIfFalse(boolean value, String message, ValidationError errors ) {
		if( !value ) {
			errors.addApplicationError(null, message);
			return true;
		}
		return false;
	}
	
	public static boolean rejectIfTrue(boolean value, String field, String message, ValidationError errors ) {
		if( value ) {
			errors.addFieldError(field, message);
			return true;
		}
		return false;
	}
	public static boolean rejectIfTrue(boolean value, String message, ValidationError errors ) {
		if( value ) {
			errors.addApplicationError(null, message);
			return true;
		}
		return false;
	}
}
