package it.poste.rilievi.sdp.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.TipologUffici;
import it.poste.rilievi.sdp.api.repository.IOfficeDao;
import it.poste.rilievi.sdp.api.service.impl.base.IValidator;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationError;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.sdp.api.service.impl.base.ValidatorBase;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
@Component
public class ProcessSDPValidator extends ValidatorBase implements IValidator{

	@Autowired
	IOfficeDao officeDao;
	
	
	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException {
		
		switch (operation) {
			case INSERT_PROCESS_SDP:
				ProcessSDPDTO process = (ProcessSDPDTO)obj[0];
				validateInsert(errors, process);
				break;
			case SDP_UNDO_OPERATION_FE:
				ProcessSDPDTO undoOperation = (ProcessSDPDTO)obj[0];
				validateUpdate(errors, undoOperation);
				break;
			default:
				break;
		}
	}
	
	
	private  void validateInsert(ValidationError errors, ProcessSDPDTO process) {
		TipologUffici office = officeDao.findById(process.getOffice().getId(), TipologUffici.class);
		ValidationUtils.rejectIfNull(office, resolveMessage("processSDP.fractional.notExits"), errors);
	}
	
	private void validateUpdate(ValidationError errors, ProcessSDPDTO undoOperation) {
		TipologUffici office = officeDao.getOfficeByFractional(undoOperation.getOffice().getFractional());
		ValidationUtils.rejectIfNull(office, "office", resolveMessage("processSDP.fractional.notExists"), errors);
		ValidationUtils.rejectIfNull(undoOperation.getContabilizationDate(), "contabilizationDate ", resolveMessage("processSDP.accountDate.notExists"), errors);
		ValidationUtils.rejectIfBlank(undoOperation.getBranchNumber(), "branchNumber", resolveMessage("processSDP.branchNumber.notExists"), errors);
		ValidationUtils.rejectIfBlank(undoOperation.getOperationNumber(), "operationNumber", resolveMessage("processSDP.operationNumber.notExists"), errors);
	}
	
}