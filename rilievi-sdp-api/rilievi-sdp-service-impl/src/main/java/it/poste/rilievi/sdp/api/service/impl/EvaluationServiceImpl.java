package it.poste.rilievi.sdp.api.service.impl;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import it.poste.rilievi.sdp.api.model.dto.AttachmentDTO;
import it.poste.rilievi.sdp.api.model.dto.DacoDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationCriteriaDTO.SearchType;
import it.poste.rilievi.sdp.api.model.dto.EvaluationDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.sdp.api.model.dto.ExportTableDTO;
import it.poste.rilievi.sdp.api.model.dto.ExportTableDTO.ExportType;
import it.poste.rilievi.sdp.api.model.dto.OfficeDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.model.dto.RegularizationType;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.AnagProfili;
import it.poste.rilievi.sdp.api.model.entity.StorageAllegati;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.model.entity.TipologCausale;
import it.poste.rilievi.sdp.api.model.entity.TipologDaco;
import it.poste.rilievi.sdp.api.model.entity.TipologFiliali;
import it.poste.rilievi.sdp.api.model.entity.TipologServizi;
import it.poste.rilievi.sdp.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.sdp.api.model.entity.TipologUffici;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IDacoDao;
import it.poste.rilievi.sdp.api.repository.IEvaluationDao;
import it.poste.rilievi.sdp.api.service.api.IDacoService;
import it.poste.rilievi.sdp.api.service.api.IEvaluationService;
import it.poste.rilievi.sdp.api.service.api.IFunctionParamService;
import it.poste.rilievi.sdp.api.service.api.ISettingsService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.base.ContextStoreHolder;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class EvaluationServiceImpl extends ServiceBase implements IEvaluationService {

	private static Logger logger = LoggerFactory.getLogger(EvaluationServiceImpl.class);

	@Autowired
	IEvaluationDao evaluationDao;
	
	@Autowired
	IFunctionParamService paramService;
	
	@Autowired
	ISettingsService settings;
	
	@Autowired
	IDacoService dacoService;
	
	@Autowired
	IDacoDao dacoDao;

	@Auditable(operation = ServicesCatalog.NEW_EVALUATION,
			trace = "Inserito rilievo con Id=${(response.id)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.NEW_EVALUATION)
	public EvaluationDTO insertEvaluation(EvaluationDTO evaluation) {

		try {
			// Default value
			evaluation.setDeletedFlag(StorageRilievi.FLAG_NON_ANNULLATO);
			evaluation.setRealignedFlag(StorageRilievi.FLAG_NON_RIALLINEATO);
			evaluation.setRealignedStatus(null);
			evaluation.setRegularizationFlag(StorageRilievi.FLAG_NON_REGOLARIZZATO);
						
			EvaluationDTO response = _insertEvaluation(evaluation);
			logger.info("inserted the following evaluation: "+ response.toString());
			return response;
			
		} catch (ServiceException e) {
			logger.error("Error insertEvaluation ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error insertEvaluation Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	
	
	
	public EvaluationDTO _insertEvaluation(EvaluationDTO evaluation) {

			// SetTime
			evaluation.getInsertDate().with(getSysDate().toLocalTime());
			StorageRilievi rilievo = DTOConverter.toEntity(evaluation);

			AnagProfili profile = evaluationDao.findById(evaluation.getInsertUser().getProfile().getId(),
					AnagProfili.class);
			rilievo.setAnagProfili(profile);

			TipologDaco daco = evaluationDao.findById(evaluation.getDaco().getId(), TipologDaco.class);
			rilievo.setTipologDaco(daco);

			// Check servizio/daco
			if (evaluation.getService() != null) {
				TipologServizi servizio = evaluationDao.findById(evaluation.getService().getId(),
						TipologServizi.class);
				rilievo.setTipologServizi(servizio);
			}
			TipologFiliali filiale = evaluationDao.findById(evaluation.getBranch().getId(), TipologFiliali.class);
			rilievo.setTipologFiliali(filiale);

			TipologUffici ufficio = evaluationDao.findById(evaluation.getOffice().getId(), TipologUffici.class);
			rilievo.setTipologUffici(ufficio);

			TipologCausale causale = evaluationDao.findById(evaluation.getReason().getId(), TipologCausale.class);
			rilievo.setTipologCausale1(causale);
			if (evaluation.getPreviousReason() != null) {
				TipologCausale causalePrev = evaluationDao.findById(evaluation.getPreviousReason().getId(),
						TipologCausale.class);
				rilievo.setTipologCausale2(causalePrev);
			}
			TipologStatoRilievo notLossState = evaluationDao.findById(settings.getAppSettings().getUnLossState().getId(), TipologStatoRilievo.class);
			rilievo.setTipologStatoRilievo(notLossState);

			// Manage Attachment
			for (AttachmentDTO attachment : evaluation.getAttachments()) {
				StorageAllegati allegato = new StorageAllegati();
				allegato.setAttivo(StorageAllegati.ACTIVE);
				allegato.setContenuto(attachment.getContent().getBytes());
				allegato.setDataInserimento(Utils.localDateTimeToDate(getSysDate()));
				allegato.setNomeAllegato(attachment.getName());
				allegato.setUtenteInserimento(evaluation.getInsertUser().getCode());
				rilievo.addStorageAllegati(allegato);
			}

			evaluationDao.insertEvaluation(rilievo);
			EvaluationDTO response = DTOConverter.toDto(rilievo);

			return response;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_EVALUATION,
			trace = "Recuperato rilievo con Id=${(evaluationId)!'?'}")
	public EvaluationDTO getEvaluation(Long evaluationId) {
		logger.info("find evaluation with id: " + evaluationId);		
		StorageRilievi evalEnt = evaluationDao.findById(evaluationId, StorageRilievi.class);
		if( evalEnt == null) {
			logger.info("Evaluation Not Found");		
			return null;
		}else {
			EvaluationDTO ret = DTOConverter.toDto(evalEnt);
			logger.info("Evaluation Founded :" +ret.toString());		
			return ret;
		}
	}

	@Transactional
	@Auditable(operation = ServicesCatalog.DELETE_EVALUATION,
			trace = "Cancellato rilievo con Id=${(id)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.DELETE_EVALUATION)
	public EvaluationDTO deleteEvaluation(Long id) {
		try {
			StorageRilievi evaluationItem = evaluationDao.findById(id, StorageRilievi.class);
		
			evaluationItem.setDataAnnullamento(Utils.localDateTimeToDate(getSysDate()));
			evaluationItem.setFlagAnnullato(StorageRilievi.FLAG_ANNULLATO);
			evaluationDao.deleteEvaluation(evaluationItem);
			
			if( evaluationItem.getRifRilievoPadre() != null ) {						
				Long idEvalLinked = evaluationItem.getRifRilievoPadre();
				logger.info("DeleteEvaluation cancellazione rilievo padre:" + idEvalLinked);
				StorageRilievi evaluationItemLink = evaluationDao.findById(idEvalLinked, StorageRilievi.class);
				evaluationItemLink.setDataAnnullamento(Utils.localDateTimeToDate(getSysDate()));
				evaluationItemLink.setFlagAnnullato(StorageRilievi.FLAG_ANNULLATO);
				evaluationDao.deleteEvaluation(evaluationItemLink);			
			}
			logger.info("evaluation deleted with id: " + id);
			return DTOConverter.toDto(evaluationItem);
		} catch (ServiceException e) {
			logger.error("Error deleteEvaluation ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error deleteEvaluation Generic Exception...", e);
			throw new ServiceException(e);
		}
	}


	@Transactional
	@Auditable(operation = ServicesCatalog.SET_LOSS_STATE,
			trace = "Connotato a perdita il rilievo con Id=${(idevaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.SET_LOSS_STATE)
	public EvaluationDTO setLossState(Long idEvaluation, EvaluationStatusDTO status) {

		try {
			StorageRilievi evaluationItem = evaluationDao.findById(idEvaluation, StorageRilievi.class);	
			TipologStatoRilievo statusEnt = evaluationDao.findById(status.getId(), TipologStatoRilievo.class);
			evaluationItem.setTipologStatoRilievo(statusEnt);
			evaluationDao.update(evaluationItem);
			logger.info("set loss state for evaluation:" + idEvaluation);
			return  DTOConverter.toDto(evaluationItem);
		} catch (ServiceException e) {
			logger.error("Error setLossState ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error setLossState Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Override
	@Auditable(operation = ServicesCatalog.CHANGE_EVALUATION_FRACTIONAL,
			trace = "Associato il frazionario con Id=${(office.id)!'?'}\" al rilievo con Id=${(idevaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.CHANGE_EVALUATION_FRACTIONAL)
	public EvaluationDTO changeFractional(OfficeDTO office, Long evaluationId) {
		try {
			logger.info("changing fractional for evaluation Id: "+ evaluationId + " - fractional:" + office.toString());
			StorageRilievi rilievo = evaluationDao.findById(evaluationId, StorageRilievi.class);
			TipologUffici newOffice = evaluationDao.findById(office.getId(), TipologUffici.class);
			rilievo.setTipologUffici(newOffice);
			evaluationDao.updateFractional(rilievo);
			EvaluationDTO ret = DTOConverter.toDto(rilievo);
			logger.info("fractional changed!");
			return ret;

		} catch (ServiceException e) {
			logger.error("Error changeFractional ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error changeFractional Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.REGULARIZATION,
			trace = "Regolarizzato il rilievo con Id=${(evaluationId)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.REGULARIZATION)
	public EvaluationDTO setRegularization(Long evaluationId, LocalDate accountDate, RegularizationType type) {
		try {
			logger.info("start regularization for evaluation Id: "+evaluationId +  " - type:" + type.name()+" Account Date:" + accountDate);
			StorageRilievi rilievo = evaluationDao.findById(evaluationId, StorageRilievi.class);
			rilievo.setFlagRegolarizzato(StorageRilievi.FLAG_REGOLARIZZATO);
			rilievo.setDataRegolarizzazione(Utils.localDateToDate(accountDate));
			rilievo.setRegolarizzatoDaUtente(type.name());
			evaluationDao.update(rilievo);
			EvaluationDTO ret = DTOConverter.toDto(rilievo);
			logger.info("end regularization: " + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error setRegularization ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error setRegularization Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	
	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.UNDO_REGULARIZATION,
			trace = "Annullata regolarizzazione per il rilievo con Id=${(evaluationId)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.UNDO_REGULARIZATION)
	public EvaluationDTO undoRegularization(Long evaluationId) {
		try {
			logger.info("start undo regularization for evaluation Id: "+evaluationId);
			StorageRilievi rilievo = evaluationDao.findById(evaluationId, StorageRilievi.class);
			rilievo.setFlagRegolarizzato(StorageRilievi.FLAG_NON_REGOLARIZZATO);
			rilievo.setDataRegolarizzazione(null);
			rilievo.setRegolarizzatoDaUtente(null);
			evaluationDao.update(rilievo);
			EvaluationDTO ret = DTOConverter.toDto(rilievo);
			logger.info("end undo regularization:" + ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error undoRegularization ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error undoRegularization Generic Exception...", e);
			throw new ServiceException(e);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_EVALUTATIONS,
			trace = "Recupero lista rilievi")
	public PaginableResultSearchImpl<EvaluationDTO> getEvaluations(EvaluationCriteriaDTO criteria) {
		try {
			PaginableResultSearchImpl<EvaluationDTO> result = new PaginableResultSearchImpl<EvaluationDTO>(criteria);
			
			if( criteria.getDacos() == null ) {
				logger.info("getEvaluations nessuan Daco nei criteri di ricerca. Recupero le Daco associate la profilo.");
				List<DacoDTO> dacoList = dacoService.getDacoUserProfile(ContextStoreHolder.getContextStore().getAppUser().getProfile().getId());
				criteria.setDacos(dacoList);
			}

			logger.info("Criteria: " + criteria.toString());
			int totalRows = evaluationDao.searchEvaluationsCount(criteria);
			List<StorageRilievi> evalList = evaluationDao.searchEvaluations(criteria);
			evalList.stream().forEach(evaluationEnt -> {
				result.addItem(DTOConverter.toDto(evaluationEnt));
			});
			result.setTotalRows(totalRows);
			result.setPageNumber(criteria.getPageNumber());
			result.setPageSize(criteria.getPageSize());

			logger.info("found {} evaluations, returned page {} with page size", totalRows, criteria.getPageNumber(), criteria.getPageSize() );
			return result;
		} catch (ServiceException e) {
			logger.error("Error getEvaluations ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getEvaluations Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_EVALUTATIONS,
			trace = "Recupero lista rilievi inseriti in giornata")
	public PaginableResultSearchImpl<EvaluationDTO> getDailyEvaluations(EvaluationCriteriaDTO criteria) {
		LocalDateTime now = getSysDate();
		criteria.setInsertDateFrom(now.toLocalDate());
		criteria.setInsertDateTo(now.toLocalDate());
		return getEvaluations(criteria);
	}


	
	private ExportTableDTO getEvaluationExport(List<EvaluationDTO> evalList, ExportType selectExport, String fileName) {

		try {
			String FILE_NAME = fileName;
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Evaluations");
			Font font = workbook.createFont();
		    font.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
		    
			CreationHelper createHelper = workbook.getCreationHelper();
		

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			
			Cell cell = row.createCell(colNum++);

			cell.setCellValue("Nr.Rilievo");
			cell = row.createCell(colNum++);
			cell.setCellValue("Nr.Generato");
			cell = row.createCell(colNum++);			
			cell.setCellValue("Data Inserimento");
			cell = row.createCell(colNum++);
			cell.setCellValue("Data Competenza");
			cell = row.createCell(colNum++);
			cell.setCellValue("Frazionario");
			cell = row.createCell(colNum++);
			cell.setCellValue("Voce Daco");
			cell = row.createCell(colNum++);
			cell.setCellValue("Importo");
			
			if( ExportType.UserDailyEvaluation.equals(selectExport) ) {
				cell = row.createCell(colNum++);
				cell.setCellValue("Causale");
			}else {
				cell = row.createCell(colNum++);
				cell.setCellValue("Data regolarizzazione");
				
				cell = row.createCell(colNum++);
				cell.setCellValue("Codice Voce Daco");				
				
				cell = row.createCell(colNum++);
				cell.setCellValue("Voce Daco");				
				
				cell = row.createCell(colNum++);
				cell.setCellValue("Conto contabile");
				
				cell = row.createCell(colNum++);
				cell.setCellValue("Descrizione Conto contabile");
				
				cell = row.createCell(colNum++);
				cell.setCellValue("Data Contabile");
			}
			
			
			cell = row.createCell(colNum++);
			cell.setCellValue("Data Cancellazione");

			for (EvaluationDTO eva : evalList) {
				
				boolean deleted = eva.getDeleteDate() != null;
				
				CellStyle cellStyleDate = workbook.createCellStyle();
				cellStyleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
				if(deleted) {
					cellStyleDate.setFont(font);
				}
				
				CellStyle cellStyleDeleted = workbook.createCellStyle();
				if(deleted) {
					cellStyleDeleted.setFont(font);
				}
				
				
				row = sheet.createRow(rowNum++);
				colNum = 0;

				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getId());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				if(eva.getIdChild() != null) {
					cell.setCellValue(eva.getIdChild());
				}else {
					cell.setCellValue((String)null);
				}
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(Utils.localDateTimeToDate(eva.getInsertDate()));
				cell.setCellStyle(cellStyleDate);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(Utils.localDateToDate(eva.getCompetenceDate()));
				cell.setCellStyle(cellStyleDate);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getOffice().getFractionalDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getDaco().getDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);				
				Double amount = null;
				if (eva.getDebitAmount() != null) {
					amount = eva.getDebitAmount();
				}else
					if (eva.getCreditAmount() != null) {
						amount = eva.getCreditAmount();
					}
				cell.setCellValue(amount);
				cell.setCellStyle(cellStyleDeleted);
				
				
				if( ExportType.UserDailyEvaluation.equals(selectExport) ) {
					cell = row.createCell(colNum++);
					cell.setCellValue(eva.getReason().getDescription());
					cell.setCellStyle(cellStyleDeleted);
				}else {
					
					cell = row.createCell(colNum++);
					cell.setCellValue(Utils.localDateTimeToDate(eva.getRegularizationDate()));
					cell.setCellStyle(cellStyleDate);
					
					cell = row.createCell(colNum++);
					cell.setCellValue(eva.getDaco().getCode());
					cell.setCellStyle(cellStyleDeleted);
					
					cell = row.createCell(colNum++);
					cell.setCellValue(eva.getDaco().getDescription());
					cell.setCellStyle(cellStyleDeleted);
					
					cell = row.createCell(colNum++);
					cell.setCellValue(eva.getDaco().getAccountNumber());
					cell.setCellStyle(cellStyleDeleted);
					
					cell = row.createCell(colNum++);
					cell.setCellValue(eva.getDaco().getAccountDescription());
					cell.setCellStyle(cellStyleDeleted);
					
					cell = row.createCell(colNum++);
					cell.setCellValue(Utils.localDateToDate(eva.getAccountDate()));
					cell.setCellStyle(cellStyleDate);
				}
				
				cell = row.createCell(colNum++);				
				cell.setCellValue(eva.getDeleteDate()!=null?Utils.localDateTimeToDate(eva.getDeleteDate()):(Date)null);
				cell.setCellStyle(cellStyleDate);

			}
			for (int i = 0; i < colNum; i++) {
				sheet.autoSizeColumn(i);
			}		

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			workbook.write(outputStream);
			workbook.close();

			ExportTableDTO export = new ExportTableDTO();
			export.setFileName(FILE_NAME);
			export.setContent(new String(Base64Utils.encode(outputStream.toByteArray())));
			export.setContentType("application/vnd.ms-excel");
			
			return export;
			
		} catch (ServiceException e) {
			logger.error("Error getAllEvaluationExport ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getAllEvaluationExport Generic Exception...", e);
			throw new ServiceException(e);
		}
	}
	private ExportTableDTO getMaxiEvaluationExport(List<EvaluationDTO> evalList,String fileName) {

		try {
			String FILE_NAME = fileName;
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Evaluations");
			Font font = workbook.createFont();
		    font.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
		    
			CreationHelper createHelper = workbook.getCreationHelper();
		

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			
			Cell cell = row.createCell(colNum++);

			cell.setCellValue("Nr.Rilievo");
			cell = row.createCell(colNum++);
			cell.setCellValue("Data Inserimento");
			cell = row.createCell(colNum++);			
			cell.setCellValue("Data Competenza");
			cell = row.createCell(colNum++);
			cell.setCellValue("Filiale");
			cell = row.createCell(colNum++);
			cell.setCellValue("Frazionario");
			cell = row.createCell(colNum++);
			cell.setCellValue("Voce Daco");
			cell = row.createCell(colNum++);
			cell.setCellValue("Causale");
			cell = row.createCell(colNum++);
			cell.setCellValue("Debito");
			cell = row.createCell(colNum++);
			cell.setCellValue("Credito");	
			cell = row.createCell(colNum++);
			cell.setCellValue("Data Cancellazione");

			for (EvaluationDTO eva : evalList) {
				
				boolean deleted = eva.getDeleteDate() != null;
				
				CellStyle cellStyleDate = workbook.createCellStyle();
				cellStyleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
				if(deleted) {
					cellStyleDate.setFont(font);
				}
				
				CellStyle cellStyleDeleted = workbook.createCellStyle();
				if(deleted) {
					cellStyleDeleted.setFont(font);
				}
				
				
				row = sheet.createRow(rowNum++);
				colNum = 0;

				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getId());
				cell.setCellStyle(cellStyleDeleted);
				
				
				cell = row.createCell(colNum++);
				cell.setCellValue(Utils.localDateTimeToDate(eva.getInsertDate()));
				cell.setCellStyle(cellStyleDate);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(Utils.localDateToDate(eva.getCompetenceDate()));
				cell.setCellStyle(cellStyleDate);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getBranch().getDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getOffice().getFractionalDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getDaco().getDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				cell.setCellValue(eva.getReason().getDescription());
				cell.setCellStyle(cellStyleDeleted);
				
				cell = row.createCell(colNum++);
				if(eva.getDebitAmount()!= null) {
					Double amountD = eva.getDebitAmount();				
					cell.setCellValue(amountD);
					cell.setCellStyle(cellStyleDeleted);	
				}
				
				
				cell = row.createCell(colNum++);
				if(eva.getCreditAmount() != null) {
					Double amountC = eva.getCreditAmount();				
					cell.setCellValue(amountC);
					cell.setCellStyle(cellStyleDeleted);	
				}
				
				cell = row.createCell(colNum++);				
				cell.setCellValue(eva.getDeleteDate()!=null?Utils.localDateTimeToDate(eva.getDeleteDate()):(Date)null);
				cell.setCellStyle(cellStyleDate);

			}
			for (int i = 0; i < colNum; i++) {
				sheet.autoSizeColumn(i);
			}		

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			workbook.write(outputStream);
			workbook.close();

			ExportTableDTO export = new ExportTableDTO();
			export.setFileName(FILE_NAME);
			export.setContent(new String(Base64Utils.encode(outputStream.toByteArray())));
			export.setContentType("application/vnd.ms-excel");
			
			return export;
			
		} catch (ServiceException e) {
			logger.error("Error getAllEvaluationExport ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getAllEvaluationExport Generic Exception...", e);
			throw new ServiceException(e);
		}
	}
	@Override
	@Auditable(operation = ServicesCatalog.RECLASSIFICATION_DACO,
			trace = "Riclassificazione voce daco del rilievo con Id=${(response.id)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.RECLASSIFICATION_DACO)
	public EvaluationDTO reclassificationDacoEvaluation(EvaluationDTO reclassification) {
		try {
			logger.info("start daco reclassification:"+ reclassification.toString());

			// Default value Origine
			reclassification.setDeletedFlag(StorageRilievi.FLAG_NON_ANNULLATO);
			reclassification.setRealignedFlag(StorageRilievi.FLAG_NON_RIALLINEATO);
			reclassification.setRealignedStatus(null);
			reclassification.setRegularizationFlag(StorageRilievi.FLAG_REGOLARIZZATO);
			reclassification.setRegularizationDate(getSysDate());
			reclassification.setRegularizationFromUser(RegularizationType.R.name());
			reclassification.setDescription("RA – Origine " + reclassification.getDescription());
			
			// Default value Generated
			reclassification.getGenerated().setDeletedFlag(StorageRilievi.FLAG_NON_ANNULLATO);
			reclassification.getGenerated().setRealignedFlag(StorageRilievi.FLAG_NON_RIALLINEATO);
			reclassification.getGenerated().setRealignedStatus(null);
			reclassification.getGenerated().setRegularizationFlag(StorageRilievi.FLAG_REGOLARIZZATO);
			reclassification.getGenerated().setRegularizationDate(getSysDate());
			reclassification.getGenerated().setRegularizationFromUser(RegularizationType.R.name());			
			reclassification.getGenerated().setDescription("RA – Generato in automatico da Riclassifica " + reclassification.getGenerated().getDescription());
			
			EvaluationDTO retParent = this._insertEvaluation(reclassification);
			EvaluationDTO retChild = this._insertEvaluation(reclassification.getGenerated());

			StorageRilievi evalParent = evaluationDao.findById(retParent.getId(), StorageRilievi.class);
			StorageRilievi evalChild = evaluationDao.findById(retChild.getId(), StorageRilievi.class);
			evalParent.setRifRilievoFiglio(evalChild.getIdRilievi());
			evalChild.setRifRilievoPadre(evalParent.getIdRilievi());

			evaluationDao.update(evalParent);
			evaluationDao.update(evalParent);
			EvaluationDTO ret = DTOConverter.toDto(evalParent);
			ret.setGenerated(DTOConverter.toDto(evalChild));
			logger.info("end daco reclassification:"+ ret.toString());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error reclassificationDacoEvaluation ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error reclassificationDacoEvaluation Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Transactional
	@Auditable(operation = ServicesCatalog.UNDO_LOSS_STATE,
			trace = "Annullo connotazione a perdita per il rilievo con Id=${(idevaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.UNDO_LOSS_STATE)
	public EvaluationDTO undoLossState(Long idEvaluation) {

		try {
			logger.info("undo loss state for evaluation id :" +idEvaluation);
			StorageRilievi evaluationItem = evaluationDao.findById(idEvaluation, StorageRilievi.class);
			TipologStatoRilievo notLossState = evaluationDao.findById(settings.getAppSettings().getUnLossState().getId(), TipologStatoRilievo.class);
			evaluationItem.setTipologStatoRilievo(notLossState);
			evaluationDao.update(evaluationItem);
			EvaluationDTO ret = DTOConverter.toDto(evaluationItem);	
			logger.info("end undo loss state");
			return  ret;
		} catch (ServiceException e) {
			logger.error("Error undoLossState ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error undoLossState Generic Exception...", e);
			throw new ServiceException(e);
		}

	}
	
	
	
	@Override
	@Transactional
	@Auditable(operation = ServicesCatalog.SET_RC_REASON,
			trace = "Impostata causale RC per il rilievo con Id=${(idEvaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.SET_RC_REASON)
	public EvaluationDTO setRCReason(Long idEvaluation) {
		 try{
		 	logger.info("set RC reason for evaluation Id:" + idEvaluation);
		 	StorageRilievi rilievo = evaluationDao.findById(idEvaluation, StorageRilievi.class);
		 	//imposto il riferimento alla causale precedente
		 	rilievo.setTipologCausale2(rilievo.getTipologCausale1());
		 	//imposto la causale di tipo RC
		 	TipologCausale reason = evaluationDao.findById(settings.getRcReason().getId(), TipologCausale.class);
		 	rilievo.setTipologCausale1(reason);
			evaluationDao.update(rilievo);
			return DTOConverter.toDto(rilievo);
		} catch (ServiceException e) {
			logger.error("Error setRCReason ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error setRCReason Generic Exception...", e);
			throw new ServiceException(e);
		}
		
	}
	
	@Transactional
	@Auditable(operation = ServicesCatalog.UNDO_RC_REASON,
			trace = "Annullo causale RC per il rilievo con Id=${(idEvaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.UNDO_RC_REASON)
	public EvaluationDTO undoRcReason(Long idEvaluation) {

		try {
			logger.info("start undo Rc reason  for evaluation Id:" + idEvaluation);
			StorageRilievi evaluationItem = evaluationDao.findById(idEvaluation, StorageRilievi.class);
			evaluationItem.setTipologCausale1(evaluationItem.getTipologCausale2());			
			evaluationItem.setTipologCausale2(null);
			evaluationDao.update(evaluationItem);
			return DTOConverter.toDto(evaluationItem);
		} catch (ServiceException e) {
			logger.error("Error undoRcReason ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error undoRcReason Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

	@Transactional
	@Auditable(operation = ServicesCatalog.SET_EVALUATION_DESCRIPTION,
			trace = "Aggiornata descrizione del rilievo con Id=${(idEvaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.SET_EVALUATION_DESCRIPTION)
	public EvaluationDTO setDescription(EvaluationDTO evaluation, Long idEvaluation) {
		try {
			logger.info("set description for evaluation Id:" + idEvaluation + " - new description: " + evaluation.getDescription());
			StorageRilievi evaluationItem = evaluationDao.findById(idEvaluation, StorageRilievi.class);
			evaluationItem.setDescrizione(evaluation.getDescription());
			evaluationDao.update(evaluationItem);
			return DTOConverter.toDto(evaluationItem);
		} catch (ServiceException e) {
			logger.error("Error setDescription ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error setDescription Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.FILE_EXPORT,
		trace = "Export su file dei rilievi inseriti in giornata")
	public ExportTableDTO exportUserDailyEvaluations(String userCode) {
		String fileName = "dailyEvaluation_"+DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(getSysDate())+"_"+userCode+".xlsx";
		EvaluationCriteriaDTO criteria = new EvaluationCriteriaDTO();
		criteria.setAppUserCode(userCode);
		PaginableResultSearchImpl<EvaluationDTO> evalRes = getDailyEvaluations(criteria);
		return getEvaluationExport(evalRes.getItems(),ExportType.UserDailyEvaluation,fileName);
	}
	
	

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.FILE_EXPORT,
		trace = "Export su file dei rilievi consultati")
	public ExportTableDTO exportEvaluations(EvaluationCriteriaDTO criteria) {
		String fileName= "exportEvaluation_"+ DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(getSysDate()) + ".xlsx";		
		PaginableResultSearchImpl<EvaluationDTO> evalRes = getEvaluations(criteria);
		return getEvaluationExport(evalRes.getItems(), ExportType.Evaluations, fileName);
	}

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.FILE_EXPORT,
		trace = "Export su file dei Maxi rilievi")
	public ExportTableDTO exportMaxiEvaluations(EvaluationCriteriaDTO criteria) {
		criteria.setPageNumber(null);
		criteria.setPageSize(null);
		String fileName= "exportMaxiEvaluation_"+ DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(getSysDate()) + ".xlsx";		
		PaginableResultSearchImpl<EvaluationDTO> evalRes = getNotRegularizedEvaluations(criteria);
		return getMaxiEvaluationExport(evalRes.getItems(), fileName);
	}


	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_EVALUATION_MAXI,
		trace = "Recupera lista rilievi ancora non regolarizzati, per funzione regolarizzazione maxi rilievi")
	public PaginableResultSearchImpl<EvaluationDTO> getNotRegularizedEvaluations(EvaluationCriteriaDTO request) {
		EvaluationCriteriaDTO criteria = new EvaluationCriteriaDTO();
		criteria.addReason(settings.getRsReason());
		criteria.addReason(settings.getRdReason());
		criteria.setDacos(request.getDacos());
		criteria.setShowRemoved(false);
		criteria.setInsertDateTo(request.getInsertDateTo());
		criteria.setInsertDateFrom(request.getInsertDateFrom());
		criteria.setAmountFrom(request.getAmountFrom());
		criteria.setAmountTo(request.getAmountTo());	
		criteria.setType(SearchType.NOT_REGULARIZED);
		logger.info("getting not regularized evaluations...");
		return this.getEvaluations(criteria);
	}


	@Transactional
	@Auditable(operation = ServicesCatalog.UPDATE_EVALUTATION_DETAIL,
			trace = "Aggiornato il rilievo con Id=${(idEvaluation)!'?'}")
	@Valid(value = EvaluationValidator.class, operation = ServicesCatalog.UPDATE_EVALUTATION_DETAIL)
	public EvaluationDTO modEvaluation(EvaluationDTO request,Long idEvaluation) {
		try {
			logger.info("set new parameter values for evaluation Id:" + idEvaluation);
			StorageRilievi evaluationItem = evaluationDao.findById(idEvaluation, StorageRilievi.class);
			StorageRilievi entityStorage = DTOConverter.toEntity(request);
			if(entityStorage.getDataCompetenza()!= null) {
				evaluationItem.setDataCompetenza(entityStorage.getDataCompetenza());	
			}
			TipologDaco dacoItem = dacoDao.findById(request.getDaco().getId(), TipologDaco.class);
			if(dacoItem !=null) {
				evaluationItem.setTipologDaco(dacoItem);	
			}
			if(entityStorage.getTipologServizi()!=null) {
				evaluationItem.setTipologServizi(entityStorage.getTipologServizi());
			}
			
			if(entityStorage.getCreditiEuro()!= null) {
				evaluationItem.setCreditiEuro(entityStorage.getCreditiEuro());
				evaluationItem.setDebitiEuro(null);
			}else {
			evaluationItem.setDebitiEuro(entityStorage.getDebitiEuro());
			evaluationItem.setCreditiEuro(null);
			 }
			
			if(entityStorage.getDescrizione()!=null) {
				evaluationItem.setDescrizione(entityStorage.getDescrizione());
				}else {}
			if(entityStorage.getTipologUffici()!=null) {
				evaluationItem.setTipologUffici(entityStorage.getTipologUffici());	
			}
			if(entityStorage.getTipologFiliali()!=null) {
				evaluationItem.setTipologFiliali(entityStorage.getTipologFiliali());
			}
			evaluationDao.update(evaluationItem);
			return DTOConverter.toDto(evaluationItem);
		} catch (ServiceException e) {
			logger.error("Error modEvaluation ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error modEvaluation Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	
}
