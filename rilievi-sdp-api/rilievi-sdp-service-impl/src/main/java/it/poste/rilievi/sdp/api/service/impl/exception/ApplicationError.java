package it.poste.rilievi.sdp.api.service.impl.exception;

public class ApplicationError {

	private String code;
	private String message;

	public ApplicationError(String cod, String mess) {
		this.code = cod;
		this.message = mess;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}