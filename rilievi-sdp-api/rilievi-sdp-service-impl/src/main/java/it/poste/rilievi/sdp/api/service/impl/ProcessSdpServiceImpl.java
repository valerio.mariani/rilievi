package it.poste.rilievi.sdp.api.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.OfficeDTO;
import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.StorageProcessiSdp;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.model.entity.TipologUffici;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IProcessSdpDao;
import it.poste.rilievi.sdp.api.service.api.IOfficeService;
import it.poste.rilievi.sdp.api.service.api.IProcessSdpService;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class ProcessSdpServiceImpl extends ServiceBase implements IProcessSdpService {
	
private static Logger logger = LoggerFactory.getLogger(ProcessSdpServiceImpl.class);
	
	@Autowired
	IProcessSdpDao processDao;
	
	@Autowired
	IOfficeService officeService;
	
	@Override
	@Transactional
	@Valid(operation = ServicesCatalog.INSERT_PROCESS_SDP,value = ProcessSDPValidator.class)
	public ProcessSDPDTO insertProcess(ProcessSDPDTO process) {
		try {	
			logger.info("Start insertProcess:" + process.toString());
			StorageProcessiSdp processoEnt = DTOConverter.toEntity(process);
			StorageRilievi rilievo = processDao.findById(process.getEvaluationId(), StorageRilievi.class);
			processoEnt.setStorageRilievi(rilievo);
			TipologUffici ufficio = processDao.findById(process.getOffice().getId(), TipologUffici.class);
			processoEnt.setFrazionario(ufficio.getFrazionario());
			processDao.insert(processoEnt);			
			ProcessSDPDTO ret =  DTOConverter.toDto(processoEnt);
			logger.info("End insertProcess: " + ret.toString());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error  insertProcess ServiceException:" + e.getMessage());
			throw e;
		}catch (Exception e) {
			logger.error("Error  insertProcess Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public ProcessSDPDTO getProcessSDP(String operNumber, String fractional, String branchNumber, LocalDate contabilizationDate) {
		try {	
			
			logger.info("Start getProcessSDP OperationNumber:" +operNumber + "-Fractional: " + fractional + " -BranchNumber:" 
					+ branchNumber + "-ContabilizationDate:" +DateTimeFormatter.ISO_LOCAL_DATE.format(contabilizationDate));
			StorageProcessiSdp processoEnt = processDao.getProcessByOperationNum(operNumber, fractional, branchNumber, Utils.localDateToDate(contabilizationDate));
			if( processoEnt == null) {
				return null;
			}
			ProcessSDPDTO ret=  DTOConverter.toDto(processoEnt);
			if(ret.getOffice() != null && StringUtils.isNotEmpty(ret.getOffice().getFractional())) {
				OfficeDTO office = officeService.getOfficeByFractional(ret.getOffice().getFractional());
				ret.setOffice(office);
			}
			return ret;
		}catch (ServiceException e) {
			logger.error("Error  getProcessSDP ServiceException:" + e.getMessage());
			throw e;
		}catch (Exception e) {
			logger.error("Error  getProcessSDP Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public ProcessSDPDTO invalidateProcess(String operNumber, String fractional, String branchNumber, LocalDate contabilizationDate) {
		try {	
			logger.info("Search Process OperationNumber:" +operNumber + "-Factional: " + fractional + " -BranchNumber:" 
					+ branchNumber + "-ContabilizationDate:" +DateTimeFormatter.ISO_LOCAL_DATE.format(contabilizationDate));
			StorageProcessiSdp processoEnt = processDao.getProcessByOperationNum(operNumber, fractional, branchNumber, Utils.localDateToDate(contabilizationDate));
			if(processoEnt == null ) {
				throw new ServiceException("Processo con Numero operazione "+operNumber +" non esistente.");
			}
			processoEnt.setFlagValidita(StorageProcessiSdp.FLAG_NON_VALIDO);
			processoEnt.setDataFineValidita(Utils.localDateTimeToDate(getSysDate()));
			processDao.invalidProcess(processoEnt);
			ProcessSDPDTO ret = DTOConverter.toDto(processoEnt);
			if(ret.getOffice() != null && StringUtils.isNotEmpty(ret.getOffice().getFractional())) {
				OfficeDTO office = officeService.getOfficeByFractional(ret.getOffice().getFractional());
				ret.setOffice(office);
			}
			return ret;
		}catch (ServiceException e) {
			logger.error("Error  invalidateProcess ServiceException:" + e.getMessage());
			throw e;
		}catch (Exception e) {
			logger.error("Error  invalidateProcess Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
	
	
}
