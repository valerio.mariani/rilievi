package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.AttachmentCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.AttachmentDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.StorageAllegati;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IAttachmentDao;
import it.poste.rilievi.sdp.api.repository.IEvaluationDao;
import it.poste.rilievi.sdp.api.service.api.IAttachmentService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class AttachmentServiceImpl extends ServiceBase implements IAttachmentService{
	
	private static Logger logger = LoggerFactory.getLogger(AttachmentServiceImpl.class);

	@Autowired
	IAttachmentDao attachmentDao;

	@Autowired
	IEvaluationDao evaluationDao;

	@Auditable(operation = ServicesCatalog.GET_EVALUATION_ATTACHMENT,
			trace = "Recuperati allegati per il rilievo con Id=${(evaluationId)!'?'}")
	public PaginableResultSearchImpl<AttachmentDTO> getAttachmentByIdEvaluation(AttachmentCriteriaDTO request, Long evaluationId) {
		try {
			logger.info("search attachments for evaluation: " + evaluationId);
			int totalRows = attachmentDao.countAttachmentById(request, evaluationId);
			List<StorageAllegati> attachmentItems = attachmentDao.getAttachmentById(request, evaluationId);
			PaginableResultSearchImpl<AttachmentDTO> ret  = new PaginableResultSearchImpl<AttachmentDTO>(request);
			attachmentItems.parallelStream().forEach(storeAllegatiEnt -> {
				ret.addItem(DTOConverter.toDto(storeAllegatiEnt));
			});
			ret.setTotalRows(totalRows);
			ret.setPageNumber(request.getPageNumber());
			ret.setPageSize(request.getPageSize());
			logger.info("found {} attachments, returned page {}, with size {}", totalRows, request.getPageNumber(), request.getPageSize());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error getAttachmentByIdEvaluation ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getAttachmentByIdEvaluation Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.DELETE_EVALUATION_ATTACHMENT,
			trace = "Cancellato allegato con Id=${(attachmentId)!'?'}")
	public AttachmentDTO deleteAttachmentById(Long attachmentId, String user) {
		try {
			StorageAllegati attachmentItem = attachmentDao.findById(attachmentId, StorageAllegati.class);
			attachmentItem.setAttivo(StorageAllegati.NOT_ACTIVE);
			attachmentItem.setUtenteRimozione(user);
			attachmentItem.setDataRimozione(Utils.localDateTimeToDate(getSysDate()));
			attachmentDao.deleteAttachment(attachmentItem);
			logger.info("deleted attachment with id: " + attachmentId);
			return DTOConverter.toDto(attachmentItem);			
		}catch (ServiceException e) {
			logger.error("Error deleteAttachment ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error deleteAttachment Generic Exception...",e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.INSERT_EVALUATION_ATTACHMENT,
			trace = "Inserito allegato con Id=${(response.id)!'?'} per il rilievo con Id=${(evaluationId)!'?'}")
	@Valid(value = AttachmentValidator.class, operation = ServicesCatalog.INSERT_EVALUATION_ATTACHMENT)
	public AttachmentDTO insertAttachmentById(AttachmentDTO insertAttachment, Long evaluationId) {
		try {
			
			insertAttachment.setInsertDate(this.getSysDate());
			insertAttachment.setActive(true);
			StorageAllegati attachmentEnt = DTOConverter.toEntity(insertAttachment);

			StorageRilievi evaluation = evaluationDao.findById(evaluationId, StorageRilievi.class);
			attachmentEnt.setStorageRilievi(evaluation);
			attachmentDao.insertAttachment(attachmentEnt);
			
			if(evaluation.getRifRilievoFiglio() != null || evaluation.getRifRilievoPadre() != null) {
				
				Long idEvalLinked = evaluation.getRifRilievoFiglio() != null ? evaluation.getRifRilievoFiglio() : evaluation.getRifRilievoPadre();
				StorageRilievi evaluationLinked = evaluationDao.findById(idEvalLinked, StorageRilievi.class);
				StorageAllegati attachmentEntLinked = DTOConverter.toEntity(insertAttachment);
				attachmentEntLinked.setStorageRilievi(evaluationLinked);
				attachmentDao.insertAttachment(attachmentEntLinked);				
			}
			logger.info("inserted new attachment with id: " + attachmentEnt.getIdAllegati());
			return DTOConverter.toDto(attachmentEnt);
		} catch (ServiceException e) {
			logger.error("Error insertAttachmentById ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error insertAttachmentById Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	
	@Override
	@Auditable(operation = ServicesCatalog.GET_EVALUATION_ATTACHMENT,
			trace = "Recuperato allegato con Id=${(attachmentId)!'?'}")
	public AttachmentDTO getAttachmentById(Long attachmentId) {
		try {
			StorageAllegati attachmentEnt = attachmentDao.getAttachmentById(attachmentId);
			logger.info("return attachment with id: " + attachmentId);
			return DTOConverter.toDto(attachmentEnt,true);
		} catch (ServiceException e) {
			logger.error("Error getAttachmenteById ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error getAttachmenteById Generic Exception...", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Auditable(operation = ServicesCatalog.INSERT_EVALUATION_ATTACHMENT,
		trace = "Inseriti nuovi allegati al rilievo con Id=${(evaluationId)!'?'}")
	public List<AttachmentDTO> insertAttachments(List<AttachmentDTO> insertAttachments, Long evaluationId) {
		List<AttachmentDTO> ret = new ArrayList<AttachmentDTO>();
		for(AttachmentDTO att : insertAttachments ) {
			ret.add(insertAttachmentById(att, evaluationId));
		}		
		return ret;
	}

}

