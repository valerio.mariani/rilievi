package it.poste.rilievi.sdp.api.service.impl.audit;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.AuditLogDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.api.IAuditService;
import it.poste.rilievi.sdp.api.service.impl.base.ContextStoreHolder;
import it.poste.rilievi.sdp.api.service.impl.conf.TraceBuilder;
import it.poste.rilievi.sdp.api.service.impl.util.AOPUtils;


@Component
@Aspect
public class AuditableAspect implements Ordered{
	private static final Logger logger = LoggerFactory.getLogger(AuditableAspect.class);
	
	
	@Autowired
	IAuditService auditService;
	
	@Autowired
	TraceBuilder traceBuilder;
	
	@Around("@annotation(it.poste.rilievi.sdp.api.service.impl.audit.Auditable)")
	public Object auditOperation(ProceedingJoinPoint joinPoint) throws Throwable {
		Object ret = joinPoint.proceed();
		audit(joinPoint,true, ret);
		return ret;		
	}
	
	@AfterThrowing(pointcut = "@annotation(it.poste.rilievi.sdp.api.service.impl.audit.Auditable)", throwing = "e" )
	public void auditOperation(JoinPoint joinPoint, Throwable e) throws Throwable {
		audit(joinPoint,false, null);
	}

	private void audit(JoinPoint joinPoint, boolean result, Object ret) throws Throwable {
		try{
			Auditable ann = AOPUtils.getAnnotation(joinPoint, Auditable.class);			
			 if (ann != null) {
				ServicesCatalog operation = ((Auditable)ann).operation();
				logger.debug("Operation: " + operation.name() + " Username: " + ContextStoreHolder.getContextStore().getAppUser().getCode());
				
				String className = joinPoint.getTarget().getClass().getName();
				String methodName = joinPoint.getSignature().getName();
				String templateKey = traceBuilder.getTemplateKey(className, methodName);
				
				String trace = traceBuilder.build(templateKey, getModel(joinPoint, ret));

				AuditLogDTO audit = new AuditLogDTO();
				audit.setResult(result ? "1" : "0");
				audit.setUser(ContextStoreHolder.getContextStore().getAppUser());
				audit.setServiceCode(operation);
				
				if (trace.length() > 500) {
					trace = StringUtils.substring(trace, 0, trace.length()-3) + " ...";
				}
				
				audit.setDescrAction(trace);
				auditService.insertAudit(audit);
			 }
		}catch (Exception e) {
			logger.error("Error Audit: " + e.getMessage(),e);
		}		
	}
	
	private Map<String, Object> getModel(JoinPoint joinPoint, Object ret) {
		// build the model
		Map<String, Object> map = new HashedMap<String, Object>();

		CodeSignature methodSignature = (CodeSignature) joinPoint.getSignature();
		String[] paramsName = methodSignature.getParameterNames();

		Object[] paramsValue = joinPoint.getArgs();

		for (int i = 0; i <= paramsName.length - 1; i++) {
			map.put(paramsName[i], paramsValue[i]);
		}

		map.put("response", ret);
		return map;
	}
	
	@Override
	public int getOrder() {
		return 8;
	}

}
