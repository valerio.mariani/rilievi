package it.poste.rilievi.sdp.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.repository.IEvaluationNoteDao;
import it.poste.rilievi.sdp.api.service.api.IEvaluationNoteService;
import it.poste.rilievi.sdp.api.service.impl.base.IValidator;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationError;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.sdp.api.service.impl.base.ValidatorBase;


@Component
public class EvaluationNoteValidator extends ValidatorBase  implements IValidator {
	
	
	@Autowired
	IEvaluationNoteService service;
	
	@Autowired
	IEvaluationNoteDao evaluationNoteDao;
	

	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... target) {

		switch (operation) {
			case NEW_EVALUATION_NOTE:
				EvaluationNoteDTO evaluationNote = (EvaluationNoteDTO) target[0];
				validateInsert(errors, evaluationNote);
				break;
			case DELETE_EVALUATION_NOTE:
				if (!ValidationUtils.rejectIfFalse(target != null && target.length > 0 && target[0] != null,
						"idEvaluationNote", resolveMessage("EvaluationNote.id.empty"), errors))
				break;
			default:
				break;
		}
	}


	private void validateInsert(ValidationError errors, EvaluationNoteDTO evaluationNote) {
		
		if( !ValidationUtils.rejectIfNull(evaluationNote.getEvaluation(), "idRilievo", resolveMessage("evaluationNote.Evaluation.empty"), errors)) {
			if(!ValidationUtils.rejectIfNull(evaluationNote.getEvaluation().getId(), "idRilievo", resolveMessage("evaluationNote.idRilievo.empty"), errors)) {
				StorageRilievi storageRilievi = evaluationNoteDao.findById(evaluationNote.getEvaluation().getId(), StorageRilievi.class);
				ValidationUtils.rejectIfNull(storageRilievi, "idRilievo", resolveMessage("evaluationNote.idRilievo.notExits"), errors);
			}
		}	
		ValidationUtils.rejectIfEmpty( evaluationNote.getNote(), "nota", resolveMessage("evaluationNote.nota.empty"), errors);
	
	}

}
