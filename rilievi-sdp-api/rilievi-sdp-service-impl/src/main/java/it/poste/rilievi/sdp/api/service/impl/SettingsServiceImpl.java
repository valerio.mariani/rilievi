package it.poste.rilievi.sdp.api.service.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.AppSettings;
import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.sdp.api.model.dto.ReasonDTO;
import it.poste.rilievi.sdp.api.model.entity.TipologCausale;
import it.poste.rilievi.sdp.api.model.entity.TipologFunzioni;
import it.poste.rilievi.sdp.api.model.entity.TipologParamFunzioni;
import it.poste.rilievi.sdp.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.sdp.api.repository.IEvaluationStatusDao;
import it.poste.rilievi.sdp.api.repository.IFunctionDao;
import it.poste.rilievi.sdp.api.repository.IFunctionParamDao;
import it.poste.rilievi.sdp.api.repository.IReasonDao;
import it.poste.rilievi.sdp.api.service.api.ISettingsService;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Transactional
@Service
public class SettingsServiceImpl implements ISettingsService {
	
	private static Logger logger = LoggerFactory.getLogger(SettingsServiceImpl.class);
	
	@Autowired
	IFunctionDao functionDao;
	
	@Autowired
	IFunctionParamDao functionParamDao;
	
	@Autowired
	IReasonDao reasonDao;
	
	@Autowired
	IEvaluationStatusDao statusDao;
	
	private ReasonDTO rcReason;
	
	private ReasonDTO rdReason;
	
	private ReasonDTO rsReason;
	
	private ReasonDTO raReason;
	
	private EvaluationStatusDTO unLossState;
	
	
	@PostConstruct
	void init() {
		//logica per valorizzazione 
		List<TipologCausale> reasonItems = reasonDao.getAllReason();
		
		reasonItems.stream().forEach(reasonEnt -> {
			if(reasonEnt.getCodiceCausale().equals("RS")) {
				rsReason = DTOConverter.toDto(reasonEnt);
			}
			if(reasonEnt.getCodiceCausale().equals("RC")) {
				rcReason = DTOConverter.toDto(reasonEnt);
			}
			if(reasonEnt.getCodiceCausale().equals("RD")) {
				rdReason= DTOConverter.toDto(reasonEnt);
			}
			if(reasonEnt.getCodiceCausale().equals("RA")) {
				raReason=DTOConverter.toDto(reasonEnt);
			}
			
		});
		
		TipologStatoRilievo stato = statusDao.getStatusByCode("788");
		if( stato != null ) {
			unLossState = DTOConverter.toDto(stato);
		}

	}
	
	

	@Override
	@Transactional(readOnly = true)
	public AppSettings getAppSettings() {
		try{
			AppSettings ret = new AppSettings();
			ret.setSysDate(getSysDate());
			ret.setRaReason(raReason);
			ret.setRcReason(rcReason);
			ret.setRdReason(rdReason);
			ret.setRsReason(rsReason);
			ret.setUnLossState(unLossState);
			
			List<TipologFunzioni> functionList = functionDao.getAllFunction(new FunctionCriteriaDTO());
			
			for(TipologFunzioni funz : functionList) {
				HashMap<String, FunctionParamDTO> parameters = new HashMap<>();
				FunctionDTO functDto = DTOConverter.toDto(funz);
				ret.getFunctions().add(functDto);
				
				List<TipologParamFunzioni> functionParamsList = functionParamDao.getAllFunctionParamById(new FunctionParamCriteriaDTO() ,funz.getIdFunzione());
				
					for(TipologParamFunzioni param : functionParamsList) {			            
						FunctionParamDTO paramDto = DTOConverter.toDto(param);			            
						parameters.put(paramDto.getNameparam(), paramDto);
				
						}	
					functDto.setParams(parameters);
			}
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getAppSettings ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getAppSettings Generic Exception...",e);
			throw new ServiceException(e);
		}		
	}


	@Override
	public LocalDateTime getSysDate() {
		return functionDao.getSysDate();
	}
	
	
	public ReasonDTO getRcReason() {
		return rcReason;
	}
	
	public ReasonDTO getRdReason() {
		return rdReason;
	}
	
	public ReasonDTO getRaReason() {
		return raReason;
	}
	
	public ReasonDTO getRsReason() {
		return rsReason;
	}

	public EvaluationStatusDTO getUnLossState() {
		return unLossState;
	}

}
