package it.poste.rilievi.sdp.api.service.impl.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

public class AOPUtils {
	
	@SuppressWarnings("unchecked")
	public static <T extends Annotation> T getAnnotation(JoinPoint joinPoint, Class<T> target) throws Throwable {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		for(Annotation ann : method.getAnnotations() ) {			
			 if (ann.annotationType().equals(target)) {
				return (T) ann;
			 }
		}
		return null;
	}
	
}
