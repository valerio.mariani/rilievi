package it.poste.rilievi.sdp.api.service.impl.base;

import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;

public interface IValidator {

	void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException;

}
