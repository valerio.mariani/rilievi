package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.ExtractionDatesDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.StorageDateEstrazione;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IExtractionDatesDao;
import it.poste.rilievi.sdp.api.service.api.IExtractionDatesService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.base.Valid;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;


@Service
@Transactional
public class ExtractionDatesServiceImpl extends ServiceBase implements IExtractionDatesService {

	private static Logger logger = LoggerFactory.getLogger(ExtractionDatesServiceImpl.class);
	
	@Autowired
	IExtractionDatesDao extractionDatesDao;
	
	
	
	@Transactional
	@Auditable(operation= ServicesCatalog.NEW_EXTRACTIONDATES,
			trace = "Inserimento Nuove Date Estrazione: Data Fissata=${(response.fixedDate)!'?'}, Data di Chiusura=${(response.closingDate)!'?'}")
	@Valid(value = ExtractionDatesValidator.class, operation = ServicesCatalog.NEW_EXTRACTIONDATES)
	public ExtractionDatesDTO insertExtractionDates(ExtractionDatesDTO extractionDates) {		
		try {
	
			logger.info("insert new extraction dates:" + extractionDates.toString());			
			StorageDateEstrazione extractionDatesEnt = DTOConverter.toEntity(extractionDates);
			extractionDatesEnt.setDataChiusura(Utils.dateToLocalDate(Utils.setEndOfDay(Utils.localDateToDate(extractionDates.getClosingDate()))));
			extractionDatesEnt.setDataFissa(Utils.dateToLocalDate(Utils.setStartOfDay(Utils.localDateToDate(extractionDates.getFixedDate()))));
			extractionDatesDao.insertExtractionDates(extractionDatesEnt);
			return DTOConverter.toDto(extractionDatesEnt);
		}catch (ServiceException e) {
			logger.error("Error insertExtractionDates ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error insertExtractionDates Generic Exception...",e);
			throw new ServiceException(e);
		}
		}
		
	

	@Override
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.SEARCH_EXTRACTIONDATES,
			trace = "Recuperate le Date di Estrazione")
	public List<ExtractionDatesDTO> getExtractionDatesItems() {
		try {
			List<StorageDateEstrazione> extractionDatesItems = extractionDatesDao.getAllExtractionDates();
			List<ExtractionDatesDTO> ret  = new ArrayList<ExtractionDatesDTO>();
			extractionDatesItems.stream().forEach(extractionDatesEnt -> {
				ret.add(DTOConverter.toDto(extractionDatesEnt));
			});
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getExtractionItems ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getExtractionDatesItems Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
}
	
