package it.poste.rilievi.sdp.api.service.impl.base;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public abstract class ValidatorBase implements IValidator {

	@Autowired
	MessageSource messageSource;
	
	protected String resolveMessage( String code, String defaultmessage ) {
		try {
			return messageSource.getMessage(code, null, Locale.ITALY);
		}catch (Throwable e) {
			return defaultmessage;
		}
	}
	
	protected String resolveMessage( String code ) {
		return resolveMessage(code, "");
	}
}
