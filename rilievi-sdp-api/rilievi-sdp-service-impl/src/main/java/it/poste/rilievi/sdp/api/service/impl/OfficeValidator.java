package it.poste.rilievi.sdp.api.service.impl;

import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.service.impl.base.IValidator;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationError;
import it.poste.rilievi.sdp.api.service.impl.base.ValidationUtils;
import it.poste.rilievi.sdp.api.service.impl.base.ValidatorBase;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;

public class OfficeValidator extends ValidatorBase implements IValidator{

	@Override
	public void validate(ValidationError errors, ServicesCatalog operation, Object... obj) throws ServiceException {
		String idBranch = (String)obj[0];
		ValidationUtils.rejectIfEmpty(idBranch, resolveMessage("office.get.arguments.empty"), errors);
	}
	
}