package it.poste.rilievi.sdp.api.service.impl.base;

public class ContextStoreHolder {

	private static ThreadLocal<ContextStore> contextStore = new ThreadLocal<ContextStore>();

	public static ContextStore getContextStore() {
		if (contextStore.get() == null) {
			setContextStore(new ContextStore());
		}
		return contextStore.get();
	}

	public static void setContextStore(ContextStore ctx) {
		contextStore.set(ctx);
	}
}
