package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.ReasonDTO;
import it.poste.rilievi.sdp.api.model.entity.TipologCausale;
import it.poste.rilievi.sdp.api.repository.IReasonDao;
import it.poste.rilievi.sdp.api.service.api.IReasonService;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class ReasonServiceImpl extends ServiceBase implements IReasonService {
	
private static Logger logger = LoggerFactory.getLogger(ReasonServiceImpl.class);
	
	@Autowired
	IReasonDao reasonDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ReasonDTO> getAllReason() {
		try {
			List<TipologCausale> reason = reasonDao.getAllReason();
			List<ReasonDTO> ret  = new ArrayList<ReasonDTO>();
			reason.stream().forEach(reasonEnt -> {
				ret.add(DTOConverter.toDto(reasonEnt));
			});
			logger.info("found and returned {} reason",  ret.size());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getAllReason ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getAllReason Generic Exception...",e);
			throw new ServiceException(e);
		}
	
	}

	
	
	
}
