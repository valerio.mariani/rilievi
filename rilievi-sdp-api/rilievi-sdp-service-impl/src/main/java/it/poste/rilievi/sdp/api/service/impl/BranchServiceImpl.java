package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.BranchDTO;
import it.poste.rilievi.sdp.api.model.dto.ServicesCatalog;
import it.poste.rilievi.sdp.api.model.entity.TipologFiliali;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IBranchDao;
import it.poste.rilievi.sdp.api.service.api.IBranchService;
import it.poste.rilievi.sdp.api.service.impl.audit.Auditable;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class BranchServiceImpl extends ServiceBase implements IBranchService{
	
	private static Logger logger = LoggerFactory.getLogger(BranchServiceImpl.class);
	
	@Autowired
	IBranchDao branchDao;
	
	@Transactional(readOnly = true)
	@Auditable(operation = ServicesCatalog.GET_BRANCHES,
		trace ="Recuperata lista delle filiali")
	public List<BranchDTO> getAllBraches() {
		try {
			List<BranchDTO> ret  = new ArrayList<BranchDTO>();
			List<TipologFiliali> branchesList = branchDao.getAllBranches(Utils.localDateTimeToDate(getSysDate()));
			branchesList.stream().forEach(branchEnt -> {
				BranchDTO branch = DTOConverter.toDto(branchEnt);
				ret.add(branch);
				branchEnt.getTipologUfficis().stream().forEach(uffEnt -> {
					branch.getOffice().add(DTOConverter.toDto(uffEnt));
				});
			});
			logger.info("found and returned {} branches", ret.size());
			return ret;
		}catch (ServiceException e) {
			logger.error("Error getAllBraches ServiceException:" + e.toString());
			throw e;
		}catch (Exception e) {
			logger.error("Error getAllBraches Generic Exception...",e);
			throw new ServiceException(e);
		}
	}
	
}
