package it.poste.rilievi.sdp.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.sdp.api.model.entity.TipologStatoRilievo;
import it.poste.rilievi.sdp.api.repository.IEvaluationStatusDao;
import it.poste.rilievi.sdp.api.service.api.IEvaluationStatusService;
import it.poste.rilievi.sdp.api.service.impl.exception.ServiceException;
import it.poste.rilievi.sdp.api.service.impl.util.DTOConverter;

@Service
@Transactional
public class EvaluationStatusServiceImpl extends ServiceBase implements IEvaluationStatusService {

	private static Logger logger = LoggerFactory.getLogger(ReasonServiceImpl.class);

	@Autowired
	IEvaluationStatusDao evaluationStatusDao;

	@Override
	@Transactional(readOnly = true)
	public List<EvaluationStatusDTO> getAllEvaluationStatus() {
		try {
			List<TipologStatoRilievo> evaluationStatus = evaluationStatusDao.getAllEvaluationStatus();
			List<EvaluationStatusDTO> ret = new ArrayList<EvaluationStatusDTO>();
			evaluationStatus.stream().forEach(evaluationStatusEnt -> {
				ret.add(DTOConverter.toDto(evaluationStatusEnt));
			});
			logger.info("found and returned {} status", ret.size());
			return ret;
		} catch (ServiceException e) {
			logger.error("Error  getAllEvaluationStatus ServiceException:" + e.toString());
			throw e;
		} catch (Exception e) {
			logger.error("Error  getAllEvaluationStatus Generic Exception...", e);
			throw new ServiceException(e);
		}

	}

}
