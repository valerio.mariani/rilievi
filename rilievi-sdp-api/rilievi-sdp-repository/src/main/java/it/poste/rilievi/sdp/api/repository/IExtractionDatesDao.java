package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.StorageDateEstrazione;

public interface IExtractionDatesDao {

	StorageDateEstrazione insertExtractionDates(StorageDateEstrazione extractionDates);

	List<StorageDateEstrazione> getAllExtractionDates();

}
