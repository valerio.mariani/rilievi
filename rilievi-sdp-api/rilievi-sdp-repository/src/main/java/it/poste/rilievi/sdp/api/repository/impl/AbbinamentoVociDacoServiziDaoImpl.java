package it.poste.rilievi.sdp.api.repository.impl;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.AbbinamentoVociDacoServizi;
import it.poste.rilievi.sdp.api.repository.IAbbinamentoVociDacoServiziDao;

@Repository
public class AbbinamentoVociDacoServiziDaoImpl extends DaoBaseImpl implements IAbbinamentoVociDacoServiziDao {

	@Override
	public boolean exitsDacoServiceAssociation(Long idDaco, Long idService) {
		Long cnt =  getEm().createNamedQuery(AbbinamentoVociDacoServizi.CheckDacoService, Long.class)
		.setParameter("idDaco", idDaco)
		.setParameter("idServizio", idService)
		.getSingleResult();
		return cnt>0;
	}

}
