package it.poste.rilievi.sdp.api.repository.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.entity.StorageQcisBean;
import it.poste.rilievi.sdp.api.repository.IQcisDao;

@Service
@Transactional
public class QcisDaoImpl extends DaoBaseImpl implements IQcisDao {

	private static final String MAIN_QUERY = "SELECT sdp.NUM_SPORTELLO, sdp.DATA_CONTABILE, sdp.FRAZIONARIO, SUM(IF(sr.CREDITI_EURO is null, 0, sr.CREDITI_EURO)) as AVERE, SUM(IF(sr.CREDITI_EURO is null, 0, 1)) as NUM_MOV_AVERE, SUM(IF(sr.DEBITI_EURO is null, 0, sr.DEBITI_EURO)) as DARE, SUM(IF(sr.DEBITI_EURO is null, 0, 1)) as NUM_MOV_DARE FROM storage_processi_sdp sdp INNER JOIN storage_rilievi sr ON sdp.RIF_RILIEVI=sr.ID_RILIEVI WHERE sdp.FRAZIONARIO={0} AND  DATE_FORMAT(sdp.DATA_CONTABILE,\"%Y%m%d\")= {1}  AND STATO_OPERAZIONE = \"N\" AND FLAG_VALIDITA = 1 AND DATA_FINE_VALIDITA is null {2} GROUP BY sdp.FRAZIONARIO";
	private static final String MOVEMENTS_QUERY = "SELECT sdp.NUM_SPORTELLO, sdp.DATA_CONTABILE, sdp.FRAZIONARIO, sr.CREDITI_EURO, sr.DEBITI_EURO, sdp.NUM_OPERAZIONE, sr.ID_RILIEVI FROM storage_processi_sdp sdp INNER JOIN storage_rilievi sr ON sdp.RIF_RILIEVI=sr.ID_RILIEVI WHERE sdp.FRAZIONARIO= {0} AND  DATE_FORMAT(sdp.DATA_CONTABILE,\"%Y%m%d\")= {1} AND STATO_OPERAZIONE = \"N\" AND FLAG_VALIDITA = 1 AND DATA_FINE_VALIDITA is null {2} {3} {4}";
	
	@Value("${poste.rilievi.codice.addebito}")
	private String codDebit;
	
	@Value("${poste.rilievi.codice.accredito}")
	private String codCredit;
	
	@SuppressWarnings("unchecked")
	@Override
	public StorageQcisBean getDataForFirstAndSecondLevel(String ufficio, String sezione, String dataContabile) {

		StorageQcisBean storageQcisBean = null;
		
		String query = MessageFormat.format(MAIN_QUERY, ufficio, dataContabile, StringUtils.isNotBlank(sezione) ? "AND sdp.NUM_SPORTELLO=" + sezione : "");

		List<Object[]> retMovements = getEm().createNativeQuery(query).getResultList();

		if (retMovements != null && retMovements.size() == 1) {
			storageQcisBean = new StorageQcisBean();
			storageQcisBean.setSection((int) retMovements.get(0)[0]);
			storageQcisBean.setAmountMovementsToHaveEuro((BigDecimal) retMovements.get(0)[3]);
			storageQcisBean.setNumberMovementsToHaveEuro((BigDecimal) retMovements.get(0)[4]);
			storageQcisBean.setAmountGivingMovementsEuro((BigDecimal) retMovements.get(0)[5]);
			storageQcisBean.setNumberGivingMovementsEuro((BigDecimal) retMovements.get(0)[6]);
		} 
		return storageQcisBean;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<StorageQcisBean> getDataForFourthLevel(String ufficio, String sezione, String data, String codeOp) {

		List<StorageQcisBean> storageQcisBeanList = new ArrayList<StorageQcisBean>();

		String query = MessageFormat.format(MOVEMENTS_QUERY, ufficio, data,
				StringUtils.isNotBlank(sezione) ? "AND sdp.NUM_SPORTELLO=" + sezione : "",
				StringUtils.equals(codeOp, codCredit) ? " AND sr.DEBITI_EURO is null" : "",
				StringUtils.equals(codeOp, codDebit) ? " AND sr.CREDITI_EURO is null" : "");

		List<Object[]> retMovements = getEm().createNativeQuery(query).getResultList();
		if (retMovements != null) {
			for (Object[] retMovement : retMovements) {
				StorageQcisBean storageQcisBean = new StorageQcisBean();
				storageQcisBean.setSection(retMovement[0] != null ? (int) retMovement[0] : null);
				storageQcisBean.setAmountMovementsToHaveEuro((BigDecimal) retMovement[3]);
				storageQcisBean.setAmountGivingMovementsEuro((BigDecimal) retMovement[4]);
				storageQcisBean.setOperationNumber(String.valueOf(retMovement[5]));
				storageQcisBean.setRifEvaluation(String.valueOf(retMovement[6]));
				storageQcisBeanList.add(storageQcisBean);
			}
		}
		return storageQcisBeanList;
	}
}

