package it.poste.rilievi.sdp.api.repository.impl;


import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.TipologTipoAzioni;
import it.poste.rilievi.sdp.api.repository.IActionTypeDao;

@Repository
public class ActionTypeDaoImpl extends DaoBaseImpl implements IActionTypeDao{

	@Override
	public TipologTipoAzioni getTipoAzioneByName(String name) {
		return getEm()
				.createNamedQuery(TipologTipoAzioni.GetTipoAzioneByAzione, TipologTipoAzioni.class)
				.setParameter("azione", name)
				.getSingleResult();
	}

	
}
