package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.AttachmentCriteriaDTO;
import it.poste.rilievi.sdp.api.model.entity.StorageAllegati;

public interface IAttachmentDao extends IDaoBase {

	List<StorageAllegati> getAttachmentById(AttachmentCriteriaDTO request, Long evaluationId);

	Integer countAttachmentById(AttachmentCriteriaDTO criteria, Long evaluationId);

	void deleteAttachment(StorageAllegati attachment);

	StorageAllegati insertAttachment(StorageAllegati attachmentEnt);

	StorageAllegati getAttachmentById(Long idAttachment);

	boolean checkExist(String name, Long evaluationID);

	Integer countAttachment(AttachmentCriteriaDTO criteria);
}
