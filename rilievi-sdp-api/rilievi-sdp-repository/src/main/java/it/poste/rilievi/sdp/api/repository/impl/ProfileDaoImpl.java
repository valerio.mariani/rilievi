package it.poste.rilievi.sdp.api.repository.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.dto.AppProfileCriteriaDTO;
import it.poste.rilievi.sdp.api.model.entity.AnagProfili;
import it.poste.rilievi.sdp.api.model.entity.StorageAbilitazioniDaco;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IProfileDao;

@SuppressWarnings("unused")
@Repository
public class ProfileDaoImpl extends DaoBaseImpl implements IProfileDao {

	@Override
	public List<AnagProfili> getProfileItems(AppProfileCriteriaDTO profileCriteria) {
		
		TypedQuery<AnagProfili> query = getEm().createQuery(buildSearchProfiles(profileCriteria, false),
				AnagProfili.class);

		if (profileCriteria.getPageNumber() != null && profileCriteria.getPageNumber() > 0) {
			query.setMaxResults(profileCriteria.getPageSize());
			query.setFirstResult(profileCriteria.getFirstRow());
		}

		
		return query.getResultList();
	}

	@Override
	public Integer countProfile(AppProfileCriteriaDTO criteria) {
		Query query = getEm().createQuery(buildSearchProfiles(criteria, true), Long.class);

		Long rows = (Long) query.getSingleResult();
		return rows.intValue();
	}

	private String buildSearchProfiles(AppProfileCriteriaDTO criteria, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT(*)" : " SELECT s ");
		sb.append(" FROM AnagProfili s ");
		
		if( StringUtils.isNotEmpty(criteria.getDBOrderField()) ) {
			sb.append(" ORDER BY "+criteria.getDBOrderField() + " " + criteria.getOrderType());
		}

		return sb.toString();
	}
	@Override
	public AnagProfili getProfileByCode(String code) {
		try {
			return getEm()
					.createNamedQuery(AnagProfili.FindAnagProfileBycode,AnagProfili.class)
					.setParameter("code", code)
					.getSingleResult();
		}catch (NoResultException  | NonUniqueResultException e) {
			return null;
		}
	}

	
	@SuppressWarnings("unused")
	@Override
	public boolean checkProfileByCode(Long profileId, Long dacoId) {
		try {
			StorageAbilitazioniDaco abilitazioniDaco =  getEm()
					.createNamedQuery(StorageAbilitazioniDaco.CheckDacoProfile,StorageAbilitazioniDaco.class)
					.setParameter("idVoceDaco", dacoId)
					.setParameter("profile", profileId)
					.setParameter("validantionDate", Utils.localDateTimeToDate(getSysDate()))
					.getSingleResult();
			return true;
		}catch (NoResultException e) {
			return false;
		}catch (NonUniqueResultException e) {
			return true;
		}
		
	}
	@Override
	public AnagProfili insertProfile(AnagProfili profile) {
		getEm().persist(profile);
		return profile;
	}


	@Override
	public boolean checkExistName(String name) {
		try {
			AnagProfili checkName = getEm()
					.createNamedQuery(AnagProfili.findAnagProfileByName, AnagProfili.class)
					.setParameter("name",name)
					.getSingleResult();
			return true;
			}
		catch (NoResultException e) {
			return false;
		}catch (NonUniqueResultException e) {
			return true;
		}
	}

	@Override
	public boolean checkExistIAM(String code) {
		try {
			AnagProfili checkIAM = getEm()
					.createNamedQuery(AnagProfili.FindAnagProfileBycode, AnagProfili.class)
					.setParameter("code",code)
					.getSingleResult();
			return true;
			}
		catch (NoResultException e) {
			return false;
		}catch (NonUniqueResultException e) {
			return true;
		}
	}
	
	@Override
	public boolean checkExistId(Long idProfilo) {
		try {
			AnagProfili checkID = getEm()
					.createNamedQuery(AnagProfili.findAnagProfileById, AnagProfili.class)
					.setParameter("idProfilo", idProfilo)
					.getSingleResult();
			return true;
			}
		catch (NoResultException e) {
			return false;
		}catch (NonUniqueResultException e) {
			return true;
		}
	}
	
	@Override
	public Integer searchProfileCount() {
		Query query = getEm().createNamedQuery(AnagProfili.profileCount, Long.class);
		Long rows= (Long) query.getSingleResult();
		return rows.intValue();
	}

}
