package it.poste.rilievi.sdp.api.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.StorageDateEstrazione;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IExtractionDatesDao;


@Repository
public class ExtractionDatesDaoImpl extends DaoBaseImpl implements IExtractionDatesDao {
	

	@Override
	public StorageDateEstrazione insertExtractionDates(StorageDateEstrazione extractionDates ) {
		getEm().createNativeQuery("INSERT INTO storage_date_estrazione (DATA_CHIUSURA,DATA_FISSA) VALUES(?,?)")
		.setParameter(1, extractionDates.getDataChiusura() )
		.setParameter(2,extractionDates.getDataFissa()).executeUpdate();
		
		return extractionDates;
	}

	@SuppressWarnings({ "unchecked"})
	@Override
	public List<StorageDateEstrazione> getAllExtractionDates() {
		List<StorageDateEstrazione> res = new ArrayList<StorageDateEstrazione>();
		List<Object[]> ret = getEm()
				.createNativeQuery("SELECT DATA_CHIUSURA, DATA_FISSA FROM storage_date_estrazione ORDER BY ID_DATE_ESTRAZIONE DESC ").getResultList();
		StorageDateEstrazione date = new StorageDateEstrazione();
		if (ret != null && ret.size() >= 1) {
			date.setDataChiusura(Utils.dateToLocalDate((Date) ret.get(0)[0]));
			date.setDataFissa(Utils.dateToLocalDate((Date) ret.get(0)[1]));

			res.add(date);
		}
		return res;
	}

}
