package it.poste.rilievi.sdp.api.repository;

import it.poste.rilievi.sdp.api.model.entity.TipologTipoAzioni;

public interface IActionTypeDao extends IDaoBase {

	TipologTipoAzioni getTipoAzioneByName(String name);

}