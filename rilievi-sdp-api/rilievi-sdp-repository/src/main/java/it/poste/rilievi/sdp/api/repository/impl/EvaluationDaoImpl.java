package it.poste.rilievi.sdp.api.repository.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationCriteriaDTO.SearchType;
import it.poste.rilievi.sdp.api.model.entity.StorageRilievi;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IEvaluationDao;
@Repository
public class EvaluationDaoImpl extends DaoBaseImpl implements IEvaluationDao {


	@Override
	public List<StorageRilievi> searchEvaluations(EvaluationCriteriaDTO criteria) {
		
		TypedQuery<StorageRilievi> query = getEm().createQuery(buildSearchEvaluations(criteria, false), StorageRilievi.class);
		setParameterSearchEvaluations(query, criteria);
		if(criteria.getPageNumber() != null && criteria.getPageNumber() > 0) {
			query.setFirstResult(criteria.getFirstRow()) ;//tupla di partenza
			query.setMaxResults(criteria.getPageSize());
		}
		
		return query.getResultList();
	}

	@Override
	public Integer searchEvaluationsCount(EvaluationCriteriaDTO criteria) {
		Query query = getEm().createQuery(buildSearchEvaluations(criteria, true), Long.class);
		setParameterSearchEvaluations(query, criteria);
		Long rows= (Long) query.getSingleResult();
		return rows.intValue();
	}
	
	
	private String buildSearchEvaluations(EvaluationCriteriaDTO criteria, boolean isCount) {
		StringBuilder sb = new StringBuilder();
		sb.append(isCount ? " SELECT COUNT( * ) " :" SELECT s ");
		sb.append(" FROM StorageRilievi s where 1=1 ");
		
		if(!criteria.isShowRemoved()) {
			sb.append(" AND s.flagAnnullato = :flagAnnullato ");
		}
		
		if( criteria.getType() != null ) {
			
			if(SearchType.REGULARIZED.equals(criteria.getType()) ) {
				sb.append(" AND s.dataRegolarizzazione IS NOT NULL ");
			}else
				if(SearchType.NOT_REGULARIZED.equals(criteria.getType()) ) {
					sb.append(" AND s.dataRegolarizzazione IS NULL");
				}
		}
		
		if(criteria.isShowOnlyAccounted()) {
			sb.append(" AND s.dataCoge IS NOT NULL ");
		}
		
		if(criteria.isShowOnlySendedBridge()) {
			sb.append(" AND s.dataInvioSrc IS NOT NULL ");
		}
		
		if( StringUtils.isNotEmpty(criteria.getAppUserCode()) ) {
			sb.append(" AND s.utenteInserimento = :utenteInserimento ");
		}
		
		if( criteria.getInsertDateFrom() != null ) {
			sb.append(" AND s.dataInserimento >= :dataInserimentoFrom ");
		}
		
		if( criteria.getInsertDateTo() != null ) {
			sb.append(" AND s.dataInserimento <= :dataInserimentoTo ");
		}
		
		if( criteria.getCompetenceDateFrom() != null ) {
			sb.append(" AND s.dataCompetenza >= :dataCompetenzaFrom ");
		}
		
		if( criteria.getCompetenceDateTo() != null ) {
			sb.append(" AND s.dataCompetenza <= :dataCompetenzaTo ");
		}
		
		if( criteria.getBranch() != null && criteria.getBranch().getId() != null ) {
			sb.append(" AND s.tipologFiliali.idFiliale = :idFiliale ");
		}
		
		if( criteria.getOffice() != null ) {
			if( criteria.getOffice().getId() != null ) {
				sb.append(" AND s.tipologUffici.idUfficio = :idUfficio ");
			}
			if( criteria.getOffice().getFractional() != null ) {
				sb.append(" AND s.tipologUffici.frazionario = :frazionario ");
			}	
		}
		
		if( criteria.getIdDaco().size() > 0 ) {
			sb.append(" AND s.tipologDaco.idVoceDaco IN ( :idVoceDaco ) ");
		}
		
		if( criteria.getIdReason().size() > 0 ) {
			sb.append(" AND s.tipologCausale1.idCausale IN ( :idCausale )");
		}
		
		if( criteria.getLossState() != null && criteria.getLossState().getId() != null ) {
			sb.append(" AND s.tipologStatoRilievo.idStatoRilievo = :idStatoRilievo ");
		}
		
		if( criteria.getAmountFrom() != null ) {
			sb.append(" AND ( (s.debitiEuro IS NOT NULL AND s.debitiEuro >= :amountFrom ) OR (s.creditiEuro IS NOT NULL AND s.creditiEuro >= :amountFrom ) )");
		}
		
		if( criteria.getAmountTo() != null ) {
			sb.append(" AND ( (s.debitiEuro IS NOT NULL AND s.debitiEuro <= :amountTo ) OR (s.creditiEuro IS NOT NULL AND s.creditiEuro <= :amountTo ) )");
		}
		
		if( StringUtils.isNotEmpty(criteria.getDBOrderField()) ) {
			sb.append(" ORDER BY "+criteria.getDBOrderField() + " " + criteria.getOrderType());
		}
		
		return sb.toString();
	}
	
	private void setParameterSearchEvaluations(Query query, EvaluationCriteriaDTO criteria ) {
		
		if(!criteria.isShowRemoved()) {
			query.setParameter("flagAnnullato",StorageRilievi.FLAG_NON_ANNULLATO);
		}
		
		if( StringUtils.isNotEmpty(criteria.getAppUserCode()) ) {
			//
			query.setParameter("utenteInserimento",criteria.getAppUserCode());
		}
		
		if( criteria.getInsertDateFrom() != null ) {
			query.setParameter("dataInserimentoFrom",Utils.setStartOfDay(Utils.localDateToDate(criteria.getInsertDateFrom())));
		}
		
		if( criteria.getInsertDateTo() != null ) {
			query.setParameter("dataInserimentoTo", Utils.setEndOfDay(Utils.localDateToDate(criteria.getInsertDateTo())));
		}
		
		if( criteria.getCompetenceDateFrom() != null ) {
			query.setParameter("dataCompetenzaFrom", Utils.setStartOfDay(Utils.localDateToDate(criteria.getCompetenceDateFrom())));
		}
		
		if( criteria.getCompetenceDateTo() != null ) {
			query.setParameter("dataCompetenzaTo", Utils.setEndOfDay(Utils.localDateToDate(criteria.getCompetenceDateTo())));
		}
		
		if( criteria.getBranch() != null && criteria.getBranch().getId() != null ) {
			query.setParameter("idFiliale",  criteria.getBranch().getId());
		}
		
		if( criteria.getOffice() != null ) {
			if( criteria.getOffice().getId() != null ) {
				query.setParameter("idUfficio",  criteria.getOffice().getId());
			}
			if( criteria.getOffice().getFractional() != null ) {
				query.setParameter("frazionario", criteria.getOffice().getFractional());
			}	
		}
		
		if( criteria.getIdDaco().size() > 0 ) {
			query.setParameter("idVoceDaco", criteria.getIdDaco());
		}
		
		if(  criteria.getIdReason().size() > 0  ) {
			query.setParameter("idCausale", criteria.getIdReason());
		}
		
		if( criteria.getLossState() != null && criteria.getLossState().getId() != null ) {
			query.setParameter("idStatoRilievo", criteria.getLossState().getId());
		}
		
		if( criteria.getAmountFrom() != null ) {
			query.setParameter("amountFrom", new BigDecimal(criteria.getAmountFrom()));
		}
		
		if( criteria.getAmountTo() != null ) {
			query.setParameter("amountTo",  new BigDecimal(criteria.getAmountTo()));
		}

	}

	@Override
	public StorageRilievi insertEvaluation(StorageRilievi rilievo) {
		getEm().persist(rilievo);
		rilievo.setNumRilievo(rilievo.getIdRilievi().intValue());
		getEm().merge(rilievo);
		
		return rilievo;
	}

	@Override
	public StorageRilievi updateFractional(StorageRilievi rilievo) {
		return getEm().merge(rilievo);
	}

	@Override
	public void deleteEvaluation(StorageRilievi rilievo) {
		getEm().merge(rilievo);
		
	}

	@Override
	public void updateEvaluationById(LocalDate dataRegolarizzazione, String regolarizzatoDaUtente, String flagRegolarizzato, Long idRilievi) {
		getEm().createNamedQuery(StorageRilievi.UpdateEvaluationById).setParameter("dataRegolarizzazione", Utils.localDateToDate(dataRegolarizzazione)).setParameter("regolarizzatoDaUtente", regolarizzatoDaUtente).setParameter("flagRegolarizzato", flagRegolarizzato).setParameter("idRilievi", idRilievi).executeUpdate();
		
	}
	@Override
	public StorageRilievi updateEvaluation(StorageRilievi rilievo) {
		return getEm().merge(rilievo);
	}


	
}