package it.poste.rilievi.sdp.api.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.poste.rilievi.sdp.api.model.entity.StorageRilieviMaxiTotale;
import it.poste.rilievi.sdp.api.repository.IMaxiEvaluationDao;
@Repository
@Transactional
public class MaxiEvaluationDaoImpl extends DaoBaseImpl implements IMaxiEvaluationDao {

	
	@Override
	@Transactional(readOnly = true)
	public List<StorageRilieviMaxiTotale> getMaxiEvaluationRaised() {
		return getEm().createNamedQuery(StorageRilieviMaxiTotale.GetMaxiEvaluationRaised,StorageRilieviMaxiTotale.class).getResultList();
	}
	
}