package it.poste.rilievi.sdp.api.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.StorageProcessiSdp;

public interface IProcessSdpDao extends IDaoBase {

	StorageProcessiSdp getProcessByOperationNum(String numOpe, String fractional, String branchNumber, Date contabilizationDate);

	StorageProcessiSdp getProcessByNumOpeFractionalDeskAccountingDate(Integer operation, String node, int desk, LocalDate accountingDate);

	List<StorageProcessiSdp> getProcessesByNumOpeFractionalDeskAccountingDate(Integer operation, String node, int desk, LocalDate accountingDate);

	void updateRegularization(String flagValidita, LocalDate dataFineValidita, Long idProcSdp);

	void deleteProcessByIdProcSdp(Long idProcSdp);

	StorageProcessiSdp insertProcess(StorageProcessiSdp process);

	StorageProcessiSdp invalidProcess(StorageProcessiSdp process);

}