package it.poste.rilievi.sdp.api.repository;

public interface IAbbinamentoVociDacoServiziDao {

	boolean exitsDacoServiceAssociation(Long idDaco, Long idService );
	
}