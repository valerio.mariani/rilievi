package it.poste.rilievi.sdp.api.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.TipologServizi;
import it.poste.rilievi.sdp.api.repository.IDacoServiceDao;

@Repository
public class DacoServiceDaoImpl extends DaoBaseImpl implements IDacoServiceDao{

	@Override
	public List<TipologServizi> getServices(Long idDaco) {
		return getEm()
				.createNamedQuery(TipologServizi.GetDacoServices, TipologServizi.class)
				.setParameter("idVoceDaco", idDaco)
				.getResultList();
	}
}
