package it.poste.rilievi.sdp.api.repository;

import java.time.LocalDateTime;

import javax.persistence.Query;

public interface IDaoBase {

	LocalDateTime getSysDate();

	<T> T findById(Long primaryKey, Class<T> entityClass);

	<T> T update(T entity);

	<T> T insert(T entity);

	int execute(Query query);
}