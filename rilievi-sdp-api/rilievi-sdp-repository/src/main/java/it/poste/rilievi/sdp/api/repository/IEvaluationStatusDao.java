package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.TipologStatoRilievo;

public interface IEvaluationStatusDao extends IDaoBase {
	
	List<TipologStatoRilievo> getAllEvaluationStatus();

	TipologStatoRilievo getStatusByCode(String code);
}
