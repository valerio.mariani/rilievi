package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.TipologUffici;

public interface IOfficeDao extends IDaoBase {

	List<TipologUffici> getOffices( Long idBranch );

	TipologUffici getOfficeByFractional(String fractional);
			
}