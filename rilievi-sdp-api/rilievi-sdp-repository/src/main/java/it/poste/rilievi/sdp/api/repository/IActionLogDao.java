package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.AuditLogCriteriaDTO;
import it.poste.rilievi.sdp.api.model.entity.StorageLogAzioni;

public interface IActionLogDao {

	StorageLogAzioni insertActionLog(StorageLogAzioni azione);

	List<StorageLogAzioni> getAuditLog(AuditLogCriteriaDTO auditLog);

	Integer searchAuditLogCount(AuditLogCriteriaDTO request);

}