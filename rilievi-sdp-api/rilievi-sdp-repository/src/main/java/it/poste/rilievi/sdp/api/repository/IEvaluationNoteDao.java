package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.sdp.api.model.entity.StorageNoteAggiuntive;

public interface IEvaluationNoteDao extends IDaoBase {
	
	StorageNoteAggiuntive insertEvaluationNote(StorageNoteAggiuntive evaluationNote);
	
	void deleteEvaluationNote(Long noteId);
	
	Integer countEvaluationNote(EvaluationNoteCriteriaDTO criteria);

	List<StorageNoteAggiuntive> getEvaluationNote(EvaluationNoteCriteriaDTO evaluationNote);

}
