package it.poste.rilievi.sdp.api.repository;

import java.util.Date;
import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.DacoCriteriaDTO;
import it.poste.rilievi.sdp.api.model.entity.TipologDaco;

public interface IDacoDao extends IDaoBase {

	TipologDaco insertDaco(TipologDaco daco);

	TipologDaco getDacoById(Long idDaco);

	TipologDaco updateDaco(TipologDaco daco);

	List<TipologDaco> getAllValidDaco(Date validationDate);

	List<TipologDaco> getDacoUserProfile(Date validationDate, Long idProfile);

	List<TipologDaco> getAllDaco(DacoCriteriaDTO criteria);

	Long countAllDaco();

	List<TipologDaco> getDacoProfileFunction(Date validationDate, Long idProfile, Long idFunction);

	List<TipologDaco> getDacoMaxiEvaluationStorage(Date validationDate);

	List<TipologDaco> getDacoMaxiEvaluationStaging(Date validationDate);

	Integer countDaco(DacoCriteriaDTO criteria);

	List<TipologDaco> getExcludedDacoStaging(Date validationDate);

	List<TipologDaco> getExcludedDacoStorage(Date validationDate);

	void removeExcludedDacoStaging();
}
