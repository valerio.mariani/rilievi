package it.poste.rilievi.sdp.api.repository;

import java.util.Date;
import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.TipologFiliali;

public interface IBranchDao {

	List<TipologFiliali> getAllBranches(Date validationDate);

}