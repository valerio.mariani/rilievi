package it.poste.rilievi.sdp.api.repository.impl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.StorageProcessiSdp;
import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IProcessSdpDao;

@Repository
public class ProcessSdpDaoImpl extends DaoBaseImpl implements IProcessSdpDao {

	@Override
	public StorageProcessiSdp getProcessByOperationNum(String numOpe, String fractional, String branchNumber, Date contabilizationDate) {
		try {
			return getEm()
					.createNamedQuery(StorageProcessiSdp.ProcessByNumOpe, StorageProcessiSdp.class)
					.setParameter("numOpe",Integer.parseInt(numOpe))
					.setParameter("fractional",fractional)
					.setParameter("branchNumber",Integer.parseInt(branchNumber))
					.setParameter("contabilizationDate",Utils.setStartOfDay(contabilizationDate),TemporalType.DATE)
					.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
		
	}

	@Override
	public StorageProcessiSdp insertProcess(StorageProcessiSdp process) {
		
		getEm().persist(process);
		return process;
	}

	@Override
	public StorageProcessiSdp invalidProcess(StorageProcessiSdp process) {
		return getEm().merge(process);
	}
	
	@Override
	public StorageProcessiSdp getProcessByNumOpeFractionalDeskAccountingDate(Integer operation, String node, int desk, LocalDate accountingDate) {
		try {
			return getEm()
					.createNamedQuery(StorageProcessiSdp.ProcessByNumOpeFractionalDeskAccountingDate, StorageProcessiSdp.class)
					.setParameter("numOpe", operation).setParameter("node", node).setParameter("desk", desk).setParameter("accountingDate", String.valueOf(accountingDate)).getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<StorageProcessiSdp> getProcessesByNumOpeFractionalDeskAccountingDate(Integer operation, String node, int desk, LocalDate accountingDate) {
		try {
			return getEm()
					.createNamedQuery(StorageProcessiSdp.ProcessesByNumOpeFractionalDeskAccountingDate, StorageProcessiSdp.class)
					.setParameter("numOpe", operation).setParameter("node", node).setParameter("desk", desk).setParameter("accountingDate", String.valueOf(accountingDate)).getResultList();
		}catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void deleteProcessByIdProcSdp(Long idProcSdp) {
		getEm().createNamedQuery(StorageProcessiSdp.DeleteProcessByIdProcSdp).setParameter("idProcSdp", idProcSdp).executeUpdate();
	}

	

	@Override
	public void updateRegularization(String flagValidita, LocalDate dataFineValidita, Long idProcSdp) {
		getEm().createNamedQuery(StorageProcessiSdp.UpdateRegularization).setParameter("flagValidita", flagValidita).setParameter("dataFineValidita", dataFineValidita).setParameter("idProcSdp", idProcSdp).executeUpdate();
	}

}
