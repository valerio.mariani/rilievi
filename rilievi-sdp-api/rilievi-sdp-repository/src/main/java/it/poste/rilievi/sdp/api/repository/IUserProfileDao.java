package it.poste.rilievi.sdp.api.repository;

import it.poste.rilievi.sdp.api.model.entity.AnagProfili;

public interface IUserProfileDao {

	AnagProfili getProfileByCode(String code);

}