package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.StorageRilieviMaxiTotale;

public interface IMaxiEvaluationDao extends IDaoBase {

	List<StorageRilieviMaxiTotale> getMaxiEvaluationRaised();

}