package it.poste.rilievi.sdp.api.repository.impl;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.TipologFiliali;
import it.poste.rilievi.sdp.api.repository.IBranchDao;

@Repository
public class BranchDaoImpl extends DaoBaseImpl implements IBranchDao{


	@Override
	public List<TipologFiliali> getAllBranches( Date validationDate) {
		return getEm()
				.createNamedQuery(TipologFiliali.GetAllBranches,TipologFiliali.class)
				.setParameter("validationDate", validationDate)
				.getResultList();
		
	}
}
