package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import it.poste.rilievi.sdp.api.model.entity.TipologCausale;

public interface IReasonDao extends IDaoBase {

	List<TipologCausale> getAllReason();

}
