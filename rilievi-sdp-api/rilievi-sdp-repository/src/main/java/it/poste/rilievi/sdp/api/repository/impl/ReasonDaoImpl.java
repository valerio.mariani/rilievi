package it.poste.rilievi.sdp.api.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.TipologCausale;
import it.poste.rilievi.sdp.api.repository.IReasonDao;

@Repository
public class ReasonDaoImpl extends DaoBaseImpl implements IReasonDao {

	@Override
	public List<TipologCausale> getAllReason() {

		return getEm().createNamedQuery(TipologCausale.QueryGetAllReason, TipologCausale.class).getResultList();
	}

}
