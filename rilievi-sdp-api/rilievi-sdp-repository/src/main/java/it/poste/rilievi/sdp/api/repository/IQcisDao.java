package it.poste.rilievi.sdp.api.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.entity.StorageQcisBean;

@Repository
public interface IQcisDao extends IDaoBase {
	
	StorageQcisBean getDataForFirstAndSecondLevel(String ufficio, String sezione, String data);

	List<StorageQcisBean> getDataForFourthLevel(String ufficio, String sezione, String data, String codeOp);
}
