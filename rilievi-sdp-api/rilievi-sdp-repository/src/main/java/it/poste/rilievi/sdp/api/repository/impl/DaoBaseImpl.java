package it.poste.rilievi.sdp.api.repository.impl;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import it.poste.rilievi.sdp.api.model.utils.Utils;
import it.poste.rilievi.sdp.api.repository.IDaoBase;

@Repository("daoBase")
public class DaoBaseImpl implements IDaoBase {

	
	@PersistenceContext
	EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	
	public LocalDateTime getSysDate() {		
		Date sysDate = (Date) em.createNativeQuery("SELECT SYSDATE()").getSingleResult();
		return Utils.dateToLocalDateTime(sysDate);
	}
	
	public <T> T findById(Long primaryKey, Class<T> entityClass) {		
		return getEm().find(entityClass, primaryKey);		
	}
	
	public <T> T update(T entity) {		
		return getEm().merge(entity);		
	}

	@Override
	public <T> T insert(T entity) {		
		getEm().persist(entity);
		return entity;
	}
	
	@Override
	public int execute(Query query) {
		return query.executeUpdate();
	}
}
