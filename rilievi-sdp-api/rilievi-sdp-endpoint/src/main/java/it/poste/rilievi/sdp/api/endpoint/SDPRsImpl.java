package it.poste.rilievi.sdp.api.endpoint;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalAlignDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalResponseDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalUndoDTO;
import it.poste.rilievi.sdp.api.service.api.IProcessSdpService;
import it.poste.rilievi.sdp.api.service.api.ISdpService;
import it.poste.rilievi.sdp.api.service.api.ISettingsService;


@RestController()
@Component
public class SDPRsImpl {

	private static final Logger logger = LoggerFactory.getLogger(SDPRsImpl.class);
	
	private ISdpService sdpService;
	
	@Autowired
	ISettingsService settingsService;
	
	@Autowired
	IProcessSdpService processSdpService;
	
	@Autowired
	public SDPRsImpl(ISdpService service) {
		this.sdpService = service;
	}


	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT, 
    		value = "evaluation/regularization")
	@ResponseStatus(code = HttpStatus.OK)
	public ProcessSDPDTO regularizzation( @RequestBody ProcessSDPDTO request) {
		return sdpService.regularization(request);
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT, 
    		value = "evaluation/undoregularization")
	@ResponseStatus(code = HttpStatus.OK)
	public ProcessSDPDTO undoRegularizzation( @RequestBody ProcessSDPDTO request) {
		return sdpService.undoRegularization(request);
	}
	
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "tech/undo")
	@ResponseStatus(code = HttpStatus.OK)
	public TechnicalResponseDTO technicalUndo( @RequestBody TechnicalUndoDTO request) {
		logger.info("technical undo with channel: {}, node: {}, desk: {}, accountingDate: {}, operation: {}", request.getChannel(), request.getNode(), request.getDesk(), request.getAccountingDate(), request.getToBeDeleted() != null && request.getToBeDeleted().getOperation() != null ? request.getToBeDeleted().getOperation() : "");
		return sdpService.technicalUndo(request);
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "tech/align")
	@ResponseStatus(code = HttpStatus.OK)
	public TechnicalResponseDTO technicalAlign( @RequestBody TechnicalAlignDTO request) {
		logger.info("technical align with channel: {}, node: {}, desk: {}, accountingDate: {}, operation: {}", request.getChannel(), request.getNode(), request.getDesk(), request.getAccountingDate(), request.getLastTransaction() != null && request.getLastTransaction().getOperation() != null ? request.getLastTransaction().getOperation() : "");
		return sdpService.technicalAlign(request);
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST,
			value = "evaluation/undo")
	@ResponseStatus(code = HttpStatus.OK)
	public void undoOperation(@RequestBody ProcessSDPDTO request) {
		logger.info("undo (FE) with fractional: {}, branchNumber: {}, contabilizationDate: {}, operationNumber: {}", request.getOffice() != null && request.getOffice().getFractional() != null ? request.getOffice().getFractional() : "", request.getBranchNumber(), request.getContabilizationDate(), request.getOperationNumber());
		sdpService.undoOperation(request);
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.GET,
			value = "/processSDP")
	@ResponseStatus(code = HttpStatus.OK)
	public ProcessSDPDTO getProcessSDP(@RequestParam("opeNumber") String opeNumber, @RequestParam("fractional") String fractional, @RequestParam("branchNumber") String branchNumber, @RequestParam("date") String date) {
		logger.info("getProcessSDP with fractional: {}, branchNumber: {}, contabilizationDate: {}, operationNumber: {}", fractional, branchNumber, opeNumber, date);
		return processSdpService.getProcessSDP(opeNumber, fractional, branchNumber, LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE));
	}
	
}
