package it.poste.rilievi.sdp.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.BranchDTO;
import it.poste.rilievi.sdp.api.service.api.IBranchService;


@RestController
@Component
public class BranchRsImpl {

	private IBranchService brachService;
	
	@Autowired
	public BranchRsImpl(IBranchService service) {
		this.brachService = service;
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/branches")
	@ResponseStatus(code = HttpStatus.OK)
	public List<BranchDTO> getAllBraches() {	
		return brachService.getAllBraches();
	}
	
	
}
