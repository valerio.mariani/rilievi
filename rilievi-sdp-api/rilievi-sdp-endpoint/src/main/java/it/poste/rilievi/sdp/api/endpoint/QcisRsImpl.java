package it.poste.rilievi.sdp.api.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.service.api.IQcisService;

@RestController
@Component
public class QcisRsImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(QcisRsImpl.class);
	
	private IQcisService qcisService;
	
	@Autowired
	public QcisRsImpl(IQcisService service) {
		this.qcisService = service;
	}
	
	@RequestMapping(consumes = MediaType.ALL_VALUE,
			produces = MediaType.TEXT_PLAIN_VALUE,
			method = RequestMethod.GET,
			value = "/qcis/inquiry")
	@ResponseStatus(code = HttpStatus.OK)
	public String getQcis(@RequestParam String ufficio, String sezione, @RequestParam String data, @RequestParam String livello, String codeOp) {
		logger.info("required data with office: {}, section: {}, date: {}, level : {}, codeOp : {}", ufficio, sezione, data, livello, codeOp);
		return qcisService.getQcis(ufficio, sezione, data, livello, codeOp);
	}
}
