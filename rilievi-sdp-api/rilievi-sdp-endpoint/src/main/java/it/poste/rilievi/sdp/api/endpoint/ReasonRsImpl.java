package it.poste.rilievi.sdp.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.ReasonDTO;
import it.poste.rilievi.sdp.api.service.api.IReasonService;

@RestController
@Component
public class ReasonRsImpl {

	
	private IReasonService reasonService;
	
	@Autowired
	public ReasonRsImpl(IReasonService service) {
		this.reasonService = service;
	}
	
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/reasons")
	@ResponseStatus(code = HttpStatus.OK)
	public List<ReasonDTO> getAllReason() {	
		return reasonService.getAllReason(); 
	}

}
