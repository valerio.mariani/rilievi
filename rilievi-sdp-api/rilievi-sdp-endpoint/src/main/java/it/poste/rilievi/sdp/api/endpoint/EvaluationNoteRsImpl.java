package it.poste.rilievi.sdp.api.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.EvaluationDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.service.api.IEvaluationNoteService;
import it.poste.rilievi.sdp.api.service.impl.base.ContextStoreHolder;

@RestController
@Component
public class EvaluationNoteRsImpl {
	

	private IEvaluationNoteService evaluationNoteService;
	
	
	@Autowired
	public EvaluationNoteRsImpl(IEvaluationNoteService service) {
		this.evaluationNoteService=service;
	}
	
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/evaluation/{id_evaluation}/note")
	@ResponseStatus(code = HttpStatus.CREATED)
	public EvaluationNoteDTO insertEvaluationNote(@RequestBody EvaluationNoteDTO request,@PathVariable("id_evaluation") Long idEvaluation) {
		request.setUser(ContextStoreHolder.getContextStore().getAppUser().getCode());
        EvaluationDTO evaluation = new EvaluationDTO();
        evaluation.setId(idEvaluation);
		request.setEvaluation(evaluation);
		return evaluationNoteService.insertEvaluationNote(request); 
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.DELETE, 
    		value = "evaluation/{id_evaluation}/note/{id_note}")
	@ResponseStatus(code = HttpStatus.OK)
	public void deleteFunctionParam(@PathVariable("id_note") Long idNote) {	
		evaluationNoteService.deleteEvaluationNote(idNote);
	}
	
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/evaluation/{id_evaluation}/notes")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<EvaluationNoteDTO> getEvaluationNote(EvaluationNoteCriteriaDTO request, @PathVariable("id_evaluation") Long idEvaluation ) {	
		EvaluationDTO evaluation = new EvaluationDTO();
	    evaluation.setId(idEvaluation);
	    request.setEvaluation(evaluation);
		return evaluationNoteService.getEvaluationNote(request, idEvaluation); 
	}
	
	
	
	
	

}
