package it.poste.rilievi.sdp.api.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.FunctionDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionParamCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.service.api.IFunctionParamService;

@RestController
@Component
public class FunctionParamRsImpl {
	
	private IFunctionParamService functionParamService;
	
	@Autowired
	public FunctionParamRsImpl(IFunctionParamService service) {
		this.functionParamService= service;
	}
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.POST, 
    		value = "/function/{id_function}/param")   		
	@ResponseStatus(code = HttpStatus.CREATED)
	public FunctionParamDTO insertFunctionParam(@RequestBody FunctionParamDTO request,@PathVariable("id_function") Long functionId) {
        FunctionDTO function = new FunctionDTO();
        function.setId(functionId);
		request.setFunction(function);
		return functionParamService.insertFunctionParam(request); 
	}
	

	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "/function/{id_function}/params")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginableResultSearchImpl<FunctionParamDTO> getAllFunctionById(FunctionParamCriteriaDTO request, @PathVariable("id_function") Long functionId ) {	
		return functionParamService.getAllFunctionById(request, functionId); 
	}
	
	
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.PUT,  
    		value = "/function/{id_function}/param/{id_param}")
	@ResponseStatus(code = HttpStatus.OK)
	public FunctionParamDTO updateFunctionParam(@RequestBody FunctionParamDTO request, @PathVariable("id_param") Long paramId) {	
		request.setId(paramId);
		return functionParamService.updateFunctionParam(request); 
	}
	
	

	@RequestMapping(consumes = MediaType.ALL_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET,  
    		value = "/function/{id_function}/param/{id_param}")
	@ResponseStatus(code = HttpStatus.OK)
	public FunctionParamDTO getFunctionParam(@PathVariable("id_function") Long functionId, @PathVariable("id_param") Long paramId) {	
		return functionParamService.getFunctionParam(paramId); 
	}
	

}
