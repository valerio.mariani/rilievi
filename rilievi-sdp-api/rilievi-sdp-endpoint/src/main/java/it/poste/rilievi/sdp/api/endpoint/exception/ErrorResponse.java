package it.poste.rilievi.sdp.api.endpoint.exception;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

public class ErrorResponse {

	protected HttpStatus status;
	protected String error_code;
	protected String message;
	protected List<String> details;
	protected HashMap<String, List<String>> fields;
	protected String timestamp;
	protected String path;

	// Builder
	public static final class ErrorResponseBuilder {
		private HttpStatus status;
		private String error_code;
		private String message;
		private List<String> details;
		private HashMap<String, List<String>> fields;
		private String path;
		private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

		public ErrorResponseBuilder() {
			details = new ArrayList<String>();
			fields = new HashMap<String, List<String>>();
		}

		public ErrorResponseBuilder(WebRequest request) {
			details = new ArrayList<String>();
			fields = new HashMap<String, List<String>>();
			path = ((ServletWebRequest) request).getRequest().getRequestURI().toString();
		}

		public static ErrorResponseBuilder anApiErrorResponse() {
			return new ErrorResponseBuilder();
		}

		public ErrorResponseBuilder withStatus(HttpStatus status) {
			this.status = status;
			return this;
		}

		public ErrorResponseBuilder withError_code(String error_code) {
			this.error_code = error_code;
			return this;
		}

		public ErrorResponseBuilder withMessage(String message) {
			this.message = message;
			return this;
		}

		public ErrorResponseBuilder withDetails(List<String> details) {
			this.details = details;
			return this;
		}

		public ErrorResponseBuilder withDetail(String detail) {
			this.details.add(detail);
			return this;
		}

		public ErrorResponseBuilder withPath(String path) {
			this.path = path;
			return this;
		}

		public ErrorResponseBuilder withErrorField(String fieldname, String errorDesc) {
			if(!fields.containsKey(fieldname)) {
				fields.put(fieldname, new ArrayList<String>());
			}
			fields.get(fieldname).add(errorDesc);
			return this;
		}
		
		public ErrorResponseBuilder withErrorFields(HashMap<String, List<String>> fieldsError) {		
			this.fields = fieldsError;
			return this;
		}
		
		public ErrorResponse build() {
			ErrorResponse response = new ErrorResponse();
			response.fields = this.fields;	
			response.timestamp = LocalDateTime.now().format(formatter);
			response.status = this.status;
			response.error_code = this.error_code;
			response.message = this.message;
			response.details = this.details;
			response.path = this.path;
			return response;
		}

	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getPath() {
		return path;
	}

	public String getError_code() {
		return error_code;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getDetails() {
		return details;
	}

	public HashMap<String, List<String>> getFields() {
		return fields;
	}

	public String getTimestamp() {
		return timestamp;
	}
	
	
}
