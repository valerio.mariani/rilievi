package it.poste.rilievi.sdp.api.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.poste.rilievi.sdp.api.model.dto.DacoServiceDTO;
import it.poste.rilievi.sdp.api.service.api.IDacoServiceService;


@RestController
@Component
public class DacoServiceRsImpl {

	
	private IDacoServiceService dacoServiceService;
	
	@Autowired
	public DacoServiceRsImpl(IDacoServiceService service) {
		this.dacoServiceService = service;
	}
	
	@RequestMapping(
    		produces = MediaType.APPLICATION_JSON_VALUE,  
    		method = RequestMethod.GET, 
    		value = "daco/{id_daco}/services")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DacoServiceDTO> getAllDaco(@PathVariable("id_daco") Long idDaco) {	
		return dacoServiceService.getServices(idDaco);
	}
}
