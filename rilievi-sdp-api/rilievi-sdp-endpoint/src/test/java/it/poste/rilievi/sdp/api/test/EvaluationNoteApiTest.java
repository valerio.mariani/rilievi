package it.poste.rilievi.sdp.api.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URL;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class EvaluationNoteApiTest extends ApiTestBase {

	@Autowired
	private TestRestTemplate restTemplate;

	EvaluationNoteDTO insertItem;

	EvaluationNoteDTO deleteItem;

	private Long id_evaluation = (long) 1;

	@Test
	public void insertNewEvaluationNoteOK() throws Exception {

		JSONObject evaluationNote = new JSONObject();

		evaluationNote.put("note", "test");

		String json = evaluationNote.toString();

		HttpHeaders requestHeaders = getheaders();

		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);

		ResponseEntity<EvaluationNoteDTO> response = restTemplate.postForEntity(
				new URL(baseUrl + "evaluation/" + id_evaluation + "/note").toString(), httpEntity,
				EvaluationNoteDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);
		assertNotNull(response.getBody().getIdnote());

		insertItem = response.getBody();

		deleteItem = response.getBody();

		testGetEvaluationNote();
		testDeleteEvaluationNote();
	}

	private void testGetEvaluationNote() throws Exception {

		ResponseEntity<PaginableResultSearchImpl<EvaluationNoteDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "evaluation/" + id_evaluation + "/notes", HttpMethod.GET,
				new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<EvaluationNoteDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		boolean founded = false;
		for (EvaluationNoteDTO functionParam : entity.getBody().getItems()) {
			if (functionParam.getIdnote() == insertItem.getIdnote()) {
				founded = true;
				break;
			}

		}
		assertEquals(founded, true);

	}

	private void testDeleteEvaluationNote() throws Exception {

		Long id = deleteItem.getIdnote();

		ResponseEntity<PaginableResultSearchImpl<EvaluationNoteDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "evaluation/" + id_evaluation + "/note/" + id, HttpMethod.DELETE, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<EvaluationNoteDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);

	}

}
