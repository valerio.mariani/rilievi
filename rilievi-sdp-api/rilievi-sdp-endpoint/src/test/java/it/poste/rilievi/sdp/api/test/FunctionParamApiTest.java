package it.poste.rilievi.sdp.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.sdp.api.model.dto.FunctionParamDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class FunctionParamApiTest extends ApiTestBase {

	@Autowired
	private TestRestTemplate restTemplate;

	FunctionParamDTO insertItem;

	FunctionParamDTO updateItem;

	FunctionParamDTO deleteItem;

	private Long id_function = new Long(1);

	@Test
	public void insertNewFunctionParamOK() throws Exception {

		JSONObject functionParam = new JSONObject();

		functionParam.put("description", "test descr");
		functionParam.put("nameparam", "test name");
		functionParam.put("value", "test value");

		String json = functionParam.toString();

		HttpHeaders requestHeaders = getheaders();

		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);

		ResponseEntity<FunctionParamDTO> response = restTemplate.postForEntity(
				new URL(baseUrl + "function/" + id_function + "/param").toString(), httpEntity, FunctionParamDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);
		assertNotNull(response.getBody().getId());

		insertItem = response.getBody();

		deleteItem = response.getBody();

		updateItem = response.getBody();

		updateFunctionParamOK();

		testGetAllFunctionParamById();

	}

	private void testGetAllFunctionParamById() throws Exception {

		ResponseEntity<PaginableResultSearchImpl<FunctionParamDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "function/" + id_function + "/params", HttpMethod.GET, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<FunctionParamDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		boolean founded = false;
		for (FunctionParamDTO functionParam : entity.getBody().getItems()) {
			if (functionParam.getId() == insertItem.getId()) {
				founded = true;
				break;
			}

		}
		assertEquals(founded, true);

	}

	private void updateFunctionParamOK() throws Exception {

		String valueMod = "value FunctionParam";
		Long id = updateItem.getId();

		JSONObject functionParam = new JSONObject();
		// functionParam.put("id", insertItem.getId());
		functionParam.put("value", valueMod);

		String json = functionParam.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
		ResponseEntity<FunctionParamDTO> response = new TestRestTemplate().exchange(
				baseUrl + "function/" + id_function + "/param/" + id, HttpMethod.PUT, httpEntity,
				FunctionParamDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertEquals(valueMod, response.getBody().getValue());
		assertNotNull(response.getBody().getId());
		updateItem = response.getBody();

	}

}
