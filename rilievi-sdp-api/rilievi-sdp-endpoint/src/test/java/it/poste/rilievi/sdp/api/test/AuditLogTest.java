package it.poste.rilievi.sdp.api.test;
//package it.poste.rilievi.api.test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.net.URL;
//import java.util.Random;
//
//import org.json.JSONObject;
//import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import it.poste.rilievi.api.model.dto.AppProfileDTO;
//import it.poste.rilievi.api.model.dto.AppUserDTO;
//import it.poste.rilievi.api.model.dto.AuditLogDTO;
//import it.poste.rilievi.api.model.dto.PaginableResultSearchImpl;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@TestMethodOrder(OrderAnnotation.class)
//public class AuditLogTest extends ApiTestBase {
//
//	@Autowired
//	private TestRestTemplate restTemplate;
//
//	@SuppressWarnings("unused")
//	public void insertNewLog() throws Exception {
//
//		Random rand = new Random();
//		String count = String.format("%04d", rand.nextInt(10000));
//
//		JSONObject log = new JSONObject();
//		AppUserDTO user = new AppUserDTO();
//		user.setCode("P01");
//		AppProfileDTO profile = new AppProfileDTO();
//		profile.setCode("P" + count);
//		profile.setName("profile" + count);
//		profile.setDescription("prova desc");
//		// profile.setCreationDate(creationDate);
//		user.setProfile(profile);
//		log.put("user", user);
//		log.put("descrAction", "Prova");
//		log.put("result", "Completato");
//
//		String json = log.toString();
//
//		HttpHeaders requestHeaders = getheaders();
//		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
//		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
//
//		ResponseEntity<AppProfileDTO> response = restTemplate
//				.postForEntity(new URL(baseUrl + "insertProfile").toString(), httpEntity, AppProfileDTO.class);
//
//	}
//
//	@Test
//	public void getAuditLog() {
//
//		ResponseEntity<PaginableResultSearchImpl<AuditLogDTO>> entity = new TestRestTemplate().exchange(
//				baseUrl + "auditlogs", HttpMethod.GET, new HttpEntity<Object>(getheaders()),
//				new ParameterizedTypeReference<PaginableResultSearchImpl<AuditLogDTO>>() {
//				});
//		assertEquals(entity.getStatusCode(), HttpStatus.OK);
//		boolean founded = false;
//		for (AuditLogDTO auditLog : entity.getBody().getItems()) {
//			if (auditLog.getId().intValue() != 0) {
//				founded = true;
//				break;
//			}
//		}
//		assertEquals(founded, true);
//	}
//
//}
