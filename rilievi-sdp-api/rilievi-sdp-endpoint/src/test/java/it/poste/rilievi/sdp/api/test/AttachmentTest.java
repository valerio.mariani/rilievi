package it.poste.rilievi.sdp.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;
import java.util.Random;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.sdp.api.model.dto.AttachmentDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)

public class AttachmentTest extends ApiTestBase {

	@Autowired
	private TestRestTemplate restTemplate;

	AttachmentDTO insertItem;

	AttachmentDTO deleteItem;

	@Test
	public void insertAttachment() throws Exception {

		// Il random serve per eseguire diversi test senza dover modificare ogni volta il codice
		Random rand = new Random();
		String count = String.format("%04d", rand.nextInt(10000));

		JSONObject attachment = new JSONObject();
		attachment.put("name", "provaTest" + count);
		attachment.put("content", "cHJvdmEgdGVzdA==");

		String json = attachment.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);

		ResponseEntity<AttachmentDTO> response = restTemplate.postForEntity(
				new URL(baseUrl + "evaluation/1/attachment").toString(), httpEntity, AttachmentDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);

		assertNotNull(response.getBody().getId());
		insertItem = response.getBody();
		deleteItem = response.getBody();

		testDelete();
	}

	private void testDelete() throws Exception {

		Long id = deleteItem.getId();

		ResponseEntity<PaginableResultSearchImpl<AttachmentDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "evaluation/null/attachment/" + id, HttpMethod.DELETE, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<AttachmentDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);

	}

}
