package it.poste.rilievi.sdp.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.sdp.api.model.dto.DacoDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;



@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class DacoApiTest extends ApiTestBase{

    @Autowired
    private TestRestTemplate restTemplate;
    
    DacoDTO insertItem;
    
    DacoDTO updatedItem;

    
   @Test
   public void insertNewDacoOK() throws Exception {
    	JSONObject daco = new JSONObject();
    	daco.put("code", "001");
    	daco.put("description", "Descrizione");
    	daco.put("accountNumber", "453463467");
    	daco.put("startDate", "2019-07-24");
    	
    	String json = daco.toString();
 
        HttpHeaders requestHeaders = getheaders();
        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
        
        ResponseEntity<DacoDTO> response = restTemplate
        		.postForEntity(new URL(baseUrl + "daco").toString(), httpEntity, DacoDTO.class);
        
        assertEquals(response.getStatusCode(),HttpStatus.CREATED);
       
        assertNotNull(response.getBody().getId());
        insertItem = response.getBody();
        
        testGetAllDaco();
        updateDacoOK();
    }
   
   public void testGetAllDaco() throws Exception {
    	
        ResponseEntity<PaginableResultSearchImpl<DacoDTO>> entity =  new TestRestTemplate().exchange(
        		baseUrl + "dacos", HttpMethod.GET, new HttpEntity<Object>(getheaders()), new ParameterizedTypeReference<PaginableResultSearchImpl<DacoDTO>>() {});       
        assertEquals(entity.getStatusCode(),HttpStatus.OK);      
        boolean founded = false;
        for(DacoDTO daco : entity.getBody().getItems() ) {
        	if(daco.getId().intValue() == insertItem.getId()) {
        		founded = true;
        		break;
        	}
        }
        assertEquals(founded, true);
    }
    


   public void updateDacoOK() throws Exception {
	   String descMod = "descrizione daco";
	   
	   	JSONObject daco = new JSONObject();
	   	daco.put("id", insertItem.getId());
	   	daco.put("code", "001");
	   	daco.put("description", descMod);
	   	daco.put("accountNumber", "453463467");
	   	daco.put("startDate", "2019-07-24");
	   	daco.put("endDate", "2100-12-24");
   	
	   	String json = daco.toString();
       
       HttpHeaders requestHeaders = getheaders();
       requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
       requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
       
       HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
       ResponseEntity<DacoDTO> response = new TestRestTemplate().exchange(baseUrl + "daco", HttpMethod.PUT,httpEntity, DacoDTO.class);
       
       assertEquals(response.getStatusCode(),HttpStatus.OK);
       assertEquals(descMod,response.getBody().getDescription());
       assertNotNull(response.getBody().getId());
       updatedItem = response.getBody();
   }
   
}
