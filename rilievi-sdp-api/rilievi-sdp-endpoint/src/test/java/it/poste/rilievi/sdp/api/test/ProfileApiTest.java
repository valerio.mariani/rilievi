package it.poste.rilievi.sdp.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;
import java.util.Random;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.poste.rilievi.sdp.api.model.dto.AppProfileDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ProfileApiTest extends ApiTestBase {

	@Autowired
	private TestRestTemplate restTemplate;

	AppProfileDTO insertItem;

	@Test
	public void insertNewProfile() throws Exception {

		// Il random serve per eseguire diversi test senza dover modificare ogni volta
		// il codice
		Random rand = new Random();
		String count = String.format("%04d", rand.nextInt(10000));

		JSONObject profile = new JSONObject();
		profile.put("code", "P" + count);
		profile.put("name", "prova" + count);
		profile.put("description", "provadesc");

		String json = profile.toString();

		HttpHeaders requestHeaders = getheaders();
		requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);

		ResponseEntity<AppProfileDTO> response = restTemplate.postForEntity(new URL(baseUrl + "profile").toString(),
				httpEntity, AppProfileDTO.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);

		assertNotNull(response.getBody().getId());
		insertItem = response.getBody();

		testGetAllProfile();

	}

	public void testGetAllProfile() throws Exception {

		ResponseEntity<PaginableResultSearchImpl<AppProfileDTO>> entity = new TestRestTemplate().exchange(
				baseUrl + "profiles", HttpMethod.GET, new HttpEntity<Object>(getheaders()),
				new ParameterizedTypeReference<PaginableResultSearchImpl<AppProfileDTO>>() {
				});
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		boolean founded = false;
		for (AppProfileDTO appProfile : entity.getBody().getItems()) {
			if (appProfile.getId().intValue() == insertItem.getId()) {
				founded = true;
				break;
			}
		}

		assertEquals(founded, true);
	}

}
