package it.poste.rilievi.sdp.api.test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.client.RestClientException;

import it.poste.rilievi.sdp.api.model.dto.DacoDTO;
import it.poste.rilievi.sdp.api.model.entity.TipologDaco;
import it.poste.rilievi.sdp.api.repository.IDacoDao;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ApiTestError extends ApiTestBase{

    @Autowired
    private TestRestTemplate restTemplate;
    

    @MockBean
    IDacoDao dacoDao;
    
    @Test
    @Order(3)
    public void dbError() throws RestClientException, MalformedURLException, JSONException {
    	MockitoAnnotations.initMocks(this);
	    when(dacoDao.insertDaco(Mockito.any(TipologDaco.class))).thenThrow(CannotCreateTransactionException.class);
	   
    	JSONObject daco = new JSONObject();
    	daco.put("code", "001");
    	daco.put("description", "Descrizione");
    	daco.put("accountNumber", "453463467");
    	daco.put("startDate", "2019-07-24");
    	System.out.println("TEST 1");
    	String json = daco.toString();
 
        HttpHeaders requestHeaders = getheaders();
        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
        ResponseEntity<DacoDTO> response = restTemplate
        		.postForEntity(new URL(baseUrl + "daco").toString(), httpEntity, DacoDTO.class);
        System.out.println("TEST 2");
        assertEquals(response.getStatusCode(),HttpStatus.BAD_REQUEST);
    	   
    }
    
    @Test
    @Order(2)
    public void testUnsopportedMediaType() throws Exception {
    	HttpHeaders requestHeaders = getheaders();
        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_VALUE);
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
        ResponseEntity<List<DacoDTO>> entity =  new TestRestTemplate().exchange(
        		baseUrl + "dacos", HttpMethod.GET, new HttpEntity<Object>(requestHeaders), new ParameterizedTypeReference<List<DacoDTO>>() {});       
        assertEquals(entity.getStatusCode(),HttpStatus.NOT_ACCEPTABLE);      
    }
 
}
