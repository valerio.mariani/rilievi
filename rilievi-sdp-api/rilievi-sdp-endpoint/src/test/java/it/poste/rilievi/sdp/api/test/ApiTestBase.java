package it.poste.rilievi.sdp.api.test;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;

import it.poste.rilievi.sdp.api.endpoint.filter.IdentityFilter;

public class ApiTestBase {
	static {
		System.setProperty("rilievi-sdp-api.spring.config.location", "classpath:");
	}
	@LocalServerPort
	protected int port;

	protected String baseUrl;

	@BeforeEach
	public void setup() {
		this.baseUrl = "http://localhost:" + port + "/rilievi-sdp-api/";
	}

	protected HttpHeaders getheaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add(IdentityFilter.HEADER_USER, "12345");
		requestHeaders.add(IdentityFilter.HEADER_USER_PROFILE, "P01");
		return requestHeaders;
	}

}
