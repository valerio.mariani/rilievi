package it.poste.rilievi.sdp.api.test;
//package it.poste.rilievi.api.test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import java.net.URL;
//import java.util.List;
//
//import org.junit.Ignore;
//import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import it.poste.rilievi.api.model.dto.ExtractionDatesDTO;
//import net.minidev.json.JSONObject;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@TestMethodOrder(OrderAnnotation.class)
//public class ExtractionDatesApiTest extends ApiTestBase {
//	
//	@Autowired
//	private TestRestTemplate restTemplate;
//	
//	ExtractionDatesDTO insertItem ;
//	
//	
//	@Test
//	@Ignore
//	public void insertNewExtractionDatesOK() throws Exception {
//		JSONObject extractionDates = new JSONObject();
//
//		extractionDates.put("fixeDate", "2019-12-25T02:00:00");
//		extractionDates.put("closingDate", "2019-12-26T00:00:00");
//
//	    extractionDates.put("closingDate", "2019-12-26T00:00:00");
//		extractionDates.put("fixedDate", "2019-12-25T02:00:00");
//   
//		
//		String json = extractionDates.toString();
//		
//		HttpHeaders requestHeaders = getheaders();
//        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
//        requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//        HttpEntity<?> httpEntity = new HttpEntity<Object>(json, requestHeaders);
//        ResponseEntity<ExtractionDatesDTO> response = restTemplate
//        		.postForEntity(new URL(baseUrl + "extractionDates").toString(), httpEntity, ExtractionDatesDTO.class);
//        
//        assertEquals(response.getStatusCode(),HttpStatus.CREATED);
//        
//
//        assertNotNull(response.getBody().getFixedDate());   
//        insertItem = response.getBody();  
//      
//        
//        testGetAllExtractionDates();   
//      
//		
//
//	}
//	
//	public void testGetAllExtractionDates() throws Exception {
//		 ResponseEntity<List<ExtractionDatesDTO>> entity =  new TestRestTemplate().exchange(
//	        		baseUrl + "extractionDates", HttpMethod.GET, new HttpEntity<Object>(getheaders()), new ParameterizedTypeReference<List<ExtractionDatesDTO>>() {});       
//	        assertEquals(entity.getStatusCode(),HttpStatus.OK);      
//	        boolean founded = false;
//	        for(ExtractionDatesDTO extractionDates : entity.getBody() ) {
//	        	if(extractionDates.getFixedDate() == insertItem.getFixedDate()) {
//	        		founded = true;
//	        		break;
//	        	}
//	        }
//	        assertEquals(founded, true);
//		
//	}
//	
//     
//
//}
