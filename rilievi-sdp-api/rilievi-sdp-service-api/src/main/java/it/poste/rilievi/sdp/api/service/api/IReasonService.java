package it.poste.rilievi.sdp.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.ReasonDTO;

@Service
public interface IReasonService extends IService {

	List<ReasonDTO> getAllReason();

}
