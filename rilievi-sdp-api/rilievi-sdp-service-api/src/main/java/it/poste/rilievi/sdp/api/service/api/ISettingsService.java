package it.poste.rilievi.sdp.api.service.api;

import it.poste.rilievi.sdp.api.model.dto.AppSettings;
import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.sdp.api.model.dto.ReasonDTO;

public interface ISettingsService extends IService {

	AppSettings getAppSettings();

	ReasonDTO getRcReason();

	ReasonDTO getRdReason();

	ReasonDTO getRaReason();

	ReasonDTO getRsReason();

	EvaluationStatusDTO getUnLossState();

}