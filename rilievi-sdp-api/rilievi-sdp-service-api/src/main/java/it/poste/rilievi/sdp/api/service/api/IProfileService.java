package it.poste.rilievi.sdp.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.AppProfileCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.AppProfileDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IProfileService extends IService {

	PaginableResultSearchImpl<AppProfileDTO> getProfileItems(AppProfileCriteriaDTO profileCriteria);

	AppProfileDTO getProfileByCode(String code);

	AppProfileDTO insertProfileItem(AppProfileDTO profile);

}
