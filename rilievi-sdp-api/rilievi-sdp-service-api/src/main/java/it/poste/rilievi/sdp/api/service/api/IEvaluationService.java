package it.poste.rilievi.sdp.api.service.api;

import java.time.LocalDate;
import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.EvaluationCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;
import it.poste.rilievi.sdp.api.model.dto.ExportTableDTO;
import it.poste.rilievi.sdp.api.model.dto.OfficeDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;
import it.poste.rilievi.sdp.api.model.dto.RegularizationType;

@Service
public interface IEvaluationService extends IService {

	EvaluationDTO insertEvaluation(EvaluationDTO evaluation);

	EvaluationDTO reclassificationDacoEvaluation(EvaluationDTO reclassification);

	EvaluationDTO deleteEvaluation(Long evaluationId);

	EvaluationDTO setLossState(Long idevaluation, EvaluationStatusDTO status);

	EvaluationDTO changeFractional(OfficeDTO office, Long evaluationId);

	PaginableResultSearchImpl<EvaluationDTO> getEvaluations(EvaluationCriteriaDTO criteria);

	PaginableResultSearchImpl<EvaluationDTO> getNotRegularizedEvaluations(EvaluationCriteriaDTO criteria);

	EvaluationDTO getEvaluation(Long evaluationId);

	EvaluationDTO undoLossState(Long idevaluation);

	EvaluationDTO setRCReason(Long idEvaluation);

	EvaluationDTO undoRcReason(Long idEvaluation);

	EvaluationDTO setDescription(EvaluationDTO request, Long idEvaluation);

	ExportTableDTO exportEvaluations(EvaluationCriteriaDTO criteria);

	ExportTableDTO exportUserDailyEvaluations(String userCode);

	PaginableResultSearchImpl<EvaluationDTO> getDailyEvaluations(EvaluationCriteriaDTO criteria);

	EvaluationDTO undoRegularization(Long evaluationId);

	EvaluationDTO setRegularization(Long evaluationId, LocalDate accountDate, RegularizationType type);

	ExportTableDTO exportMaxiEvaluations(EvaluationCriteriaDTO criteria);

	EvaluationDTO modEvaluation(EvaluationDTO request, Long idEvaluation);

}
