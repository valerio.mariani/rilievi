package it.poste.rilievi.sdp.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.ExtractionDatesDTO;

@Service
public interface IExtractionDatesService extends IService {

	ExtractionDatesDTO insertExtractionDates(ExtractionDatesDTO extractionDates);

	List<ExtractionDatesDTO> getExtractionDatesItems();

}
