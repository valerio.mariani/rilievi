package it.poste.rilievi.sdp.api.service.api;

import java.util.List;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.EvaluationStatusDTO;

@Service
public interface IEvaluationStatusService extends IService {

	List<EvaluationStatusDTO> getAllEvaluationStatus();
}
