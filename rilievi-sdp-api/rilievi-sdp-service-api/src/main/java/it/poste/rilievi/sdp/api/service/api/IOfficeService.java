package it.poste.rilievi.sdp.api.service.api;

import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.OfficeDTO;

public interface IOfficeService {

	List<OfficeDTO> getOffices(Long idBranch);

	OfficeDTO getOfficeByFractional(String fractional);

}
