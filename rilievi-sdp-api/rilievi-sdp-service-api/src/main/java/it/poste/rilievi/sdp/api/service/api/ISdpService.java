package it.poste.rilievi.sdp.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalAlignDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalResponseDTO;
import it.poste.rilievi.sdp.api.model.dto.TechnicalUndoDTO;

@Service
public interface ISdpService extends IService {

	ProcessSDPDTO regularization(ProcessSDPDTO process);

	ProcessSDPDTO undoRegularization(ProcessSDPDTO process);

	TechnicalResponseDTO technicalUndo(TechnicalUndoDTO request);

	TechnicalResponseDTO technicalAlign(TechnicalAlignDTO request);

	void undoOperation(ProcessSDPDTO request);

}
