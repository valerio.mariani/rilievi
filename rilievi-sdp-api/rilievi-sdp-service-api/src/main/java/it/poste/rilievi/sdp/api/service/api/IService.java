package it.poste.rilievi.sdp.api.service.api;

import java.time.LocalDateTime;

public interface IService {

	LocalDateTime getSysDate();

}
