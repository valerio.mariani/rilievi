package it.poste.rilievi.sdp.api.service.api;


import java.time.LocalDate;
import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.ProcessSDPDTO;

@Service
public interface IProcessSdpService extends IService{
	
	ProcessSDPDTO insertProcess(ProcessSDPDTO process);
	
	ProcessSDPDTO getProcessSDP(String operNumber, String fractional, String branchNumber, LocalDate contabilizationDate);
	
	ProcessSDPDTO invalidateProcess(String operNumber, String fractional, String branchNumber, LocalDate contabilizationDate);

}
