package it.poste.rilievi.sdp.api.service.api;

import org.springframework.stereotype.Service;

import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteCriteriaDTO;
import it.poste.rilievi.sdp.api.model.dto.EvaluationNoteDTO;
import it.poste.rilievi.sdp.api.model.dto.PaginableResultSearchImpl;

@Service
public interface IEvaluationNoteService extends IService {

	EvaluationNoteDTO insertEvaluationNote(EvaluationNoteDTO evaluationNote);

	void deleteEvaluationNote(Long noteId);

	PaginableResultSearchImpl<EvaluationNoteDTO> getEvaluationNote(EvaluationNoteCriteriaDTO evaluationNote,Long EvaluationId);

}
