package it.poste.rilievi.sdp.api.service.api;

import java.util.List;

import it.poste.rilievi.sdp.api.model.dto.BranchDTO;

public interface IBranchService {

	List<BranchDTO> getAllBraches();

}
